/**!//////////////////// | Site Global Function | ////////////////////!**/


/*### Fix Nav on Scroll */
$(document).ready(function(){
	$(window).scroll(function () {
		if ($(this).scrollTop() >= 420) {
			$("#data_link").addClass("fixnav");
		} else {
			$("#data_link").removeClass("fixnav");
		}
	});


	if ($(window).width() < 768) {
		$(window).scroll(function () {
			if ($(this).scrollTop() >= 300) {
				$(".cat_area").fadeOut("slow");
			} else {
				$(".cat_area").fadeIn("slow");
			}
		});
	}
});

/*### Head Banner */
// $(document).ready(function(){

// 	if ($(window).width() <= 576) {
// 		$("img.banner-md, img.banner-xl").css({ display: "none" });
// 	} else if ($(window).width() <= 1366) {
// 		$("img.banner-sm, img.banner-xl").css({ display: "none" });
// 	} else {
// 		$("img.banner-sm, img.banner-md").css({ display: "none" });
// 	}


// 	if ($(window).width() <= 576) {
// 		$(".head-banner").append("<a href='register.php'><img src='images/banner-sm.png'/></a>");
// 	} else if ($(window).width() <= 1366) {
// 		$(".head-banner").append("<a href='register.php'><img src='images/banner-lg.png'/></a>");
// 	} else {
// 		$(".head-banner").append("<a href='register.php'><img src='images/banner-xl.png'/></a>");
// 	}

// });


/*### Head Nav on Scroll */
$(document).ready(function(){
	$(".btn-menu").click(function(){
		$(".head-bar-top").toggle("fast");
	});
	$(".btn-cate").click(function(){
		$(".head-bar-bot").toggle("fast");
	});

	if ($(window).width() <= 576) {

		$(".thbanks").removeClass("thbanks-3x");
		$(".thbanks").addClass("thbanks-4x");

		$(".btn-menu").click(function(){
			$(".head-bar-bot").hide("fast");
		});
		$(".btn-cate").click(function(){
			$(".head-bar-top").hide("fast");
		});
		$(".btn-search-toggle").click(function(){
			$(".search-box").slideToggle("fast");
			$("#search-input").focus();
			$(".openclosesearch").toggle();
			$(".head-bar-bot, .head-bar-top").hide("fast");
		});

		$(window).scroll(function () {
			if ($(this).scrollTop() >= 1) {
				$("#site-header").addClass("head-fix-top");
				$("#site-logo, .head-banner").slideUp("fast");
				$(".head-bar-top, .head-bar-bot").hide("fast");
				$(".head-bar-mid").css({ padding: "0px" });
			} else {
				$("#site-header").removeClass("head-fix-top");
				$("#site-logo, .head-banner").slideDown("fast");
		    	$(".head-bar-mid").removeAttr("style");
			}
		});

	} else if ($(window).width() <= 992) {

		$(".btn-menu").click(function(){
			$(".head-bar-bot").hide("fast");
		});
		$(".btn-cate").click(function(){
			$(".head-bar-top").hide("fast");
		});
		$(".btn-search-toggle").click(function(){
			$(".search-box").slideToggle("fast");
			$("#search-input").focus();
			$(".openclosesearch").toggle();
			$(".head-bar-bot, .head-bar-top").hide("fast");
			$(".head-banner").slideToggle("fast");
		});

		$(window).scroll(function () {
			if ($(this).scrollTop() >= 1) {
				$("#site-header").addClass("head-fix-top");
				$(".head-bar-top, .head-bar-bot").hide("fast");
				$("#site-logo img").css({ height: "45px", margin: "0px" });
			} else {
				$("#site-header").removeClass("head-fix-top");
		    	$("#site-logo img").removeAttr("style");
			}
		});

	} else {

		$(window).scroll(function () {
		    if ($(this).scrollTop() >= 1) {
				$("#site-header").addClass("head-fix-top");
				$(".head-bar-top, .head-bar-bot, .head-banner").hide(100);
				$(".search-box").css({ width: "73%" });
				$(".btn-menu, .btn-cate").fadeIn().css({ display: "inline-block" });
				$("#site-logo img").css({ height: "45px" });
		    } else {
				$("#site-header").removeClass("head-fix-top");
				$(".head-bar-top, .head-bar-bot, .head-banner").show(100);
		    	$(".btn-menu, .btn-cate").fadeOut().removeAttr("style");
		    	$(".search-box, #site-logo img").removeAttr("style");
		    }
		});

	}

});


/* Tigger Tab Function */
$(document).ready(function(){
	/* Tigger Tab Banner >>> index.php */
	$("#brand, #category").hide();

	$("#banner_tab").mouseover(function () {
		$(this).trigger("click");
		$(this).addClass("active");
		$("#brand_tab, #category_tab").removeClass("active");

		$("#banner").fadeIn();
		$("#brand, #category").fadeOut();
	});
	$("#brand_tab").mouseover(function () {
		$(this).trigger("click");
		$(this).addClass("active");
		$("#banner_tab, #category_tab").removeClass("active");

		$("#brand").fadeIn();
		$("#banner, #category").fadeOut();
	});
	$("#category_tab").mouseover(function () {
		$(this).trigger("click");
		$(this).addClass("active");
		$("#banner_tab, #brand_tab").removeClass("active");

		$("#category").fadeIn();
		$("#banner, #brand").fadeOut();
	});


	/* Tigger Tab Category >>> Product.php */
	$("#cat-main-01, #cat-main-02, #cat-main-03, #cat-main-04, #cat-main-05, #cat-main-06, #cat-main-07, #cat-main-08, #cat-main-09, #cat-main-10").mouseover(function () {
		$(this).trigger("click");
	});
	$(".cat_area").mouseleave(function() {
		$("#cat_nav > li >a.active, #cat_nav_sub > div.tab-pane").removeClass("active");
	});

});

/*### Profile Image */
// $("#cove_pic").click(function () {
// 	$(".up_img_cover").trigger("click");
// });
// $("#profile_pic").click(function () {
// 	$(".up_img_profile").trigger("click");
// });

/*### Popover */
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({
		trigger: 'hover'
	})
});


/*### Go Top */
$(document).ready(function(){
	$(window).scroll(function () {
		if ($(this).scrollTop() >= 50) {
		 	$("#goTop").fadeIn();
		} else {
		 	$("#goTop").fadeOut();
		}
 	});
  // scroll body to 0px on click
	$("#goTop").click(function () {
		$("#goTop").tooltip("hide");
		$("html,body").animate({ scrollTop: 0 }, 500);
		return false;
	});
	$("#goTop").tooltip("over");
});

/*### Review Form */
$(document).ready(function(){
	$("#form_review").hide();
	$("#write_review").click(function(){
		$("#form_review").slideDown("fast");
		$(this).hide("fast");
	});
});


/*### Call Cart Modal */
$(document).ready(function() {
	// Support for AJAX loaded modal window.
	// Focuses on first input textbox after it loads the window.
	$('[data-toggle="modal"]').click(function(e) {
		e.preventDefault();
		var url = $(this).attr('href');
		if (url.indexOf('#') == 0) {
			$(url).modal('open');
		} else {
			$.get(url, function(data) {
				$('<div class="modal hide fade">' + data + '</div>').modal();
			}).success(function() { $('input:text:visible:first').focus(); });
		}
	});
});


/*### Product Gallery */
$(document).ready(function () {
	// reference for main items
	var slider = $('#slider');
	// reference for thumbnail items
	var thumbnailSlider = $('#thumbnailSlider');
	//transition time in ms
	var duration = 250;

	// carousel function for main slider
	slider.owlCarousel({
		loop:false,
		nav:false,
		items:1
	}).on('changed.owl.carousel', function (e) {
		//On change of main item to trigger thumbnail item
		thumbnailSlider.trigger('to.owl.carousel', [e.item.index, duration, true]);
	});

	// carousel function for thumbnail slider
	thumbnailSlider.owlCarousel({
		loop:false,
		center:true, //to display the thumbnail item in center
		nav:false,
		responsive:{
			0:    { items:3 },
			600:  { items:4 }
		}
	}).on('click', '.owl-item', function () {
	// On click of thumbnail items to trigger same main item
	slider.trigger('to.owl.carousel', [$(this).index(), duration, true]);

	}).on('changed.owl.carousel', function (e) {
		// On change of thumbnail item to trigger main item
		slider.trigger('to.owl.carousel', [e.item.index, duration, true]);
	});

	//These two are navigation for main items
	$('.slider-right').click(function() {
		slider.trigger('next.owl.carousel');
	});
	$('.slider-left').click(function() {
		slider.trigger('prev.owl.carousel');
	});
});



/*### Minus and Plus */
$(document).ready(function () {
	//plugin bootstrap minus and plus
	//http://jsfiddle.net/laelitenetwork/puJ6G/
	$('.btn-number').click(function(e){
		e.preventDefault();

		fieldName = $(this).attr('data-field');
		type      = $(this).attr('data-type');
		var input = $("input[name='"+fieldName+"']");
		var currentVal = parseInt(input.val());
		if (!isNaN(currentVal)) {
			if(type == 'minus') {
				if(currentVal > input.attr('min')) {
					input.val(currentVal - 1).change();
				}
				if(parseInt(input.val()) == input.attr('min')) {
					$(this).attr('disabled', true);
				}
			} else if(type == 'plus') {
				if(currentVal < input.attr('max')) {
					input.val(currentVal + 1).change();
				}
				if(parseInt(input.val()) == input.attr('max')) {
					$(this).attr('disabled', true);
				}
			}
		} else {
			input.val(0);
		}
	});

	$('.input-number').focusin(function(){
		$(this).data('oldValue', $(this).val());
	});

	$('.input-number').change(function() {

		minValue =  parseInt($(this).attr('min'));
		maxValue =  parseInt($(this).attr('max'));
		valueCurrent = parseInt($(this).val());

		name = $(this).attr('name');
		if(valueCurrent >= minValue) {
			$(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
		} else {
			alert('Sorry, the minimum value was reached');
			$(this).val($(this).data('oldValue'));
		}
		if(valueCurrent <= maxValue) {
			$(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
		} else {
			alert('Sorry, the maximum value was reached');
			$(this).val($(this).data('oldValue'));
		}
	});

	$(".input-number").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
		    // Allow: Ctrl+A
		    (e.keyCode == 65 && e.ctrlKey === true) ||
		    // Allow: home, end, left, right
		    (e.keyCode >= 35 && e.keyCode <= 39)) {
		    // let it happen, don't do anything
		    return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		    e.preventDefault();
		}
	});
});

/*###  Image Profile Upload */
$(document).ready( function() {

		function readURL(input) {
			if($('#imgInp').val()!=''){
				var files = document.getElementById("imgInp").value;
				var extall="jpg,jpeg,gif,png";
				ext = files.split('.').pop().toLowerCase();
				if(parseInt(extall.indexOf(ext)) < 0)
				{
					swal({
		        title: "กรุณาเลือกภาพที่ถูกต้อง",
		        text: 'สามารถใช้ได้เฉพาะไฟล์ : ' + extall,
		        icon: "warning",
		        button: "OK",
		      });
		      files='';
					return false;
				}else{
					if (input.files && input.files[0]) {
						var reader = new FileReader();
						reader.onload = function (e) {
							$('#profile_pic').attr('src', e.target.result);
						}
						reader.readAsDataURL(input.files[0]);
					}
				}
			}

		}
		$("#imgInp").change(function(){
			readURL(this);
		});
	});

$(document).ready( function() {

	function readURL(input) {
		if($('#imgCoverInp').val()!=''){
			var files = document.getElementById("imgCoverInp").value;
			var extall="jpg,jpeg,gif,png";
			ext = files.split('.').pop().toLowerCase();
			if(parseInt(extall.indexOf(ext)) < 0)
			{
				swal({
		      title: "กรุณาเลือกภาพที่ถูกต้อง",
		      text: 'สามารถใช้ได้เฉพาะไฟล์ : ' + extall,
		      icon: "warning",
		      button: "OK",
		    });
		    files='';
				return false;
			}else{
				if (input.files && input.files[0]) {
					var reader = new FileReader();
					reader.onload = function (e) {
						$('#cove_pic').attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				}
			}
		}

	}
	$("#imgCoverInp").change(function(){
		readURL(this);
	});
});


/*### Scrollspy */
/* Scrollspy */
$(document).ready(function(){
  $('body').scrollspy({target:'.con-scroll',offset:220});
});

$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a.nav-scroll").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();
      // Store hash
      var hash = $(this.hash).offset() ? $(this.hash).offset().top : 0;
      //change this number to create the additional off set
      var customoffset = 100
      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({scrollTop:hash - customoffset}, 300, function(){
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});






/*### Footer section-01 */
$(document).ready(function() {
	$(".switch").click(function(){
		$(".section-01 ul").slideToggle("fast");
		$(".switch").toggle();
	});

});
