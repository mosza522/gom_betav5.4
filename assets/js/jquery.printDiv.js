( function($) {

  /**
   * Prints single div content. Adds a class to the body of 'js-print' for
   *  targeting specific styles.
   * 
   * @return void
   */
   $.fn.printDiv = function() {
     var printContents = $(this).html();
     var originalContents = $('body').html();
     $('body').html(printContents);
     $('body').addClass('js-print');
     window.print();
     $('body').html(originalContents);
     $('body').removeClass('js-print');
   };
 })(jQuery);