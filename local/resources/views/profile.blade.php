@extends('layout.main')
@section('head')
  <link rel="stylesheet" href="{{asset('assets/css/photoswipe.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/photoswipe-skin.css')}}">
@endsection
@section('content')
  <!-- Section Nav -->
  <div id="data_link">
    <ul class="con-scroll doc-pills nav nav-pills nav-justified font-cloud">
      <li class="nav-item w-20">
        <a class="nav-scroll h-100 nav-link active" id="doc_tab_01" href="#doc_tab_con_01">
          <i class="fa fa-diamond mr-sm-2"></i>
          <span class="d-none d-sm-inline-block">ประวัติบริษัท</span>
        </a>
      </li>
      <li class="nav-item w-20">
        <a class="nav-scroll h-100 nav-link" id="doc_tab_02" href="#doc_tab_con_02">
          <i class="fa fa-lightbulb-o mr-sm-2"></i>
          <span class="d-none d-sm-inline-block">วิสัยทัศน์</span>
        </a>
      </li>
      <li class="nav-item w-20">
        <a class="nav-scroll h-100 nav-link" id="doc_tab_03" href="#doc_tab_con_03">
          <i class="fa fa-play-circle-o mr-sm-2"></i>
          <span class="d-none d-sm-inline-block">มีเดีย</span>
        </a>
      </li>
      <li class="nav-item w-20">
        <a class="nav-scroll h-100 nav-link" id="doc_tab_04" href="#doc_tab_con_04">
          <i class="fa fa-podcast mr-sm-2"></i>
          <span class="d-none d-sm-inline-block">ติดต่อเรา</span>
        </a>
      </li>
      <li class="nav-item w-20">
        <a class="h-100 nav-link" href="product">
          <i class="fa fa-shopping-bag mr-sm-2"></i>
          <span class="d-none d-sm-inline-block">ชมสินค้า</span>
        </a>
      </li>
    </ul>
  </div>
  <!-- Section Nav -->

  <!-- container -->
  <div class="container">

    <!-- Profile -->

    <!-- Section 01 -->
    <div class="site-content pt-0">
      <div data-target=".con-scroll" id="doc_tab_con_01">
        <div class="row">
          <div class="col-12 px-0">
            <div class="image_upload mt-0">
              <!-- Image Cover -->
              <div class="img_cover">
                <div class="img_cover_Inp d-none">
                  <input class="up_img_cover" id="imgCoverInp" type="file"/>
                  <label class="up_img_cover m-0" for="imgCoverInp" title="Edit"><i class="fa fa-camera"></i></label>
                </div>
                <div class="cove_pic owl-carousel owl-theme">
                  @php
                    $banner=\App\Models\BrandBanner::where('brand_id',$id)->get();
                    $band=\App\Models\ProductBrands::find($id);
                  @endphp
                  @foreach ($banner as $element)
                    <a class="item" href="javascript:void(0)" title="View Products">
                      <img id="cove_pic" src="{{asset('local/storage/app/images/banner')}}/{{ $element->banner }}">
                    </a>
                  @endforeach
                  {{-- <a class="item" href="product" title="View Products">
                    <img id="cove_pic" src="{{asset('assets/images/company_banner.jpg')}}">
                  </a>
                  <a class="item" href="product" title="View Products">
                    <img id="cove_pic" src="{{asset('assets/images/company_banner3.jpg')}}">
                  </a>
                  <a class="item" href="product" title="View Products">
                    <img id="cove_pic" src="{{asset('assets/images/company_banner2.jpg')}}">
                  </a> --}}
                </div>
              </div>
              <!-- Image Profile -->
              <div class="img_profile">
                <div class="profile_pic rounded-circle">
                  <a href="product" title="View Products">
                    <img id="profile_pic" class="rounded-circle" src="{{asset('local/storage/app/images/logo')}}/{{ $band->logo }}">
                  </a>
                </div>
                <div class="img_profile_Inp d-none">
                </div>
              </div>
            </div>
          </div>
        </div>

        <h3 class="font-cloud text-center mx-3 pt-4">ประวัติบริษัท</h3>
        <div class="fr-view">
          {!! $band->history !!}
        </div>
      </div>
    </div>

    <!-- Section 02 -->
    <div class="site-content">
      <div data-target=".con-scroll" id="doc_tab_con_02">
        <div class="fr-view">
          {!! $band->vision !!}
        </div>
      </div>
    </div>

    <!-- Section 03 -->
    <div class="site-content">
      <div data-target=".con-scroll" id="doc_tab_con_03">
        <h3 class="font-cloud text-center mx-3 pt-4">ภาพถ่ายและวีดีโอ</h3>
        <hr class="border-dashed mx-3">
        <div class="row px-3">
          <div class="col-12">
            @php
            $img=\App\Models\BrandImg::where('brand_id',$id)->get();
            @endphp

            <div class="photoswipe-wrapper">
              @foreach ($img as $element)
                <figure class="photoswipe-item border float-left w-25 m-0">
                  <a href="{{asset('local/storage/app/images/img')}}/{{ $element->img }}">
                    <img class="card-img rounded-0" src="{{asset('local/storage/app/images/img')}}/{{ $element->img }}">
                  </a>
                  <figcaption class="caption d-none">{{ $element->caption }}</figcaption>
                </figure>

              @endforeach
              {{-- <figure class="photoswipe-item border float-left w-25 m-0">
                <a href="{{asset('assets/images/brand/picture.jpg')}}">
                  <img class="card-img rounded-0" src="{{asset('assets/images/brand/picture.jpg')}}">
                </a>
                <figcaption class="caption d-none">Image caption 1</figcaption>
              </figure>
              <figure class="photoswipe-item border float-left w-25 m-0">
                <a href="{{asset('assets/images/brand/picture.jpg')}}">
                  <img class="card-img rounded-0" src="{{asset('assets/images/brand/picture.jpg')}}">
                </a>
                <figcaption class="caption d-none">Image caption 2</figcaption>
              </figure>
              <figure class="photoswipe-item border float-left w-25 m-0">
                <a href="{{asset('assets/images/brand/picture.jpg')}}">
                  <img class="card-img rounded-0" src="{{asset('assets/images/brand/picture.jpg')}}">
                </a>
                <figcaption class="caption d-none">Image caption 3</figcaption>
              </figure>
              <figure class="photoswipe-item border float-left w-25 m-0">
                <a href="{{asset('assets/images/brand/picture.jpg')}}">
                  <img class="card-img rounded-0" src="{{asset('assets/images/brand/picture.jpg')}}">
                </a>
                <figcaption class="caption d-none">Image caption 4</figcaption>
              </figure>
              <figure class="photoswipe-item border float-left w-25 m-0">
                <a href="{{asset('assets/images/brand/picture.jpg')}}">
                  <img class="card-img rounded-0" src="{{asset('assets/images/brand/picture.jpg')}}">
                </a>
                <figcaption class="caption d-none">Image caption 5</figcaption>
              </figure>
              <figure class="photoswipe-item border float-left w-25 m-0">
                <a href="{{asset('assets/images/brand/picture.jpg')}}">
                  <img class="card-img rounded-0" src="{{asset('assets/images/brand/picture.jpg')}}">
                </a>
                <figcaption class="caption d-none">Image caption 6</figcaption>
              </figure>
              <figure class="photoswipe-item border float-left w-25 m-0">
                <a href="{{asset('assets/images/brand/picture.jpg')}}">
                  <img class="card-img rounded-0" src="{{asset('assets/images/brand/picture.jpg')}}">
                </a>
                <figcaption class="caption d-none">Image caption 7</figcaption>
              </figure>
              <figure class="photoswipe-item border float-left w-25 m-0">
                <a href="{{asset('assets/images/brand/picture.jpg')}}">
                  <img class="card-img rounded-0" src="{{asset('assets/images/brand/picture.jpg')}}">
                </a>
                <figcaption class="caption d-none">Image caption 8</figcaption>
              </figure> --}}
            </div>
          </div>
        </div>
        <div class="row px-3 mt-3">
          <div class="col-12">
            <iframe style="width: 100%; height: 380px;" frameborder="0" allowfullscreen src="{{ $band->video }}"></iframe>
          </div>
        </div>
      </div>
    </div>

    <!-- Section 04 -->
    <div class="site-content">
      <div data-target=".con-scroll" id="doc_tab_con_04">
        <!-- Contact Data -->
        <h3 class="font-cloud text-center mx-3 pt-4">ข้อมูลติดต่อ</h3>
        <hr class="border-dashed mx-3">
        <div class="row px-3">
          <div class="col-md-6 border border-top-0 border-bottom-0 border-left-0 border-dashed">
            <div class="row">
              <div class="col-5 col-lg-4">เบอร์โทรศัพท์ :</div>
              <div class="col-7 col-lg-8">{{$band->phone}}</div>
            </div>
            <div class="row">
              <div class="col-5 col-lg-4">เบอร์แแฟกซ์ :</div>
              <div class="col-7 col-lg-8">{{$band->fax}}</div>
            </div>
            <div class="row">
              <div class="col-5 col-lg-4">เบอร์มือถือ :</div>
              <div class="col-7 col-lg-8">{{$band->mobile}}</div>
            </div>
            <div class="row">
              <div class="col-5 col-lg-4">อีเมล์ :</div>
              <div class="col-7 col-lg-8">{{ $band->email }}</div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="col-5 col-lg-4">ตำแหน่งที่ตั้ง :</div>
              <div class="col-7 col-lg-8">{{$band->address}}</div>
            </div>
            <div class="row">
              <div class="col-5 col-lg-4">Website :</div>
              <div class="col-7 col-lg-8"><a target="_blank" href="https://{{$band->website}}">{{$band->website}}</a></div>
            </div>
            <div class="row">
              <div class="col-5 col-lg-4">Facebook :</div>
              <div class="col-7 col-lg-8"><a target="_blank" href="https://www.facebook.com/{{$band->facebook}}">{{$band->facebook}}</a></div>
            </div>
            <div class="row">
              <div class="col-5 col-lg-4">Line-ID :</div>
              <div class="col-7 col-lg-8"><a target="_blank" href="http://line.me/ti/p/{{$band->line}}">{{$band->line}}</a></div>
            </div>
          </div>
        </div>
        <!-- Map -->
        <h5 class="font-cloud mx-3 pt-4">แผนที่</h5>
        <hr class="border-dashed mx-3">
        <div class="row px-3">
          <div class="col-12">
            <div id="mapDealers" style="height: 70vh; width: 100%;"></div>
          </div>
        </div>
      </div>
    </div>

    <!-- Profile -->

  </div>
  <!-- container -->

  <!-- Site footer -->
  @include('inc_footer')
  <!-- Site footer -->

  <!-- Site js -->
  @include('inc_js')
  <!-- Site js -->

  <!-- Site Sidewarp -->
  @include('inc_sidewarp')
  <!-- Site Sidewarp -->

  <!-- PhotoSwipe -->
  @include('inc_photoswipe-root')
  <script src="{{asset('assets/js/photoswipe.min.js')}}"></script>
  <script src="{{asset('assets/js/photoswipe-ui-default.min.js')}}"></script>
  <script src="{{asset('assets/js/photoswipe-custom.js')}}"></script>
  <!-- PhotoSwipe -->

  <!-- Map -->
  <script type="text/javascript">
      // Referance >>> https://www.aspsnippets.com/Articles/Google-Maps-V3-Draggable-Markers-Example-Drag-and-Drop-Markers-in-Google-Maps-using-JavaScript.aspx
      var markers = [
          {
              "title": '{{ $band->name }}',
              "lat": '{{ $band->lat }}',
              "lng": '{{ $band->lng }}',
              "description": '{{ $band->address }}'
          }
      ];
      window.onload = function () {
          var mapOptions = {
              center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
              zoom: 12,
              mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          var infoWindow = new google.maps.InfoWindow();
          var latlngbounds = new google.maps.LatLngBounds();
          var geocoder = geocoder = new google.maps.Geocoder();
          var map = new google.maps.Map(document.getElementById("mapDealers"), mapOptions);
          for (var i = 0; i < markers.length; i++) {
              var data = markers[i]
              var myLatlng = new google.maps.LatLng(data.lat, data.lng);
              var marker = new google.maps.Marker({
                  position: myLatlng,
                  map: map,
                  title: data.title,
                  draggable: true,
                  animation: google.maps.Animation.DROP
              });
              (function (marker, data) {
                  google.maps.event.addListener(marker, "click", function (e) {
                      infoWindow.setContent(data.description);
                      infoWindow.open(map, marker);
                  });
                  google.maps.event.addListener(marker, "dragend", function (e) {
                      var lat, lng, address;
                      geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                          if (status == google.maps.GeocoderStatus.OK) {
                              lat = marker.getPosition().lat();
                              lng = marker.getPosition().lng();
                              address = results[0].formatted_address;
                              alert("Latitude: " + lat + "\nLongitude: " + lng + "\nAddress: " + address);
                          }
                      });
                  });
              })(marker, data);
              latlngbounds.extend(marker.position);
          }

          // var bounds = new google.maps.LatLngBounds();   // For Multiple markers
          // map.setCenter(latlngbounds.getCenter());       // For Multiple markers
          // map.fitBounds(latlngbounds);                   // For Multiple markers
      }
  </script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGqNz-KUAKL435y6zraREiT-o5s4tuiEM&callback=initMap"></script>
  <!-- Map -->

  <!-- Owl Carousel -->
  <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/owl.theme.default.css')}}">
  <script src="{{asset('assets/js/owl.carousel.js')}}"></script>
  <script>
    $(document).ready(function() {
      var owl = $('.cove_pic');
      owl.owlCarousel({
        navText: [ '', '' ],
        items: 1,
        loop: true,
        autoplay: true,
        autoplayTimeout: 6000,
        autoHeight: true,
        autoplayHoverPause: true,
        // animateOut: 'fadeOut',
        nav: true,
        dots: false
      });
      $('.play').on('click', function() {
        owl.trigger('play.owl.autoplay', [1000])
      })
      $('.stop').on('click', function() {
        owl.trigger('stop.owl.autoplay')
      })
    })
  </script>
  <!-- Owl Carousel -->
@endsection
