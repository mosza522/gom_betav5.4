
<!-- Side Sign -->
<div id="site-sidesign" class="side-warp"></div>
<!-- Side Order History -->
<div id="site-sideOrder" class="side-warp"></div>

<script>

  function openSign() {
    document.getElementById("site-sidesign").style.width = "100%";
    $("body").addClass("no-scroll");
    $(".side-warp").load("{{url('loginSlide')}}");
  }
  function closeSign() {
    document.getElementById("site-sidesign").style.width = "0";
    $("body").removeClass("no-scroll");
  }
  function openOrderHistory() {
    document.getElementById("site-sideOrder").style.width = "100%";
    $("body").addClass("no-scroll");
    if ($(window).width() < 767) {
      $(".side-warp").load("{{url('order_history-mobile')}}");
    } else {
      $(".side-warp").load("{{url('order_history')}}");
    }
  }
  function closeOrderHistory() {
    document.getElementById("site-sideOrder").style.width = "0";
    $("body").removeClass("no-scroll");
    $(".remove-this").remove();
    // location.reload(true);
  }


</script>
