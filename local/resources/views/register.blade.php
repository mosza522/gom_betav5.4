@extends('layout.main')
@section('head')
  <style media="screen">
  #pac-input {
    background-color: #fff;
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
    margin-left: 12px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 400px;
  }

  #pac-input:focus {
    border-color: #4d90fe;
  }
  .user{
    margin-top: 30px;
  }
  </style>
@endsection
@section('content')



  <!-- Site Content -->
  <div class="container px-xs-0">
    <div id="site-content" class="m-xs-0">

      <div class="col-12"><h3 class="font-cloud">สร้างบัญชี</h3><hr></div>
      <div class="col-12">
        <form id="registeration" action="{{ action('UserController@store') }}" method="post" enctype="multipart/form-data" autocomplete="new-password" onsubmit="return checkall();"  novalidate>
          {{ csrf_field() }}
          <!-- Profile Image -->
          <div class="row">
            <div class="col-12">
              <div class="image_upload">
                <!-- Image Cover -->
                <div  class="img_cover">
                  <div class="img_cover_Inp">
                    <input class="up_img_cover" id="imgCoverInp" name="img_cover"  type="file"/ required>
                    <label class="up_img_cover m-0" for="imgCoverInp" title="Edit"><i class="fa fa-camera"></i></label>
                  </div>
                  <div class="cove_pic">
                    <img id="cove_pic" src="{{asset('assets/images/company_banner2.jpg')}}" title="Cover Image">
                  </div>
                </div>
                <!-- Image Profile -->
                <div  class="img_profile">
                  <div class="profile_pic rounded-circle">
                    <img id="profile_pic" class="rounded-circle" src="{{asset('assets/images/icons/user_01.png')}}" title="Profile Image">
                  </div>
                  <div class="img_profile_Inp">
                    <input class="up_img_profile" id="imgInp"  name="img_profile" type="file"/ required>
                    <label class="up_img_profile m-0" for="imgInp" title="Edit"><i class="fa fa-camera"></i></label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Profile Image -->

          <!-- Account -->
          <h5 class="font-cloud pt-4">ข้อมูลบัญชี</h5>
          <div class="alert alert-light border-dashed">
            <div class="row">
              <div class="col-md-12 mb-2 was-validated">
                <div class="custom-controls-stacked d-block">
                  <label>ประเภทสมาชิก&nbsp;&nbsp;&nbsp;</label>
                  <label class="custom-control custom-radio align-items-center d-block d-md-inline-block">
                    <input id="member_type1" name="member_type" type="radio" class="custom-control-input" value="Customer" required>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">ผู้ซื้อ</span>
                  </label>
                  <label class="custom-control custom-radio align-items-center d-block d-md-inline-block">
                    <input id="member_type2" name="member_type" type="radio" class="custom-control-input" value="Dealers" required>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">ผู้ขาย</span>
                  </label>
                  <div class="invalid-feedback">*กรุณากรอกข้อมูลในช่องนี้ด้วยค่ะ</div>
                </div>
              </div>
            </div>
            <div class="row" id="customer_type">
              <div class="col-md-12 mb-2 was-validated">
                <div class="custom-controls-stacked d-block">
                  <label>เลือกกลุ่มผู้ซื้อ&nbsp;&nbsp;&nbsp;</label>
                  <label class="custom-control custom-radio align-items-center d-block d-md-inline-block">
                    <input id="member_group1" name="member_group" type="radio" class="custom-control-input" value="store" >
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description" data-toggle="popover" data-content="กรุณาแนบใบอุนญาตมีไว้ในครอบครองซึ่งวัตถุอันตรายด้วยค่ะ">ร้านค้า</span>
                  </label>
                  <label class="custom-control custom-radio align-items-center d-block d-md-inline-block">
                    <input id="member_group2" name="member_group" type="radio" class="custom-control-input" value="sugar_factory" >
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description" data-toggle="popover" data-content="กรุณาแนบใบอุนญาตประกอบกิจการโรงงานน้ำตาลด้วยค่ะ">โรงงานน้ำตาล</span>
                  </label>
                  <label class="custom-control custom-radio align-items-center d-block d-md-inline-block">
                    <input id="member_group3" name="member_group" type="radio" class="custom-control-input" value="Government" >
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description" data-toggle="popover" data-content="กรุณาแนบหนังสือรับรองการเป็นหน่วยงานรัฐด้วยค่ะ">หน่วยงานรัฐบาล</span>
                  </label>
                  <label class="custom-control custom-radio align-items-center d-block d-md-inline-block">
                    <input id="member_group4" name="member_group" type="radio" class="custom-control-input" value="farmer" >
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">เกษตรกร (โครงการเกษตรแปลงใหญ่)</span>
                  </label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-3 mb-2">
                <label>ชื่อ-นามสกุล</label>
                <input id="name" name="fullname" type="text" class="form-control form-control-sm"  required>
              </div>
              <div class="col-sm-6 col-lg-3 mb-2">
                <label>ชื่อบริษัท/หน่วยงาน/ร้านค้า</label>
                <input id="company_name" name="company_name" type="text" class="form-control form-control-sm" required>
              </div>
              <div class="col-sm-6 col-lg-3 mb-2">
                <label>เลขที่ สำนักงาน/หน่วยงาน/ร้านค้า</label>
                <input id="company_code" name="company_code" type="text" class="form-control form-control-sm" required>
              </div>
              <div class="col-sm-6 col-lg-3 mb-2">
                <label>เลขประจำตัวผู้เสียภาษี</label>
                <input id="tax_id" name="tax_id" type="text" class="form-control form-control-sm" onkeypress="return check_number(this);" required>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-3 mb-2">
                <label>ที่อยู่</label>
                <input id="address" name="address" type="text" class="form-control form-control-sm" required>
              </div>
              <div class="col-sm-6 col-lg-3 mb-2">
                <label>รหัสไปรษณีย์</label>
                <input id="zipcode" name="zipcode" type="text" class="form-control form-control-sm" onkeypress="return check_number(this);" required>
              </div>
              {{-- <div class="col-sm-6 col-lg-3 mb-2">
              <label>เขต/อำเภอ</label>
              <select id="district" class="form-control form-control-sm custom-select d-block" required>
              <option value="">-</option>
              <option value="1">Choise 1</option>
              <option value="2">Choise 2</option>
              <option value="3">Choise 3</option>
            </select>
          </div> --}}
          <div class="col-sm-6 col-lg-3 mb-2">
            @php
            $amphur = App\Models\Amphur::get();
            $province = App\Models\Province::get();
            @endphp
            <label>เขต/อำเภอ</label>
            <select id="district"  name="district" class="form-control" required>
              <option value=""></option>
              @foreach ($amphur as $element)
                <option value="{{ $element->AMPHUR_ID }}">{{ $element->AMPHUR_NAME }}</option>
              @endforeach
            </select>
          </div>
          <div class="col-sm-6 col-lg-3 mb-2">
            <label>จังหวัด</label>
            <select id="province" name="province" class="form-control" required>
              <option value=""></option>
              @foreach ($province as $element)
                <option value="{{ $element->PROVINCE_ID }}">{{ $element->PROVINCE_NAME }}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6 col-lg-3 mb-2">
            <label>อีเมล์</label>
            <input id="email" name="email" type="email" class="form-control form-control-sm" required>
          </div>
          <div class="col-sm-6 col-lg-3 mb-2">
            <label>โทรศัพท์มือถือ</label>
            <input id="mobile" name="mobile" type="tel" class="form-control form-control-sm" onkeypress="return check_number(this);" required>
          </div>
          <div class="col-sm-6 col-lg-3 mb-2">
            <label>โทรศัพท์</label>
            <input id="phone" name="phone" type="tel" class="form-control form-control-sm" onkeypress="return check_number(this);">
          </div>
          <div class="col-sm-6 col-lg-3 mb-2">
            <label>แฟกซ์</label>
            <input id="fax" name="fax" type="tel" class="form-control form-control-sm" onkeypress="return check_number(this);">
          </div>
        </div>
      </div>
      <!-- Account -->

      <!-- Document -->
      <div id="document">
        <h5 class="font-cloud pt-4">ใบอนุญาต</h5>
        <div class="alert alert-light border-dashed">
          <h6 class="font-cloud txt_alert alert_1"><i class="fa fa-info mr-2"></i>กรุณาแนบใบอุนญาตมีไว้ในครอบครองซึ่งวัตถุอันตราย และกรอกเลขที่ใบอนุญาตด้วยค่ะ</h6>
          <h6 class="font-cloud txt_alert alert_2"><i class="fa fa-info mr-2"></i>กรุณาแนบใบอุนญาตประกอบกิจการโรงงานน้ำตาล และกรอกเลขที่ใบอนุญาตด้วยค่ะ</h6>
          <h6 class="font-cloud txt_alert alert_3"><i class="fa fa-info mr-2"></i>กรุณาแนบกรุณาแนบหนังสือรับรองการเป็นหน่วยงานรัฐ และกรอกเลขที่เอกสารด้วยค่ะ</h6>
          <div class="row">

            <div class="col-sm-12 col-lg-6">
              <div class="cloned-cer">
                <div class="row">
                  <div class="col-sm-6 col-lg-6 mb-0 mb-lg-2">
                    <label>ใบอนุญาต</label>
                    <input id="certificate" name="certificate[]" onchange="return checkfile();" type="file" class="form-control form-control-sm form-control-file p-0" >
                  </div>
                  <div class="col-sm-6 col-lg-6 mb-0 mb-lg-2">
                    <label>เลขที่ใบอนุญาต</label>
                    <input id="certificate-number" name="certificate_number[]" type="text" class="form-control form-control-sm" >
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12 col-lg-6 pt-2 pt-lg-4">
              <button type="button" class="btn btn-sm cloned-cer-btn"><i class="fa fa-plus-circle mr-2"></i>เพิ่มเอกสาร</button>
            </div>

          </div>
        </div>
      </div>
      <!-- Document -->

      <!-- User -->
      <h5 class="font-cloud pt-4">ข้อมูลผู้ใช้</h5>
      <div class="alert alert-light border-dashed">
        <div class="row">
          <div class="col-sm-6 col-lg-3 mb-2">
            <label>ชื่อในระบบ</label>
            <input id="username" name="username" type="text" class="form-control form-control-sm" placeholder="username" autocomplete="new-password" required>
          </div>
          <div class="col-sm-6 col-lg-3 mb-2">
            <label>รหัสผ่าน</label>
            <input id="password" name="password" type="password" class="form-control form-control-sm" placeholder="password" required>
          </div>
          <div class="col-sm-12 col-lg-3 mb-2">
            <label class="user" id="checkuser" style="color : red;">กรุณากรอก username เพื่อเช็คในระบบ</label>
            <input type="hidden" id="hidden_checkuser" name="check" value="">
          </div>
        </div>
      </div>
      <!-- User -->

      <!-- Map -->
      <div class="alert alert-light border-dashed mt-4 p-0 p-sm-3">
        <div class="row">
          <div class="col-md-12 mb-2">
            <h5 class="font-cloud p-3 p-sm-0"><i class="fa fa-map-marker mb-2 mr-2"></i>ตำแหน่งบนแผนที่ : <small>กรุณาระบุตำแหน่งโดยการเลื่อนหมุดสีแดงบนแผนที่ไปยังตำแหน่งที่ตั้งของคุณ เพื่อสิธิประโยชน์ทางการค้าค่ะ</small></h5>
            {{-- <input id="pac-input" class="controls" type="text" placeholder="Search Box"> --}}
            <div id="dvMap" style="height: 70vh; width: 100%;"></div>
          </div>
        </div>
      </div>
      <!-- Map -->

      <!-- Area -->
      <div id="area">
        <h5 class="font-cloud p-3 p-sm-0"><i class="fa fa-street-view mb-2 mr-2"></i>กันเขตพื้นที่ : <small>กรุณาระบุตำแหน่งโดยการเลื่อนหมุดสีแดงบนแผนที่ไปยังตำแหน่งที่ตั้งของคุณ เพื่อสิธิประโยชน์ทางการค้าค่ะ</small></h5>
        <div class="alert alert-light border-dashed">
          <div class="row">
            <div class="col-sm-6 col-lg-3 mb-2">
              <label>รัศมี (กิโลเมตร)</label>
              <input id="area-radius" name="area_radius" type="text" onkeypress="return check_number(this);" class="form-control form-control-sm">
            </div>
            <div class="col-sm-6 col-lg-3 mb-2">
              <label>ระยะเวลา (จำนวนวัน)</label>
              <input id="area-time" name="area_time" type="text" onkeypress="return check_number(this);" class="form-control form-control-sm">
            </div>
            <input type="hidden" name="lat" id="lat" value="">
            <input type="hidden" name="long" id="long" value="">
          </div>
        </div>
      </div>
      <!-- Area -->

      <!-- Newsletter -->
      <hr class="border-dashed">
      <div class="row">
        <div class="col-md-12 mb-2 information">
          <label class="custom-control custom-checkbox align-items-center">
            <input id="subscribe" type="checkbox" class="custom-control-input" value="">
            <span class="custom-control-indicator"></span>
            <span class="custom-control-description">ฉันต้องการรับข่าวสารทางอีเมล์</span>
          </label>
        </div>
      </div>
      <!-- Newsletter -->

      <button id="complete" class="btn btn-success mt-2" type="submit"><i class="fa fa-user-plus mr-2"></i>สร้างบัญชี</button>

    </form>
  </div>

</div>
</div>
<!-- Site Content -->

<!-- Site footer -->
@include('inc_footer')
<!-- Site footer -->

<!-- Site js -->
@include('inc_js')
<!-- Site js -->

<!-- Site Sidewarp -->
@include('inc_sidewarp')
<!-- Site Sidewarp -->

<!-- Member Group Function -->
<script>
var checkusername='';
function checkall() {


  if($('#subscribe:checked').length == 1){
    $('.information').append('<input type="hidden" name="need" value="1" id="need" />');
  }else{
    $("#need").remove();
  }
  // alert($('input[name="certificate"]'.val()));
  if($('#imgCoverInp').val()==""){
    $('#imgCoverInp').focus();
    swal({
      title: "กรุณาใส่รูปภาพหน้าปก",
      text: "เพื่อประโยชน์ของท่านกรุณาใส่ภาพหน้าปก",
      icon: "warning",
      button: "OK",
    });
    return false;
  }
  if($('#imgInp').val()==""){
    $('#imgInp').focus();
    swal({
      title: "กรุณาใส่รูปภาพโปรไฟล์",
      text: "เพื่อประโยชน์ของท่านกรุณาใส่ภาพหน้าปก",
      icon: "warning",
      button: "OK",
    });
    return false;
  }
  if($('input[name="member_type"]:checked').length < 1){
    $('#member_type1').focus();
    swal({
      title: "กรุณาเลือกประเภทสมาชิก",
      text: "เพื่อประโยชน์ของท่านกรุณาเลือกประเภทสมาชิก",
      icon: "warning",
      button: "OK",
    });
    return false;
  }else{
    if($('#member_type1:checked').length == 1 && $('input[name="member_group"]:checked').length < 1){
      swal({
        title: "กรุณาเลือกกลุ่มผู้ซื้อ",
        text: "เพื่อประโยชน์ของท่านกรุณาเลือกกลุ่มผู้ซื้อ",
        icon: "warning",
        button: "OK",
      });
      return false;
    }
    if($('#lat').val()=="" || $('#long').val()==""){
      $('#dvMap').focus();
      swal({
        title: "กรุณาระบุตำแหน่งของร้านท่าน",
        text: "เพื่อประโยชน์ของท่านกรุณาระบุตำแหน่งของร้านท่าน",
        icon: "warning",
        button: "OK",
      });
      return false;
    }
    if($('#member_group1:checked').length == 1 || $('#member_group2:checked').length == 1 || $('#member_group3:checked').length == 1){
      // var certificate = $("input[name='certificate[]']")
      // .map(function(){return $(this).val();}).get();
      var certificate = $("input[name='certificate[]']");
      var certificate_number = $("input[name='certificate_number[]']");
      var extall="jpg,jpeg,gif,png";

      // alert(certificate.length);
      for (var i = 0; i < certificate.length; i++) {
        ext = certificate.eq(i).val().split('.').pop().toLowerCase();
        if(certificate.eq(i).val()==""){
          swal({
            title: "กรุณาเลือกรูปใบเซอร์",
            text: "เพื่อประโยชน์ของท่านกรุณาเลือกรูปใบเซอร์",
            icon: "warning",
            button: "OK",
          });
          $("input[name='certificate[]']")[i].focus();
          return false;
        }else{
          if(parseInt(extall.indexOf(ext)) < 0) {
            swal({
              title: "กรุณาเลือกภาพที่ถูกต้อง",
              text: 'สามารถใช้ได้เฉพาะไฟล์ : ' + extall,
              icon: "warning",
              button: "OK",
            });
            $("input[name='certificate[]']")[i].focus();
            certificate.eq(i).val('');
            return false;
          }
        }
        if (certificate_number.eq(i).val()=="") {
          swal({
            title: "กรุณากรอกหมายเลขใบเซอร์",
            text: "เพื่อประโยชน์ของท่านกรุณากรอกหมายเลขใบเซอร์",
            icon: "warning",
            button: "OK",
          });
          $("input[name='certificate_number[]']")[i].focus();
          return false;
        }
      }

    }

  }
  if($('#name').val()==""){
    $('#name').focus();
    swal({
      title: "กรุณาใส่ชื่อและนามสกุล",
      text: "เพื่อประโยชน์ของท่านกรุณาใส่ชื่อและนามสกุล",
      icon: "warning",
      button: "OK",
    });
    return false;
  }
  if($('#company_name').val()==""){
    $('#company_name').focus();
    swal({
      title: "กรุณากรอกชื่อ บริษัท/หน่วยงาน/ร้านค้า",
      text: "เพื่อประโยชน์ของท่านกรุณากรอกชื่อบริษัท/หน่วยงาน/ร้านค้า",
      icon: "warning",
      button: "OK",
    });
    return false;
  }
  if($('#company_code').val()==""){
    $('#company_code').focus();
    swal({
      title: "กรุณากรอกเลขที่ สำนักงาน/หน่วยงาน/ร้านค้า",
      text: "เพื่อประโยชน์ของท่านกรุณากรอกเลขที่ สำนักงาน/หน่วยงาน/ร้านค้า",
      icon: "warning",
      button: "OK",
    });
    return false;
  }
  if($('#tax_id').val()==""){
    $('#tax_id').focus();
    swal({
      title: "กรุณากรอกเลขที่เลขประจำตัวผู้เสียภาษี",
      text: "เพื่อประโยชน์ของท่านกรุณากรอกเลขที่เลขประจำตัวผู้เสียภาษี",
      icon: "warning",
      button: "OK",
    });
    return false;
  }
  if($('#address').val()==""){
    $('#address').focus();
    swal({
      title: "กรุณากรอกที่อยู่",
      text: "เพื่อประโยชน์ของท่านกรุณากรอกที่อยู่",
      icon: "warning",
      button: "OK",
    });
    return false;
  }
  if($('#zipcode').val()==""){
    $('#zipcode').focus();
    swal({
      title: "กรุณากรอกรหัสไปรษณีย์",
      text: "เพื่อประโยชน์ของท่านกรุณากรอกรหัสไปรษณีย์",
      icon: "warning",
      button: "OK",
    });
    return false;
  }
  if($('#district').val()==""){
    $('#district').focus();
    swal({
      title: "กรุณาเลือกเขต/อำเภอ",
      text: "เพื่อประโยชน์ของท่านกรุณาเลือกเขต/อำเภอ",
      icon: "warning",
      button: "OK",
    });
    return false;
  }
  if($('#province').val()==""){
    $('#province').focus();
    swal({
      title: "กรุณาเลือกจังหวัด",
      text: "เพื่อประโยชน์ของท่านกรุณาเลือกจังหวัด",
      icon: "warning",
      button: "OK",
    });
    return false;
  }
  if($('#email').val()==""){
    $('#email').focus();
    swal({
      title: "กรุณากรอกอีเมล์",
      text: "เพื่อประโยชน์ของท่านกรุณากรอกอีเมล",
      icon: "warning",
      button: "OK",
    });
    return false;
  }else{
    mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!($('#email').val().match(mailformat)))
    {
      $('#email').focus();
      swal({
        title: "รูปแบบอีเมลไม่ถูกต้อง",
        text: "เพื่อประโยชน์ของท่านกรุณากรอกอีเมลให้ถูกต้อง",
        icon: "warning",
        button: "OK",
      });
      return false;
    }
  }
  if($('#mobile').val()==""){
    $('#mobile').focus();
    swal({
      title: "กรุณากรอกเบอร์โทรศัพท์มือถือ",
      text: "เพื่อประโยชน์ของท่านกรุณากรอกเบอร์โทรศัพท์มือถือ",
      icon: "warning",
      button: "OK",
    });
    return false;
  }
  if(checkusername!='pass'){
    $('#username').focus();
    swal({
      title: "Username นี่มีผู้ใช้แล้ว",
      text: "เพื่อประโยชน์ของท่านกรุณากรอก username ให้ถูกต้อง",
      icon: "warning",
      button: "OK",
    });
    return false;
  }
  if($('#username').val()==''){
    $('#username').focus();
    swal({
      title: "กรุณากรอก username ให้ถูกต้อง",
      text: "เพื่อประโยชน์ของท่านกรุณากรอก username ให้ถูกต้อง",
      icon: "warning",
      button: "OK",
    });
    return false;
  }

  if($('#password').val()=='' || $('#password').val().length<=6){
    $('#password').focus();
    swal({
      title: "กรุณากรอก password ให้ถูกต้อง ",
      text: "เพื่อประโยชน์ของท่านกรุณากรอก password ให้ถูกต้อง \n กรอก Password 6 ตัวขึ้นไป",
      icon: "warning",
      button: "OK",
    });
    return false;
  }







}
function check_number(salary) {
  var vchar = String.fromCharCode(event.keyCode);
  if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
  salary.onKeyPress=vchar;

}
// $("input[name='certificate[]']").change(function(){
// var certificate = $("input[name='certificate[]']");
// for (var i = 0; i < certificate.length; i++) {
//
// }
// });

$(document).ready(function(){




  $("#customer_type, #document,  #area, .txt_alert").hide();

  $('input:radio[name="member_type"]').change(function(){
    if($(this).val()=="Customer")
    {
      $("#customer_type, #area").fadeIn();
      $('#member_group1').prop("required","required");
      $('#certificate').prop("required","required");
      $('#certificate-number').prop("required","required");

    }
    else if($(this).val()=="Dealers")
    {
      $("#customer_type, #document, #area").fadeOut();
      $('#member_group1').removeAttr("required");
      $('#certificate').removeAttr("required");
      $('#certificate-number').removeAttr("required");
    }
  });

  $('input:radio[name="member_group"]').change(function(){
    if($(this).val()=="store")
    {
      $("#document, .alert_1").fadeIn();
      $(".alert_2, .alert_3").fadeOut();
    }
    else if($(this).val()=="sugar_factory")
    {
      $("#document, .alert_2").fadeIn();
      $(".alert_1, .alert_3").fadeOut();
    }
    else if($(this).val()=="Government")
    {
      $("#document, .alert_3").fadeIn();
      $(".alert_1, .alert_2").fadeOut();
    }
    else if($(this).val()=="farmer")
    {
      $("#document").fadeOut();
    }
  });
  var clone="";
  clone+="<div class=\"row\"\>";
  clone+="<div class=\"col-sm-6 col-lg-6 mb-0 mb-lg-2\"\>";
  clone+="<label>ใบอนุญาต</label>";
  clone+="<input id=\"certificate\" name=\"certificate[]\" onchange=\"return checkfile();\" type=\"file\" class=\"form-control form-control-sm form-control-file p-0\"\>";
  clone+="</div>";
  clone+="<div class=\"col-sm-6 col-lg-6 mb-0 mb-lg-2\"\>";
  clone+="<label>เลขที่ใบอนุญาต</label>";
  clone+="<input id=\"certificate-number\" name=\"certificate_number[]\" type=\"text\" class=\"form-control form-control-sm\"\>";
  clone+="</div>";
  clone+="</div>";
  $('.cloned-cer-btn').click(function (){
    $('.cloned-cer').append(clone);
  })

});
</script>

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  "use strict";
  window.addEventListener("load", function() {
    var form = document.getElementById("registeration");
    form.addEventListener("submit", function(event) {
      if (form.checkValidity() == false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add("was-validated");
    }, false);
  }, false);
}());
</script>

<!-- Map -->
<script type="text/javascript">
var allow='' ;
// Referance >>> https://www.aspsnippets.com/Articles/Google-Maps-V3-Draggable-Markers-Example-Drag-and-Drop-Markers-in-Google-Maps-using-JavaScript.aspx
var markers = [
  {
    "title": 'Victory Monument',
    "lat": '13.765256',
    "lng": '100.538264',
    "description": 'Victory Monument Phayathai Rd, Ratchathewi, Bangkok 10400, Thailand.'
  }
];
function getLocation() {

  navigator.geolocation.watchPosition(function(position) {
    allow='allow';
  console.log("i'm tracking you!");
  console.log(allow);

},
function (error) {
  if (error.code == error.PERMISSION_DENIED)
      console.log("you denied me :-(");
      allow='denined';
});
setTimeout(function(){
  if(allow==''){
    swal({
      title: "กรุณาอนุญาตการเข้าถึงตำแหน่งของคุณ ",
      text: "เพื่อความสะดวกของตัวท่าน กรุณาอนุญาตการเข้าถึงตำแหน่งของคุณ",
      icon: "warning",
      button: "OK",
    });
  }
}, 3000);

  if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        alert("Geolocation is not supported by this browser.");
    }
    getMaps();

}
function showPosition(position) {
  markers[0].lat=position.coords.latitude;
  markers[0].lng=position.coords.longitude;

  getMaps();
}
window.onload = function () {

  getLocation();

}
function getMaps() {
var mapOptions = {
    center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
    zoom: 12,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var infoWindow = new google.maps.InfoWindow();
  var latlngbounds = new google.maps.LatLngBounds();
  var geocoder = geocoder = new google.maps.Geocoder();
  var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
  for (var i = 0; i < markers.length; i++) {
    var data = markers[i]
    var myLatlng = new google.maps.LatLng(data.lat, data.lng);
    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: data.title,
      draggable: true,
      animation: google.maps.Animation.DROP
    });
    (function (marker, data) {
      google.maps.event.addListener(marker, "click", function (e) {
        infoWindow.setContent(data.description);
        infoWindow.open(map, marker);
      });
      google.maps.event.addListener(marker, "dragend", function (e) {
        var lat, lng, address;
        geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            lat = marker.getPosition().lat();
            lng = marker.getPosition().lng();
            address = results[0].formatted_address;
            $('#lat').val(lat);
            $('#long').val(lng);
            // alert("Latitude: " + lat + "\nLongitude: " + lng + "\nAddress: " + address);

          }
        });
      });
    })(marker, data);
    latlngbounds.extend(marker.position);
  }

  // var bounds = new google.maps.LatLngBounds();   // For Multiple markers
  // map.setCenter(latlngbounds.getCenter());       // For Multiple markers
  // map.fitBounds(latlngbounds);                   // For Multiple markers
}
function initAutocomplete() {
  var map = new google.maps.Map(document.getElementById('DvMap'), {
    center: {lat: -33.8688, lng: 151.2195},
    zoom: 15,
    mapTypeId: 'roadmap'
  });
  // var mapOptions = {
  //     center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
  //     zoom: 5,
  //     mapTypeId: google.maps.MapTypeId.ROADMAP
  // };

  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });

  var markers = [];
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      if (!place.geometry) {
        console.log("Returned place contains no geometry");
        return;
      }
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA5xzy3yIqTuHubzsxj1SedWkQMsTxFwPo&libraries=places&callback=initAutocomplete"
async defer></script>
<!-- Map -->
<script type="text/javascript">
$('#district').select2({
  placeholder : 'กรุณาเลือกอำเภอ'
});
$('#province').select2({
  placeholder : 'กรุณาเลือกจังหวัด'
});
</script>
<script type="text/javascript">

$(document).ready(function() {

$('#username').change(function(){
  $.ajax({
          type: "POST",
          url: "assets/js/checkname.php",
          data:{username:$('#username').val()},
          type: "POST",
          dataType: "html",
          success: function (data) {
            if(data=='pass'){
            $('#checkuser').html('สามารถใช้ user นี้ได้');
            $('#checkuser').css("color", "green");
            checkusername=data;
            }else{
            $('#checkuser').html('User นี้มีผู้ใช้แล้วแล้ว');
            $('#checkuser').css("color", "red");
            checkusername=data;
            }
          },
});
});

});
</script>

@endsection
