@extends('backoffice.layouts.main')
@section('page_title','GomBeta | Promotion Of Month')
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Promotion
        <small>Promotion Of Month</small>
        <p>
          {{-- {{Html::link('backoffice/add_admin','<i class="fa fa-plus-square" aria-hidden="true"></i>Add Admin',array('class'=>'btn btn-primary'))}} --}}

        </p>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-cog"></i> Backoffice</a></li>
        <li class="active">Promotion Of Month</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      @if(Session::has('success'))
        {{-- <div class="alert alert-success">
          <ul>
          <li> {{ Session::get('success') }}</li>
          </ul>
        </div> --}}
        <script>
            swal("{{ Session::get('success') }}", "", "success");
        </script>
    @endif

    {{-- <script>
        swal("Good job!", "You clicked the button!", "success");
    </script> --}}
    <a  href="" data-toggle="modal" data-target="#myModalInsert"><button type="button" class="btn btn-primary"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม Promotion Of Month</button></a>
    <p></p>

    <div class="modal fade" id="myModalInsert"  role="dialog" aria-labelledby="myModalLabel" style="display: none;">
      <div class="modal-dialog" role="document" style="width:50%;">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times; </button> <span aria-hidden="true"> </span>
            <h3 class="text-center">เพิ่ม Promotion Of Month</h3>
          </div>
          <form class="" id="insert_promotion" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{-- <input type="hidden" name="type" value="category"> --}}
            <div class="modal-body">
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        สินค้า
                      </div>
                      <div class="col-md-6">
                        @php
                        $product= \App\Models\Product::whereNotIn('product.id',function($q){
                          $q->select('product_id')->from('hl_promotionofmonth');
                        })->get();
                        @endphp
                        <select class="form-control" name="product" id="product" style="width:100%">
                          <option value=""></option>

                          @foreach ($product as $element)
                            <option value="{{$element->id}}">{{ $element->name }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
              <div class="modal-footer" style="">
                <button type="submit" href="javascript:void(0);" class="btn btn-success" style="margin-top:30px" onclick="" >เพิ่ม</button>
                <button class="btn btn-danger" type="reset" style="margin-top:30px" data-dismiss="modal">Close</button>
              </div>
            </form>

        </div>
      </div>
      </div>
      {{-- Modal edit  --}}
      <div class="modal fade" id="myModalupdate"  role="dialog" aria-labelledby="myModalLabel" style="display: none;">
        <div class="modal-dialog" role="document" style="width:50%;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times; </button> <span aria-hidden="true"> </span>
              <h3 class="text-center">แก้ไข Promotion Of Month</h3>
            </div>
            <form class="" id="edit_promotion" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <input type="hidden" name="id" id="hidden_id" value="">
              <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                      <div style="">
                        <div class="col-md-6 text-center">
                          สินค้า
                        </div>
                        <div class="col-md-6">
                          @php
                          $product= \App\Models\Product::whereNotIn('product.id',function($q){
                            $q->select('product_id')->from('hl_promotionofmonth');
                          })->get();
                          @endphp
                          <select class="form-control" name="product" id="product_edit" style="width:100%">
                            <option value=""></option>

                            @foreach ($product as $element)
                              <option value="{{$element->id}}">{{ $element->name }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
                <div class="modal-footer" style="">
                  <button type="submit" href="javascript:void(0);" class="btn btn-success" style="margin-top:30px" onclick="" >แก้ไข</button>
                  <button class="btn btn-danger" type="reset" style="margin-top:30px" data-dismiss="modal">Close</button>
                </div>
              </form>

          </div>
        </div>
        </div>




      <table id="example" class="table">
        <thead>
          <tr>
            <th class="text-center" style="width: 5%;">No .</th>
            <th class="text-center" style="width: 50%;">Product Name</th>
            <th class="text-center" style="width: 30%;">Action</th>
          </tr>
        </thead>
    <tbody id="data">
      @php
        $datas= \App\Models\PromotionOfMonth::leftJoin('product','hl_promotionofmonth.product_id','=','product.id')
        ->select('hl_promotionofmonth.*','product.name')
        ->get();
        $i=1;
      @endphp
      @foreach ($datas as $element)
        <tr>
          <td>{{ $i++ }}</td>
          <td>{{ $element->name }}</td>
          <td>
            <a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return show({{$element->id}})" class="btn btn-warning">Edit</a>
            <a href='javascript:void(0);' onclick='return sweetalert({{$element->id}})' class='btn btn-danger'>Delete</a>
          </td>
        </tr>
      @endforeach
    </tbody>
    </table>

    </section>

  </div>
<script type="text/javascript">
function show(id) {
  $.ajax({
    url: '{{ url('promotionofmonth') }}/'+id,
    type: 'GET',
    data: '',
    dataType:'json',
    success: function (html) {
      // alert(JSON.stringify(html));
      var $el = $("#product_edit");
      $el.empty();
      change_selected(document.getElementById('product_edit'));
      function change_selected(selector)
      {
        selector.options[0] = new Option("","");
        for (var i = 0; i < html[0].select.length; i++) {
          selector.options[i+1] = new Option(html[0].select[i].name,html[0].select[i].id);
        }

      }
      $('#product_edit').val(html[0].data.product_id);
      $('#hidden_id').val(html[0].data.id);

    }
  });

}
$('#insert_promotion').submit(function() {
  if($('#product').val()==""){
    swal("ข้อมูลไม่ครบถ้วน", "กรุณาเลือกสินค้า !!", "warning");
    return false;
  }

  var formData = new FormData($(this)[0]);
  var dataTable = $("#example").DataTable();
  $.ajax({
    url: '{{url('promotionofmonth')}}',
    type: 'POST',
    data: formData,
    dataType:'json',
    success: function (html) {
      // alert(html);
        swal("Successfully", "เพิ่ม Promotion of month สำเร็จ !!", "success");
        dataTable.clear();
        dataTable.rows.add(html[0].table).draw();
        $("#insert_promotion")[0].reset();
        $('#myModalInsert').modal('toggle');
        var $el = $("#product");
        $el.empty();
        change_selected(document.getElementById('product'));
        function change_selected(selector)
          {
            selector.options[0] = new Option("","");
            for (var i = 0; i < html[0].select.length; i++) {
              selector.options[i+1] = new Option(html[0].select[i].name,html[0].select[i].id);
            }

        }
        $('#product').val();
    },
    cache: false,
    contentType: false,
    processData: false
  });
return false;
});
$('#edit_promotion').submit(function() {
  var formData = new FormData($(this)[0]);
  var dataTable = $("#example").DataTable();
  $.ajax({
    url: '{{url('promotionofmonth')}}/'+$('#hidden_id').val()+'/update',
    type: 'POST',
    data: formData,
    dataType:'json',
    success: function (html) {
      swal("Successfully", "แก้ไข Promotion of month สำเร็จ !!", "success");
      dataTable.clear();
      dataTable.rows.add(html[0].table).draw();
      $('#myModalupdate').modal('toggle');
      var $el = $("#product");
      $el.empty();
      change_selected(document.getElementById('product'));
      function change_selected(selector)
      {
        selector.options[0] = new Option("","");
        for (var i = 0; i < html[0].select.length; i++) {
          selector.options[i+1] = new Option(html[0].select[i].name,html[0].select[i].id);
        }

      }
    },
    cache: false,
    contentType: false,
    processData: false
  });
return false;
});
function sweetalert(id) {
  var dataTable = $("#example").DataTable();
  swal({
    title: "แน่ใจหรือ ?",
    text: "คุณต้องการลบข้อมูลนี้ใช่หรือไม่",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((sweetalert) => {
    if (sweetalert) {
      // setTimeout(function () {
      $.ajax({
        type: 'DELETE',
        url: '{{url('promotionofmonth')}}'+'/'+id,
        data: {_token: "{{ csrf_token() }}"},
        dataType:'json',
        success:function(html){
          // alert(JSON.stringify(html[0].select[0].id));
          swal("Successfully", "ลบเสร็จสิ้น !!", "success");
          dataTable.clear();
          dataTable.rows.add(html[0].table).draw();
          var $el = $("#product");
          $el.empty();
          change_selected(document.getElementById('product'));
          function change_selected(selector)
            {
              selector.options[0] = new Option("","");
              for (var i = 0; i < html[0].select.length; i++) {
                selector.options[i+1] = new Option(html[0].select[i].name,html[0].select[i].id);
              }

          }
          $('#product').val();

        }
      });
      // }, 1000);
    } else {
      swal("ยกเลิกการลบ !","","error");
    }
  });
}
$(document).ready(function(){
  $('#example').DataTable({
    "order": [[ 0, "asc" ]]
  });
  $('#product').select2({
    placeholder:'กรุณาเลือกสินค้า'
  });
  $('#product_edit').select2({
    placeholder:'กรุณาเลือกสินค้า'
  });
});
</script>
@endsection

{{-- [{"No .":1,
"Name Category":
"sdfg","Note":"sdfg","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":2,"Name Category":"qwer","Note":"qwer","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":3,"Name Category":"xxxxx","Note":"xxxx","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":4,"Name Category":"123456","Note":"123456","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":5,"Name Category":"dddddddd","Note":"dddddddd","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":6,"Name Category":"5555","Note":"5555","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":7,"Name Category":"000000","Note":"0000000","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":8,"Name Category":"0222222","Note":"22222","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":9,"Name Category":"45646","Note":"346346","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":10,"Name Category":"tyty","Note":"wergwerg","Action":"<a href='#' class='btn btn-... --}}
