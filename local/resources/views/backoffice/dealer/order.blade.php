@extends('backoffice.layouts.main')
@section('page_title','GomBeta | Order')
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ใบสั่งซื้อสินค้า
        <small>ใบสั่งซื้อสินค้า</small>
        <p>
          {{-- {{Html::link('backoffice/add_admin','<i class="fa fa-plus-square" aria-hidden="true"></i>Add Admin',array('class'=>'btn btn-primary'))}} --}}

        </p>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-cog"></i> Backoffice</a></li>
        <li class="active">ใบสั่งซื้อสินค้า</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      @if(Session::has('success'))
        {{-- <div class="alert alert-success">
          <ul>
          <li> {{ Session::get('success') }}</li>
          </ul>
        </div> --}}
        <script>
            swal("{{ Session::get('success') }}", "", "success");
        </script>
    @endif

    {{-- <script>
        swal("Good job!", "You clicked the button!", "success");
    </script> --}}
        <p></p>






      <table id="example" class="table">
      <thead>
        <tr>
          <th class="text-center" >Order id .</th>
          <th class="text-center" >ชื่อสินค้า</th>
          <th class="text-center" >จำนวน</th>
          <th class="text-center" >ส่วนลด</th>
          <th class="text-center" >ราคารวม</th>
          <th class="text-center" >Date</th>
        </tr>
      </thead>
    <tbody id="data">
      @php
        $datas= \App\Models\OrderDetail::leftJoin('product','order_detail.product_id','product.id')
        ->leftJoin('order_data','order_detail.order_id','order_data.id')
        ->leftJoin('users','order_data.user_id','users.id')
        ->select('users.name as username','order_data.*','product.common_name','product.name','product.id as product_id','product.price','order_detail.qty')
        ->where('product.created_by',Auth::user()->id)
        ->orderBy('id','DESC')
        ->get();
        // $datas= \App\Models\OrderData::leftJoin('users','order_data.user_id','users.id')
        // ->select('order_data.*','users.name as name')
        // ->where('order_data.status_confirm','1')
        // ->orderBy('status_confirm','ASC')
        // ->get();
        $i=1;
      @endphp
      @foreach ($datas as $element)
        @php
        $discount=0;

        $promotion=\App\Models\ProductPromotion::where('product_id',$element->product_id)->orderBy('amount','DESC')->get();
        // dd($promotion);
        foreach ($promotion as $pro ) {
          if ($element->qty>=$pro->amount) {
            $discount=$pro->discount;
            break;
          }
        }
        // dd($promotion);
        $discount=($discount/100)*$element->price*$element->qty;
        @endphp
        <tr>
          <td>
            <a href="javascript:void(0);" data-toggle="modal" data-target="#myModalShow{{ $element->id }}" onclick="return show({{$element->id}})">{{ $element->order_id }}</a>
          </td>
          <td>{{ $element->name }}</td>
          <td>{{ $element->qty }}</td>
          <td>{{ $discount }}</td>
          <td>
            {{ $element->qty*$element->price-$discount }}
          </td>
          <td class="text-center">
            {{ $element->created_at }}
            {{-- <a  href="" data-toggle="modal" data-target="#myModalImg" onclick="return img({{$element->id}})"><img src="" width="250px" height="250px" alt=""></a> --}}
          </td>

        </tr>
      @endforeach
    </tbody>
    </table>
    @php

    // $order_details=\App\Models\orderDetail::leftJoin('product','order_detail.product_id','=','product.id')
    // ->leftJoin('product_category_sub','product.category_sub_id','=','product_category_sub.id')
    // ->leftJoin('product_category','product_category_sub.category_id','=','product_category.id')
    // ->select('order_detail.*','product_category.name as name_category','product.*')
    // ->where('order_id',$key->id)->get();
    @endphp
    @foreach ($datas as $key )
      <div class="modal fade modalshow" id="myModalShow{{ $key->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
        <div class="modal-dialog" role="document" style="width:80%;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times; </button> <span aria-hidden="true"> </span>
              <h3 class="text-center">ข้อมูลใบสั่งซื้อ</h3>
            </div>
            <form class="" id="show_order" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="modal-body">
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        เลขที่ใบสั่งซื้อ
                      </div>
                      <div class="col-md-6">
                        <input class="form-control" type="text" name="name" value="{{ $key->order_id }}" id="order_id" >
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        ชื่อผู้ใช้
                      </div>
                      <div class="col-md-6">
                        <input class="form-control" type="text" name="name" value="{{ $key->name }}" id="user_name" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-12 text-center">
                        <h3 class="text-center">สินค้า</h3>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <table class="table table-bordered">
                      <thead class="table-light">
                        <tr class="font-cloud">
                          <th class="position-sticky">#</th>
                          <th class="position-sticky">สินค้า</th>
                          <th class="position-sticky">หมวด</th>
                          <th class="position-sticky">จำนวน</th>
                          <th class="position-sticky">ราคา/ชิ้น</th>
                          <th class="position-sticky">ส่วนลด/ชิ้น</th>
                          <th class="position-sticky">ราคารวม</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php

                          $num=1;
                          $order_details=\App\Models\orderDetail::leftJoin('product','order_detail.product_id','=','product.id')
                          ->leftJoin('product_category_sub','product.category_sub_id','=','product_category_sub.id')
                          ->leftJoin('product_category','product_category_sub.category_id','=','product_category.id')
                          ->select('order_detail.*','product_category.name as name_category','product.*')
                          ->where('order_id',$key->id)->get();
                          // dd($order_details);
                          $total=0;
                        @endphp
                        @foreach ($order_details as $order_detail )
                          @php
                          $discount=0;

                          $promotion=\App\Models\ProductPromotion::where('product_id',$order_detail->product_id)->orderBy('amount','DESC')->get();
                          foreach ($promotion as $pro ) {
                            if ($order_detail->qty>=$pro->amount) {
                              $discount=$pro->discount;
                              break;
                            }
                          }
                          $discountPerPiece=($order_detail->price/$order_detail->piece)-(($order_detail->price/$order_detail->piece)*((100-$discount)/100));

                          @endphp
                          <tr>
                          <th>{{$num++}}</th>
                          <td>{{ $order_detail->name }}<br><small class="text-muted"></small></td>
                          <td>{{ $order_detail->name_category }}</td>
                          <td class="text-right">{{ $order_detail->qty }} ลัง <small>({{ $order_detail->piece }}ชิ้น)</small></td>
                          <td class="text-right">{{number_format($order_detail->price/$order_detail->piece)}}</td>
                          <td class="text-right"><small>({{ $discount }}%)</small> {{number_format($discountPerPiece) }}</td>
                          <td class="text-right">{{ number_format((($order_detail->price/$order_detail->piece)-$discountPerPiece)*$order_detail->qty*$order_detail->piece) }} </td>
                        </tr>
                        @php
                          $total+=(($order_detail->price/$order_detail->piece)-$discountPerPiece)*$order_detail->qty*$order_detail->piece;
                        @endphp
                        @endforeach
                        <tr>
                          <td colspan="6" class="text-right"><b>ราคารวม</b></td>
                          <td class="text-right">{{ number_format($total) }} บาท</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-12 text-center">
                        <h3 class="text-center">ที่อยู่จัดส่ง</h3>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        ชื่อ - นามสกุล
                      </div>
                      <div class="col-md-6">
                        <input class="form-control" type="text" name="name" value="{{ $key->name1 }}" id="user_name" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        ที่อยู่
                      </div>
                      <div class="col-md-6">
                        <input class="form-control" type="text" name="name" value="{{ $key->address1 }}" id="user_name" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        รหัสไปรษณีย์
                      </div>
                      <div class="col-md-6">
                        <input class="form-control" type="text" name="name" value="{{ $key->zipcode1 }}" id="user_name" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        เขต/อำเภอ
                      </div>
                      <div class="col-md-6">
                        <input class="form-control" type="text" name="name" value="{{ $key->district1 }}" id="user_name" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        จังหวัด
                      </div>
                      <div class="col-md-6">
                        <input class="form-control" type="text" name="name" value="{{ $key->province1 }}" id="user_name" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        อีเมล์
                      </div>
                      <div class="col-md-6">
                        <input class="form-control" type="text" name="name" value="{{ $key->email1 }}" id="user_name" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        โทรศัพท์มือถือ
                      </div>
                      <div class="col-md-6">
                        <input class="form-control" type="text" name="name" value="{{ $key->mobile1 }}" id="user_name" required>
                      </div>
                    </div>
                  </div>
                </div>
                @if ($key->tax_invoice>0)
                  <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-12 text-center">
                        <h3 class="text-center">ที่อยู่สำหรับใบเสร็จ/ใบกำกับภาษี</h3>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        ชื่อ-นามสกุล
                      </div>
                      <div class="col-md-6">
                        <input class="form-control" type="text" name="name" value="{{ $key->name2 }}" id="user_name" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        ที่อยู่
                      </div>
                      <div class="col-md-6">
                        <textarea class="form-control" name="name" rows="5" cols="80">{{ $key->address2 }}</textarea>
                        {{-- <input class="form-control" type="text" name="name" value="{{ $key->address2 }}" id="user_name" required> --}}
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        รหัสไปรษณีย์
                      </div>
                      <div class="col-md-6">
                        <input class="form-control" type="text" name="name" value="{{ $key->zipcode2 }}" id="user_name" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        เขต/อำเภอ
                      </div>
                      <div class="col-md-6">
                        <input class="form-control" type="text" name="name" value="{{ $key->district2 }}" id="user_name" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        จังหวัด
                      </div>
                      <div class="col-md-6">
                        <input class="form-control" type="text" name="name" value="{{ $key->province2 }}" id="user_name" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        อีเมล์
                      </div>
                      <div class="col-md-6">
                        <input class="form-control" type="text" name="name" value="{{ $key->email2 }}" id="user_name" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        โทรศัพท์มือถือ
                      </div>
                      <div class="col-md-6">
                        <input class="form-control" type="text" name="name" value="{{ $key->mobile2 }}" id="user_name" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        เลขประจำตัวผู้เสียภาษี
                      </div>
                      <div class="col-md-6">
                        <input class="form-control" type="text" name="name" value="{{ $key->taxid }}" id="user_name" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        เลขที่ สำนักงาน/หน่วยงาน/ร้านค้า
                      </div>
                      <div class="col-md-6">
                        <input class="form-control" type="text" name="name" value="{{ $key->trader }}" id="user_name" required>
                      </div>
                    </div>
                  </div>
                </div>
                @endif
                <div class="form-group">
                <div class="row">
                  <div style="">
                    <div class="col-md-12 text-center">
                      <h3 class="text-center">สถานะการจัดส่งสินค้า</h3>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="row">
                <div style="">
                  <div class="col-md-4 text-center">
                  </div>
                  <div class="col-md-4 text-center">
                    <select class="form-control" name="" id="select{{ $key->id }}" onchange="return changeStatus(this.value,{{ $key->id }})">
                      @if ($key->status_order==0)
                        <option selected value="0">ยังไม่ส่ง</option>
                        <option value="1">กำลังส่ง</option>
                        <option value="2">รับแล้ว</option>
                      @elseif ($key->status_order==1)
                        <option  value="0">ยังไม่ส่ง</option>
                        <option selected value="1">กำลังส่ง</option>
                        <option value="2">รับแล้ว</option>
                      @else
                        <option  value="0">ยังไม่ส่ง</option>
                        <option  value="1">กำลังส่ง</option>
                        <option selected value="2">รับแล้ว</option>
                      @endif

                    </select>
                  </div>
                </div>
              </div>
            </div>
                </div>
              <div class="modal-footer" style="">
                {{-- <button type="button" href="javascript:void(0);" class="btn btn-success" style="margin-top:30px" onclick="return confirm({{ $key->id }})" >ยืนยันการชำระเงิน</button> --}}
                <button class="btn btn-danger" type="reset" style="margin-top:30px" data-dismiss="modal">Close</button>
              </div>
            </form>

          </div>
        </div>
      </div>
    @endforeach


    </section>

  </div>
  <script type="text/javascript">
  function changeStatus(value,modalId) {
    var dataTable = $("#example").DataTable();
    swal({
      title: "แน่ใจหรือ ?",
      text: "ยืนยันการเปลี่ยนสถานะการจัดส่งสินค้า",
      icon: "warning",
      buttons: true,
    })
    .then((sweetalert) => {
      if (sweetalert) {
    $.ajax({
      url: '{{url('changeStatus')}}'+'/'+value+'/'+modalId,
      type: 'get',
      dataType:'json',
      success: function (html) {
        // alert(JSON.stringify(html));
        // if(html != 'Failed'){
        //   // alert(html);
          swal("Successfully", "เปลี่ยนสถานะการจัดส่งสินค้าเสร็จสิ้น !!", "success");
          $('#myModalShow'+modalId).modal('toggle');
          $('#select'+modalId).val(html.status_order);
        //   $("#insert_product")[0].reset();

        // }
      },
      cache: false,
      contentType: false,
      processData: false
    });
  } else {
    swal("ยกเลิกการยืนยัน !","","error");
  }
  });
  return false;
  }

  function sweetalert(id) {
    var dataTable = $("#example").DataTable();
    swal({
      title: "แน่ใจหรือ ?",
      text: "คุณต้องการลบข้อมูลนี้ใช่หรือไม่",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((sweetalert) => {
      if (sweetalert) {
        // setTimeout(function () {
        $.ajax({
          type: 'get',
          url: '{{url('delconformOrder')}}'+'/'+id,
          data: {_token: "{{ csrf_token() }}"},
          dataType:'json',
          success:function(html){
            // alert(JSON.stringify(html));
            swal("Successfully", "ลบเสร็จสิ้น !!", "success");
            dataTable.clear();
            dataTable.rows.add(html[0].data).draw();
            $("#order_num").text(html[0].orderNotCheck);
            $("#order_check_num").text(html[0].orderCheck);
          }
        });
        // }, 1000);
      } else {
        swal("ยกเลิกการลบ !","","error");
      }
    });
  }
  $(".modalshow input").prop("readonly", true);
  $(".modalshow textarea").prop("readonly", true);
  $(document).ready(function(){
    $('#example').DataTable({
      "order": [[ 2, "asc" ]]
      // "scrollX": true
    });
  });
</script>
@endsection
