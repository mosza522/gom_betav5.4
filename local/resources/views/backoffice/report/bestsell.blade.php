@extends('backoffice.layouts.main')
@section('page_title','GomBeta | รายงานสินค้าขายดี')
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        รายงานสินค้าขายดี
        <p>
          {{-- {{Html::link('backoffice/add_admin','<i class="fa fa-plus-square" aria-hidden="true"></i>Add Admin',array('class'=>'btn btn-primary'))}} --}}

        </p>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-cog"></i> Backoffice</a></li>
        <li class="active">รายงานสินค้าขายดี</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      @if(Session::has('success'))
        {{-- <div class="alert alert-success">
          <ul>
          <li> {{ Session::get('success') }}</li>
          </ul>
        </div> --}}
        <script>
            swal("{{ Session::get('success') }}", "", "success");
        </script>
    @endif

    {{-- <script>
        swal("Good job!", "You clicked the button!", "success");
    </script> --}}
    @php
    $monthThaiArray = array(
    "มกราคม",
    "กุมภาพันธ์",
    "มีนาคม",
    "เมษายน",
    "พฤษภาคม",
    "มิถุนายน",
    "กรกฎาคม",
    "สิงหาคม",
    "กันยายน",
    "ตุลาคม",
    "พฤษจิกายน",
    "ธันวาคม",
    );
    // set start and end year range
    $yearThaiArray = range(2010, date("Y"));
    @endphp
    <div class="col-md-12">
      <div class="col-md-8"></div>
      <div class="col-md-2">
        <select class="form-control" name="" id="month">
          <option value="All Month">All Month</option>
          @for ($i=0; $i < count($monthThaiArray); $i++)
            <option value="{{ $i+1 }}">{{ $monthThaiArray[$i] }}</option>
          @endfor
        </select>
      </div>
      <div class="col-md-2">
        <select class="form-control" name="" id="year">
          <option value="All Year" >All Years</option>
          @foreach ($yearThaiArray as $year)
            <option value="{{ $year }}">{{ $year+543 }}</option>
          @endforeach
        </select>
      </div>
    </div>
      <table id="example" class="table table-hover">
      <thead>
        <tr>
          <th class="text-center" ># .</th>
          <th class="text-center" >จำนวนที่ขาย (ลัง)</th>
          <th class="text-center" >กำไรทั้งหมด (บาท)</th>
          <th class="text-center" >ราคาขาย (บาท)</th>
          <th class="text-center" >ต้นทุน (บาท)</th>
          <th class="text-center" >ชื่อสินค้า</th>
          <th class="text-center" >ประเภทสินค้า</th>
          <th class="text-center" >แบรนด์</th>
          <th class="text-center" >ผู้ขาย</th>

        </tr>
      </thead>
    <tbody id="data" >
      @php
      $datas= new \App\Models\OrderDetail;
        // $datas= \App\Models\OrderDetail::leftJoin('product','order_detail.product_id','=','product.id')
        // ->leftJoin('product_category_sub','product.category_sub_id','=','product_category_sub.id')
        // ->leftJoin('product_category','product_category_sub.category_id','=','product_category.id')
        // ->leftJoin('product_brand','product.band_id','=','product_brand.id')
        // ->leftJoin('order_data','order_detail.order_id','=','order_data.id')
        // // ->whereMonth('order_data.created_at',6)
        // ->groupBy('product_id')
        // ->select('product_category.name as category','product_category_sub.name as subCategory','product_brand.name as brand')
        // ->selectRaw('sum(qty) as sum')
        // ->orderBy('sum','DESC')
        // ->get();
        $i=1;
      @endphp
      @foreach ($datas->bestSell() as $element)
        <tr>
          <td>{{ $i++ }}.</td>
          <td>{{ $element->sum }} </td>
          <td>{{ $element->profit }} </td>
          <td>{{ $element->price }} </td>
          <td>{{ $element->cost }} </td>
          <td>{{ $element->name }}</td>
          <td>{{ $element->category }}</td>
          <td>{{ $element->brand }}</td>
          <td>{{ $element->username }}</td>
        </tr>
      @endforeach
    </tbody>
    </table>

    </section>

  </div>
<script type="text/javascript">
$(document).ready(function(){
  $('#example').DataTable({
    "order": [[ 0, "asc" ]],
    dom: 'Bfrtip',
        buttons: [
             'excel', 'print','copy','csv'
        ]
  });
  $('#product').select2({
    placeholder:'กรุณาเลือกสินค้า'
  });
  $('#product_edit').select2({
    placeholder:'กรุณาเลือกสินค้า'
  });
});
$('#month').change(function(){
  var dataTable = $("#example").DataTable();
  $.ajax({
    url: '{{url('changeMYBestSell')}}/'+$('#month').val()+'/'+$('#year').val(),
    type: 'get',
    dataType:'JSON',
    success: function (html) {
      // alert(JSON.stringify(html));
      // swal("Successfully", "แก้ไข Recommend สำเร็จ !!", "success");
      dataTable.clear();
      dataTable.rows.add(html).draw();
    },
    cache: false,
    contentType: false,
    processData: false
  });
  return false;
});
$('#year').change(function(){
  var dataTable = $("#example").DataTable();
  $.ajax({
    url: '{{url('changeMYBestSell')}}/'+$('#month').val()+'/'+$('#year').val(),
    type: 'get',
    dataType:'JSON',
    success: function (html) {
      // alert(JSON.stringify(html));
      // swal("Successfully", "แก้ไข Recommend สำเร็จ !!", "success");
      dataTable.clear();
      dataTable.rows.add(html).draw();
    },
    cache: false,
    contentType: false,
    processData: false
  });
  return false;
});
</script>
@endsection

{{-- [{"No .":1,
"Name Category":
"sdfg","Note":"sdfg","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":2,"Name Category":"qwer","Note":"qwer","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":3,"Name Category":"xxxxx","Note":"xxxx","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":4,"Name Category":"123456","Note":"123456","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":5,"Name Category":"dddddddd","Note":"dddddddd","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":6,"Name Category":"5555","Note":"5555","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":7,"Name Category":"000000","Note":"0000000","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":8,"Name Category":"0222222","Note":"22222","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":9,"Name Category":"45646","Note":"346346","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":10,"Name Category":"tyty","Note":"wergwerg","Action":"<a href='#' class='btn btn-... --}}
