@extends('backoffice.layouts.main')
@section('page_title','GomBeta | สินค้า')
@section('head')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@endsection
@section('content')

  <meta name="csrf-token" content="{{ csrf_token() }}">
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        สินค้า
        <small>สินค้า</small>
        <p>
          {{-- {{Html::link('backoffice/add_admin','<i class="fa fa-plus-square" aria-hidden="true"></i>Add Admin',array('class'=>'btn btn-primary'))}} --}}

        </p>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-cog"></i> Backoffice</a></li>
        <li class="active">สินค้า</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      @if(Session::has('success'))
        {{-- <div class="alert alert-success">
        <ul>
        <li> {{ Session::get('success') }}</li>
      </ul>
    </div> --}}
    <script>
    swal("{{ Session::get('success') }}", "", "success");
    </script>
  @endif




<table id="example" class="table">
  <thead>
    <tr>
      <th class="text-center" style="">No .</th>
      <th class="text-center" style="">Order id</th>
      <th class="text-center" style="">ชื่อ</th>
      <th class="text-center" style="">รัศมีการกั้นเขต</th>
      <th class="text-center" style="text-align:center;">ระยะเวลาที่กั้นเขต</th>
      <th class="text-center" style="">วันที่สิ้นสุดการกั้นเขต</th>
    </tr>
  </thead>
  <tbody id="data">
    @php
    $product_data=\App\Models\OrderDetail::leftJoin('order_data','order_detail.order_id','=','order_data.id')
    ->leftJoin('product','order_detail.product_id','=','product.id')
    ->leftJoin('users','order_data.user_id','=','users.id')
    ->select('order_data.order_id','product.name','order_data.created_at','order_detail.product_id','order_detail.qty','users.lat','users.longtitude as lng','users.id as userId')
    ->where('order_data.status_confirm','1')
    ->where('order_data.user_id',Auth::user()->id)
    ->get();
    $i=1;
    // dd($product_data);
    @endphp
    @foreach ($product_data as $element)
    @php
    $pro=\App\Models\Zone::where('product_id',$element->product_id)
    ->where('num','<=',$element->qty)
    ->orderBy('num','DESC')
    ->first();
    $dt=Carbon::createFromFormat('Y-m-d H:i:s', $element->created_at);
    @endphp
      <tr>
        <td>{{ $i++ }}.</td>
        <td>{{ $element->order_id }}</td>
        <td>{{ $element->name }}</td>
        <td>{{ $pro->radius }} กม.</td>
        <td>{{ $pro->time }} วัน</td>
        <td>{{ $dt->addDays($pro->time)->formatLocalized('%A %d %B %Y') }} {{ $dt->addDays($pro->time)->format('H:i:s') }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>

</section>
<!-- /.content -->
<script type="text/javascript">

$(document).ready(function(){
  $('#example').DataTable({
      "order": [[ 0, "asc" ]]
  });
});
</script>

</div>
@endsection
