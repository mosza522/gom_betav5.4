@extends('backoffice.layouts.main')
@section('page_title','GomBeta | Contact')
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ติดต่อเรา
        <small>ติดต่อเรา</small>
        <p>
          {{-- {{Html::link('backoffice/add_admin','<i class="fa fa-plus-square" aria-hidden="true"></i>Add Admin',array('class'=>'btn btn-primary'))}} --}}

        </p>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-cog"></i> Backoffice</a></li>
        <li class="active">ติดต่อเรา</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      @if(Session::has('success'))
        {{-- <div class="alert alert-success">
          <ul>
          <li> {{ Session::get('success') }}</li>
          </ul>
        </div> --}}
        <script>
            swal("{{ Session::get('success') }}", "", "success");
        </script>
    @endif

    {{-- <script>
        swal("Good job!", "You clicked the button!", "success");
    </script> --}}
        <p></p>
        @php
          $contactnum= \App\Models\Contact::count();
          echo "<script>var num =".$contactnum.";
          console.log(num);
           </script>";
          if($contactnum>0){
            $contact= \App\Models\Contact::first();
            echo "<script>
            var latt ='".$contact->lat."';
            var lonn ='".$contact->lng."';
            console.log(latt+' '+lonn);
             </script>";
          }
          // dd($contact);
        @endphp
        @if ($contactnum>0)
          <form class="form_contact" id="form_contact" >
            {{ csrf_field() }}
          <div class='form-group' >
            <div class="row">
              <div class="col-md-3  text-center">
                <h4>line</h4>
              </div>
              <div class="col-md-3  text-center">
                <input type="text" class="form-control" name="line" id="line" value="{{ $contact->line }}">
              </div>
              <div class="col-md-3  text-center">
                <h4>facebook</h4>
              </div>
              <div class="col-md-3  text-center">
                <input type="text" class="form-control" name="facebook" id="facebook" value="{{ $contact->facebook }}">
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-3  text-center">
                <h4>youtube</h4>
              </div>
              <div class="col-md-3  text-center">
                <input type="text" class="form-control" name="youtube" id="youtube" value="{{ $contact->youtube }}">
              </div>
              <div class="col-md-3  text-center">
                <h4>email</h4>
              </div>
              <div class="col-md-3  text-center">
                <input type="text" class="form-control" name="email" id="email" value="{{ $contact->email }}">
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-3  text-center">
                <h4>address</h4>
              </div>
              <div class="col-md-3  text-center">
                <textarea class="form-control" name="address" id="address" rows="3" >{{ $contact->address }}</textarea>
                {{-- <input type="text" class="form-control" name="" value=""> --}}
              </div>
              <div class="col-md-3  text-center">
                <h4>phone</h4>
              </div>
              <div class="col-md-3  text-center">
                <input type="text" class="form-control" name="phone" id="phone" value="{{ $contact->phone }}">
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-3  text-center">
                <h4>fax</h4>
              </div>
              <div class="col-md-3  text-center">
                <input type="text" class="form-control" name="fax" id="fax" value="{{ $contact->fax }}">
              </div>
              <div class="col-md-3  text-center">
                <h4>mobile</h4>
              </div>
              <div class="col-md-3  text-center">
                <input type="text" class="form-control" name="mobile" id="mobile" value="{{ $contact->mobile }}">
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-3  text-center">
                <h4>latitude</h4>
              </div>
              <div class="col-md-3  text-center">
                <input type="text" class="form-control" name="lat" value="{{ $contact->lat }}" id="lat" readonly>
              </div>
              <div class="col-md-3  text-center">
                <h4>longtitude</h4>
              </div>
              <div class="col-md-3  text-center">
                <input type="text" class="form-control" name="lng" value="{{ $contact->lng }}" id="lng" readonly>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12  text-center" id="">
              <button type="submit" class="btn btn-success" name="button">บันทึก</button>
              {{-- @if ($contact!=null) --}}
                <button type="button" class="btn btn-danger" id="clear" name="button">Clear</button>
              {{-- @endif --}}
              </div>
            </div>
          </div>
        </form>
      @else
        <form class="form_contact" id="form_contact" >
          {{ csrf_field() }}
        <div class='form-group' >
          <div class="row">
            <div class="col-md-3  text-center">
              <h4>line</h4>
            </div>
            <div class="col-md-3  text-center">
              <input type="text" class="form-control" name="line" id="line" value="">
            </div>
            <div class="col-md-3  text-center">
              <h4>facebook</h4>
            </div>
            <div class="col-md-3  text-center">
              <input type="text" class="form-control" name="facebook" id="facebook" value="">
            </div>
          </div>
        </div>
        <div class='form-group' >
          <div class="row">
            <div class="col-md-3  text-center">
              <h4>youtube</h4>
            </div>
            <div class="col-md-3  text-center">
              <input type="text" class="form-control" name="youtube" id="youtube" value="">
            </div>
            <div class="col-md-3  text-center">
              <h4>email</h4>
            </div>
            <div class="col-md-3  text-center">
              <input type="text" class="form-control" name="email" id="email" value="">
            </div>
          </div>
        </div>
        <div class='form-group' >
          <div class="row">
            <div class="col-md-3  text-center">
              <h4>address</h4>
            </div>
            <div class="col-md-3  text-center">
              <textarea class="form-control" name="address" id="address" rows="3" ></textarea>
              {{-- <input type="text" class="form-control" name="" value=""> --}}
            </div>
            <div class="col-md-3  text-center">
              <h4>phone</h4>
            </div>
            <div class="col-md-3  text-center">
              <input type="text" class="form-control" name="phone" id="phone" value="">
            </div>
          </div>
        </div>
        <div class='form-group' >
          <div class="row">
            <div class="col-md-3  text-center">
              <h4>fax</h4>
            </div>
            <div class="col-md-3  text-center">
              <input type="text" class="form-control" name="fax" id="fax" value="">
            </div>
            <div class="col-md-3  text-center">
              <h4>mobile</h4>
            </div>
            <div class="col-md-3  text-center">
              <input type="text" class="form-control" name="mobile" id="mobile" value="">
            </div>
          </div>
        </div>
        <div class='form-group' >
          <div class="row">
            <div class="col-md-3  text-center">
              <h4>latitude</h4>
            </div>
            <div class="col-md-3  text-center">
              <input type="text" class="form-control" name="lat" value="" id="lat" readonly>
            </div>
            <div class="col-md-3  text-center">
              <h4>longtitude</h4>
            </div>
            <div class="col-md-3  text-center">
              <input type="text" class="form-control" name="lng" value="" id="lng" readonly>
            </div>
          </div>
        </div>
        <div class='form-group' >
          <div class="row">
            <div class="col-md-12  text-center">
            <button type="submit" class="btn btn-success" name="button">บันทึก</button>
            {{-- @if ($contact!=null) --}}
              <button type="button" class="btn btn-danger" id="clear" name="button" disabled>Clear</button>
            {{-- @endif --}}
            </div>
          </div>
        </div>
      </form>
        @endif

        <div class='form-group'>
          <div class="row">
            <div class="alert alert-light border-dashed mt-4 p-0 p-sm-3">
              <div class="row">
                <div class="col-md-12 mb-2">
                  <h5 class="font-cloud p-3 p-sm-0"><i class="fa fa-map-marker mb-2 mr-2"></i>ตำแหน่งบนแผนที่ : <small>กรุณาระบุตำแหน่งโดยการเลื่อนหมุดสีแดงบนแผนที่ไปยังตำแหน่งที่ตั้งของคุณ เพื่อสิธิประโยชน์ทางการค้าค่ะ</small></h5>
                  {{-- <input id="pac-input" class="controls" type="text" placeholder="Search Box"> --}}
                  <div id="dvMap" style="height: 70vh; width: 100%;"></div>
                </div>
              </div>
            </div>
        </div>
        </div>


    </section>

  </div>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGqNz-KUAKL435y6zraREiT-o5s4tuiEM&callback=initMap"></script>
  <script type="text/javascript">
  $('#form_contact').submit(function () {
    var formData = new FormData($(this)[0]);
    $.ajax({
      url: '{{url('contact/store')}}',
      type: 'POST',
      data: formData,
      dataType:'html',
      success: function (html) {
        swal("Successfully", "แก้ไขข้อมูลติดต่อเราสำเร็จ !!", "success");
        $('#clear').removeAttr('disabled');
      },
      cache: false,
      contentType: false,
      processData: false
    });
    return false;
  });


  var allow='' ;
  // Referance >>> https://www.aspsnippets.com/Articles/Google-Maps-V3-Draggable-Markers-Example-Drag-and-Drop-Markers-in-Google-Maps-using-JavaScript.aspx
  var markers = [
  {
    "title": 'Your location',
    "lat": '',
    "lng": '',
    "description": 'Your location'
  }
  ];
  function getLocation() {

  navigator.geolocation.watchPosition(function(position) {
    allow='allow';
  console.log("i'm tracking you!");
  console.log(allow);

  },
  function (error) {
  if (error.code == error.PERMISSION_DENIED)
      console.log("you denied me :-(");
      allow='denined';
  });
  setTimeout(function(){
  if(allow==''){
    swal({
      title: "กรุณาอนุญาตการเข้าถึงตำแหน่งของคุณ ",
      text: "เพื่อความสะดวกของตัวท่าน กรุณาอนุญาตการเข้าถึงตำแหน่งของคุณ",
      icon: "warning",
      button: "OK",
    });
  }
}, 3000);

  if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        alert("Geolocation is not supported by this browser.");
    }
    getMaps();

  }

  function showPosition(position) {
  setTimeout(function(){
      if(num==0){
  markers[0].lat=position.coords.latitude;
  markers[0].lng=position.coords.longitude;
  $('#lat').val(position.coords.latitude);
  $('#lng').val(position.coords.longitude);
  getMaps();
}else{
  markers[0].lat=latt;
  markers[0].lng=lonn;
  $('#lat').val(latt);
  $('#lng').val(lonn);
  getMaps();
}
}, 1000);
  }

  window.onload = function () {

    getLocation();



  }
  function getMaps() {
  var mapOptions = {
    center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
    zoom: 12,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var infoWindow = new google.maps.InfoWindow();
  var latlngbounds = new google.maps.LatLngBounds();
  var geocoder = geocoder = new google.maps.Geocoder();
  var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
  for (var i = 0; i < markers.length; i++) {
    var data = markers[i]
    var myLatlng = new google.maps.LatLng(data.lat, data.lng);
    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: data.title,
      draggable: true,
      animation: google.maps.Animation.DROP
    });
    (function (marker, data) {
      google.maps.event.addListener(marker, "click", function (e) {
        infoWindow.setContent(data.description);
        infoWindow.open(map, marker);
      });
      google.maps.event.addListener(marker, "dragend", function (e) {
        var lat, lng, address;
        geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            lat = marker.getPosition().lat();
            lng = marker.getPosition().lng();
            // alert(lat+"\n"+lng);
            address = results[0].formatted_address;
            $('#lat').val(lat);
            $('#lng').val(lng);
            // alert($('#lat').val()+'\n'+$('#lng').val());
            // alert("Latitude: " + lat + "\nLongitude: " + lng + "\nAddress: " + address);

          }
        });
      });
    })(marker, data);
    latlngbounds.extend(marker.position);
  }

  // var bounds = new google.maps.LatLngBounds();   // For Multiple markers
  // map.setCenter(latlngbounds.getCenter());       // For Multiple markers
  // map.fitBounds(latlngbounds);                   // For Multiple markers
  }
  $('#clear').click(function(){
    swal({
      title: "แน่ใจหรือ ?",
      text: "คุณต้องการลบข้อมูลนี้ใช่หรือไม่",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((sweetalert) => {
      if (sweetalert) {
        // setTimeout(function () {
        $.ajax({
            url: '{{url('contact/clear')}}',
            type: 'GET',
            dataType:'html',
            success: function (html) {
              if (html='finished') {
              $(".form_contact")[0].reset();
              $('#clear').prop('disabled',true);
                swal("Successfully", "เคลียร์ข้อมูลการติดต่อเสร็จสิ้น !!", "success");
                location.reload();
                num = 0;
                // Reset Maps
                // getLocation();
              }
            },
            cache: false,
            contentType: false,
            processData: false
          });
        // }, 1000);
      } else {
        swal("ยกเลิกการลบ !","","error");
      }
    });
  });
</script>
@endsection
