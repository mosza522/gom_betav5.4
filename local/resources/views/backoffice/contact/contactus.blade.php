@extends('backoffice.layouts.main')
@section('page_title','GomBeta | ข้อความที่ถูกส่งเข้ามา')
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ข้อความที่ถูกส่งเข้ามา
        <p>
          {{-- {{Html::link('backoffice/add_admin','<i class="fa fa-plus-square" aria-hidden="true"></i>Add Admin',array('class'=>'btn btn-primary'))}} --}}

        </p>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-cog"></i> Backoffice</a></li>
        <li class="active">ข้อความที่ถูกส่งเข้ามา</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      @if(Session::has('success'))
        {{-- <div class="alert alert-success">
          <ul>
          <li> {{ Session::get('success') }}</li>
          </ul>
        </div> --}}
        <script>
            swal("{{ Session::get('success') }}", "", "success");
        </script>
    @endif

    {{-- <script>
        swal("Good job!", "You clicked the button!", "success");
    </script> --}}






      <table id="example" class="table table-hover">
        <thead>
          <tr>
            <th class="text-center" style="">No .</th>
            <th class="text-center" style="">ขื่อผู้ส่ง</th>
            <th class="text-center" style="">อีเมลล์</th>
            <th class="text-center" style="">หัวข้อ</th>
            <th class="text-center" style="">เนื้อหา</th>
            <th class="text-center" style="">Action</th>
          </tr>
        </thead>
    <tbody id="data">
      @php
        $datas= \App\Models\ContactUs::get();
        $i=1;
      @endphp
      @foreach ($datas as $element)
        <tr>
          <td>{{ $i++ }}</td>
          <td>{{ $element->name }} </td>
          <td>{{ $element->mail }}</td>
          <td>{{ $element->title }}</td>
          <td>{{ $element->detail }}</td>
          <td>
            <a href='javascript:void(0);' onclick='return sweetalert({{$element->id}})' class='btn btn-danger'>Delete</a>
          </td>
        </tr>
      @endforeach
    </tbody>
    </table>

    </section>

  </div>
<script type="text/javascript">
function sweetalert(id) {
  var dataTable = $("#example").DataTable();
  swal({
    title: "แน่ใจหรือ ?",
    text: "คุณต้องการลบข้อมูลนี้ใช่หรือไม่",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((sweetalert) => {
    if (sweetalert) {
      // setTimeout(function () {
      $.ajax({
        type: 'get',
        url: '{{url('delContactus')}}'+'/'+id,
        dataType:'json',
        success:function(html){
          // alert(JSON.stringify(html));
          swal("Successfully", "ลบเสร็จสิ้น !!", "success");
          dataTable.clear();
          dataTable.rows.add(html).draw();
        }
      });
      // }, 1000);
    } else {
      swal("ยกเลิกการลบ !","","error");
    }
  });
}
$(document).ready(function(){
  $('#example').DataTable({
    "order": [[ 0, "asc" ]]
  });
  $('#bank').select2({
    placeholder:'กรุณาเลือกธนาคาร',
  });
  $('#bank_edit').select2({
    placeholder:'กรุณาเลือกธนาคาร',
  });
});
</script>
@endsection
