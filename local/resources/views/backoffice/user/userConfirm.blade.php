@extends('backoffice.layouts.main')
@section('page_title','GomBeta | UserConfirm')
@section('content')
  <style media="screen">
    .break {
      margin-bottom: 15px;
    }
  </style>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Confirm
        <small>User</small>
        <p>
          {{-- {{Html::link('backoffice/add_admin','<i class="fa fa-plus-square" aria-hidden="true"></i>Add Admin',array('class'=>'btn btn-primary'))}} --}}

        </p>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-cog"></i> Backoffice</a></li>
        <li class="active">User Confirm</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      @if(Session::has('success'))
        {{-- <div class="alert alert-success">
          <ul>
          <li> {{ Session::get('success') }}</li>
          </ul>
        </div> --}}
        <script>
            swal("{{ Session::get('success') }}", "", "success");
        </script>
    @endif

    {{-- <script>
        swal("Good job!", "You clicked the button!", "success");
    </script> --}}
    {{-- <a  href="" data-toggle="modal" data-target="#myModalInsert"><button type="button" class="btn btn-primary"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มประเภทสินค้า</button></a> --}}
    <p></p>

        <div class="modal fade" id="myModalDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
            <div class="modal-dialog" role="document" style="width:80%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times; </button> <span aria-hidden="true"> </span>
                        <h3 class="text-center">รายละเอียดผู้ใช้</h3>
                      </div>
                      <div class="modal-body">
                        <div class="form-group">
                            <div class="row">

                              <div class="col-md-12">
                                <div class="form-group">
                                  <p class="text-center">ภาพหน้าปก</p>
                                  <div class="tz-gallery">
                                    <div class="row">
                                      <div class="col-md-4">

                                      </div>
                                      <div class="col-sm-12 col-md-4">
                                        <a class="lightbox cov" href="" >
                                          <img class="img_cov" src="" >
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                  {{-- <img style="display: block;margin-left: auto;margin-right: auto;" src="" alt="" width="100%" id="img_cov"> --}}
                                </div>
                                <div class="form-group">
                                  <p class="text-center">ภาพโปรไฟล์</p>
                                  <div class="tz-gallery">
                                    <div class="row">
                                      <div class="col-md-4">

                                      </div>
                                      <div class="col-sm-12 col-md-4">
                                        <a class="lightbox pro" href="">
                                          <img class="img_pro" src=""  >
                                        </a>
                                      </div>
                                    </div>
                    </div>
                        {{-- <img style="display: block;margin-left: auto;margin-right: auto;"  src="" alt="" width="30%" id="img_pro"> --}}
                        </div>

                        <div class="col-md-3">
                          <div class="form-group">
                            <p>ชื่อ</p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="" value="" id='name' >
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <p>ประเภทสมาชิก และ กลุ้มผู้ซื้อ</p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="" value="" id='member_type'>
                          </div>
                        </div>

                        <div class="col-md-3">
                          <div class="form-group">
                            <p>กลุ่มผู้ซื้อ</p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="" value="" id='member_group'>
                          </div>
                        </div>


                        <div class="col-md-3">
                          <div class="form-group">
                            <p>ชื่อบริษัท/หน่วยงาน/ร้านค้า</p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="" value="" id='company_name'>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <p>เลขที่ สำนักงาน/หน่วยงาน/ร้านค้า</p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="" value="" id='company_code'>
                          </div>
                        </div>


                        <div class="col-md-3">
                          <div class="form-group">
                            <p>เลขประจำตัวผู้เสียภาษี</p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="" value="" id='tax_id'>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <p>ที่อยู่</p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="" value="" id='address'>
                          </div>
                        </div>

                        <div class="col-md-3">
                          <div class="form-group">
                            <p>รหัสไปรษณีย์</p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="" value="" id="zipcode">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <p>เขต/อำเภอ</p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="" value="" id="district">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <p>จังหวัด</p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="" value="" id="province">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <p>ที่อยู่</p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="" value="" id="email">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <p>โทรศัพท์มือถือ</p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="" value="" id="mobile">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <p>โทรศัพท์</p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="" value="" id="phone">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <p>แฟกซ์</p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="" value="" id="fax">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <p>ชื่อในระบบ</p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="" value="" id="username">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <p>Latitude</p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="" value="" id="lat">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <p>Longtitude</p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="" value="" id="longtitude">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <p>รัศมี (กิโลเมตร)</p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="" value="" id="area_radius">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <p>ระยะเวลา (จำนวนวัน)</p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="" value="" id="area_time">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12" id="for_certificate">


                      </div>

                    </div>
                  </div>
                </div>

                    <div class="modal-footer" style="" >
                      {{-- <a href="javascript:void(0);" class="btn btn-success" style="margin-top:30px" onclick="return update()" >บันทึก</a> --}}
                      <a href="" class="btn btn-danger" style="margin-top:30px" data-dismiss="modal">Close</a>
                    </div>
                </div>
                </div>
            </div>


      <table id="example" class="table">
      <thead>
        <tr>
<th class="text-center" style="width: 5%;">No .</th>
          <th class="text-center" style="width: 50%;">Name</th>
          <th class="text-center" style="width: 30%;">รายละเอียด</th>
<th class="text-center" style="width: 20%;">Action</th>
  </tr>
      </thead>
    <tbody id="data">
      @php
      $datas= \App\Models\user::where('status_user','0')->get();
        $i=1;
      @endphp
      @foreach ($datas as $element)
        <tr>
          <td>{{ $i++ }}</td>
          <td>{{ $element->name }}</td>
          <td class="text-center">
            <a href="javascript:void(0);" data-toggle="modal" data-target="#myModalDetail" onclick="return detail({{$element->id}})" class="btn btn-info">รายละเอียด</a>
          </td>
          <td>
            <a href="javascript:void(0);" onclick="return confirm({{$element->id}})" class="btn btn-success">Confirm</a>
            <a href="javascript:void(0);" onclick="return sweetalert({{$element->id}})" class="btn btn-danger">Delete</a></td>
        </tr>
      @endforeach
    </tbody>
    </table>

    </section>
    <!-- /.content -->
    <script type="text/javascript">
    $(document).ready(function(){
      $('#example').DataTable({
          "order": [[ 0, "asc" ]]
      });
      $('#myModalDetail input[type=text]').attr("readonly",true);
    });
    $('#myModalDetail').on('hidden.bs.modal', function () {
      $('#img_cov').attr("src",'');
      $('#img_pro').attr("src",'');
    })

    function detail(id) {
      $.ajax({
        type: 'GET',
        url: '{{url('user/detail')}}'+'/'+id,
        data: '',
        dataType:'json',
        success:function(html){
          // alert(JSON.stringify(html));
          var user=html.user;
          var cer=html.certificate;
          $('#img_cov').attr("src",'{{url('local/storage/app/images/user/')}}'+'/'+user.img_cover);
          $('#img_pro').attr("src",'{{url('local/storage/app/images/user/')}}'+'/'+user.img_profile);
          $('.cov').attr("href",'{{url('local/storage/app/images/user/')}}'+'/'+user.img_cover);
          $('.img_cov').attr("src",'{{url('local/storage/app/images/user/')}}'+'/'+user.img_cover);
          $('.pro').attr("href",'{{url('local/storage/app/images/user/')}}'+'/'+user.img_profile);
          $('.img_pro').attr("src",'{{url('local/storage/app/images/user/')}}'+'/'+user.img_profile);
          $('#name').val(user.name);
          $('#member_type').val(user.member_type);
          $('#member_group').val(user.member_group);
          $('#company_name').val(user.company_name);
          $('#company_code').val(user.company_code);
          $('#tax_id').val(user.tax_id);
          $('#address').val(user.address);
          $('#zipcode').val(user.zipcode);
          $('#district').val(user.district);
          $('#province').val(user.province);
          $('#email').val(user.email);
          $('#mobile').val(user.mobile);
          $('#phone').val(user.phone);
          $('#fax').val(user.fax);
          $('#username').val(user.username);
          $('#lat').val(user.lat);
          $('#longtitude').val(user.longtitude);
          $('#area_radius').val(user.area_radius);
          $('#area_time').val(user.area_time);
          baguetteBox.run('.tz-gallery');
          if(user.member_type=="Customer"){
            text='<h3 class="text-center">Certificate</h3>';
              text+="<div class='row'><div class='col-md-6 text-center'><h4>ภาพ Certificate</h4></div><div class='col-md-6 text-center'><h4>หมายเลข Certificate</h4></div></div><br />";
            for (var i = 0; i < cer.length; i++) {
              text+="<div class='row'><div class='col-md-6'><a href='"+window.location.protocol + "//" + window.location.host + "/5.4/gom_betaV5.4/local/storage/app/images/certificate/"+html.certificate[i].certificate+
              "' target='_blank'><img style='width:200px;height:300px;display: block;margin-left: auto;margin-right: auto;' src='"+window.location.protocol + "//" + window.location.host + "/5.4/gom_betaV5.4/local/storage/app/images/certificate/"+html.certificate[i].certificate+"'></a></div>";
              text+="<div class='col-md-6' >"+html.certificate[i].cer_num+"</div></div>";
            }
            $('#for_certificate').html(text);
          }
        }
    });
    }
    function confirm(id) {
      var dataTable = $("#example").DataTable();
      $.ajax({
        type: 'get',
        url: '{{url('user/confirm')}}'+'/'+id,
        data: '',
        dataType:'json',
        success:function(html){
          if(html != 'Failed'){
            swal("Successfully", "ยืนยันเสร็จสิ้น !!", "success");
            dataTable.clear();
            dataTable.rows.add(html).draw();
          }
        }

      });
    }

    function show(id) {
      $.ajax({
        type: 'GET',
        url: '{{url('product_category')}}'+'/'+id+'/show',
        data: '',
        dataType:'json',
        success:function(html){
          // alert(html.id);
          $('#id').val(html.id);
          $('#name_category').val(html.name);
          $('#note').val(html.note);
        }
    });
    }

    function update() {
      var dataTable = $("#example").DataTable();
      if($("#name_category").val()==""){
        swal("ผิดพลาด", "กรุณากรอกชื่อประเภทสินค้า !!", "error");
        return false;
      }
      $.ajax({
        type: 'POST',
        url: '{{url('product_category/update')}}',
        data: $("#update_category").serialize(),
        dataType:'JSON',
        success:function(html){
          if(html != 'Failed'){
            swal("Successfully", "แก้ไขเสร็จสิ้น !!", "success");
            dataTable.clear();
            dataTable.rows.add(html).draw();
            $("#update_category")[0].reset();
            $('#myModalupdate').modal('toggle');
          }
        }
    });
    }
    function sweetalert(id) {
      var dataTable = $("#example").DataTable();
      swal({
    title: "แน่ใจหรือ ?",
    text: "คุณต้องการลบข้อมูลนี้ใช่หรือไม่",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((sweetalert) => {
    if (sweetalert) {
      // setTimeout(function () {
      $.ajax({
        type: 'GET',
        url: '{{url('user/del')}}'+'/'+id,
        data: '',
        dataType:'json',
        success:function(html){
          // alert(html);
          if(html != 'Failed'){
            swal("Successfully", "ลบเสร็จสิ้น !!", "success");
            dataTable.clear();
            dataTable.rows.add(html).draw();
            // $("#insert_category")[0].reset();
          }


        }
    });
  // }, 1000);
    } else {
      swal("ยกเลิกการลบ !","","error");
    }
  });
    }
    </script>
  </div>
@endsection

{{-- [{"No .":1,
"Name Category":
"sdfg","Note":"sdfg","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":2,"Name Category":"qwer","Note":"qwer","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":3,"Name Category":"xxxxx","Note":"xxxx","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":4,"Name Category":"123456","Note":"123456","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":5,"Name Category":"dddddddd","Note":"dddddddd","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":6,"Name Category":"5555","Note":"5555","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":7,"Name Category":"000000","Note":"0000000","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":8,"Name Category":"0222222","Note":"22222","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":9,"Name Category":"45646","Note":"346346","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":10,"Name Category":"tyty","Note":"wergwerg","Action":"<a href='#' class='btn btn-... --}}
