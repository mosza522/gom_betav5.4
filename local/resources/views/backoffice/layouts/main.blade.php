<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="{{asset('assets/images/icons/favicon.png')}}">
  <title>@yield('page_title')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  {{Html::style('assets/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css')}}
  <!-- Font Awesome -->
  {{Html::style('assets/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css')}}
  <!-- Ionicons -->
  {{Html::style('assets/AdminLTE/bower_components/Ionicons/css/ionicons.min.css')}}
  <!-- Theme style -->
  {{Html::style('assets/AdminLTE/dist/css/AdminLTE.min.css')}}
  {{Html::style('assets/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
  <link rel="stylesheet" href="{{ asset('assets/grid/gallery-grid.css') }}">
  {{ Html::style('assets/css/thbanklogos.css') }}
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  {{ Html::script('https://code.jquery.com/jquery-1.12.4.js')}}
  {{-- <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script> --}}
  {{ Html::script('https://code.jquery.com/ui/1.12.1/jquery-ui.js')}}
  {{ Html::style('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css')}}
  {{ Html::script('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js')}}
  {{Html::style('assets/AdminLTE/dist/css/skins/_all-skins.min.css')}}
  {{-- <script src="ckeditor/ckeditor.js"></script> --}}
  {{-- {{Html::script('assets/ckeditor/ckeditor.js')}} --}}
  {{-- <script src="{{ asset('assets/ckeditor/ckeditor.js') }}"></script> --}}
  {{Html::script('https://unpkg.com/sweetalert/dist/sweetalert.min.js')}}
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
{{-- Froala  --}}
@yield('head')


{{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.17/css/jquery.dataTables.min.css"> --}}
{{-- <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"> --}}
<link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/froala_editor.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/froala_style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/code_view.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/colors.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/emoticons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/image.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/line_breaker.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/table.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/char_counter.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/video.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/fullscreen.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/file.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/quick_insert.css') }}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css"><link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" /><link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/css/froala_style.min.css" rel="stylesheet" type="text/css" />
  <!-- Google Font -->
  {{Html::style('https://fonts.googleapis.com/css?family=Kanit')}}
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<style media="screen">
body {
  text-decoration: none;
}
</style>
<body class="hold-transition skin-green sidebar-mini">
@php
// if (!(Auth::user()->member_type == 'Admin' or Auth::user()->member_type == 'Dealers'))
//   {
//     Session::flash('wrong', 'ไม่มีสิทธิ์เข้าถึง');
//   echo "<script>
//      window.location = '".url('/')."';
//   </script>";
//
//   }

  $storagePath  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
  $storagePath=$storagePath.'images\\';
@endphp
@if (session('wrong'))
  <script>
      swal("Something Wrong!!", "{{ Session::get('wrong') }}", "error");
  </script>
@endif
  <div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

      <!-- Logo -->
      <a href="{{ url('/backoffice') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>G</b>B</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b> GomBeta </b></span>
      </a>

      <!-- Header Navbar -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->

            <!-- /.messages-menu -->

            <!-- Notifications Menu -->

            <!-- Tasks Menu -->

            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                {{-- <img src="{{ $storagePath }}" alt=""> --}}

                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                {{-- @php
                $id=Session::get('user');
                  $user=DB::table('tb_admin')->where('id', '=', $id)->first();
                @endphp --}}
                <span class="hidden-xs">
                  {{-- {{$user->admin_fullname}} --}}
                  @if (Auth::user()->member_type=='Admin')
                    {{Html::image('assets\images\icons\favicon.png','',array('class'=>'user-image'))}}
                  @else
                    {{Html::image('local\storage\app\images\user\\'.Auth::user()->img_profile,'',array('class'=>'user-image'))}}
                  @endif
                  {{ Auth::user()->name }}
                </span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  {{-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> --}}
                  {{-- {{Html::image('assets/images/icons/favicon.png','',array('class'=>'img-circle'))}} --}}
                  @if (Auth::user()->member_type=='Admin')
                    {{Html::image('assets\images\icons\favicon.png','',array('class'=>'img-circle'))}}
                  @else
                    {{Html::image('local\storage\app\images\user\\'.Auth::user()->img_profile,'',array('class'=>'img-circle'))}}
                  @endif

                  <p>
                    {{ Auth::user()->name }} - {{ Auth::user()->member_type }}
                    {{-- {{$user->admin_fullname}} --}}
                    {{-- <small>Member since Nov. 2012</small> --}}
                  </p>
                </li>
                <!-- Menu Body -->
                <li class="user-body">
                  <div class="row">

                  </div>
                  <!-- /.row -->
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="text-center">
                    <a href="{{ url('index') }}">กลับสู่หน้าบ้าน</a><br>
                    {{HTML::link('logout', 'Sign out', array('class'=>'btn btn-danger btn-flat'))}}
                    {{-- <a href="logout" class="btn btn-danger btn-flat">Sign out</a> --}}
                  </div>
                </li>
              </ul>
            </li>
            <!-- Control Sidebar Toggle Button -->

          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
          <div class="pull-left image">
            @if (Auth::user()->member_type=='Admin')
              {{Html::image('assets\images\icons\favicon.png','',array('class'=>'img-circle'))}}
            @else
              {{Html::image('local\storage\app\images\user\\'.Auth::user()->img_profile,'',array('class'=>'img-circle'))}}
            @endif
          </div>
          <div class="pull-left info">
            <p>{{ Auth::user()->name }}</p>
            <!-- Status -->
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>

        <!-- search form (Optional) -->
        {{-- <form action="#" method="get" class="sidebar-form">
          <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
          </div>
        </form> --}}
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">HEADER</li>
          <!-- Optionally, you can add icons to the links -->
          @php
          $user='';
          $index='';
          $userConfirm='';
          $category='';
          $brands='';
          $product='';
          $highlight='';
          $promotionOfMonth='';
          $saleOfMonth='';
          $recommend='';
          $subcategory='';
          $order="";
          $confirmOrder='';
          $contact="";
          $controlZone="";
          $payment="";
          $helpcenter="";
          $reportbestsell="";
          $reportsell="";
          $contactus="";
          $orderDealers="";
            switch (Session::get('page')) {
              case 'index':
              $index='active';
              break;
              case 'category':
              $category='active';
              break;
              case 'brand':
              $brand='active';
              break;
              case 'product':
              $product='active';
              break;
              case 'brands':
              $brands='active';
              break;
              case 'user':
              $user='active';
              break;
              case 'userConfirm':
              $userConfirm='active';
              break;
              case 'highlight':
              $highlight='active';
              break;
              case 'promotionofmonth':
              $promotionOfMonth='active';
              break;
              case 'saleofmonth':
              $saleOfMonth='active';
              break;
              case 'recommend':
              $recommend='active';
              break;
              case 'subcategory':
              $subcategory='active';
              break;
              case 'confirmorder':
              $confirmOrder='active';
              break;
              case 'order':
              $order='active';
              break;
              case 'contact':
              $contact='active';
              break;
              case 'controlzone':
              $controlZone='active';
              break;
              case 'payment':
              $payment='active';
              break;
              case 'helpcenter':
              $helpcenter='active';
              break;
              case 'reportbestsell':
              $reportbestsell='active';
              break;
              case 'reportsell':
              $reportsell='active';
              break;
              case 'contactus':
              $contactus='active';
              break;
              case 'orderdealers':
              $orderDealers='active';
              break;





            }
          @endphp
          <li class='{{$index}}'><a href="{{ url('/backoffice') }}"><i class="fa fa-home"></i> <span>หน้าหลัก</span></a></li>
          {{-- <li class='{{$admin}}'><a href="#"><i class="fa fa-user-circle"></i> <span>ผู้ดูแลระบบ</span></a></li> --}}

          @if (Auth::user()->member_type=="Admin")
            <li class='{{$contact}}'><a href="{{ url('/backoffice/contact') }}"><i class="fa fa-tty"></i> <span>ติดต่อเรา</span></a></li>
            <li class='{{$contactus}}'><a href="{{ url('/backoffice/contactus') }}"><i class="fa fa-inbox"></i> <span>ข้อความที่ถูกส่งเข้ามา</span></a></li>
            <li class='{{$payment}}'><a href="{{ url('/backoffice/payment') }}"><i class="fa fa-credit-card"></i> <span>ช่องทางการชำระเงิน</span></a></li>
            <li class='{{$helpcenter}}'><a href="{{ url('/backoffice/helpcenter') }}"><i class="fa fa-university"></i> <span>ศูนย์ช่วยเหลือ</span></a></li>
            <li class="treeview menu {{$user}} {{$userConfirm}}">
            <a href="#"><i class="fa fa-user-circle-o" aria-hidden="true"></i> <span>User</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu menu">
              <li class="{{$user}}"><a href="{{ url('/backoffice/user') }}"><i class="fa fa-circle-o" style="color : #008d4c" aria-hidden="true"></i> User ทั้งหมด
                <span class="pull-right-container">
                  @php
                  $numuser=\App\Models\user::where('status_user','1')->get();
                  @endphp
                  <small class="label pull-right bg-red">@if (count($numuser)>0)
                    {{count($numuser)}}
                  @endif</small>
                </span>
              </a></li>
              <li class="{{$userConfirm}}"><a href="{{ url('/backoffice/userConfirm') }}"><i class="fa fa-circle-o" style="color : #008d4c" aria-hidden="true"></i> User Confirm
                <span class="pull-right-container">
                  @php
                  $numcon=\App\Models\user::where('status_user','0')->get();
                  @endphp
                  <small class="label pull-right bg-red">@if (count($numcon)>0)
                    {{count($numcon)}}
                  @endif</small>
                </span>
              </a></li>
            </ul>
          </li>
        @endif
        @if (Auth::user()->member_type=="Admin" or Auth::user()->member_type=="Dealers")
            <li class="treeview {{$category}}  {{$brands}} {{$product}} {{$subcategory}}">
              <a href="#"><i class="fa fa-cart-plus"></i> <span>สินค้า</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              @endif
              <ul class="treeview-menu">
                @if (Auth::user()->member_type=="Admin")
                  <li class="{{$category}}"><a href="{{ url('/backoffice/productCategory') }}"><i class="fa fa-circle-o" style="color : #008d4c" aria-hidden="true"></i> ประเภทสินค้า</a></li>
                  <li class="{{$subcategory}}"><a href="{{ url('/backoffice/productSubCategory') }}"><i class="fa fa-circle-o" style="color : #008d4c" aria-hidden="true"></i> ประเภทสินค้าย่อย</a></li>

                @endif
                <li class="{{$brands}}"><a href="{{ url('/backoffice/productBrands') }}"><i class="fa fa-circle-o" style="color : #008d4c" aria-hidden="true"></i> ยี่ห้อสินค้า</a></li>
                <li class="{{$product}}"><a href="{{ url('/backoffice/product') }}"><i class="fa fa-circle-o" style="color : #008d4c" aria-hidden="true"></i> สินค้า</a></li>
              </ul>
            </li>
            @if (Auth::user()->member_type!="Admin")
              <li class='{{$controlZone}}'><a href="{{ url('/backoffice/controlzone') }}"><i class="fa fa-flag"></i> <span>สินค้าที่ถูกกั้นเขต</span></a></li>
            @endif
              @if (Auth::user()->member_type=="Admin")
            <li class="treeview {{$highlight}}  {{$promotionOfMonth}} {{$saleOfMonth}} {{ $recommend }}">
              <a href="#"><i class="fa fa-bars" aria-hidden="true"></i> สินค้าแนะนำ
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                <li class="{{$highlight}}"><a href="{{ url('/backoffice/highlight') }}"><i class="fa fa-circle-o" style="color : #008d4c" aria-hidden="true"></i> Highlight</a></li>
                <li class="{{$promotionOfMonth}}"><a href="{{ url('/backoffice/promotionofmonth') }}"><i class="fa fa-circle-o" style="color : #008d4c" aria-hidden="true"></i> Promotion of month</a></li>
                <li class="{{$saleOfMonth}}"><a href="{{ url('/backoffice/saleofmonth') }}"><i class="fa fa-circle-o" style="color : #008d4c" aria-hidden="true"></i> Sale of month</a></li>
                <li class="{{$recommend}}"><a href="{{ url('/backoffice/recommend') }}"><i class="fa fa-circle-o" style="color : #008d4c" aria-hidden="true"></i> Recommend</a></li>
              </ul>
              </li>
              @endif
              @if (Auth::user()->member_type=="Dealers")
                <li class="{{$orderDealers}}" ><a href="{{ url('/backoffice/orderDealers') }}"><i class="fa fa-shopping-basket" aria-hidden="true"></i> Order</a></li>
              @endif
              @if (Auth::user()->member_type=="Admin")
                {{-- <li class='{{$confirmOrder}}'><a href="{{ url('/backoffice/confirmorder') }}"><i class="fa fa-check-square-o"></i> <span>ตรวจสอบการชำระเงิน</span>
                  <span class="pull-right-container">
                    @php
                    $numOrder=\App\Models\OrderData::where('status_confirm','0')->get()->count();
                    @endphp
                    <small class="label pull-right bg-red">@if ($numOrder>0)
                      {{$numOrder}}
                    @endif</small>
                  </span>
                </a></li> --}}
                @php
                $numOrder0=\App\Models\OrderData::where('status_confirm','0')->get()->count();
                @endphp
                <li class="treeview {{$confirmOrder}} {{$order}} ">
                  <a href="#"><i class="fa fa-shopping-basket" aria-hidden="true"></i> Order
                    <span class="pull-right-container">
                      @if ($numOrder0>0)
                        <small class="label pull-right bg-red" id="order_check_num">{{ $numOrder0 }}</small>
                          <i class="fa fa-angle-left pull-right"></i>
                      @else
                        <i class="fa fa-angle-left pull-right"></i>
                      @endif

                      </span>
                  </a>
                  <ul class="treeview-menu">
                    <li class="{{$order}}"><a href="{{ url('/backoffice/order') }}"><i class="fa fa-circle-o" style="color : #008d4c" aria-hidden="true"></i> ใบสั่งซื้อสินค้า
                      <span class="pull-right-container">
                        @php
                        $numOrder=\App\Models\OrderData::where('status_confirm','1')->get()->count();
                        @endphp
                        <small class="label pull-right bg-red" id="order_num">@if ($numOrder>0)
                          {{$numOrder}}
                        @else
                          {{ '0' }}
                        @endif</small>
                      </span>
                    </a></li>
                    {{-- <li class="{{$confirmOrder}}"><a href="{{ url('/backoffice/confirmorder') }}"><i class="fa fa-circle-o" style="color : #008d4c" aria-hidden="true"></i> ตรวจสอบการชำระเงิน</a></li> --}}
                    <li class='{{$confirmOrder}}'><a href="{{ url('/backoffice/confirmorder') }}"><i class="fa fa-circle-o" style="color : #008d4c" aria-hidden="true"></i> <span>ตรวจสอบการชำระเงิน</span>
                      <span class="pull-right-container">
                        <small class="label pull-right bg-red" id="order_check_num">@if ($numOrder0>0)
                          {{$numOrder0}}
                        @else
                          {{ '0' }}
                        @endif</small>
                      </span>
                    </a></li>
                    {{-- <li class="{{$saleOfMonth}}"><a href="{{ url('/backoffice/saleofmonth') }}"><i class="fa fa-circle-o" style="color : #008d4c" aria-hidden="true"></i> Sale of month</a></li> --}}
                    {{-- <li class="{{$recommend}}"><a href="{{ url('/backoffice/recommend') }}"><i class="fa fa-circle-o" style="color : #008d4c" aria-hidden="true"></i> Recommend</a></li> --}}
                  </ul>
                  </li>
                  @endif
                  @if (Auth::user()->member_type=="Admin")
                  <li class="treeview {{$reportbestsell}} {{$reportsell}} ">
                    <a href="#"><i class="fa fa-file-code-o" aria-hidden="true"></i> รายงาน
                      <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                      <li class="{{$reportsell}}" ><a href="{{ url('/backoffice/reportSell') }}"><i class="fa fa-circle-o"  style="color : #008d4c"></i> รายงานการขายสินค้า</a></li>
                      <li class="{{$reportbestsell}}" ><a href="{{ url('/backoffice/reportBestSell') }}"><i class="fa fa-circle-o"  style="color : #008d4c"></i> รายงานสินค้าขายดี</a></li>
                    </ul>
                  </li>
                </ul>
                    </li>
                    @endif
                    {{-- <li class="treeview">
                      <a href="#">
                        <i class="fa fa-share"></i> <span>Multilevel</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                        <li class="treeview">
                          <a href="#"><i class="fa fa-circle-o"></i> Level One
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                          </a>
                          <ul class="treeview-menu">
                            <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                            <li class="treeview">
                              <a href="#"><i class="fa fa-circle-o"></i> Level Two
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                              </a>
                              <ul class="treeview-menu">
                                <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                              </ul>
                            </li>
                          </ul>
                        </li>
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                      </ul>
                    </li> --}}



        </ul>
        <!-- /.sidebar-menu -->
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    @yield('content')
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
      {{-- <div class="pull-right hidden-xs">
        Anything you want
      </div> --}}
      <!-- Default to the left -->
      <strong>Copyright &copy; 2017 <a target="_blank" href="https://www.orange-thailand.com/">Orange-Thailand</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>

{{-- froala --}}

<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/froala_editor.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/align.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/char_counter.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/code_beautifier.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/code_view.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/colors.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/draggable.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/emoticons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/entities.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/file.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/font_size.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/font_family.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/fullscreen.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/image.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/image_manager.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/line_breaker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/inline_style.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/link.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/lists.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/paragraph_format.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/paragraph_style.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/quick_insert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/quote.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/table.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/save.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/url.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/froalaEditor/js/plugins/video.min.js') }}"></script>
{{-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script> --}}
<script type="text/javascript" src="https://cdn.datatables.net/1.10.17/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>









<!-- jQuery 3 -->

{{-- {{Html::script('assets/AdminLTE/bower_components/jquery/dist/jquery.min.js')}} --}}
<!-- Bootstrap 3.3.7 -->
{{Html::script('assets/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js')}}
<!-- AdminLTE App -->
{{Html::script('assets/AdminLTE/dist/js/adminlte.min.js')}}
{{-- {{Html::script('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}} --}}
{{Html::script('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>

</body>
</html>
