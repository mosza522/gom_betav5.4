@extends('backoffice.layouts.main')
@section('page_title','GomBeta | ช่องทางการชำระเงิน')
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ช่องทางการชำระเงิน
        <small>ช่องทางการชำระเงิน</small>
        <p>
          {{-- {{Html::link('backoffice/add_admin','<i class="fa fa-plus-square" aria-hidden="true"></i>Add Admin',array('class'=>'btn btn-primary'))}} --}}

        </p>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-cog"></i> Backoffice</a></li>
        <li class="active">ช่องทางการชำระเงิน</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      @if(Session::has('success'))
        {{-- <div class="alert alert-success">
          <ul>
          <li> {{ Session::get('success') }}</li>
          </ul>
        </div> --}}
        <script>
            swal("{{ Session::get('success') }}", "", "success");
        </script>
    @endif

    {{-- <script>
        swal("Good job!", "You clicked the button!", "success");
    </script> --}}
    <a  href="" data-toggle="modal" data-target="#myModalInsert"><button type="button" class="btn btn-primary"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มช่องทางการชำระเงิน</button></a>
    <p></p>

    <div class="modal fade" id="myModalInsert"  role="dialog" aria-labelledby="myModalLabel" style="display: none;">
      <div class="modal-dialog" role="document" style="width:50%;">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times; </button> <span aria-hidden="true"> </span>
            <h3 class="text-center">เพิ่มช่องทางการชำระเงิน</h3>
          </div>
          <form class="" id="insert_payment" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{-- <input type="hidden" name="type" value="category"> --}}
            <div class="modal-body">
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        ธนาคาร
                      </div>
                      <div class="col-md-6">
                        @php
                        $bank= \App\Models\Bank::whereNotIn('bank.id',function($q){
                          $q->select('bank_id')->from('payment');
                        })->select('bank.*')->get();
                        @endphp
                        <select class="form-control" name="bank_id" id="bank" style="width:100%">
                          <option value=""></option>

                          @foreach ($bank as $element)
                            <option value="{{$element->id}}">{{ $element->name }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6 text-center">
                      เลขที่บัญชี
                    </div>
                    <div class="col-md-6 text-center">
                      <input type="text" class="form-control" name="account_no" value=""required>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6 text-center">
                      ชื่อบัญชี
                    </div>
                    <div class="col-md-6 text-center">
                      <input type="text" class="form-control" name="account_name" value="" required>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6 text-center">
                      ประเภทบัญชี
                    </div>
                    <div class="col-md-6 text-center">
                      <select class="form-control" name="type">
                        <option value="บัญชีเงินฝากออมทรัพย์">บัญชีเงินฝากออมทรัพย์</option>
                        <option value="บัญชีเงินฝากประจำ">บัญชีเงินฝากประจำ</option>
                        <option value="บัญชีเงินฝากกระแสรายวัน">บัญชีเงินฝากกระแสรายวัน</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6 text-center">
                      ชื่อสาขา
                    </div>
                    <div class="col-md-6 text-center">
                      <input type="text" class="form-control" name="branch" value="" required>
                    </div>
                  </div>
                </div>
            </div>
              <div class="modal-footer" style="">
                <button type="submit" href="javascript:void(0);" class="btn btn-success" style="margin-top:30px" onclick="" >เพิ่ม</button>
                <button class="btn btn-danger" type="reset" style="margin-top:30px" data-dismiss="modal">Close</button>
              </div>
            </form>

        </div>
      </div>
      </div>
      {{-- Modal edit  --}}
      <div class="modal fade" id="myModalupdate"  role="dialog" aria-labelledby="myModalLabel" style="display: none;">
        <div class="modal-dialog" role="document" style="width:50%;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times; </button> <span aria-hidden="true"> </span>
              <h3 class="text-center">แก้ไข Promotion Of Month</h3>
            </div>
            <form class="" id="edit_bank" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <input type="hidden" name="id" id="hidden_id" value="">
              <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                      <div style="">
                        <div class="col-md-6 text-center">
                          สินค้า
                        </div>
                        <div class="col-md-6">
                          @php
                          $bank= \App\Models\Bank::whereNotIn('bank.id',function($q){
                            $q->select('bank_id')->from('payment');
                          })->select('bank.*')->get();
                          @endphp
                          <select class="form-control" name="bank_id" id="bank_edit" style="width:100%">
                            <option value=""></option>

                            @foreach ($bank as $element)
                              <option value="{{$element->id}}">{{ $element->name }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6 text-center">
                        เลขที่บัญชี
                      </div>
                      <div class="col-md-6 text-center">
                        <input type="text" class="form-control" name="account_no" id="account_no_edit" value=""required>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6 text-center">
                        ชื่อบัญชี
                      </div>
                      <div class="col-md-6 text-center">
                        <input type="text" class="form-control" name="account_name" id="account_name_edit" value="" required>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6 text-center">
                        ประเภทบัญชี
                      </div>
                      <div class="col-md-6 text-center">
                        <select class="form-control" name="type" id="type_edit">
                          <option value="บัญชีเงินฝากออมทรัพย์">บัญชีเงินฝากออมทรัพย์</option>
                          <option value="บัญชีเงินฝากประจำ">บัญชีเงินฝากประจำ</option>
                          <option value="บัญชีเงินฝากกระแสรายวัน">บัญชีเงินฝากกระแสรายวัน</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6 text-center">
                        ชื่อสาขา
                      </div>
                      <div class="col-md-6 text-center">
                        <input type="text" class="form-control" name="branch" id="branch_edit" value="" required>
                      </div>
                    </div>
                  </div>
              </div>
                <div class="modal-footer" style="">
                  <button type="submit" href="javascript:void(0);" class="btn btn-success" style="margin-top:30px" onclick="" >แก้ไข</button>
                  <button class="btn btn-danger" type="reset" style="margin-top:30px" data-dismiss="modal">Close</button>
                </div>
              </form>

          </div>
        </div>
        </div>




      <table id="example" class="table">
        <thead>
          <tr>
            <th class="text-center" style="">No .</th>
            <th class="text-center" style="">ชื่อธนาคาร</th>
            <th class="text-center" style="">ICON</th>
            <th class="text-center" style="">ชื่อบัญชี</th>
            <th class="text-center" style="">เลขบัญชี</th>
            <th class="text-center" style="">ประเภทบัญชี</th>
            <th class="text-center" style="">สาขา</th>
            <th class="text-center" style="">Action</th>
          </tr>
        </thead>
    <tbody id="data">
      @php
        $datas= \App\Models\Payment::leftJoin('bank','payment.bank_id','=','bank.id')
        ->select('payment.*','bank.name','bank.icon')
        ->get();
        $i=1;
      @endphp
      @foreach ($datas as $element)
        <tr>
          <td>{{ $i++ }}</td>
          <td>{{ $element->name }} </td>
          <td><i class="opacity-8 thbanks thbanks-1x {{$element->icon}}" aria-hidden="true"></i></td>
          <td>{{ $element->account_name }}</td>
          <td>{{ $element->account_no }}</td>
          <td>{{ $element->type }}</td>
          <td>{{ $element->branch }}</td>
          <td>
            <a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return show({{$element->id}})" class="btn btn-warning">Edit</a>
            <a href='javascript:void(0);' onclick='return sweetalert({{$element->id}})' class='btn btn-danger'>Delete</a>
          </td>
        </tr>
      @endforeach
    </tbody>
    </table>

    </section>

  </div>
<script type="text/javascript">
function show(id) {
  $.ajax({
    url: '{{ url('payment') }}/'+id,
    type: 'GET',
    data: '',
    dataType:'json',
    success: function (html) {
      // alert(JSON.stringify(html));
      var $el = $("#bank_edit");
      $el.empty();
      change_selected(document.getElementById('bank_edit'));
      function change_selected(selector)
      {
        selector.options[0] = new Option("","");
        for (var i = 0; i < html[0].select.length; i++) {
          selector.options[i+1] = new Option(html[0].select[i].name,html[0].select[i].id);
        }

      }
      // alert(JSON.stringify(html[0].data.type))
      $('#bank_edit').val(html[0].data.bank_id);
      $('#account_no_edit').val(html[0].data.account_no);
      $('#account_name_edit').val(html[0].data.account_name);
      $('#type_edit').val(html[0].data.type);
      $('#branch_edit').val(html[0].data.branch);
      $('#hidden_id').val(html[0].data.id);

    }
  });

}
$('#insert_payment').submit(function() {
  if($('#bank').val()==""){
    swal("ข้อมูลไม่ครบถ้วน", "กรุณาเลือกธนาคาร !!", "warning");
    return false;
  }

  var formData = new FormData($(this)[0]);
  var dataTable = $("#example").DataTable();
  $.ajax({
    url: '{{url('payment')}}',
    type: 'POST',
    data: formData,
    dataType:'json',
    success: function (html) {
      // alert(html);
      // alert(html);
        swal("Successfully", "เพิ่มช่องทางการชำระสำเร็จ !!", "success");
        dataTable.clear();
        dataTable.rows.add(html[0].table).draw();
        $("#insert_payment")[0].reset();
        $('#myModalInsert').modal('toggle');
        var $el = $("#bank");
        $el.empty();
        change_selected(document.getElementById('bank'));
        function change_selected(selector)
          {
            selector.options[0] = new Option("","");
            for (var i = 0; i < html[0].select.length; i++) {
              selector.options[i+1] = new Option(html[0].select[i].name,html[0].select[i].id);
            }

        }
        $('#bank').val();
    },
    cache: false,
    contentType: false,
    processData: false
  });
return false;
});
$('#edit_bank').submit(function() {
  var formData = new FormData($(this)[0]);
  var dataTable = $("#example").DataTable();
  $.ajax({
    url: '{{url('payment')}}/'+$('#hidden_id').val()+'/update',
    type: 'POST',
    data: formData,
    dataType:'json',
    success: function (html) {
      swal("Successfully", "แก้ไขช่องทางการชำระสำเร็จ !!", "success");
      dataTable.clear();
      dataTable.rows.add(html[0].table).draw();
      $('#myModalupdate').modal('toggle');
      var $el = $("#bank");
      $el.empty();
      change_selected(document.getElementById('bank'));
      function change_selected(selector)
      {
        selector.options[0] = new Option("","");
        for (var i = 0; i < html[0].select.length; i++) {
          selector.options[i+1] = new Option(html[0].select[i].name,html[0].select[i].id);
        }

      }
    },
    cache: false,
    contentType: false,
    processData: false
  });
return false;
});
function sweetalert(id) {
  var dataTable = $("#example").DataTable();
  swal({
    title: "แน่ใจหรือ ?",
    text: "คุณต้องการลบข้อมูลนี้ใช่หรือไม่",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((sweetalert) => {
    if (sweetalert) {
      // setTimeout(function () {
      $.ajax({
        type: 'DELETE',
        url: '{{url('payment')}}'+'/'+id,
        data: {_token: "{{ csrf_token() }}"},
        dataType:'json',
        success:function(html){
          // alert(JSON.stringify(html[0].select[0].id));
          swal("Successfully", "ลบเสร็จสิ้น !!", "success");
          dataTable.clear();
          dataTable.rows.add(html[0].table).draw();
          var $el = $("#bank");
          $el.empty();
          change_selected(document.getElementById('bank'));
          function change_selected(selector)
            {
              selector.options[0] = new Option("","");
              for (var i = 0; i < html[0].select.length; i++) {
                selector.options[i+1] = new Option(html[0].select[i].name,html[0].select[i].id);
              }

          }
          $('#bank').val();

        }
      });
      // }, 1000);
    } else {
      swal("ยกเลิกการลบ !","","error");
    }
  });
}
$(document).ready(function(){
  $('#example').DataTable({
    "order": [[ 0, "asc" ]]
  });
  $('#bank').select2({
    placeholder:'กรุณาเลือกธนาคาร',
  });
  $('#bank_edit').select2({
    placeholder:'กรุณาเลือกธนาคาร',
  });
});
</script>
@endsection
