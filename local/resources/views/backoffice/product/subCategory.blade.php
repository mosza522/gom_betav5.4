@extends('backoffice.layouts.main')
@section('page_title','GomBeta | Sub Category')
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ประเภทสินค้าย่อย
        <small>ประเภทสินค้าย่อย</small>
        <p>
          {{-- {{Html::link('backoffice/add_admin','<i class="fa fa-plus-square" aria-hidden="true"></i>Add Admin',array('class'=>'btn btn-primary'))}} --}}

        </p>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-cog"></i> Backoffice</a></li>
        <li class="active">ประเภทสินค้าย่อย</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      @if(Session::has('success'))
        {{-- <div class="alert alert-success">
          <ul>
          <li> {{ Session::get('success') }}</li>
          </ul>
        </div> --}}
        <script>
            swal("{{ Session::get('success') }}", "", "success");
        </script>
    @endif

    {{-- <script>
        swal("Good job!", "You clicked the button!", "success");
    </script> --}}
    <a  href="" data-toggle="modal" data-target="#myModalInsert"><button type="button" class="btn btn-primary"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม ประเภทสินค้าย่อย</button></a>
    <p></p>

    <div class="modal fade" id="myModalInsert"  role="dialog" aria-labelledby="myModalLabel" style="display: none;">
      <div class="modal-dialog" role="document" style="width:50%;">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times; </button> <span aria-hidden="true"> </span>
            <h3 class="text-center">เพิ่ม ประเภทสินค้าย่อย</h3>
          </div>
          <form class="" id="insert_subcategory" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{-- <input type="hidden" name="type" value="category"> --}}
            <div class="modal-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-md-6 text-center">
                    ประเภทสินค้า
                  </div>
                  <div class="col-md-6">
                    @php
                    $cat= \App\Models\ProductCategory::get();
                    @endphp
                    <select class="form-control" name="category_id" id="category" style="width:100%">
                      <option value=""></option>

                      @foreach ($cat as $element)
                        <option value="{{$element->id}}">{{ $element->name }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-md-6 text-center">
                    ชื่อประเภทสินค้าย่อย
                  </div>
                  <div class="col-md-6 text-center">
                    <input class="form-control" type="text" name="name" value="" required>
                  </div>
                </div>
              </div>
            </div>
              <div class="modal-footer" style="">
                <button type="submit" href="javascript:void(0);" class="btn btn-success" style="margin-top:30px" onclick="" >เพิ่ม</button>
                <button class="btn btn-danger" type="reset" style="margin-top:30px" data-dismiss="modal">Close</button>
              </div>
            </form>

        </div>
      </div>
      </div>
      {{-- Modal edit  --}}
      <div class="modal fade" id="myModalupdate"  role="dialog" aria-labelledby="myModalLabel" style="display: none;">
        <div class="modal-dialog" role="document" style="width:50%;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times; </button> <span aria-hidden="true"> </span>
              <h3 class="text-center">แก้ไข ประเภทสินค้าย่อย</h3>
            </div>
            <form class="" id="edit_subcategory" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <input type="hidden" name="id" id="hidden_id" value="">
              <div class="modal-body">
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6 text-center">
                      ประเภทสินค้า
                    </div>
                    <div class="col-md-6">
                      @php
                      $cat= \App\Models\ProductCategory::get();
                      @endphp
                      <select class="form-control" name="category_id" id="category_edit" style="width:100%">
                        @foreach ($cat as $element)
                          <option value="{{$element->id}}">{{ $element->name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6 text-center">
                      ชื่อประเภทสินค้าย่อย
                    </div>
                    <div class="col-md-6 text-center">
                      <input class="form-control" type="text" name="name" value="" id="name_edit" required>
                    </div>
                  </div>
                </div>
              </div>
                <div class="modal-footer" style="">
                  <button type="submit" href="javascript:void(0);" class="btn btn-success" style="margin-top:30px" onclick="" >แก้ไข</button>
                  <button class="btn btn-danger" type="reset" style="margin-top:30px" data-dismiss="modal">Close</button>
                </div>
              </form>

          </div>
        </div>
        </div>




      <table id="example" class="table">
        <thead>
          <tr>
            <th class="text-center" style="width: 5%;">No .</th>
            <th class="text-center" style="width: 35%;">ชื่อประเภทสินค้า</th>
            <th class="text-center" style="width: 35%;">ชื่อประเภทสินค้าย่อย</th>
            <th class="text-center" style="width: 25%;">Action</th>
          </tr>
        </thead>
    <tbody id="data">
      @php
        $datas= \App\Models\SubCategory::leftJoin('product_category','product_category_sub.category_id','=','product_category.id')
        ->select('product_category_sub.name as subCategoryName','product_category.name as CategoryName','product_category_sub.id')
        ->get();
        $i=1;
      @endphp
      @foreach ($datas as $element)
        <tr>
          <td>{{ $i++ }}</td>
          <td>{{ $element->CategoryName }}</td>
          <td>{{ $element->subCategoryName }}</td>
          <td>
            <a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return show({{$element->id}})" class="btn btn-warning">Edit</a>
            <a href='javascript:void(0);' onclick='return sweetalert({{$element->id}})' class='btn btn-danger'>Delete</a>
          </td>
        </tr>
      @endforeach
    </tbody>
    </table>

    </section>

  </div>
<script type="text/javascript">
// $('#myModalupdate').is(':visible');
$('#myModalupdate').on('hidden.bs.modal', function (e) {
  $('#category_edit').select2({
    placeholder:'กรุณาเลือกสินค้า'
  });
})
function show(id) {
  $.ajax({
    url: '{{ url('subCategory') }}/'+id,
    type: 'GET',
    data: '',
    dataType:'json',
    success: function (html) {
      // alert(JSON.stringify(html));
      // $('#category_edit').val(html.category_id);
      $("#category_edit").val(html.category_id).trigger('change');
      $('#name_edit').val(html.name);
      $('#hidden_id').val(html.id);
      $('#category_edit').select2();

    }
  });

}
$('#insert_subcategory').submit(function() {
  if($('#category').val()==""){
    swal("ข้อมูลไม่ครบถ้วน", "กรุณาเลือกประเภทสินค้า !!", "warning");
    return false;
  }

  var formData = new FormData($(this)[0]);
  var dataTable = $("#example").DataTable();
  $.ajax({
    url: '{{url('subCategory')}}',
    type: 'POST',
    data: formData,
    dataType:'json',
    success: function (html) {
      // alert(html);
      swal("Successfully", "ลบเสร็จสิ้น !!", "success");
      dataTable.clear();
      dataTable.rows.add(html).draw();
      $("#insert_subcategory")[0].reset();
      $('#category').select2({
        placeholder:'กรุณาเลือกประเภทสินค้า'
      });
      $('#myModalInsert').modal('toggle');
    },
    cache: false,
    contentType: false,
    processData: false
  });
return false;
});
$('#edit_subcategory').submit(function() {
  var formData = new FormData($(this)[0]);
  var dataTable = $("#example").DataTable();
  $.ajax({
    url: '{{url('subCategory')}}/'+$('#hidden_id').val()+'/update',
    type: 'POST',
    data: formData,
    dataType:'json',
    success: function (html) {
      swal("Successfully", "แก้ไข ประเภทสินค้าย่อย สำเร็จ !!", "success");
      dataTable.clear();
      dataTable.rows.add(html).draw();
      $('#myModalupdate').modal('toggle');
    },
    cache: false,
    contentType: false,
    processData: false
  });
return false;
});
function sweetalert(id) {
  var dataTable = $("#example").DataTable();
  swal({
    title: "แน่ใจหรือ ?",
    text: "คุณต้องการลบข้อมูลนี้ใช่หรือไม่",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((sweetalert) => {
    if (sweetalert) {
      // setTimeout(function () {
      $.ajax({
        type: 'DELETE',
        url: '{{url('subCategory')}}'+'/'+id,
        data: {_token: "{{ csrf_token() }}"},
        dataType:'json',
        success:function(html){
          // alert();
          swal("Successfully", "ลบเสร็จสิ้น !!", "success");
          dataTable.clear();
          dataTable.rows.add(html).draw();
          $('#product').val();

        }
      });
      // }, 1000);
    } else {
      swal("ยกเลิกการลบ !","","error");
    }
  });
}
$(document).ready(function(){
  $('#example').DataTable({
    "order": [[ 0, "asc" ]]
  });
  $('#category').select2({
    placeholder:'กรุณาเลือกประเภทสินค้า'
  });
  $('#category_edit').select2({
    placeholder:'กรุณาเลือกสินค้า'
  });
});
</script>
@endsection

{{-- [{"No .":1,
"Name Category":
"sdfg","Note":"sdfg","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":2,"Name Category":"qwer","Note":"qwer","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":3,"Name Category":"xxxxx","Note":"xxxx","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":4,"Name Category":"123456","Note":"123456","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":5,"Name Category":"dddddddd","Note":"dddddddd","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":6,"Name Category":"5555","Note":"5555","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":7,"Name Category":"000000","Note":"0000000","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":8,"Name Category":"0222222","Note":"22222","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":9,"Name Category":"45646","Note":"346346","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":10,"Name Category":"tyty","Note":"wergwerg","Action":"<a href='#' class='btn btn-... --}}
