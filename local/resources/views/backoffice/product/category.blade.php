@extends('backoffice.layouts.main')
@section('page_title','GomBeta | ประเภทสินค้า')
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ประเภทสินค้า
        <small>ประเภทสินค้า</small>
        <p>
          {{-- {{Html::link('backoffice/add_admin','<i class="fa fa-plus-square" aria-hidden="true"></i>Add Admin',array('class'=>'btn btn-primary'))}} --}}

        </p>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-cog"></i> Backoffice</a></li>
        <li class="active">ประเภทสินค้า</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      @if(Session::has('success'))
        {{-- <div class="alert alert-success">
          <ul>
          <li> {{ Session::get('success') }}</li>
          </ul>
        </div> --}}
        <script>
            swal("{{ Session::get('success') }}", "", "success");
        </script>
    @endif

    {{-- <script>
        swal("Good job!", "You clicked the button!", "success");
    </script> --}}
    <a  href="" data-toggle="modal" data-target="#myModalInsert"><button type="button" class="btn btn-primary"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มประเภทสินค้า</button></a>
    <p></p>

    <div class="modal fade" id="myModalInsert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
      <div class="modal-dialog" role="document" style="width:50%;">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times; </button> <span aria-hidden="true"> </span>
            <h3 class="text-center">เพิ่มประเภทสินค้า</h3>
          </div>
          <form class="" id="insert_category" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="type" value="category">
            <div class="modal-body">
              <div class="form-group">
                  <div class="row">
                    <div class="col-md-6 text-center">
                      ชื่อประเภทสินค้า
                    </div>
                    <div class="col-md-6">
                      <input class="form-control" type="text" name="name_category" value="" id="name_insert">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6 text-center">
                      รูปภาพ
                    </div>
                    <div class="col-md-6">
                      <input class="form-control" type='file' name="img" onchange="readURL(this);" id="img" />
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-12 text-center">
                      <img id="blah" width="175px" height="175px" src="{{ asset('assets/images/no-photo.jpg') }}" alt="your image" />
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div style="">
                      <div class="col-md-6 text-center">
                        Note
                      </div>
                      <div class="col-md-6">
                        <input class="form-control" type="text" name="note" value="">
                      </div>
                    </div>
                  </div>
                </div>
            </div>
              <div class="modal-footer" style="">
                <button type="submit" href="javascript:void(0);" class="btn btn-success" style="margin-top:30px" onclick="" >เพิ่ม</button>
                <button class="btn btn-danger" type="reset" style="margin-top:30px" data-dismiss="modal">Close</button>
              </div>
            </form>

        </div>
      </div>
      </div>

      {{-- Modal Image  --}}
      <div class="modal fade" id="myModalImg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
        <div class="modal-dialog" role="document" style="width:50%;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times; </button> <span aria-hidden="true"> </span>
              <h3 class="text-center">รูปภาพ</h3>
            </div>

              <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                      <div class="col-md-12text-center">
                        <img src="" width="100%" id="img_show" alt="">
                      </div>
                    </div>
                  </div>

              </div>
                <div class="modal-footer" style="">
                  <button class="btn btn-danger" type="reset" style="margin-top:30px"  data-dismiss="modal">Close</button>
                </div>
              </form>

          </div>
        </div>
        </div>
      {{-- Modal update  --}}
      <div class="modal fade" id="myModalupdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
        <div class="modal-dialog" role="document" style="width:50%;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times; </button> <span aria-hidden="true"> </span>
              <h3 class="text-center">แก้ไขประเภทสินค้า</h3>
            </div>
            <form class="" id="update_category" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <input type="hidden" name="id" value="category" id="id">
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">

                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-6 text-center">
                          ชื่อประเภทสินค้า
                        </div>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="name_category" id="name_category" value="">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-6 text-center">
                          รูปภาพ
                        </div>
                        <div class="col-md-6">
                          <input class="form-control" type='file' name="img" onchange="readURL2(this);" id="img_update" />
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-12 text-center">
                          <img id="show_img" width="100%" src="{{ asset('assets/images/no-photo.jpg') }}" alt="your image" />
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-6 text-center">
                          Note
                        </div>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="note" id="note" value="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button class="btn btn-success" style="margin-top:30px" >บันทึก</button>
                <button class="btn btn-danger" type="reset" style="margin-top:30px" data-dismiss="modal">Close</button>
              </div>

            </form>
          </div>
        </div>
      </div>


      <table id="example" class="table">
      <thead>
        <tr>
<th class="text-center" style="width: 5%;">No .</th>
          <th class="text-center" style="width: 20%;">Category name</th>
          <th class="text-center" style="width: 35%;">Image</th>
          <th class="text-center" style="width: 20%;text-align:center;">Note</th>
<th class="text-center" style="width: 20%;">Action</th>
  </tr>
      </thead>
    <tbody id="data">
      @php
        $datas= \App\Models\ProductCategory::get();
        $i=1;
      @endphp
      @foreach ($datas as $element)
        <tr>
          <td>{{ $i++ }}</td>
          <td>{{ $element->name }}</td>
          <td class="text-center">
            <div class="tz-gallery">
              <div class="row">
                <div class="col-sm-12 col-md-12">
                    <a class="lightbox" href="{{ asset('local/storage/app/images/img-category/').'/'.$element->img }}">
                        <img src="{{ asset('local/storage/app/images/img-category/').'/'.$element->img }}"  width="250px" height="250px" alt="Park">
                    </a>
                </div>
              </div>
            </div>
            {{-- <a  href="" data-toggle="modal" data-target="#myModalImg" onclick="return img({{$element->id}})"><img src="" width="250px" height="250px" alt=""></a> --}}
           </td>
          <td>{{ $element->note }}</td>
          <td>
            <a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return show({{$element->id}})" class="btn btn-warning">Edit</a>
            <a href='javascript:void(0);' onclick='return sweetalert({{$element->id}})' class='btn btn-danger'>Delete</a></td>
        </tr>
      @endforeach
    </tbody>
    </table>

    </section>
    <!-- /.content -->
    <script type="text/javascript">
    $('#myModalImg').on('hidden.bs.modal', function () {
    $('#img_show').attr("src",'');
    })
    // $('#myModalInsert').on('hidden.bs.modal', function (e) {
    //   $(this)
    //   .find("input,textarea,select")
    //   .val('')
    //   .end()
    //   .find("input[type=checkbox], input[type=radio]")
    //   .prop("checked", "")
    //   .end();
    // })

    function img(id) {
      $.ajax({
        type: 'GET',
        url: '{{url('product_category')}}'+'/'+id+'/img',
        data: '',
        dataType:'html',
        success:function(html){
          // alert(html);
          $('#img_show').attr("src",'{{ asset('local/storage/app/images/img-category/') }}'+'/'+html);
          }
    });
    }
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
                $('#blah').attr('width', "100%");
                $('#blah').attr('height', "");
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#show_img').attr('src', e.target.result);
                    $('#show_img').attr('width', "100%");
                    $('#show_img').attr('height', "");
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).ready(function(){
      $('#example').DataTable({
          "order": [[ 0, "asc" ]]
      });
    });
    $('#insert_category').submit(function () {
      var formData = new FormData($(this)[0]);
      var dataTable = $("#example").DataTable();
      if($("#name_insert").val()==""){
        $("#name_insert").focus();
        swal("ผิดพลาด", "กรุณากรอกชื่อประเภทสินค้า !!", "error");
        return false;
      }
      if($("#img").val()==""){
        swal("ผิดพลาด", "กรุณาเลือกรูปภาพ !!", "error");
        $("#img").focus();
        return false;
      }
      $.ajax({
        type: 'POST',
        url: '{{url('product_category')}}',
        data: formData,
        dataType:'json',
        success:function(html){
          if(html != 'Failed'){
            swal("Successfully", "เพิ่มประเภทสินค้าสำเร็จ !!", "success");
            dataTable.clear();
            dataTable.rows.add(html).draw();
            $("#insert_category")[0].reset();
            $('#blah').attr('src', '{{ asset('assets/images/no-photo.jpg') }}');
            $('#blah').attr('width', "175px");
            $('#blah').attr('height', "175px");
            $('#myModalInsert').modal('toggle');
            baguetteBox.run('.tz-gallery');
          }

        },
        cache: false,
        contentType: false,
        processData: false
    });

    return false;
});
    function show(id) {
      $.ajax({
        type: 'GET',
        url: '{{url('product_category')}}'+'/'+id+'/show',
        data: '',
        dataType:'json',
        success:function(html){
          // alert(html.id);
          $('#id').val(html.id);
          $('#show_img').attr("src",'{{ asset('local/storage/app/images/img-category/') }}'+'/'+html.img);
          $('#name_category').val(html.name);
          $('#note').val(html.note);
        }
    });
    }
    $('#update_category').submit(function () {
      var dataTable = $("#example").DataTable();
      if($("#name_category").val()==""){
        swal("ผิดพลาด", "กรุณากรอกชื่อประเภทสินค้า !!", "error");
        return false;
      }
      var formData = new FormData($(this)[0]);
      $.ajax({
        type: 'POST',
        url: '{{url('product_category/')}}'+'/'+$('#id_hidden').val()+'/update',
        data: formData,
        dataType:'JSON',
        success:function(html){
          if(html != 'Failed'){
            swal("Successfully", "แก้ไขเสร็จสิ้น !!", "success");
            dataTable.clear();
            dataTable.rows.add(html).draw();
            $("#update_category")[0].reset();
            $('#myModalupdate').modal('toggle');
            baguetteBox.run('.tz-gallery');
          }
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
    });
    function sweetalert(id) {
      var dataTable = $("#example").DataTable();
      swal({
    title: "แน่ใจหรือ ?",
    text: "คุณต้องการลบข้อมูลนี้ใช่หรือไม่",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((sweetalert) => {
    if (sweetalert) {
      // setTimeout(function () {
      $.ajax({
        type: 'GET',
        url: '{{url('product_category')}}'+'/'+id,
        data: '',
        dataType:'json',
        success:function(html){
          if(html != 'Failed'){
            swal("Successfully", "ลบเสร็จสิ้น !!", "success");
            dataTable.clear();
            dataTable.rows.add(html).draw();
            baguetteBox.run('.tz-gallery');
            // $("#insert_category")[0].reset();
          }


        }
    });
  // }, 1000);
    } else {
      swal("ยกเลิกการลบ !","","error");
    }
  });
    }
    </script>
  </div>
@endsection

{{-- [{"No .":1,
"Name Category":
"sdfg","Note":"sdfg","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":2,"Name Category":"qwer","Note":"qwer","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":3,"Name Category":"xxxxx","Note":"xxxx","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":4,"Name Category":"123456","Note":"123456","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":5,"Name Category":"dddddddd","Note":"dddddddd","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":6,"Name Category":"5555","Note":"5555","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":7,"Name Category":"000000","Note":"0000000","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":8,"Name Category":"0222222","Note":"22222","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":9,"Name Category":"45646","Note":"346346","Action":"<a href='#' class='btn btn-warning'>Edit</a>\n              <a href='#' class='btn btn-danger'>Delete</a>"},{"No .":10,"Name Category":"tyty","Note":"wergwerg","Action":"<a href='#' class='btn btn-... --}}
