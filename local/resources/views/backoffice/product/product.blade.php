@extends('backoffice.layouts.main')
@section('page_title','GomBeta | สินค้า')
@section('head')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@endsection
@section('content')

  <meta name="csrf-token" content="{{ csrf_token() }}">
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        สินค้า
        <small>สินค้า</small>
        <p>
          {{-- {{Html::link('backoffice/add_admin','<i class="fa fa-plus-square" aria-hidden="true"></i>Add Admin',array('class'=>'btn btn-primary'))}} --}}

        </p>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-cog"></i> Backoffice</a></li>
        <li class="active">สินค้า</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      @if(Session::has('success'))
        {{-- <div class="alert alert-success">
        <ul>
        <li> {{ Session::get('success') }}</li>
      </ul>
    </div> --}}
    <script>
    swal("{{ Session::get('success') }}", "", "success");
    </script>
  @endif

  {{-- <script>
  swal("Good job!", "You clicked the button!", "success");
</script> --}}
<a  href="" data-toggle="modal" data-target="#myModalInsert"><button type="button" class="btn btn-primary"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มสินค้า</button></a>
<p></p>
{{-- Modal insert  --}}
<div class="modal fade" id="myModalInsert"  role="dialog" aria-labelledby="myModalLabel" style="display: none;">
  <div class="modal-dialog" role="document" style="width:80%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times; </button> <span aria-hidden="true"> </span>
        <h3 class="text-center">เพิ่มสินค้า</h3>
      </div>
      <form class="" id="insert_product" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="modal-body">
          <div class="form-group">
            <div class="row">
              <div class="col-md-6 text-center">
                ประเภทสินค้า
              </div>
              <div class="col-md-6">
                <select class="categories form-control" style="width:100%" name="category" id="category">
                  @php
                  $category= \App\Models\ProductCategory::get();
                  @endphp
                  <option value=""></option>
                  @foreach ($category as $key )
                    <option value="{{ $key->id }}">{{ $key->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-6 text-center">
                ประเภทสินค้าย่อย
              </div>
              <div class="col-md-6">
                <select class="categories form-control" style="width:100%" placeholder="เลือกประเภทสินค้าก่อน" name="sub_category" id="sub_category" disabled>
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-6 text-center">
                ยี่ห้อสินค้า
              </div>
              <div class="col-md-6">
                <select class="brands form-control" style="width:100%" name="brand" id="brand">
                  @php
                  $brands= \App\Models\ProductBrands::get();
                  @endphp
                  <option value=""></option>
                  @foreach ($brands as $key )
                    <option value="{{ $key->id }}">{{ $key->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  ชื่อสามัญ
                </div>
                <div class="col-md-6">
                  <input class="form-control" type="text" name="common_name" value="" id="common_name" required>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  ชื่อการขาย
                </div>
                <div class="col-md-6">
                  <input class="form-control" type="text" name="name" value="" id="name" required>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  ราคาขาย
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input class="form-control" type="text" name="price" id="price" onkeypress="return check_number(this);" required>
                    <span class="input-group-addon" >บาท</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  ราคาต้นทุน
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input class="form-control" type="text" name="cost" id="cost" onkeypress="return check_number(this);" required>
                    <span class="input-group-addon" >บาท</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  ขั้นต่ำ
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input class="form-control" type="text" name="order_minimum" id="order_minimum" onkeypress="return check_number(this)" required>
                    <span class="input-group-addon">ลัง</span>
                  </div>
                  {{-- <input class="form-control" type="text" name="" value=""> --}}
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  จำนวนชิ้น /ลัง
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input class="form-control" type="text" name="piece" id="piece" onkeypress="return check_number(this)" required>
                    <span class="input-group-addon">ชิ้น</span>
                  </div>
                  {{-- <input class="form-control" type="text" name="piece" value=""> --}}
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  ความสามารถในการจัดหา
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input class="form-control" type="text" name="supply" id="supply" onkeypress="return check_number(this)" required>
                    <span class="input-group-addon">ลัง / เดือน</span>
                  </div>
                  {{-- <input class="form-control" type="text" name="supply" value=""> --}}
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  เวลาส่งมอบ
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input class="form-control" type="text" name="time" id="time" onkeypress="return check_number(this)" required>
                    <span class="input-group-addon">วัน</span>
                  </div>
                  {{-- <input class="form-control" type="text" name="time" value=""> --}}
                </div>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12  text-center">
                โปรโมชั่น
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-4  text-center">
                จำนวน
              </div>
              <div class="col-md-4 text-center">
                <button type="button" class="btn btn-sm clone_promotion"><i class="fa fa-plus-circle mr-2"></i> เพิ่มโปรโมชั่น</button>
              </div>
              <div class="col-md-4 text-center">
                ส่วนลด
              </div>
            </div>
          </div>

          <div class='form-group'>
            <div class="row">
              <div class="col-md-1 text-center">
                1.
              </div>
              <div class="col-md-5">
                <div class="input-group">
                  <input class="form-control" type="text" name="amount[]" onkeypress="return check_number(this)" required>
                  <span class="input-group-addon" id="basic-addon2">ลัง</span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="input-group">
                  <input class="form-control" type="text" name="discount[]"  aria-describedby="basic-addon2" onkeypress="return check_number(this)" required>
                  <span class="input-group-addon" id="basic-addon2">%</span>
                </div>
              </div>
            </div>
          </div>
          <div id="addPromotion">

          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-6 text-center">
                รูปภาพ
              </div>
              <div class="col-md-6">
                <input class="form-control" id="files" type='file' name="img[]" multiple required/>
                <span style="color:red;">* สามารถใช้รูปภาพได้ไม่เกิน 5 รูป</span>
                {{-- <img id="blah" width="175px" height="175px" src="{{ asset('assets/images/no-photo.jpg') }}" alt="your image" /> --}}
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                ข้อมูลเบื้องต้น
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                <textarea class="detail_title" name="detail_title" rows="8" cols="80" required></textarea>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                ข้อมูลสินค้า
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                <textarea class="detail" name="detail" rows="8" cols="80" required></textarea>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                SKU ข้อมูลจำเพาะสินค้า
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-1  text-center">
                ข้อ
              </div>
              <div class="col-md-7 text-center">
                ข้อมูล
              </div>
              <div class="col-md-4 text-center">
                <button type="button" class="btn btn-sm clone_sku"><i class="fa fa-plus-circle mr-2"></i> เพิ่ม SKU</button>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-1  text-center">
                1.
              </div>
              <div class="col-md-11  text-center">
                <input type="text" class="form-control" name="sku[]" value="" required>
              </div>
            </div>
          </div>
          <div id="sku">

          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                กันเขตร้านค้า
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                <button class="btn btn-sm clone_zone" type="button" name="button"><i class="fa fa-plus-circle mr-2"></i> เพิ่มการกั้นเขต </button>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-4 text-center">
                จำนวนสินค้า
              </div>
              <div class="col-md-4 text-center">
                รัศมี
              </div>
              <div class="col-md-4 text-center">
                ระยะเวลา
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-1 text-center">
                1.
              </div>
              <div class="col-md-3 text-center">
                <input class="form-control" type="text" name="zone_num[]" value="" required>
              </div>
              <div class="col-md-4 text-center">
                <input class="form-control" type="text" name="zone_radius[]" value="" required>
              </div>
              <div class="col-md-4 text-center">
                <input class="form-control" type="text" name="zone_time[]" value="" required>
              </div>
            </div>
          </div>
          <div id="zone">

          </div>
        </div>
        <div class="modal-footer" style="">
          <button type="submit" href="javascript:void(0);" class="btn btn-success" style="margin-top:30px" onclick="" >เพิ่ม</button>
          <button class="btn btn-danger" type="reset" style="margin-top:30px" data-dismiss="modal">Close</button>
        </div>
      </form>

    </div>
  </div>
</div>
{{-- Modal show  --}}
<div class="modal fade" id="myModalShow"  role="dialog" aria-labelledby="myModalLabel" style="display: none;">
  <div class="modal-dialog" role="document" style="width:80%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times; </button> <span aria-hidden="true"> </span>
        <h3 class="text-center">รายละเอียดสินค้า</h3>
      </div>
      <form class="" id="show_product" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="modal-body">
          <div class="form-group">
            <div class="row">
              <div class="col-md-6 text-center">
                ประเภทสินค้า
              </div>
              <div class="col-md-6">
                <input type="text" class="form-control" name="" value="" id="category_show">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-6 text-center">
                ประภทสินค้าย่อย
              </div>
              <div class="col-md-6">
                <input type="text" class="form-control" name="" value="" id="category_sub_show">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-6 text-center">
                ยี่ห้อสินค้า
              </div>
              <div class="col-md-6">
                <input type="text" class="form-control" name="" value="" id="brand_show">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  ชื่อสามัญ
                </div>
                <div class="col-md-6">
                  <input class="form-control" type="text" name="common_name" value="" id="common_name_show" required>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  ชื่อการขาย
                </div>
                <div class="col-md-6">
                  <input class="form-control" type="text" name="name" value="" id="name_show" required>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  ราคาขาย
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input class="form-control" type="text" name="price" id="price_show" onkeypress="return check_number(this);" required>
                    <span class="input-group-addon" >บาท</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  ราคาต้นทุน
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input class="form-control" type="text" name="cost" id="cost_show" onkeypress="return check_number(this);" required>
                    <span class="input-group-addon" >บาท</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  ขั้นต่ำ
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input class="form-control" type="text" name="order_minimum" id="order_minimum_show" onkeypress="return check_number(this)" required>
                    <span class="input-group-addon">ลัง</span>
                  </div>
                  {{-- <input class="form-control" type="text" name="" value=""> --}}
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  จำนวนชิ้น /ลัง
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input class="form-control" type="text" name="piece" id="piece_show" onkeypress="return check_number(this)" required>
                    <span class="input-group-addon">ชิ้น</span>
                  </div>
                  {{-- <input class="form-control" type="text" name="piece" value=""> --}}
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  ความสามารถในการจัดหา
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input class="form-control" type="text" name="supply" id="supply_show" onkeypress="return check_number(this)" required>
                    <span class="input-group-addon">ลัง / เดือน</span>
                  </div>
                  {{-- <input class="form-control" type="text" name="supply" value=""> --}}
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  เวลาส่งมอบ
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input class="form-control" type="text" name="time" id="time_show" onkeypress="return check_number(this)" required>
                    <span class="input-group-addon">วัน</span>
                  </div>
                  {{-- <input class="form-control" type="text" name="time" value=""> --}}
                </div>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12  text-center">
                โปรโมชั่น
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-4  text-center">
                จำนวน
              </div>
              <div class="col-md-4 text-center">

              </div>
              <div class="col-md-4 text-center">
                ส่วนลด
              </div>
            </div>
          </div>
          <div id="show_promotion">

          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-12 text-center">
                รูปภาพ
              </div>
            </div>
          </div>

          <div class="tz-gallery" id="show_img">

          </div>

          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                ข้อมูลเบื้องต้น
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                <div class="fr-view show_detail_title" style="margin-top:50px;margin-bottom:50px;" >

                </div>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                ข้อมูลสินค้า
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                <div class="fr-view show_detail" style="margin-top:50px;margin-bottom:50px;">

                </div>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                SKU ข้อมูลจำเพาะสินค้า
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-1  text-center">
                ข้อ
              </div>
              <div class="col-md-11 text-center">
                ข้อมูล
              </div>

            </div>
          </div>
          <div id="show_sku">

          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                กันเขตร้านค้า
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-4 text-center">
                จำนวนสินค้า
              </div>
              <div class="col-md-4 text-center">
                รัศมี
              </div>
              <div class="col-md-4 text-center">
                ระยะเวลา
              </div>
            </div>
          </div>
          <div id="show_zone">

          </div>

        </div>

        <div class="modal-footer" style="">
          {{-- <button type="submit" href="javascript:void(0);" class="btn btn-success" style="margin-top:30px" onclick="" >เพิ่ม</button> --}}
          <button class="btn btn-danger" type="reset" style="margin-top:30px" data-dismiss="modal">Close</button>
        </div>
      </form>

    </div>
  </div>
</div>
{{-- Modal update  --}}
<div class="modal fade" id="myModalupdate"  role="dialog" aria-labelledby="myModalLabel" style="display: none;">
  <div class="modal-dialog" role="document" style="width:80%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times; </button> <span aria-hidden="true"> </span>
        <h3 class="text-center">แก้ไขสินค้า</h3>
      </div>
      <form class="" id="edit_product" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" id="id_hidden" >
        <div class="modal-body">
          <div class="form-group">
            <div class="row">
              <div class="col-md-6 text-center">
                ประเภทสินค้า
              </div>
              <div class="col-md-6">
                <select class="form-control" style="width:100%" name="category" id="edit_category">
                  @php
                  $category= \App\Models\ProductCategory::get();
                  @endphp
                  @foreach ($category as $key )
                    <option value="{{ $key->id }}">{{ $key->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-6 text-center">
                ประเภทสินค้าย่อย
              </div>
              <div class="col-md-6">
                <select class="categories form-control" style="width:100%" placeholder="เลือกประเภทสินค้าก่อน" name="sub_category" id="edit_sub_category" disabled>
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-6 text-center">
                ยี่ห้อสินค้า
              </div>
              <div class="col-md-6">
                <select class="form-control" style="width:100%" name="brand" id="edit_brand">
                  @php
                  $brands= \App\Models\ProductBrands::get();
                  @endphp
                  @foreach ($brands as $key )
                    <option value="{{ $key->id }}">{{ $key->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  ชื่อสามัญ
                </div>
                <div class="col-md-6">
                  <input class="form-control" type="text" name="common_name" value="" id="common_name_edit" required>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  ชื่อการขาย
                </div>
                <div class="col-md-6">
                  <input class="form-control" type="text" name="name" value="" id="name_edit" required>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  ราคาขาย
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input class="form-control" type="text" name="price" id="price_edit" onkeypress="return check_number(this);" required>
                    <span class="input-group-addon" >บาท</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  ราคาต้นทุน
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input class="form-control" type="text" name="cost" id="cost_edit" onkeypress="return check_number(this);" required>
                    <span class="input-group-addon" >บาท</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  ขั้นต่ำ
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input class="form-control" type="text" name="order_minimum" id="order_minimum_edit" onkeypress="return check_number(this)" required>
                    <span class="input-group-addon">ลัง</span>
                  </div>
                  {{-- <input class="form-control" type="text" name="" value=""> --}}
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  จำนวนชิ้น /ลัง
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input class="form-control" type="text" name="piece" id="piece_edit" onkeypress="return check_number(this)" required>
                    <span class="input-group-addon">ชิ้น</span>
                  </div>
                  {{-- <input class="form-control" type="text" name="piece" value=""> --}}
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  ความสามารถในการจัดหา
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input class="form-control" type="text" name="supply" id="supply_edit" onkeypress="return check_number(this)" required>
                    <span class="input-group-addon">ลัง / เดือน</span>
                  </div>
                  {{-- <input class="form-control" type="text" name="supply" value=""> --}}
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div style="">
                <div class="col-md-6 text-center">
                  เวลาส่งมอบ
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <input class="form-control" type="text" name="time" id="time_edit" onkeypress="return check_number(this)" required>
                    <span class="input-group-addon">วัน</span>
                  </div>
                  {{-- <input class="form-control" type="text" name="time" value=""> --}}
                </div>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12  text-center">
                โปรโมชั่น
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-4  text-center">
                จำนวน
              </div>
              <div class="col-md-4 text-center">
                <button type="button" class="btn btn-sm clone_promotion_edit"><i class="fa fa-plus-circle mr-2"></i> เพิ่มโปรโมชั่น</button>
              </div>
              <div class="col-md-4 text-center">
                ส่วนลด
              </div>
            </div>
          </div>
          <div id="edit_promotion">

          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-md-6 text-center">
                รูปภาพ
              </div>
              <div class="col-md-6" id="for_file">
                <input class="form-control" id="files_edit" type='file' name="img[]" multiple />
                {{-- <img id="blah" width="175px" height="175px" src="{{ asset('assets/images/no-photo.jpg') }}" alt="your image" /> --}}
              </div>
            </div>
          </div>

          <div class="tz-gallery" id="edit_img">

          </div>

          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                ข้อมูลเบื้องต้น
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                <textarea class="editor" name="detail_title" rows="30" id="detail_title_edit"></textarea>

                </div>
              </div>
            </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                ข้อมูลสินค้า
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                <textarea class="editor" name="detail" rows="30" id="detail_edit"></textarea>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                SKU ข้อมูลจำเพาะสินค้า
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-1  text-center">
                ข้อ
              </div>
              <div class="col-md-7 text-center">
                ข้อมูล
              </div>
              <div class="col-md-4 text-center">
                <button type="button" class="btn btn-sm clone_sku_edit"><i class="fa fa-plus-circle mr-2"></i> เพิ่ม SKU</button>
              </div>
            </div>
          </div>
          <div id="edit_sku">

          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                กันเขตร้านค้า
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                <button class="btn btn-sm clone_zone_edit" type="button" name="button"><i class="fa fa-plus-circle mr-2"></i> เพิ่มการกั้นเขต </button>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-4 text-center">
                จำนวนสินค้า
              </div>
              <div class="col-md-4 text-center">
                รัศมี
              </div>
              <div class="col-md-4 text-center">
                ระยะเวลา
              </div>
            </div>
          </div>
          <div id="edit_zone">

          </div>

        </div>

        <div class="modal-footer" style="">
          <button type="submit" href="javascript:void(0);" class="btn btn-success" style="margin-top:30px" onclick="" >บันทึก</button>
          <button class="btn btn-danger" type="reset" style="margin-top:30px" data-dismiss="modal">Close</button>
        </div>
      </form>

    </div>
  </div>
</div>


<table id="example" class="table">
  <thead>
    <tr>
      <th class="text-center" style="width: 5%;">No .</th>
      <th class="text-center" style="width: 20%;">ชื่อสามัญ</th>
      <th class="text-center" style="width: 35%;">ชื่อ</th>
      <th class="text-center" style="width: 20%;text-align:center;">รายละเอียดสินค้า</th>
      <th class="text-center" style="width: 20%;">Action</th>
    </tr>
  </thead>
  <tbody id="data">
    @php
    if(Auth::user()->member_type!="Admin"){
      $datas= \App\Models\Product::where('created_by', Auth::user()->id)->get();
    }else{
      $datas= \App\Models\Product::get();
    }

    $i=1;
    @endphp
    @foreach ($datas as $element)
      <tr>
        <td>{{ $i++ }}</td>
        <td>{{ $element->common_name }}</td>
        <td>{{ $element->name }}</td>
        <td><a href="javascript:void(0);" data-toggle="modal" data-target="#myModalShow" onclick="return show('{{ $element->id }}')" class="btn btn-primary">Details</a></td>
        <td>
          @if (Auth::user()->member_type=="Admin")
            <a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return edit({{$element->id}})" class="btn btn-warning">Edit</a>
            <a href='javascript:void(0);' onclick='return sweetalert({{$element->id}})' class='btn btn-danger'>Delete</a></td>
          @endif
        </tr>
      @endforeach
    </tbody>
  </table>

</section>
<!-- /.content -->
<script type="text/javascript">
var edit_promotion=0;
var check_img=0;
var zone_edit=0;
var sku_edit=0;
$("#show_product input").prop("readonly", true);
function check_number(salary) {
  var vchar = String.fromCharCode(event.keyCode);
  if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
  salary.onKeyPress=vchar;

}
$('#category').change(function(){
  $.ajax({
    url: '{{url('chooseCategory')}}/'+$('#category').val(),
    type: 'get',
    data: '',
    dataType:'json',
    success: function (html) {
      text="";
      if(html != 'Failed'){
        for (var i = 0; i < html.length; i++) {
          text+='<option value="'+html[i].id+'">'+html[i].name+'</option>';
        }
        if(html.length==0){
          $('#sub_category').empty();
          $('#sub_category').select2({
            placeholder:'ไม่มีประเภทสินค้าย่อย'
          });
          $("#sub_category").attr("disabled", true);
        }else{
          $('#sub_category').empty();
          $('#sub_category').html(text);
          $('#sub_category').removeAttr("disabled");
        }
      }
    }
  });
});
$('#edit_category').change(function(){
  $.ajax({
    url: '{{url('chooseCategory')}}/'+$('#edit_category').val(),
    type: 'get',
    data: '',
    dataType:'json',
    success: function (html) {
      text="";
      if(html != 'Failed'){
        for (var i = 0; i < html.length; i++) {
          text+='<option value="'+html[i].id+'">'+html[i].name+'</option>';
        }
        if(html.length==0){
          $('#edit_sub_category').empty();
          $('#edit_sub_category').select2({
            placeholder:'ไม่มีประเภทสินค้าย่อย'
          });
          $("#edit_sub_category").attr("disabled", true);
        }else{
          $('#edit_sub_category').empty();
          $('#edit_sub_category').html(text);
          $('#edit_sub_category').removeAttr("disabled");
        }
      }
    }
  });
});


$('#files').change(function(){
  //get the input and the file list
  var input = document.getElementById('files');
  var extall="jpg,jpeg,gif,png";
  if(input.files.length>5){
    swal("ผิดพลาด", "สามารถเพิ่มรูปได้ไม่เกิน 5 รูป", "error");
    $('#files').val("");
  }
for (var i=0; i<input.files.length; i++) {
    var ext = input.files[i].name.split('.').pop().toLowerCase();;
    if(parseInt(extall.indexOf(ext)) < 0) {
      swal({
        title: "กรุณาเลือกภาพที่ถูกต้อง",
        text: 'สามารถใช้ได้เฉพาะไฟล์ : ' + extall,
        icon: "warning",
        button: "OK",
      });
      $("#files").focus();
      $("#files").val('');
      return false;
    }
}
});
$('#files_edit').change(function(){
  //get the input and the file list
  var input = document.getElementById('files_edit');
  var extall="jpg,jpeg,gif,png";
  if(input.files.length>(5-check_img)){
    swal("ผิดพลาด", "สามารถเพิ่มรูปได้ไม่เกิน 5 รูป", "error");
    $('#files_edit').val("");
  }
for (var i=0; i<input.files.length; i++) {
    var ext = input.files[i].name.split('.').pop().toLowerCase();;
    if(parseInt(extall.indexOf(ext)) < 0) {
      swal({
        title: "กรุณาเลือกภาพที่ถูกต้อง",
        text: 'สามารถใช้ได้เฉพาะไฟล์ : ' + extall,
        icon: "warning",
        button: "OK",
      });
      $("#files_edit").focus();
      $("#files_edit").val('');
      return false;
    }
}
});
$('#insert_product').submit(function() {
  if($('#sub_category').val()==null){
    swal("ข้อมูลไม่ครบถ้วน", "กรุณาเลือกประเภทสินค้าย่อย!!", "warning");
    $('#sub_category').select2('open');
    return false;
  }
  if($('.categories').val()==""){
    swal({
      title: "ผิดพลาด",
      text: "กรุณาเลือกประเภทสินค้า",
      icon: "warning",
      button: "OK",
    });
    $('.categories').focus();
    return false;
  }
  if($('.brands').val()==""){
    swal({
      title: "ผิดพลาด",
      text: "กรุณาเลือกยี่ห้อสินค้า",
      icon: "warning",
      button: "OK",
    });
    $('.brands').focus();
    return false;
  }
  var amount = $("input[name='amount[]']");
  var discount = $("input[name='discount[]']");


  for (var i = 0; i < amount.length; i++) {
    if(amount.eq(i).val()==""){
      swal({
        title: "ผิดพลาด",
        text: "กรุณากรอกจำนวน",
        icon: "warning",
        button: "OK",
      });
      $("input[name='amount[]']")[i].focus();
      return false;
    }
    if(discount.eq(i).val()==""){
      swal({
        title: "ผิดพลาด",
        text: "กรุณากรอกส่วนลด",
        icon: "warning",
        button: "OK",
      });
      $("input[name='discount[]']")[i].focus();
      return false;
    }
  }

  var formData = new FormData($(this)[0]);
  var dataTable = $("#example").DataTable();
  $.ajax({
    url: '{{url('productController')}}',
    type: 'POST',
    data: formData,
    dataType:'json',
    success: function (html) {
      // alert(html);
      if(html != 'Failed'){
        // alert(html);
        swal("Successfully", "เพิ่มประเภทสินค้าสำเร็จ !!", "success");
        dataTable.clear();
        dataTable.rows.add(html).draw();
        $("#insert_product")[0].reset();
        $('#myModalInsert').modal('toggle');
      }
    },
    cache: false,
    contentType: false,
    processData: false
  });
return false;
});
function show(id) {
  $.ajax({
    url: '{{url('productController')}}'+'/'+id,
    type: 'GET',
    data: '',
    dataType:'json',
    success: function (html) {
      // alert(JSON.stringify(html));
      $('#category_show').val(html[0].product.nameCategory);
      $('#category_sub_show').val(html[0].product.nameSubCategory);
      $('#brand_show').val(html[0].product.brand);
      $('#common_name_show').val(html[0].product.common_name);
      $('#name_show').val(html[0].product.name);
      $('#price_show').val(html[0].product.price);
      $('#cost_show').val(html[0].product.cost);
      $('#order_minimum_show').val(html[0].product.order_minimum);
      $('#piece_show').val(html[0].product.piece);
      $('#supply_show').val(html[0].product.supply);
      $('#time_show').val(html[0].product.time);
      text="";
      for (var i = 0; i < html[0].promotion.length; i++) {
        text+='  <div class="form-group">';
        text+='<div class="row">';
        text+='<div class="col-md-1" style="vertical-align: middle;">';
        text+='<span class="text-center">'+(i+1)+'</span>';
        text+='</div>';
        text+='<div class="col-md-5">';
        text+='<div class="input-group">';
        text+='<input class="form-control" type="text" value="'+html[0].promotion[i].amount+'" readonly>';
        text+='<span class="input-group-addon" id="basic-addon2">ลัง</span>';
        text+='</div>';
        text+='</div>';
        text+='<div class="col-md-6">';
        text+='<div class="input-group">';
        text+='<input class="form-control" type="text" value="'+html[0].promotion[i].discount+'" readonly>';
        text+='<span class="input-group-addon" id="basic-addon2">%</span>';
        text+='</div>';
        text+='</div>';
        text+='</div>';
        text+='</div>';
      }
      $('#show_promotion').html(text);
      text="";
      for (var i = 0; i < html[0].img.length; i++) {

        // text+='<div class="col-md-6">';
        // text+='<img src="{{ url('/') }}/local/storage/app/images/product/'+html[0].img[i].name+'" width="175px" height="175px" />';
        // text+='</div>';
        text+='<div class="col-sm-6 col-md-4">';
        text+='<a class="lightbox" href="{{ asset('local/storage/app/images/product').'/'}}'+html[0].img[i].name+'">';
        text+='<img src="{{ asset('local/storage/app/images/product').'/'}}'+html[0].img[i].name+'"  width="250px" height="250px" >';
        text+='</a>';
        text+='</div>';
      }
      $('#show_img').html(text);
      $('.show_detail_title').html(html[0].product.detail_title);
      $('.show_detail').html(html[0].product.detail);
      text="";
      for (var i = 0; i < html[0].sku.length; i++) {
        text+='<div class="form-group" >';
        text+='<div class="row">';
        text+='<div class="col-md-1  text-center">';
        text+=i+1;
        text+='</div>';
        text+='<div class="col-md-11  text-center">';
        text+='<input type="text" class="form-control" value="'+html[0].sku[i].sku+'" readonly>';
        text+='</div>';
        text+='</div>';
        text+='</div>';
      }
      $('#show_sku').html(text);
      // alert(JSON.stringify(html[0].zone));
      text="";

      for (var i = 0; i < html[0].zone.length; i++) {

        text+='<div class="form-group" >';
        text+='<div class="row">';
        text+='<div class="col-md-1 text-center">';
        text+=(i+1)+'.';
        text+='</div>';
        text+='<div class="col-md-3 text-center">';
        text+='<input class="form-control" type="text" name="zone_num[]" value="'+html[0].zone[i].num+'" readonly>';
        text+='</div>';
        text+='<div class="col-md-4 text-center">';
        text+='<input class="form-control" type="text" name="zone_radius[]" value="'+html[0].zone[i].radius+'" readonly>';
        text+='</div>';
        text+='<div class="col-md-4 text-center">';
        text+='<input class="form-control" type="text" name="zone_time[]" value="'+html[0].zone[i].time+'" readonly>';
        text+='</div>';
        text+='</div>';
        text+='</div>';
      }
      $('#show_zone').html(text);
      baguetteBox.run('.tz-gallery');
    }
  });
}

function edit(id) {
  var formData = new FormData($(this)[0]);
  var dataTable = $("#example").DataTable();
  $.ajax({
    url: '{{url('productController')}}'+'/'+id,
    type: 'GET',
    data: '',
    dataType:'json',
    success: function (html) {

      $('#id_hidden').val(html[0].product.id);
      // alert($('#id_hidden').val());
      // alert(JSON.stringify(html));
      // $('#edit_category').select2("val",html[0].product.category_id);
      // $('#edit_brand').select2("val",html[0].product.brand_id);
      text="";
      for (var i = 0; i < html[0].sub.length; i++) {
        text+="<option value='"+html[0].sub[i].id+"'>"+html[0].sub[i].name+"</option>";
      }
      $('#edit_sub_category').empty();
      $('#edit_sub_category').html(text);
      $('#edit_sub_category').removeAttr("disabled");
      $("#edit_sub_category").val(html[0].product.category_sub_id).trigger('change');
      $('#detail_title_edit').froalaEditor('html.set', html[0].product.detail_title);
      $('#detail_edit').froalaEditor('html.set', html[0].product.detail);
      $('#detail_title_edit').val(html[0].product.detail_title);
      $('#detail_edit').val(html[0].product.detail);
      $("#edit_category").val(html[0].product.idCategory).trigger('change');
      $("#edit_brand").val(html[0].product.brand_id).trigger('change');
      $('#edit_category').select2();
      $('#edit_brand').select2();
      // $('#edit_category').val(html[0].product.brand_id).trigger('change');
      // $('#edit_brand').val(html[0].product.category_id).trigger('change');
      // $('#edit_category:eq('+html[0].product.category_id+')').prop('selected', true);
      // $('#edit_brand:eq('+html[0].product.brand_id+')').prop('selected', true);
      $('#common_name_edit').val(html[0].product.common_name);
      $('#name_edit').val(html[0].product.name);
      $('#price_edit').val(html[0].product.price);
      $('#cost_edit').val(html[0].product.cost);
      $('#order_minimum_edit').val(html[0].product.order_minimum);
      $('#piece_edit').val(html[0].product.piece);
      $('#supply_edit').val(html[0].product.supply);
      $('#time_edit').val(html[0].product.time);
      text="";
      edit_promotion=html[0].promotion.length;
      for (var i = 0; i < html[0].promotion.length; i++) {
        text+='  <div class="form-group">';
        text+='<div class="row">';
        text+='<div class="col-md-1 text-center" >';
        text+=(i+1)+'.';
        text+='</div>';
        text+='<div class="col-md-5">';
        text+='<div class="input-group">';
        text+='<input class="form-control" type="text" name="amount[]" value="'+html[0].promotion[i].amount+'" required>';
        text+='<span class="input-group-addon" id="basic-addon2">ลัง</span>';
        text+='</div>';
        text+='</div>';
        text+='<div class="col-md-6">';
        text+='<div class="input-group">';
        text+='<input class="form-control" type="text" name="discount[]" value="'+html[0].promotion[i].discount+'" required>';
        text+='<span class="input-group-addon" id="basic-addon2">%</span>';
        text+='</div>';
        text+='</div>';
        text+='</div>';
        text+='</div>';
      }
      $('#edit_promotion').html(text);
      text="";
      check_img=html[0].img.length;
      for (var i = 0; i < html[0].img.length; i++) {
        // text+='<div class="col-md-6">';
        // text+='<img src="{{ url('/') }}/local/storage/app/images/product/'+html[0].img[i].name+'" width="175px" height="175px" />';
        // text+='</div>';
        text+='<div class="col-sm-6 col-md-4">';
        text+='<a class="lightbox" href="{{ asset('local/storage/app/images/product').'/'}}'+html[0].img[i].name+'">';
        text+='<img src="{{ asset('local/storage/app/images/product').'/'}}'+html[0].img[i].name+'"  width="250px" height="250px" >';
        text+='</a>';
        text+='<div class="col-md-12 text-center" style="margin-bottom:40px"><button type="button" class="btn btn-danger" onclick="return delImg('+html[0].img[i].id+');"><i class="fa fa-times" aria-hidden="true"></i> Delete</button></div>';
        text+='</div>';
      }
      $('#edit_img').html(text);
      $('#edit_history').froalaEditor('html.set', html[0].product.detail_title);
      $('#edit_vision').froalaEditor('html.set', html[0].product.detail);
      $('#edit_history').val(html[0].product.detail_title);
      $('#edit_vision').val(html[0].product.detail);
      text="";
      sku_edit=html[0].sku.length;
      for (var i = 0; i < html[0].sku.length; i++) {
        text+='<div class="form-group" >';
        text+='<div class="row">';
        text+='<div class="col-md-1  text-center">';
        text+=i+1;
        text+='</div>';
        text+='<div class="col-md-11  text-center">';
        text+='<input type="text" class="form-control" name="sku[]" value="'+html[0].sku[i].sku+'">';
        text+='</div>';
        text+='</div>';
        text+='</div>';
      }
      $('#edit_sku').html(text);
      text="";
      for (var i = 0; i < html[0].zone.length; i++) {

        text+='<div class="form-group" >';
        text+='<div class="row">';
        text+='<div class="col-md-1 text-center">';
        text+=(i+1)+'.';
        text+='</div>';
        text+='<div class="col-md-3 text-center">';
        text+='<input class="form-control" type="text" name="zone_num[]" value="'+html[0].zone[i].num+'" >';
        text+='</div>';
        text+='<div class="col-md-4 text-center">';
        text+='<input class="form-control" type="text" name="zone_radius[]" value="'+html[0].zone[i].radius+'" >';
        text+='</div>';
        text+='<div class="col-md-4 text-center">';
        text+='<input class="form-control" type="text" name="zone_time[]" value="'+html[0].zone[i].time+'" >';
        text+='</div>';
        text+='</div>';
        text+='</div>';
      }
      text="";
      zone_edit=html[0].zone.length;
      for (var i = 0; i < html[0].zone.length; i++) {
        text+='<div class="form-group" >';
        text+='<div class="row">';
        text+='<div class="col-md-1 text-center">';
        text+=(i+1)+'.';
        text+='</div>';
        text+='<div class="col-md-3 text-center">';
        text+='<input class="form-control" type="text" name="zone_num[]" value="'+html[0].zone[i].num+'">';
        text+='</div>';
        text+='<div class="col-md-4 text-center">';
        text+='<input class="form-control" type="text" name="zone_radius[]" value="'+html[0].zone[i].radius+'">';
        text+='</div>';
        text+='<div class="col-md-4 text-center">';
        text+='<input class="form-control" type="text" name="zone_time[]" value="'+html[0].zone[i].time+'">';
        text+='</div>';
        text+='</div>';
        text+='</div>';
      }
      $('#edit_zone').html(text);
      baguetteBox.run('.tz-gallery');
    }
  });
}
$('#edit_product').submit(function () {
  if($('#edit_sub_category').val()==null){
    swal("ข้อมูลไม่ครบถ้วน", "กรุณาเลือกประเภทสินค้าย่อย!!", "warning");
    $('#edit_sub_category').select2('open');
    return false;
  }

  var formData = new FormData($(this)[0]);
  var dataTable = $("#example").DataTable();
  $.ajax({
    url: '{{url('productController')}}'+'/'+$('#id_hidden').val()+'/update',
    type: 'POST',
    data: formData,
    dataType:'json',
    success: function (html) {
      // alert(html);
      if(html != 'Failed'){
        swal("Successfully", "แก้ไขสำเร็จ!!", "success");
        dataTable.clear();
        dataTable.rows.add(html).draw();
        $("#edit_product")[0].reset();
        $('#myModalupdate').modal('toggle');
      }
    },cache: false,
      contentType: false,
      processData: false
  });
  return false;
});
// $('#edit_product').submit(function () {
//   var formData = new FormData($(this)[0]);
//   var dataTable = $("#example").DataTable();
//   $.ajax({
//       url: '{{url('productController')}}'+'/'+$('#id_hidden').val()+'/update',
//       type: 'POST',
//       data: formData,
//       dataType:'html',
//       success: function (html) {
//         alert(html);
//         // if(html != 'Failed'){
//         //   swal("Successfully", "เพิ่มประเภทสินค้าสำเร็จ !!", "success");
//         //   dataTable.clear();
//         //   dataTable.rows.add(html).draw();
//         //   $("#edit_brands")[0].reset();
//         //   $('#myModalupdate').modal('toggle');
//         // }
//       },cache: false,
//       contentType: false,
//       processData: false
//   });
//   return false;
// });
function delImg(id) {
  swal({
    title: "แน่ใจหรือ ?",
    text: "คุณต้องการลบข้อมูลนี้ใช่หรือไม่",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((sweetalert) => {
    if (sweetalert) {
      // setTimeout(function () {
      $.ajax({
        type: 'GET',
        url: '{{url('productController')}}'+'/'+id+'/delImg',
        data: '',
        dataType:'JSON',
        success:function(html){
          img='';
          for (var i = 0; i < html.length; i++) {
            img+='<div class="col-sm-6 col-md-4">';
            img+='<a class="lightbox" href="{{ asset('local/storage/app/images/product').'/'}}'+html[i].name+'">';
            img+='<img src="{{ asset('local/storage/app/images/product').'/'}}'+html[i].name+'"  width="250px" height="250px" >';
            img+='</a>';
            img+='<div class="col-md-12 text-center" style="margin-bottom:40px"><button type="button" class="btn btn-danger" onclick="return delImg('+html[i].id+');"><i class="fa fa-times" aria-hidden="true"></i> Delete</button></div>';
            img+='</div>';
          }
          $('#edit_img').html(img);
          swal("Successfully", "ลบรูปภาพเสร็จสิ้น !!", "success");
        }
      });
      // }, 1000);
    } else {
      swal("ยกเลิกการลบ !","","error");
    }
  });


}
function sweetalert(id) {
  var dataTable = $("#example").DataTable();
  swal({
    title: "แน่ใจหรือ ?",
    text: "คุณต้องการลบข้อมูลนี้ใช่หรือไม่",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((sweetalert) => {
    if (sweetalert) {
      // setTimeout(function () {
      $.ajax({
        type: 'DELETE',
        url: '{{url('productController')}}'+'/'+id,
        data: {_token: "{{ csrf_token() }}"},
        dataType:'json',
        success:function(html){
          // alert(JSON.stringify(html));
          swal("Successfully", "ลบเสร็จสิ้น !!", "success");
          dataTable.clear();
          dataTable.rows.add(html).draw();
        }
      });
      // }, 1000);
    } else {
      swal("ยกเลิกการลบ !","","error");
    }
  });
}
$(document).ready(function(){
  $('#example').DataTable({
    "order": [[ 0, "asc" ]]
  });
  $('.categories').select2({
    placeholder:'กรุณาเลือกประเภทสินค้า'
  });
  $('.brands').select2({
    placeholder:'กรุณาเลือกยี่ห้อสินค้า'
  });


});
  var num=2;
$('.clone_promotion').click(function (){
  text='<div class="form-group">';
  text+='<div class="row">';
  text+='<div class="col-md-1 text-center" >';
  text+=num+'.';
  text+='</div>';
  text+='<div class="col-md-5">';
  text+='<div class="input-group">';
  text+='<input class="form-control" type="text" name="amount[]" required >';
  text+='<span class="input-group-addon" id="basic-addon2">ลัง</span>';
  text+='</div>';
  text+='</div>';
  text+='<div class="col-md-6">';
  text+='<div class="input-group">';
  text+='<input class="form-control" type="text" name="discount[]"  required>';
  text+='<span class="input-group-addon" id="basic-addon2">%</span>';
  text+='</div>';
  text+='</div>';
  text+='</div>';
  text+='</div>';
  num++;
  $('#addPromotion').append(text);
});

$('.clone_promotion_edit').click(function (){
text='<div class="form-group">';
text+='<div class="row">';
text+='<div class="col-md-1 text-center" >';
text+=(edit_promotion+1)+'.';
text+='</div>';
text+='<div class="col-md-5">';
text+='<div class="input-group">';
text+='<input class="form-control" type="text" name="amount[]"  >';
text+='<span class="input-group-addon" id="basic-addon2">ลัง</span>';
text+='</div>';
text+='</div>';
text+='<div class="col-md-6">';
text+='<div class="input-group">';
text+='<input class="form-control" type="text" name="discount[]"  >';
text+='<span class="input-group-addon" id="basic-addon2">%</span>';
text+='</div>';
text+='</div>';
text+='</div>';
text+='</div>';
edit_promotion++;
$('#edit_promotion').append(text);
});
var sku=2;
$('.clone_sku').click(function (){
  text='<div class="form-group">';
  text+='<div class="row">';
  text+='<div class="col-md-1 text-center">';
  text+=sku+'.';
  text+='</div>';
  text+='<div class="col-md-11">';
  text+='<input type="text" class="form-control" name="sku[]" required>';
  text+='</div>';
  text+='</div>';
  text+='</div>';
  sku++;
  $('#sku').append(text);
});
$('.clone_sku_edit').click(function (){
  sku_edit++;
  text='<div class="form-group">';
  text+='<div class="row">';
  text+='<div class="col-md-1 text-center">';
  text+=sku_edit+'.';
  text+='</div>';
  text+='<div class="col-md-11">';
  text+='<input type="text" class="form-control" name="sku[]" >';
  text+='</div>';
  text+='</div>';
  text+='</div>';
  $('#edit_sku').append(text);
});
var zone=2;
$('.clone_zone').click(function (){
  text='<div class="form-group" >';
  text+='<div class="row">';
  text+='<div class="col-md-1 text-center">';
  text+=zone+'.';
  text+='</div>';
  text+='<div class="col-md-3 text-center">';
  text+='<input class="form-control" type="text" name="zone_num[]" value="" required>';
  text+='</div>';
  text+='<div class="col-md-4 text-center">';
  text+='<input class="form-control" type="text" name="zone_radius[]" value="" required>';
  text+='</div>';
  text+='<div class="col-md-4 text-center">';
  text+='<input class="form-control" type="text" name="zone_time[]" value="" required>';
  text+='</div>';
  text+='</div>';
  text+='</div>';
  zone++;
  $('#zone').append(text);
});

$('.clone_zone_edit').click(function (){
  zone_edit++;
  text='<div class="form-group" >';
  text+='<div class="row">';
  text+='<div class="col-md-1 text-center">';
  text+=zone_edit+'.';
  text+='</div>';
  text+='<div class="col-md-3 text-center">';
  text+='<input class="form-control" type="text" name="zone_num[]" value="">';
  text+='</div>';
  text+='<div class="col-md-4 text-center">';
  text+='<input class="form-control" type="text" name="zone_radius[]" value="">';
  text+='</div>';
  text+='<div class="col-md-4 text-center">';
  text+='<input class="form-control" type="text" name="zone_time[]" value="">';
  text+='</div>';
  text+='</div>';
  text+='</div>';

  $('#edit_zone').append(text);
});
$(function() {
  $('.detail_title').froalaEditor({
    toolbarButtons: ['undo', 'redo', 'html', '-', 'fontSize', 'paragraphFormat', 'align', 'quote', '|', 'formatOL', 'formatUL', '|', 'bold', 'italic', 'underline', '|', 'insertLink'],
  });
  $('.detail').froalaEditor({
    // toolbarButtons: ['undo', 'redo', 'html', '-', 'fontSize', 'paragraphFormat', 'align', 'quote', '|', 'formatOL', 'formatUL', '|', 'bold', 'italic', 'underline', '|', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable'],
      imageUploadURL: '{{ url('upload_image')}}',
      imageUploadParams: {
        froala: 'true', // This allows us to distinguish between Froala or a regular file upload.
        _token: "{{ csrf_token() }}" // This passes the laravel token with the ajax request.
      },
      imageStyles: {
        class1: 'Class 1',
        class2: 'Class 2'
      },
      imageEditButtons: ['imageReplace', 'imageAlign', 'imageRemove', '|', 'imageLink', 'linkOpen', 'linkEdit', 'linkRemove', '-', 'imageDisplay', 'imageStyle', 'imageAlt', 'imageSize'],
      zIndex:2000
  });
  $('#detail_title_edit').froalaEditor({
    toolbarButtons: ['undo', 'redo', 'html', '-', 'fontSize', 'paragraphFormat', 'align', 'quote', '|', 'formatOL', 'formatUL', '|', 'bold', 'italic', 'underline', '|', 'insertLink'],
  });
  $('#detail_edit').froalaEditor({
    // toolbarButtons: ['undo', 'redo', 'html', '-', 'fontSize', 'paragraphFormat', 'align', 'quote', '|', 'formatOL', 'formatUL', '|', 'bold', 'italic', 'underline', '|', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable'],
      imageUploadURL: '{{ url('upload_image')}}',
      imageUploadParams: {
        froala: 'true', // This allows us to distinguish between Froala or a regular file upload.
        _token: "{{ csrf_token() }}" // This passes the laravel token with the ajax request.
      },
      imageStyles: {
        class1: 'Class 1',
        class2: 'Class 2'
      },
      imageEditButtons: ['imageReplace', 'imageAlign', 'imageRemove', '|', 'imageLink', 'linkOpen', 'linkEdit', 'linkRemove', '-', 'imageDisplay', 'imageStyle', 'imageAlt', 'imageSize'],
      zIndex:2000
  });
});



</script>

</div>
@endsection
