@extends('backoffice.layouts.main')
@section('page_title','GomBeta | Brands')
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ยี่ห้อสินค้า
        <small>ยี่ห้อสินค้า</small>
        <p>
          {{-- {{Html::link('backoffice/add_admin','<i class="fa fa-plus-square" aria-hidden="true"></i>Add Admin',array('class'=>'btn btn-primary'))}} --}}

        </p>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-cog"></i> Backoffice</a></li>
        <li class="active">ยี่ห้อสินค้า</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      @if(Session::has('success'))
        {{-- <div class="alert alert-success">
          <ul>
          <li> {{ Session::get('success') }}</li>
          </ul>
        </div> --}}
        <script>
            swal("{{ Session::get('success') }}", "", "success");
        </script>
    @endif

    {{-- <script>
        swal("Good job!", "You clicked the button!", "success");
    </script> --}}
    <a  href="" data-toggle="modal" data-target="#myModalInsert"><button type="button" class="btn btn-primary"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มยี่ห้อสินค้า</button></a>
    <p></p>

        <div class="modal fade" id="myModalInsert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
            <div class="modal-dialog" role="document" style="width:80%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times; </button> <span aria-hidden="true"> </span>
                        <h3 class="text-center">เพิ่มยี่ห้อสินค้า</h3>
                      </div>
                        <div class="modal-body">
                          <div class="row">
                            <form class="" id="insert_brands" method="post" enctype="multipart/form-data">
                              {{ csrf_field() }}
                            <div class="col-md-12">
                              <div class="form-group">
                                <div class="row">
                                  <div class="col-md-6 text-center">
                                    ชื่อประเภทยี่ห้อ
                                  </div>
                                  <div class="col-md-6">
                                    <input class="form-control" type="text" name="name" value="" id="name_insert" required>
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-6 text-center">
                                    Logo
                                  </div>
                                  <div class="col-md-6">
                                    <input class="form-control" type="file" name="logo" value="" required>
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-6 text-center">
                                    Banner
                                  </div>
                                  <div class="col-md-6">
                                    <input class="form-control" type="file" name="banner[]" value="" multiple>
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-12 text-center">
                                    ประวัติบริษัท
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-12">
                                    <textarea class="editor" name="history" rows="30"></textarea>
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-12 text-center">
                                    วิสัยทัศน์
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-12">
                                    <textarea class="editor" name="vision" rows="30"></textarea>
                                  </div>
                                </div>
                              </div>
                              <div class='form-group' >
                                <div class="row">
                                  <div class="col-md-12  text-center">
                                    ภาพถ่ายและวีดีโอ
                                  </div>
                                </div>
                              </div>
                              <div class='form-group' >
                                <div class="row">
                                  <div class="col-md-4  text-center">
                                    รูปภาพ
                                  </div>
                                  <div class="col-md-4 text-center">
                                    <button type="button" class="btn btn-sm clone_img"><i class="fa fa-plus-circle mr-2"></i> เพิ่มรูปภาพ</button>
                                  </div>
                                  <div class="col-md-4 text-center">
                                    คำอธิบาย
                                  </div>
                                </div>
                              </div>

                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-6">
                                    <input class="form-control" type="file" name="gallary[]" value="">
                                  </div>
                                  <div class="col-md-6">
                                    <input class="form-control" type="text" name="caption[]" value="">
                                  </div>
                                </div>
                              </div>
                              <div id="Addimg">

                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-6 text-center">
                                    Video
                                  </div>
                                  <div class="col-md-6">
                                    <input type="text" id="insert_video" class="form-control" placeholder="example : https://www.youtube.com/watch?" onchange="return getId(this.value);" name="video">
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-12 text-center">
                                    ข้อมูลติดต่อ
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-6 text-center">
                                    เบอร์โทรศัพท์
                                  </div>
                                  <div class="col-md-6">
                                    <input class="form-control" type="text" name="phone" id="phone" value="" onkeypress="return check_number(this);">
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-6 text-center">
                                    เบอร์แแฟกซ์
                                  </div>
                                  <div class="col-md-6">
                                    <input class="form-control" type="text" name="fax" id="fax" value="" onkeypress="return check_number(this);">
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-6 text-center">
                                    เบอร์มือถือ
                                  </div>
                                  <div class="col-md-6">
                                    <input class="form-control" type="text" name="mobile" id="mobile" value="" onkeypress="return check_number(this);">
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-6 text-center">
                                    อีเมล์
                                  </div>
                                  <div class="col-md-6">
                                    <input class="form-control" type="text" name="email" value="">
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-6 text-center">
                                    ตำแหน่งที่ตั้ง
                                  </div>
                                  <div class="col-md-6">
                                    <textarea class="form-control" name="address" rows="8" cols="80"></textarea>
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-6 text-center">
                                    Website
                                  </div>
                                  <div class="col-md-6">
                                    <input class="form-control" type="text" name="website" value="">
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-6 text-center">
                                    Facebook
                                  </div>
                                  <div class="col-md-6">
                                    <input class="form-control" type="text" name="facebook" value="">
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-6 text-center">
                                    Line-ID
                                  </div>
                                  <div class="col-md-6">
                                    <input class="form-control" type="text" name="line" value="">
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="alert alert-light border-dashed mt-4 p-0 p-sm-3">
                                    <div class="row">
                                      <div class="col-md-12 mb-2">
                                        <h5 class="font-cloud p-3 p-sm-0"><i class="fa fa-map-marker mb-2 mr-2"></i>ตำแหน่งบนแผนที่ : <small>กรุณาระบุตำแหน่งโดยการเลื่อนหมุดสีแดงบนแผนที่ไปยังตำแหน่งที่ตั้งของคุณ เพื่อสิธิประโยชน์ทางการค้าค่ะ</small></h5>
                                        {{-- <input id="pac-input" class="controls" type="text" placeholder="Search Box"> --}}
                                        <div id="dvMap" style="height: 70vh; width: 100%;"></div>
                                      </div>
                                    </div>
                                  </div>
                              </div>
                              </div>

                            </div>
                          </div>
                        </div>
                    <div class="modal-footer" style="" >
                      <button type="submit" class="btn btn-success" style="">เพิ่ม</button>
                      {{-- <a href="javascript:void(0);" class="btn btn-success" style="margin-top:30px" onclick="return insert()" >เพิ่ม</a> --}}
                      <a href="" class="btn btn-danger" style="" data-dismiss="modal">Close</a>
                    </div>
                    <input type="hidden" name="lat" value="" id="lat">
                    <input type="hidden" name="lng" value="" id="lng">
                  </form>

                </div>
              </div>
            </div>
        {{-- Modal update  --}}
        <div class="modal fade" id="myModalupdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
          <div class="modal-dialog" role="document" style="width:80%">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times; </button> <span aria-hidden="true"> </span>
                <h3 class="text-center">แก้ไขประเภทสินค้า</h3>
              </div>
                <div class="modal-body">
                  <div class="row">
                    <form class="" id="edit_brands" method="PUT" enctype="multipart/form-data">
                      <input type="hidden" name="id" id="id_hidden">
                      {{ csrf_field() }}
                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6 text-center">
                            ชื่อประเภทยี่ห้อ
                          </div>
                          <div class="col-md-6">
                            <input class="form-control" type="text" name="name" value="" id="name_edit" required>
                          </div>
                        </div>
                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="col-md-6 text-center">
                            Logo
                          </div>
                          <div class="col-md-6">
                            <img src="" alt="" width="200px" height="200px" id="logo_edit">
                            <input class="form-control" type="file" name="logo" value="" >
                          </div>
                        </div>
                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="col-md-6 text-center">
                            Banner
                          </div>
                          <div class="col-md-6">
                            <input class="form-control" type="file" name="banner[]" value="" multiple>
                          </div>
                        </div>
                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="col-md-12 text-center">
                            Gallery Banner
                          </div>
                        </div>
                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="tz-gallery" id="gallery_banner_edit">

                          </div>
                        </div>
                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="col-md-12 text-center">
                            ประวัติบริษัท
                          </div>
                        </div>
                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="col-md-12">
                            <textarea class="editor" name="history" rows="30" id="edit_history"></textarea>
                          </div>
                        </div>
                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="col-md-12 text-center">
                            วิสัยทัศน์
                          </div>
                        </div>
                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="col-md-12">
                            <textarea class="editor" name="vision" rows="30" id="edit_vision"></textarea>
                          </div>
                        </div>
                      </div>
                      <div class='form-group' >
                        <div class="row">
                          <div class="col-md-12  text-center">
                            ภาพถ่ายและวีดีโอ
                          </div>
                        </div>
                      </div>
                      <div class='form-group' >
                        <div class="row">
                          <div class="col-md-4  text-center">
                            รูปภาพ
                          </div>
                          <div class="col-md-4 text-center">
                            <button type="button" class="btn btn-sm clone_img_edit"><i class="fa fa-plus-circle mr-2"></i> เพิ่มรูปภาพ</button>
                          </div>
                          <div class="col-md-4 text-center">
                            คำอธิบาย
                          </div>
                        </div>
                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="col-md-6">
                            <input class="form-control" type="file" name="gallary[]" value="">
                          </div>
                          <div class="col-md-6">
                            <input class="form-control" type="text" name="caption[]" value="">
                          </div>
                        </div>
                      </div>
                      <div id="Addimg_edit">

                      </div><div class='form-group' >
                        <div class="row">
                          <div class="col-md-12 text-center">
                            Gallery Images
                          </div>
                          <div class="col-md-6 text-center">
                            รูปภาพ
                          </div>
                          <div class="col-md-6 text-center">
                            คำอธิบาย
                          </div>
                        </div>
                      </div>
                      <div class="tz-gallery" id="gallery_img_edit">

                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="col-md-6 text-center">
                            Video
                          </div>
                          <div class="col-md-6">
                            <input type="text" id="insert_video_edit" class="form-control" placeholder="example : https://www.youtube.com/watch?" onchange="return getIdEdit(this.value);" name="video">
                          </div>
                        </div>
                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="col-md-12 text-center">
                            ข้อมูลติดต่อ
                          </div>
                        </div>
                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="col-md-6 text-center">
                            เบอร์โทรศัพท์
                          </div>
                          <div class="col-md-6">
                            <input class="form-control" type="text" name="phone" id="phone_edit" value="" onkeypress="return check_number(this);">
                          </div>
                        </div>
                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="col-md-6 text-center">
                            เบอร์แแฟกซ์
                          </div>
                          <div class="col-md-6">
                            <input class="form-control" type="text" name="fax" id="fax_edit" value="" onkeypress="return check_number(this);">
                          </div>
                        </div>
                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="col-md-6 text-center">
                            เบอร์มือถือ
                          </div>
                          <div class="col-md-6">
                            <input class="form-control" type="text" name="mobile" id="mobile_edit" value="" onkeypress="return check_number(this);">
                          </div>
                        </div>
                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="col-md-6 text-center">
                            อีเมล์
                          </div>
                          <div class="col-md-6">
                            <input class="form-control" type="text" name="email" value="" id="email_edit">
                          </div>
                        </div>
                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="col-md-6 text-center">
                            ตำแหน่งที่ตั้ง
                          </div>
                          <div class="col-md-6">
                            <textarea class="form-control" name="address" rows="8" cols="80" id="address_edit"></textarea>
                          </div>
                        </div>
                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="col-md-6 text-center">
                            Website
                          </div>
                          <div class="col-md-6">
                            <input class="form-control" type="text" name="website" value="" id="website_edit">
                          </div>
                        </div>
                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="col-md-6 text-center">
                            Facebook
                          </div>
                          <div class="col-md-6">
                            <input class="form-control" type="text" name="facebook" value="" id="facebook_edit">
                          </div>
                        </div>
                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="col-md-6 text-center">
                            Line-ID
                          </div>
                          <div class="col-md-6">
                            <input class="form-control" type="text" name="line" value="" id="line_edit">
                          </div>
                        </div>
                      </div>
                      <div class='form-group'>
                        <div class="row">
                          <div class="alert alert-light border-dashed mt-4 p-0 p-sm-3">
                            <div class="row">
                              <div class="col-md-12 mb-2">
                                <h5 class="font-cloud p-3 p-sm-0"><i class="fa fa-map-marker mb-2 mr-2"></i>ตำแหน่งบนแผนที่ : <small>กรุณาระบุตำแหน่งโดยการเลื่อนหมุดสีแดงบนแผนที่ไปยังตำแหน่งที่ตั้งของคุณ เพื่อสิธิประโยชน์ทางการค้าค่ะ</small></h5>
                                {{-- <input id="pac-input" class="controls" type="text" placeholder="Search Box"> --}}
                                <div id="dvMap_update" style="height: 70vh; width: 100%;"></div>
                              </div>
                            </div>
                          </div>
                      </div>
                      </div>

                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-success" style="">บันทึก</button>
                  {{-- <a href="javascript:void(0);" class="btn btn-success" style="margin-top:30px" >บันทึก</a> --}}
                  <a href="" class="btn btn-danger" style="" data-dismiss="modal">Close</a>
                </div>
                <input type="hidden" name="lat" value="" id="lat_edit">
                <input type="hidden" name="lng" value="" id="lng_edit">
              </form>
            </div>
          </div>
        </div>

        {{--Modal show--}}
        <div class="modal fade" id="myModalShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
            <div class="modal-dialog" role="document" style="width:80%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times; </button> <span aria-hidden="true"> </span>
                        <h3 class="text-center">รายละเอียดยี่ห้อสินค้า</h3>
                      </div>
                        <div class="modal-body">
                          <div class="row">
                            <form class="" id="show_brands" method="post" enctype="multipart/form-data">
                              {{ csrf_field() }}
                            <div class="col-md-12">
                              <div class="form-group">
                                <div class="row">
                                  <div class="col-md-12 text-center">
                                    <h4> ชื่อประเภทยี่ห้อ</h4>
                                  </div>
                                </div>
                              </div>
                              <div class="form-group">
                                <div class="row">
                                  <div class="col-md-12">
                                    <div id="name_bands" class="text-center">

                                    </div>
                                    {{-- <input class="form-control" type="text" name="name" value="" > --}}
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-12 text-center">
                                  <h4>  Logo</h4>
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="tz-gallery" id="logo">

                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-12 text-center">
                                    <h4>Banner</h4>
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="tz-gallery" id="banner">


                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-12 text-center">
                                    <h4>ประวัติบริษัท</h4>
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="fr-view" id="history">

                                    </div>
                                    {{-- <textarea class="form-control" name="history" id="history" rows="30"></textarea> --}}
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-12 text-center">
                                    <h4>วิสัยทัศน์</h4>
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="fr-view" id="vision">

                                    </div>
                                    {{-- <textarea class="form-control" name="vision" id="vision" rows="30"></textarea> --}}
                                  </div>
                                </div>
                              </div>
                              <div class='form-group' >
                                <div class="row">
                                  <div class="col-md-12  text-center">
                                    <h4>ภาพถ่ายและวีดีโอ</h4>
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="tz-gallery" id="img">


                                  </div>
                                  </div>
                                </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-12" id="video">

                                  </div>
                                </div>
                              </div>
                              <div class='form-group' >
                                <div class="row">
                                  <div class="col-md-12  text-center">
                                    <h4>ข้อมูลติดต่อ</h4>
                                  </div>
                                </div>
                              </div>
                              <div class='form-group' >
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="col-md-3">
                                      <div class='form-group' >
                                        <div class="row">
                                          เบอร์โทรศัพท์ :
                                        </div>
                                      </div>
                                      <div class='form-group' >
                                        <div class="row">
                                          เบอร์แแฟกซ์ :
                                        </div>
                                      </div>
                                      <div class='form-group' >
                                        <div class="row">
                                          เบอร์มือถือ :
                                        </div>
                                      </div>
                                      <div class='form-group' >
                                        <div class="row">
                                          อีเมล์ :
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class='form-group' >
                                        <div class="row" id="show_phone">
                                        </div>
                                      </div>
                                      <div class='form-group' >
                                        <div class="row" id="show_fax">
                                        </div>
                                      </div>
                                      <div class='form-group' >
                                        <div class="row" id="show_mobile">
                                        </div>
                                      </div>
                                      <div class='form-group' >
                                        <div class="row" id="show_email">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class='form-group' >
                                        <div class="row">
                                          ตำแหน่งที่ตั้ง :
                                        </div>
                                      </div>
                                      <div class='form-group' >
                                        <div class="row">
                                          Website :
                                        </div>
                                      </div>
                                      <div class='form-group' >
                                        <div class="row">
                                          Facebook :
                                        </div>
                                      </div>
                                      <div class='form-group' >
                                        <div class="row">
                                          Line-ID :
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class='form-group' >
                                        <div class="row" id="show_address">
                                        </div>
                                      </div>
                                      <div class='form-group' >
                                        <div class="row" id="show_website">
                                        </div>
                                      </div>
                                      <div class='form-group' >
                                        <div class="row" id="show_facebook">
                                        </div>
                                      </div>
                                      <div class='form-group' >
                                        <div class="row" id="show_line">
                                        </div>
                                      </div>
                                      </div>
                                  </div>
                                </div>
                              </div>
                              <div class='form-group'>
                                <div class="row">
                                  <div class="col-md-12 text-center">
                                  <h4>แผ่นที่</h4>
                                  </div>
                                </div>
                              </div>

                              <div class='form-group'>
                                <div class="row">
                                  <div class="alert alert-light border-dashed mt-4 p-0 p-sm-3">
                                    <div class="row">
                                      <div class="col-md-12 mb-2">
                                        <div id="mapDealers" style="height: 70vh; width: 100%;"></div>
                                      </div>
                                    </div>
                                  </div>
                              </div>
                              </div>


                            </div>
                          </div>

                    <div class="modal-footer" style="" >
                      {{-- <a href="javascript:void(0);" class="btn btn-success" style="margin-top:30px" onclick="return insert()" >เพิ่ม</a> --}}
                      <a href="" class="btn btn-danger" style="" data-dismiss="modal">Close</a>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <table id="example" class="table">
            <thead>
              <tr>
                <th class="text-center" style="width: 5%;">No .</th>
                <th class="text-center" style="width: 50%;">Brand name</th>
                <th class="text-center" style="width: 25%;text-align:center;">Details</th>
                <th class="text-center" style="width: 20%;">Action</th>
              </tr>
            </thead>
            <tbody id="data">
              @php
              $datas= \App\Models\ProductBrands::get();
              $i=1;
              @endphp
              @foreach ($datas as $element)
                <tr>
                  <td>{{ $i++ }}</td>
                  <td>{{ $element->name }}</td>
                  <td class="text-center"><a href="javascript:void(0);" data-toggle="modal" data-target="#myModalShow" onclick="return show({{$element->id}})" class="btn btn-primary">Details</a></td>
                  <td class="text-center">
                    @if (Auth::user()->member_type=="Admin")
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return edit({{$element->id}})" class="btn btn-warning">Edit</a>
                    <a href='javascript:void(0);' onclick='return sweetalert({{$element->id}})' class='btn btn-danger'>Delete</a></td>
                    @endif
                  </tr>
                @endforeach
              </tbody>
            </table>

    </section>
    <!-- /.content -->
    <script type="text/javascript">
    function checkall() {
      var certificate = $("input[name='gallary[]']");
      var certificate_number = $("input[name='caption[]']");
      var extall="jpg,jpeg,gif,png";

      // alert(certificate.length);
      for (var i = 0; i < certificate.length; i++) {
        ext = certificate.eq(i).val().split('.').pop().toLowerCase();
        if(certificate.eq(i).val()==""){
          swal({
            title: "กรุณาเลือกรูปภาพถ่าย",
            text: "เพื่อประโยชน์ของท่านกรุณาเลือกรูปภาพถ่าย",
            icon: "warning",
            button: "OK",
          });
          $("input[name='gallary[]']")[i].focus();
          return false;
        }else{
          if(parseInt(extall.indexOf(ext)) < 0) {
            swal({
              title: "กรุณาเลือกภาพที่ถูกต้อง",
              text: 'สามารถใช้ได้เฉพาะไฟล์ : ' + extall,
              icon: "warning",
              button: "OK",
            });
            $("input[name='gallary[]']")[i].focus();
            certificate.eq(i).val('');
            return false;
          }
        }
        if (certificate_number.eq(i).val()=="") {
          swal({
            title: "กรุณากรอกหมายเลขใบเซอร์",
            text: "เพื่อประโยชน์ของท่านกรุณากรอกหมายเลขใบเซอร์",
            icon: "warning",
            button: "OK",
          });
          $("input[name='caption[]']")[i].focus();
          return false;
        }
      }
    }


function check_number(num) {
  var vchar = String.fromCharCode(event.keyCode);
  if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
  num.onKeyPress=vchar;

}
    // convert link youtube to embed
    $("#show_brands :input").attr("disabled", true);
    $('.clone_img').click(function (){
      text='<div class="form-group">';
      text+='<div class="row">';
      text+='<div class="col-md-6">';
      text+='<input class="form-control" type="file" name="gallary[]" value="">';
      text+='</div>';
      text+='<div class="col-md-6">';
      text+='<input class="form-control" type="text" name="caption[]" value="">';
      text+='</div>';
      text+='</div>';
      text+='</div>';
      $('#Addimg').append(text);
    });
    $('.clone_img_edit').click(function (){
      text='<div class="form-group">';
      text+='<div class="row">';
      text+='<div class="col-md-6">';
      text+='<input class="form-control" type="file" name="gallary[]" value="">';
      text+='</div>';
      text+='<div class="col-md-6">';
      text+='<input class="form-control" type="text" name="caption[]" value="">';
      text+='</div>';
      text+='</div>';
      text+='</div>';
      $('#Addimg_edit').append(text);
    });
    function getId(url) {
       regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
       match = url.match(regExp);

      if (match && match[2].length == 11) {
        $('#insert_video').val('https://www.youtube.com/embed/'+match[2]);
      } else {
        alert( 'สามารถใช้ลิงค์จาก Youtube เท่านั้น' );
        $('#insert_video').val('');
      }
    }
    function getIdEdit(url) {
       regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
       match = url.match(regExp);

      if (match && match[2].length == 11) {
        $('#insert_video_edit').val('https://www.youtube.com/embed/'+match[2]);
      } else {
        alert( 'สามารถใช้ลิงค์จาก Youtube เท่านั้น' );
        $('#insert_video_edit').val('');
      }
    }

    $(document).ready(function(){
      $('#example').DataTable({
          "order": [[ 0, "asc" ]]
      });
    });
    $('#insert_brands').submit(function () {
      var formData = new FormData($(this)[0]);
      var dataTable = $("#example").DataTable();
      $.ajax({
          url: '{{url('product_bands')}}',
          type: 'POST',
          data: formData,
          dataType:'JSON',
          success: function (html) {
            if(html != 'Failed'){
              // alert(html);
              swal("Successfully", "เพิ่มประเภทสินค้าสำเร็จ !!", "success");
              dataTable.clear();
              dataTable.rows.add(html).draw();
              $("#insert_brands")[0].reset();
              $('#myModalInsert').modal('toggle');
            }
            baguetteBox.run('.tz-gallery');
          },
          cache: false,
          contentType: false,
          processData: false
      });

      return false;
  });


    function show(id) {
      // getDistance(markers[0].lat,markers[0].lng);
      // alert(markers[0].lat+'\n'+markers[0].lng);
      $.ajax({
        type: 'GET',
        url: '{{url('product_bands')}}'+'/'+id,
        data: '',
        dataType:'JSON',
        success:function(html){
          $('#name_bands').html(html[0].brand.name);
          logo='<div class="col-md-4"></div>'
          logo+='<div class="col-sm-6 col-md-4">';
          logo+='<a class="lightbox" href="{{ asset('local/storage/app/images/logo').'/'}}'+html[0].brand.logo+'">';
          logo+='<img src="{{ asset('local/storage/app/images/logo').'/'}}'+html[0].brand.logo+'"  width="175" height="175" style="margin-left: auto;margin-right: auto;display: block;">';
          logo+='</a>';
          logo+='</div>';
          banner='';
          img='';
          for (var i = 0; i < html[0].banner.length; i++) {
            banner+='<div class="col-sm-6 col-md-4">';
            banner+='<a class="lightbox" href="{{ asset('local/storage/app/images/banner').'/'}}'+html[0].banner[i].banner+'">';
            banner+='<img src="{{ asset('local/storage/app/images/banner').'/'}}'+html[0].banner[i].banner+'"  width="250px" height="250px" >';
            banner+='</a>';
            banner+='</div>';          }
          for (var i = 0; i < html[0].img.length; i++) {
            // img+='<button class="btn btn-danger" type="button">del</button>';
            img+='<div class="col-sm-6 col-md-4">';
            img+='<a class="lightbox" href="{{ asset('local/storage/app/images/img').'/'}}'+html[0].img[i].img+'">';
            img+='<img src="{{ asset('local/storage/app/images/img').'/'}}'+html[0].img[i].img+'"  width="250px" height="250px" >';
            img+='</a>';
            img+='</div>';

            // img+='<div class="row"><img class="img-thumbnail" src="{{ url('/') }}'+'/local/storage/app/images/img/'+html[0].img[i].img+'"  style="margin-left: auto;margin-right: auto;display: block;" /></div>'
          }
          video='';
          if(html[0].brand.video!=null){
            video='<iframe  style="width: 100%; height: 380px;" frameborder="0" allowfullscreen src="'+html[0].brand.video+'"></iframe>';
          }
          // alert(html[0].banner.length);
          $('#logo').html(logo);
          $('#banner').html(banner);
          $('#history').html(html[0].brand.history);
          $('#vision').html(html[0].brand.vision);
          $('#img').html(img);
          $('#video').html(video);
          $('#show_phone').html(html[0].brand.phone);
          $('#show_fax').html(html[0].brand.fax);
          $('#show_mobile').html(html[0].brand.mobile);
          $('#show_email').html(html[0].brand.email);
          $('#show_address').html(html[0].brand.address);
          $('#show_website').html(html[0].brand.website);
          $('#show_facebook').html(html[0].brand.facebook);
          $('#show_line').html(html[0].brand.line);
          var lat_change=parseFloat(html[0].brand.lat);
          var lng_change=parseFloat(html[0].brand.lng);

          var p1 ={
              "lat": parseFloat(html[0].brand.lat),
              "lng": parseFloat(html[0].brand.lng),
            };
          var p2 = {
              "lat": parseFloat(markers[0].lat),
              "lng": parseFloat(markers[0].lng),
            };
            var rad = function(x) {
              return x * Math.PI / 180;
            };

            var getDistance = function(p1, p2) {
              var R = 6378137; // Earth’s mean radius in meter
              var dLat = rad(p2.lat - p1.lat);
              var dLong = rad(p2.lng - p1.lng);
              var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
              Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
              Math.sin(dLong / 2) * Math.sin(dLong / 2);
              var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
              var d = (R * c)/1000;
              return d; // returns the distance in meter
            };
          console.log(parseFloat(getDistance(p1,p2)).toFixed(2)+" KM");
          maps(lat_change,lng_change);
          baguetteBox.run('.tz-gallery');
          // $('#note').val(html.note);
        }
    });
    }
    function edit(id) {
      $.ajax({
        type: 'GET',
        url: '{{url('product_bands')}}'+'/'+id,
        data: '',
        dataType:'JSON',
        success:function(html){
          $('#id_hidden').val(html[0].brand.id);
          $('#name_edit').val(html[0].brand.name);
          $('#logo_edit').attr('src','{{ url('/') }}'+'/local/storage/app/images/logo/'+html[0].brand.logo);
          banner='';

          for (var i = 0; i < html[0].banner.length; i++) {
            banner+='<div class="col-sm-6 col-md-4">';
            banner+='<a class="lightbox" href="{{ asset('local/storage/app/images/banner').'/'}}'+html[0].banner[i].banner+'">';
            banner+='<img src="{{ asset('local/storage/app/images/banner').'/'}}'+html[0].banner[i].banner+'"  width="250px" height="250px" >';
            banner+='</a>';
            banner+='<div class="text-center" style="margin-bottom:40px"><button type="button" class="btn btn-danger" onclick="return delBanner('+html[0].banner[i].id+');"><i class="fa fa-times" aria-hidden="true"></i> Delete</button></div>';
            banner+='</div></div>';
            // banner+='<div class="col-md-4 text-center"><img id="banner'+i+'" width="350px" height="350px" class="img" src="{{ url('/') }}'+'/local/storage/app/images/banner/'+html[0].banner[i].banner+'"/>';
          }
          $('#gallery_banner_edit').html(banner);
          $('#edit_history').froalaEditor('html.set', html[0].brand.history);
          $('#edit_vision').froalaEditor('html.set', html[0].brand.vision);
          $('#edit_history').val(html[0].brand.history);
          $('#edit_vision').val( html[0].brand.vision);

          img='';
          for (var i = 0; i < html[0].img.length; i++) {
            img+='<div class="form-group">';
            img+='<div class="row">';
            img+='<div class="col-sm-6 col-md-6">';
            img+='<a class="lightbox" href="{{ asset('local/storage/app/images/img').'/'}}'+html[0].img[i].img+'">';
            img+='<img src="{{ asset('local/storage/app/images/img').'/'}}'+html[0].img[i].img+'"  width="250px" height="250px" >';
            img+='</a>';
            img+='</div>';
            img+='<div class="col-md-6">';
            img+=html[0].img[i].caption;
            img+='</div>';
            img+='<div class="col-md-12 text-center"><button type="button" class="btn btn-danger" onclick="return delImg('+html[0].img[i].id+');"><i class="fa fa-times" aria-hidden="true"></i> Delete</button></div>';
            img+='</div>';
            img+='</div>';
          }
          $('#gallery_img_edit').html(img);
          $('#insert_video_edit').val(html[0].brand.video);
          $('#phone_edit').val(html[0].brand.phone);
          $('#fax_edit').val(html[0].brand.fax);
          $('#mobile_edit').val(html[0].brand.mobile);
          $('#email_edit').val(html[0].brand.email);
          $('#address_edit').val(html[0].brand.address);
          $('#website_edit').val(html[0].brand.website);
          $('#facebook_edit').val(html[0].brand.facebook);
          $('#line_edit').val(html[0].brand.line);
          getMapsEdit(html[0].brand.lat,html[0].brand.lng,html[0].brand.address);
          baguetteBox.run('.tz-gallery');
          // $('#edit_history').val(html[0].brand.history);
          // $('#edit_vision').val(html[0].brand.vision);

          // if(html != 'Failed'){
          //   swal("Successfully", "แก้ไขเสร็จสิ้น !!", "success");
          //   dataTable.clear();
          //   dataTable.rows.add(html).draw();
          //   $("#update_category")[0].reset();
          //   $('#myModalupdate').modal('toggle');
          // }
        }
    });
    }
    $('#edit_brands').submit(function () {
      var formData = new FormData($(this)[0]);
      var dataTable = $("#example").DataTable();
      $.ajax({
          url: '{{url('product_bands')}}'+'/'+$('#id_hidden').val()+'/update',
          type: 'POST',
          data: formData,
          dataType:'JSON',
          success: function (html) {
            if(html != 'Failed'){
              swal("Successfully", "เพิ่มประเภทสินค้าสำเร็จ !!", "success");
              dataTable.clear();
              dataTable.rows.add(html).draw();
              $("#edit_brands")[0].reset();
              $('#myModalupdate').modal('toggle');
            }
          },cache: false,
          contentType: false,
          processData: false
      });
      return false;
  });
    function delBanner(id) {
      swal({
        title: "แน่ใจหรือ ?",
        text: "คุณต้องการลบข้อมูลนี้ใช่หรือไม่",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((sweetalert) => {
        if (sweetalert) {
          // setTimeout(function () {
          $.ajax({
            type: 'GET',
            url: '{{url('product_bands')}}'+'/'+id+'/delBanner',
            data: '',
            dataType:'JSON',
            success:function(html){
              banner='';
              for (var i = 0; i < html.length; i++) {
                banner+='<div class="col-sm-6 col-md-4">';
                banner+='<a class="lightbox" href="{{ asset('local/storage/app/images/banner').'/'}}'+html[i].banner+'">';
                banner+='<img src="{{ asset('local/storage/app/images/banner').'/'}}'+html[i].banner+'"  width="250px" height="250px" >';
                banner+='</a>';
                banner+='<div class="text-center" style="margin-bottom:40px"><button type="button" class="btn btn-danger" onclick="return delBanner('+html[0].banner[i].id+');"><i class="fa fa-times" aria-hidden="true"></i> Delete</button></div>';
                banner+='</div></div>';
              }
              $('#gallery_banner_edit').html(banner);
              swal("Successfully", "ลบ Banner เสร็จสิ้น !!", "success");
            }
          });
          // }, 1000);
        } else {
          swal("ยกเลิกการลบ !","","error");
        }
      });


    }
    function delImg(id) {
      swal({
        title: "แน่ใจหรือ ?",
        text: "คุณต้องการลบข้อมูลนี้ใช่หรือไม่",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((sweetalert) => {
        if (sweetalert) {
          // setTimeout(function () {
          $.ajax({
            type: 'GET',
            url: '{{url('product_bands')}}'+'/'+id+'/delImg',
            data: '',
            dataType:'JSON',
            success:function(html){
              img='';
              for (var i = 0; i < html.length; i++) {
                img+='<div class="form-group">';
                img+='<div class="row">';
                img+='<div class="col-sm-6 col-md-6">';
                img+='<a class="lightbox" href="{{ asset('local/storage/app/images/img').'/'}}'+html[i].img+'">';
                img+='<img src="{{ asset('local/storage/app/images/img').'/'}}'+html[i].img+'"  width="250px" height="250px" >';
                img+='</a>';
                img+='</div>';
                img+='<div class="col-md-6">';
                img+=html[i].caption;
                img+='</div>';
                img+='<div class="col-md-12 text-center"><button type="button" class="btn btn-danger" onclick="return delImg('+html[0].img[i].id+');"><i class="fa fa-times" aria-hidden="true"></i> Delete</button></div>';
                img+='</div>';
                img+='</div>';
              }
              $('#gallery_img_edit').html(img);
              swal("Successfully", "ลบรูปภาพเสร็จสิ้น !!", "success");
            }
          });
          // }, 1000);
        } else {
          swal("ยกเลิกการลบ !","","error");
        }
      });


    }

    function sweetalert(id) {
      var dataTable = $("#example").DataTable();
      swal({
    title: "แน่ใจหรือ ?",
    text: "คุณต้องการลบข้อมูลนี้ใช่หรือไม่",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((sweetalert) => {
    if (sweetalert) {
      // setTimeout(function () {
      $.ajax({
        type: 'DELETE',
        url: '{{url('product_bands')}}'+'/'+id,
        data: {_token: "{{ csrf_token() }}"},
        dataType:'JSON',
        success:function(html){
            swal("Successfully", "ลบเสร็จสิ้น !!", "success");
            dataTable.clear();
            dataTable.rows.add(html).draw();
          }
    });
  // }, 1000);
    } else {
      swal("ยกเลิกการลบ !","","error");
    }
  });
    }
    function expand(id) {
      $('#'+id).removeAttr("width");
      $('#'+id).removeAttr("height");
    }
    </script>
    <script>
    $(function() {
  $('.editor').froalaEditor({
    // toolbarButtons: ['undo', 'redo', 'html', '-', 'fontSize', 'paragraphFormat', 'align', 'quote', '|', 'formatOL', 'formatUL', '|', 'bold', 'italic', 'underline', '|', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable'],
      imageUploadURL: '{{ url('upload_image')}}',
      imageUploadParams: {
        froala: 'true', // This allows us to distinguish between Froala or a regular file upload.
        _token: "{{ csrf_token() }}" // This passes the laravel token with the ajax request.
      },
      imageStyles: {
        class1: 'Class 1',
        class2: 'Class 2'
      },
      imageEditButtons: ['imageReplace', 'imageAlign', 'imageRemove', '|', 'imageLink', 'linkOpen', 'linkEdit', 'linkRemove', '-', 'imageDisplay', 'imageStyle', 'imageAlt', 'imageSize'],
      zIndex:2000
  });
});
</script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGqNz-KUAKL435y6zraREiT-o5s4tuiEM&callback=initMap"></script>
<script type="text/javascript">
var allow='' ;
// Referance >>> https://www.aspsnippets.com/Articles/Google-Maps-V3-Draggable-Markers-Example-Drag-and-Drop-Markers-in-Google-Maps-using-JavaScript.aspx
var markers = [
  {
    "title": 'Your location',
    "lat": '',
    "lng": '',
    "description": 'Your location'
  }
];
function getLocation() {

  navigator.geolocation.watchPosition(function(position) {
    allow='allow';
  console.log("i'm tracking you!");
  console.log(allow);

},
function (error) {
  if (error.code == error.PERMISSION_DENIED)
      console.log("you denied me :-(");
      allow='denined';
});
setTimeout(function(){
  if(allow==''){
    swal({
      title: "กรุณาอนุญาตการเข้าถึงตำแหน่งของคุณ ",
      text: "เพื่อความสะดวกของตัวท่าน กรุณาอนุญาตการเข้าถึงตำแหน่งของคุณ",
      icon: "warning",
      button: "OK",
    });
  }
}, 3000);

  if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        alert("Geolocation is not supported by this browser.");
    }
    getMaps();

}
function showPosition(position) {
  markers[0].lat=position.coords.latitude;
  markers[0].lng=position.coords.longitude;
  $('#lat').val(position.coords.latitude);
  $('#lng').val(position.coords.longitude);
  getMaps();
}
window.onload = function () {

  getLocation();

}
function getMaps() {
var mapOptions = {
    center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
    zoom: 12,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var infoWindow = new google.maps.InfoWindow();
  var latlngbounds = new google.maps.LatLngBounds();
  var geocoder = geocoder = new google.maps.Geocoder();
  var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
  for (var i = 0; i < markers.length; i++) {
    var data = markers[i]
    var myLatlng = new google.maps.LatLng(data.lat, data.lng);
    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: data.title,
      draggable: true,
      animation: google.maps.Animation.DROP
    });
    (function (marker, data) {
      google.maps.event.addListener(marker, "click", function (e) {
        infoWindow.setContent(data.description);
        infoWindow.open(map, marker);
      });
      google.maps.event.addListener(marker, "dragend", function (e) {
        var lat, lng, address;
        geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            lat = marker.getPosition().lat();
            lng = marker.getPosition().lng();
            address = results[0].formatted_address;
            $('#lat').val(lat);
            $('#lng').val(lng);
            // alert($('#lat').val()+'\n'+$('#lng').val());
            // alert("Latitude: " + lat + "\nLongitude: " + lng + "\nAddress: " + address);

          }
        });
      });
    })(marker, data);
    latlngbounds.extend(marker.position);
  }

  // var bounds = new google.maps.LatLngBounds();   // For Multiple markers
  // map.setCenter(latlngbounds.getCenter());       // For Multiple markers
  // map.fitBounds(latlngbounds);                   // For Multiple markers
}

function getMapsEdit(lat,lng,description) {
  $('#lat_edit').val(lat);
  $('#lng_edit').val(lng);
  markers = [
    {
      "title": 'Your location',
      "lat": lat,
      "lng": lng,
      "description": description
    }
  ]
var mapOptions = {
    center: new google.maps.LatLng(lat, lng),
    zoom: 12,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var infoWindow = new google.maps.InfoWindow();
  var latlngbounds = new google.maps.LatLngBounds();
  var geocoder = geocoder = new google.maps.Geocoder();
  var map = new google.maps.Map(document.getElementById("dvMap_update"), mapOptions);
  for (var i = 0; i < markers.length; i++) {
    var data = markers[i]
    var myLatlng = new google.maps.LatLng(data.lat, data.lng);
    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: data.title,
      draggable: true,
      animation: google.maps.Animation.DROP
    });
    (function (marker, data) {
      google.maps.event.addListener(marker, "click", function (e) {
        infoWindow.setContent(description);
        infoWindow.open(map, marker);
      });
      google.maps.event.addListener(marker, "dragend", function (e) {
        var lat, lng, address;
        geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            lat = marker.getPosition().lat();
            lng = marker.getPosition().lng();
            address = results[0].formatted_address;
            // alert(lat+'\n'+lng);
            $('#lat_edit').val(lat);
            $('#lng_edit').val(lng);
            // alert($('#lat').val()+'\n'+$('#lng').val());
            // alert("Latitude: " + lat + "\nLongitude: " + lng + "\nAddress: " + address);

          }
        });
      });
    })(marker, data);
    latlngbounds.extend(marker.position);
  }

  // var bounds = new google.maps.LatLngBounds();   // For Multiple markers
  // map.setCenter(latlngbounds.getCenter());       // For Multiple markers
  // map.fitBounds(latlngbounds);                   // For Multiple markers
}
function initAutocomplete() {
  var map = new google.maps.Map(document.getElementById('dvMap'), {
    center: {lat: -33.8688, lng: 151.2195},
    zoom: 15,
    mapTypeId: 'roadmap'
  });
  // var mapOptions = {
  //     center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
  //     zoom: 5,
  //     mapTypeId: google.maps.MapTypeId.ROADMAP
  // };

  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });

  var markers = [];
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      if (!place.geometry) {
        console.log("Returned place contains no geometry");
        return;
      }
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
}
function maps(x,y) {
  var uluru = {lat: x, lng: y};
  var map = new google.maps.Map(document.getElementById('mapDealers'), {
    zoom: 12,
    center: uluru
  });
  var marker = new google.maps.Marker({
    position: uluru,
    map: map
  });
}

</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGqNz-KUAKL435y6zraREiT-o5s4tuiEM&callback=initMap"></script>



  </div>
@endsection
