@extends('backoffice.layouts.main')
@section('page_title','GomBeta | ศูนย์ช่วยเหลือ')
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ศูนย์ช่วยเหลือ
        <small>ศูนย์ช่วยเหลือ</small>
        <p>
          {{-- {{Html::link('backoffice/add_admin','<i class="fa fa-plus-square" aria-hidden="true"></i>Add Admin',array('class'=>'btn btn-primary'))}} --}}

        </p>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-cog"></i> Backoffice</a></li>
        <li class="active">ศูนย์ช่วยเหลือ</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      @if(Session::has('success'))
        {{-- <div class="alert alert-success">
          <ul>
          <li> {{ Session::get('success') }}</li>
          </ul>
        </div> --}}
        <script>
            swal("{{ Session::get('success') }}", "", "success");
        </script>
    @endif

    {{-- <script>
        swal("Good job!", "You clicked the button!", "success");
    </script> --}}
        <p></p>

        @php
        $help=\App\Models\HelpCenter::first();
        if($help!=null){
          $ans = json_decode($help->ans);
          $question = json_decode($help->question);
        }
        @endphp
        @if ($help!=null)
          <form class="form_helpcenter" id="form_helpcenter" >
            {{ csrf_field() }}
            <div class='form-group' >
              <div class="row">
                <div class="col-md-12  text-center">
                  <h3>การสั่งซื้อสินค้า</h3>
                </div>
              </div>
            </div>
            <div class='form-group' >
              <div class="row">
                <div class="col-md-12  text-center">
                  <textarea class="textarea" name="how_to_buy" rows="8" cols="80" >{{ $help->how_to_buy }}</textarea>
                </div>
              </div>
            </div>
            <div class='form-group' >
              <div class="row">
                <div class="col-md-12  text-center">
                  <h3>การชำระเงิน</h3>
                </div>
              </div>
            </div>
            <div class='form-group' >
              <div class="row">
                <div class="col-md-12  text-center">
                  <textarea class="textarea" name="how_to_pay" rows="8" cols="80" >{{ $help->how_to_pay }}</textarea>
                </div>
              </div>
            </div>
            <div class='form-group' >
              <div class="row">
                <div class="col-md-12  text-center">
                  <h3>การส่งซื้อสินค้า</h3>
                </div>
              </div>
            </div>
            <div class='form-group' >
              <div class="row">
                <div class="col-md-12  text-center">
                  <textarea class="textarea" name="how_to_send" rows="8" cols="80" >{{ $help->how_to_send }}</textarea>
                </div>
              </div>
            </div>
            <div class='form-group' >
              <div class="row">
                <div class="col-md-12  text-center">
                  <h3>คำถามที่พบบ่อย</h3>
                </div>
              </div>
            </div>
            {!!  "<script>var num=".(count($question)+1).";</script>" !!}
            <div class='form-group'>
              <div class="row" id="qna">
                @for ($i=0; $i < count($question); $i++)
                  <div class="col-md-4 text-center">
                    คำถาม {{$i+1}}.
                  </div>
                  <div class="col-md-8">
                    <input class="form-control" type="text" name="question[]" value="{{ $question[$i] }}" placeholder="คำถาม">
                  </div>
                  <div class="col-md-4 text-center">
                    คำตอบ {{$i+1}}.
                  </div>
                  <div class="col-md-8">
                    <input class="form-control" type="text" name="answer[]" value="{{ $ans[$i] }}" placeholder="คำตอบ">
                  </div>
                @endfor
              </div>
            </div>
            <div class='form-group' >
              <div class="row">
                <div class="col-md-12  text-center">
                  <button class="btn btn" id="add" type="button" name="button">+ เพิ่มคำถาม/ตอบ</button>
                </div>
              </div>
            </div>
            <div class='form-group' >
              <div class="row">
                <div class="col-md-12  text-center">
                  <h3>นโยบายความเป็นส่วนตัว</h3>
                </div>
              </div>
            </div>
            <div class='form-group' >
              <div class="row">
                <div class="col-md-12  text-center">
                  <textarea class="textarea" name="Privacy_Policy" rows="8" cols="80" >{{ $help->policy }}</textarea>
                </div>
              </div>
            </div>
            <div class='form-group' >
              <div class="row">
                <div class="col-md-12 text-center">
                  <button class="btn btn-success" type="submit" >บันทึก</button>
                  <button class="btn btn-danger" type="button" id="clear" >Clear</button>
                </div>
              </div>
            </div>
          </form>
        @else
          {!! "<script>var num=2;</script>" !!}
        <form class="form_helpcenter" id="form_helpcenter" >
          {{ csrf_field() }}
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12  text-center">
                <h3>การสั่งซื้อสินค้า</h3>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12  text-center">
                <textarea class="textarea" name="how_to_buy" rows="8" cols="80" ></textarea>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12  text-center">
                <h3>การชำระเงิน</h3>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12  text-center">
                <textarea class="textarea" name="how_to_pay" rows="8" cols="80" ></textarea>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12  text-center">
                <h3>การส่งซื้อสินค้า</h3>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12  text-center">
                <textarea class="textarea" name="how_to_send" rows="8" cols="80" ></textarea>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12  text-center">
                <h3>คำถามที่พบบ่อย</h3>
              </div>
            </div>
          </div>
          <div class='form-group'>
            <div class="row" id="qna">
              <div class="col-md-4 text-center">
                คำถาม 1.
              </div>
              <div class="col-md-8">
                <input class="form-control" type="text" name="question[]" value="" placeholder="คำถาม">
              </div>
              <div class="col-md-4 text-center">
                คำตอบ 1.
              </div>
              <div class="col-md-8">
                <input class="form-control" type="text" name="answer[]" value="" placeholder="คำตอบ">
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12  text-center">
                <button class="btn btn" id="add" type="button" name="button">+ เพิ่มคำถาม/ตอบ</button>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12  text-center">
                <h3>นโยบายความเป็นส่วนตัว</h3>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12  text-center">
                <textarea class="textarea" name="Privacy_Policy" rows="8" cols="80" ></textarea>
              </div>
            </div>
          </div>
          <div class='form-group' >
            <div class="row">
              <div class="col-md-12 text-center">
                <button class="btn btn-success" type="submit" >บันทึก</button>
                <button class="btn btn-danger" type="button" id="clear" disabled >Clear</button>
              </div>
            </div>
          </div>
        </form>
      @endif


    </section>

  </div>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGqNz-KUAKL435y6zraREiT-o5s4tuiEM&callback=initMap"></script>
  <script type="text/javascript">
  $('#form_helpcenter').submit(function () {
    var formData = new FormData($(this)[0]);
    $.ajax({
      url: '{{url('helpcenter')}}',
      type: 'POST',
      data: formData,
      dataType:'html',
      success: function (html) {
        $('#clear').removeAttr('disabled');
        swal("เสร็จสิ้น", "บันทึกเสร็จสิ้นแล้ว", "success");
      },
      cache: false,
      contentType: false,
      processData: false
    });
    return false;
  });



  $('#add').click(function(){

    $('#qna').append('<div class="col-md-4 text-center">'+
      'คำถาม '+num+
    '. </div>'+
    '<div class="col-md-8">'+
      '<input class="form-control" type="text" name="question[]" value="" placeholder="คำถาม">'+
    '</div>'+
    '<div class="col-md-4 text-center">'+
      'คำตอบ '+num+
    '. </div>'+
    '<div class="col-md-8">'+
      '<input class="form-control" type="text" name="answer[]" value="" placeholder="คำตอบ">'+
    '</div>');
    num++;
  });



  $(function() {
  $('.textarea').froalaEditor({
    // toolbarButtons: ['undo', 'redo', 'html', '-', 'fontSize', 'paragraphFormat', 'align', 'quote', '|', 'formatOL', 'formatUL', '|', 'bold', 'italic', 'underline', '|', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable'],
      imageUploadURL: '{{ url('upload_image')}}',
      imageUploadParams: {
        froala: 'true', // This allows us to distinguish between Froala or a regular file upload.
        _token: "{{ csrf_token() }}" // This passes the laravel token with the ajax request.
      },
      imageStyles: {
        class1: 'Class 1',
        class2: 'Class 2'
      },
      imageEditButtons: ['imageReplace', 'imageAlign', 'imageRemove', '|', 'imageLink', 'linkOpen', 'linkEdit', 'linkRemove', '-', 'imageDisplay', 'imageStyle', 'imageAlt', 'imageSize'],
      zIndex:2000
  });
});

$('#clear').click(function(){
  swal({
    title: "แน่ใจหรือ ?",
    text: "คุณต้องการลบข้อมูลนี้ใช่หรือไม่",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((sweetalert) => {
    if (sweetalert) {
      // setTimeout(function () {
      $.ajax({
          url: '{{url('helpcenter/clear')}}',
          type: 'GET',
          dataType:'html',
          success: function (html) {
            swal("Successfully", "เคลียร์ข้อมูลศูนย์ช่วยเหลือเสร็จสิ้น !!", "success");
            setTimeout(function(){
              location.reload();
            },1000);
            },
          cache: false,
          contentType: false,
          processData: false
        });
      // }, 1000);
    } else {
      swal("ยกเลิกการลบ !","","error");
    }
  });
});
</script>
@endsection
