@extends('layout.main')
@section('content')

  <!-- Site Content -->
  <div class="container px-xs-0">
  <div id="site-content" class="m-xs-0">

    <div class="col-12"><h3 class="font-cloud text-center text-md-left">ตั้งค่าบัญชี</h3><hr></div>
    <div class="col-12">
      <form id="setting" method="post" action="{{ url('updateUser') }}" enctype="multipart/form-data" onsubmit="return checkall();" novalidate>
        {{ csrf_field() }}
        <!-- Profile Image -->
        <div class="row">
          <div class="col-12">
            <div class="image_upload">
              <!-- Image Cover -->
              <div  class="img_cover">
                <div class="img_cover_Inp">
                  <input class="up_img_cover" id="imgCoverInp" name="imgCover" type="file"/>
                  <label class="up_img_cover m-0" for="imgCoverInp" title="Edit"><i class="fa fa-camera"></i></label>
                </div>
                <div class="cove_pic">
                  <img id="cove_pic" src="{{asset('local/storage/app/images/user/')}}/{{ Auth::user()->img_cover }}" title="Cover Image">
                </div>
              </div>
              <!-- Image Profile -->
              <div  class="img_profile">
                <div class="profile_pic rounded-circle">
                  <img id="profile_pic" class="rounded-circle" src="{{asset('local/storage/app/images/user/')}}/{{ Auth::user()->img_profile }}" title="Profile Image">
                </div>
                <div class="img_profile_Inp">
                  <input class="up_img_profile" id="imgInp" name="imgProfile" type="file"/>
                  <label class="up_img_profile m-0" for="imgInp" title="Edit"><i class="fa fa-camera"></i></label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Profile Image -->

        <!-- Account -->
        <h5 class="font-cloud pt-4">ข้อมูลบัญชี</h5>
        <div class="alert alert-light border-dashed">
          <h6 class="font-cloud text-black">ประเภทสมาชิก : <span class="text-success">@if (Auth::user()->Customer)
            {{ 'ผู้ซื้อ' }}
          @else
            {{ 'ผู้ขาย' }}
          @endif</span></h6>
          <hr class="border-dashed">
          <div class="row">
            <div class="col-sm-6 col-lg-3 mb-2">
              <label>ชื่อ-นามสกุล</label>
              <input id="name" type="text" name="name" class="form-control form-control-sm" value="{{Auth::user()->name}}" required>
            </div>
            <div class="col-sm-6 col-lg-3 mb-2">
              <label>ชื่อบริษัท/หน่วยงาน/ร้านค้า</label>
              <input id="company_name" type="text" name="company_name" class="form-control form-control-sm" value="{{Auth::user()->company_name}}" required>
            </div>
            <div class="col-sm-6 col-lg-3 mb-2">
              <label>เลขที่ สำนักงาน/หน่วยงาน/ร้านค้า</label>
              <input id="company_code" type="text" name="company_code" class="form-control form-control-sm"value="{{Auth::user()->company_code}}" required>
            </div>
            <div class="col-sm-6 col-lg-3 mb-2">
              <label>เลขประจำตัวผู้เสียภาษี</label>
              <input id="tax_id" type="text" name="tax_id" class="form-control form-control-sm" value="{{Auth::user()->tax_id}}"required>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6 col-lg-3 mb-2">
              <label>ที่อยู่</label>
              <input id="address" name="address" type="text" class="form-control form-control-sm" value="{{Auth::user()->address}}" required>
            </div>
            <div class="col-sm-6 col-lg-3 mb-2">
              <label>รหัสไปรษณีย์</label>
              <input id="zipcode" name="zipcode" type="text" class="form-control form-control-sm" value="{{Auth::user()->zipcode}}" required>
            </div>
            <div class="col-sm-6 col-lg-3 mb-2">
              @php
              $amphur = App\Models\Amphur::get();
              $province = App\Models\Province::get();
              @endphp
              <label>เขต/อำเภอ</label>
              <select id="district"  name="district" class="form-control" required>
                <option value=""></option>
                @foreach ($amphur as $element)
                  @if (Auth::user()->district==$element->AMPHUR_ID)
                    <option value="{{ $element->AMPHUR_ID }}" selected>{{ $element->AMPHUR_NAME }}</option>
                  @else
                    <option value="{{ $element->AMPHUR_ID }}">{{ $element->AMPHUR_NAME }}</option>
                  @endif
                @endforeach
              </select>
            </div>
            <div class="col-sm-6 col-lg-3 mb-2">
              <label>จังหวัด</label>
              <select id="province" name="province" class="form-control" required>
                <option value=""></option>
                @foreach ($province as $element)
                  @if (Auth::user()->province==$element->PROVINCE_ID)
                    <option value="{{ $element->PROVINCE_ID }}" selected >{{ $element->PROVINCE_NAME }}</option>
                  @else
                    <option value="{{ $element->PROVINCE_ID }}">{{ $element->PROVINCE_NAME }}</option>
                  @endif
                @endforeach
              </select>
            </div>
          </div>
          {{-- </div> --}}
          <div class="row">
            <div class="col-sm-6 col-lg-3 mb-2">
              <label>อีเมล์</label>
              <input id="email" name="email" type="email" class="form-control form-control-sm" value="{{Auth::user()->email}}"  required>
            </div>
            <div class="col-sm-6 col-lg-3 mb-2">
              <label>โทรศัพท์มือถือ</label>
              <input id="mobile" name="mobile" type="tel" class="form-control form-control-sm" value="{{Auth::user()->mobile}}" required>
            </div>
            <div class="col-sm-6 col-lg-3 mb-2">
              <label>โทรศัพท์</label>
              <input id="phone" name="phone"  type="tel"  class="form-control form-control-sm" value="{{Auth::user()->phone}}">
            </div>
            <div class="col-sm-6 col-lg-3 mb-2">
              <label>แฟกซ์</label>
              <input id="fax" name="fax" type="tel" class="form-control form-control-sm" value="{{Auth::user()->fax}}">
            </div>
          </div>
        </div>
        <!-- Account -->
        {{-- {{ dd(Session::get('banproduct')) }} --}}
        @if (Auth::user()->member_type=='Customer')
          @php
            $cer=\App\Models\Certificate::where('user_id',Auth::user()->id)->get();
          @endphp
          <!-- Document -->
        <h5 class="font-cloud pt-4">ใบอนุญาต</h5>
        <div class="alert alert-light border-dashed">
          <div class="row">
            <div class="col-sm-12 col-lg-6">
              <div class="cloned-cer">
                <div class="row">

                  <div class="col-sm-12 col-lg-6 mb-0 mb-lg-2">

                    <label>ใบอนุญาต</label>

                  </div>

                  <div class="col-sm-12 col-lg-6 mb-0 mb-lg-2">

                    <label>เลขที่ใบอนุญาต</label>

                  </div>

                </div>

                <div class="row">
                  <div class="col-sm-12 col-lg-6 mb-0 mb-lg-2">
                    @foreach ($cer as $key )
                      <a href="{{url('local/storage/app/images/certificate/')}}/{{ $key->certificate }}" target="_blank" class="cer btn btn-sm btn-primary btn-block text-left" role="button">{{ substr($key->certificate,0,25) }}<i class="fa fa-link float-right py-1"></i></a>
                      {{-- <input id="certificate" type="file" class="cer form-control form-control-sm form-control-file p-0" required> --}}
                    @endforeach
                  </div>
                  <div class="col-sm-12 col-lg-6 mb-0 mb-lg-2">
                    @foreach ($cer as $key )
                      <input style="margin-top:5px" type="text" value="{{ $key->cer_num }}" class="form-control form-control-sm cer" readonly="true" required>
                    @endforeach
                  </div>
                  {{-- <div class="col-sm-2 col-lg-1 mb-0 mb-lg-2">
                    <button type="button" class="btn btn-sm font-cloud border-0 cer-edit" style="margin-top: 22px; float: right;">
                      <span><i class="fa fa-edit mt-1"></i></span>
                      <span style="display: none;"><i class="fa fa-refresh mt-1"></i></span>
                    </button>
                  </div> --}}
                </div>
              </div>
            </div>
            <div class="col-sm-12 col-lg-6">
              <button type="button" class="btn btn-sm cloned-cer-btn" style="margin-top: 22px;"><i class="fa fa-plus-circle mr-2"></i>เพิ่มเอกสาร</button>
            </div>
          </div>
        </div>
        @endif
        {{-- <!-- Document -->
        <h5 class="font-cloud pt-4">ใบอนุญาต</h5>
        <div class="alert alert-light border-dashed">
          <div class="row">
            <div class="col-sm-12 col-lg-6">
              <div class="added-cer">
                <div class="row">
                  <div class="col-sm-12 col-lg-6 mb-0 mb-lg-2">
                    <label>ใบอนุญาต</label>
                  </div>
                  <div class="col-sm-10 col-lg-5 mb-0 mb-lg-2">
                    <label>เลขที่ใบอนุญาต</label>
                  </div>
                  </div>
                  @php
                    $cer=\App\Models\Certificate::where('user_id','2')->get();
                  @endphp
                  @if (count($cer)>=0)
                    @foreach ($cer as $key )
                      <div class="row">
                        <div class="col-sm-12 col-lg-6 mb-0 mb-lg-2">
                          <a href="{{asset('local/storage/app/images/certificate')}}/{{ $key->certificate }}" target="_blank" class="cer btn btn-sm btn-primary btn-block text-left" role="button">@if (strlen($key->certificate>=23)){{substr($key->certificate,0,23)."..."}}@else{{ $key->certificate }}

                          @endif<i class="fa fa-link float-right py-1"></i></a>
                          <input id="certificate" type="file" class="cer form-control form-control-sm form-control-file p-0" required>
                        </div>
                        <div class="col-sm-10 col-lg-5 mb-0 mb-lg-2">
                          <input id="certificate-number" type="text" value="{{ $key->cer_num }}" class="form-control form-control-sm" readonly="true" required>
                        </div>
                        </div>
                    @endforeach
                  @else
                    <div class="row">
                      <div class="col-sm-12 col-lg-6 mb-0 mb-lg-2">
                      </div>
                      <div class="col-sm-10 col-lg-5 mb-0 mb-lg-2">
                      </div>
                      </div>
                  @endif

              </div>
            </div>
            <div class="col-sm-12 col-lg-6">
              <button type="button" class="btn btn-sm added-cer-btn" style="margin-top: 22px;"><i class="fa fa-plus-circle mr-2"></i>เพิ่มเอกสาร</button>
            </div>
          </div>
        </div> --}}
        <!-- User -->
        {{-- <h5 class="font-cloud pt-4">ข้อมูลผู้ใช้</h5>
        <div class="alert alert-light border-dashed">
          <div class="row">
            <div class="col-sm-12 col-lg-6 mb-2">
              <label>ชื่อในระบบ</label>
              <input id="username" type="text" value="{{ Auth::user()->username }}" class="form-control form-control-sm" readonly="true" required>
            </div>
            <div class="col-sm-10 col-lg-5 mb-2">
              <label>รหัสผ่าน</label>
              <input id="password" type="password" value="{{ Auth::user()->password }}" class="form-control form-control-sm" readonly="true" required>
            </div>
            <div class="col-sm-2 col-lg-1 mb-0 mb-lg-2">
              <button type="button" class="btn btn-sm font-cloud border-0 usr-edit" style="margin-top: 22px; float: right;">
                <span><i class="fa fa-edit mt-1"></i></span>
                <span style="display: none;"><i class="fa fa-refresh mt-1"></i></span>
              </button>
            </div>
          </div>
        </div> --}}
        <!-- User -->

        <!-- Map -->
        <div class="alert alert-secondary font-cloud mt-4 p-0 p-sm-3">
          <div class="row">
            <div class="col-md-12 mb-2">
              <h5 class="p-3 p-sm-0"><i class="fa fa-map-marker mb-2 mr-2"></i>ตำแหน่งบนแผนที่ : <small>กรุณาระบุตำแหน่งโดยการเลื่อนหมุดสีแดงบนแผนที่ไปยังตำแหน่งที่ตั้งของคุณ เพื่อสิธิประโยชน์ทางการค้าค่ะ</small></h5>
              <div id="dvMap" class="rounded" style="height: 70vh; width: 100%;"></div>
            </div>
          </div>
        </div>
        <input type="text" name="lat" id="lati" value="{{ Auth::user()->lat }}"  hidden>
        <input type="text" name="lng" id="lngi" value="{{ Auth::user()->longtitude }}" hidden>
        <!-- Map -->

        <!-- Area -->
        {{-- <div id="area">
        <h5 class="font-cloud p-3 p-sm-0"><i class="fa fa-street-view mb-2 mr-2"></i>กันเขตพื้นที่ : <small>กรุณาระบุตำแหน่งโดยการเลื่อนหมุดสีแดงบนแผนที่ไปยังตำแหน่งที่ตั้งของคุณ เพื่อสิธิประโยชน์ทางการค้าค่ะ</small></h5>
        <div class="alert alert-light border-dashed">
          <div class="row">
            <div class="col-sm-6 col-lg-3 mb-2">
              <label>รัศมี (กิโลเมตร)</label>
              <input id="area-radius" type="text" class="form-control form-control-sm">
            </div>
            <div class="col-sm-6 col-lg-3 mb-2">
              <label>ระยะเวลา (จำนวนวัน)</label>
              <input id="area-time" type="text" class="form-control form-control-sm">
            </div>
          </div>
        </div>
        </div> --}}
        <!-- Area -->

        <!-- Newsletter -->
        <hr class="border-dashed">
        <div class="row">
          <div class="col-md-12 mb-2 need">
            @if (Auth::user()->need=='1')
              <input type="hidden" name="need" value="1" id="need" />
              <label class="custom-control custom-checkbox align-items-center">
                <input id="subscribe" type="checkbox" class="custom-control-input" checked>
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">ฉันต้องการรับข่าวสารทางอีเมล์</span>
              </label>
            @else
              <label class="custom-control custom-checkbox align-items-center">
                <input id="subscribe" type="checkbox" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">ฉันต้องการรับข่าวสารทางอีเมล์</span>
              </label>
            @endif

          </div>
        </div>
        <!-- Newsletter -->

        <button id="complete" class="btn btn-success mt-2" type="submit"><i class="fa fa-user-plus mr-2"></i>บันทึกการแก้ไข</button>

      </form>
    </div>

  </div>
  </div>
  <!-- Site Content -->

  <!-- Site footer -->
  @include('inc_footer')
  <!-- Site footer -->

  <!-- Site js -->
  @include('inc_js')
  <!-- Site js -->

  <!-- Site Sidewarp -->
  @include('inc_sidewarp')
  <!-- Site Sidewarp -->

  <!-- Media -->
  <script>
    /* Add Certificate Row */
    $(document).ready(function() {
        $(".added-cer-btn").click(function (){
            $(".added-cer").append("<div class='row mt-2'><div class='col-sm-6 col-lg-6 mb-0 mb-lg-2'><input id='#certificate_new' type='file' name='certificate[]' class='form-control form-control-sm form-control-file p-0 new' required></div><div class='col-sm-5 col-lg-5 mb-0 mb-lg-2'><input id='certificate-number' name='certificate_number[]' type='text' value='' class='form-control form-control-sm' required></div></div>");
        });
    });

    /* Edit Certificate, Username, Password */
    $(document).ready(function() {
      $("#certificate.cer").hide();
      window.cerVal = $("#certificate-number").val();
      window.pasVal = $("#username").val();

      $(".cer-edit").click(function() {
        $(".cer, .cer-edit > span").fadeToggle("fast");
        $("#certificate-number").attr('readonly', function (_, attr) { return !attr });

        $(this).toggleClass('btn btn-warning');
        if($(this).hasClass('btn')){
          $("#certificate-number").val(window.cerVal);
        }else{
          $("#certificate-number").val('');
        }
      });

      $(".usr-edit").click(function() {
        $(".usr-edit > span").fadeToggle("fast");
        $("#username, #password").attr('readonly', function (_, attr) { return !attr });

        $(this).toggleClass('btn btn-warning');
        if($(this).hasClass('btn')){
          $("#password").val(window.pasVal);
        }else{
          $("#password").val('');
        }
      });

    });

  </script>

  <!-- Map -->
  <script type="text/javascript">
      // Referance >>> https://www.aspsnippets.com/Articles/Google-Maps-V3-Draggable-Markers-Example-Drag-and-Drop-Markers-in-Google-Maps-using-JavaScript.aspx
      var markers = [
          {
              "title": '',
              "lat": '{{ Auth::user()->lat }}',
              "lng": '{{ Auth::user()->longtitude }}',
              "description": ''
          }
      ];
      window.onload = function () {
          var mapOptions = {
              center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
              zoom: 10,
              mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          var infoWindow = new google.maps.InfoWindow();
          var latlngbounds = new google.maps.LatLngBounds();
          var geocoder = geocoder = new google.maps.Geocoder();
          var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
          for (var i = 0; i < markers.length; i++) {
              var data = markers[i]
              var myLatlng = new google.maps.LatLng(data.lat, data.lng);
              var marker = new google.maps.Marker({
                  position: myLatlng,
                  map: map,
                  title: data.title,
                  draggable: true,
                  animation: google.maps.Animation.DROP
              });
              (function (marker, data) {
                  google.maps.event.addListener(marker, "click", function (e) {
                      infoWindow.setContent(data.description);
                      infoWindow.open(map, marker);
                  });
                  google.maps.event.addListener(marker, "dragend", function (e) {
                      var lat, lng, address;
                      geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                          if (status == google.maps.GeocoderStatus.OK) {
                              lat = marker.getPosition().lat();
                              lng = marker.getPosition().lng();
                              address = results[0].formatted_address;
                              $('#lati').val(marker.getPosition().lat());
                              $('#lngi').val(marker.getPosition().lng());
                          }
                          ;

                      });
                  });
              })(marker, data);
              latlngbounds.extend(marker.position);
          }

          // var bounds = new google.maps.LatLngBounds();   // For Multiple markers
          // map.setCenter(latlngbounds.getCenter());       // For Multiple markers
          // map.fitBounds(latlngbounds);                   // For Multiple markers
      }
  </script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGqNz-KUAKL435y6zraREiT-o5s4tuiEM&callback=initMap"></script>
  <!-- Map -->
  <script type="text/javascript">
  var checkusername='';
  function checkall() {
    if($('#subscribe:checked').length == 1){
      $('#setting').append('<input type="hidden" name="need" value="1" id="need" />');
    }else{
      $("#need").remove();
    }
    // alert($('input[name="certificate"]'.val()));
    // if($('#imgCoverInp').val()==""){
    //   $('#imgCoverInp').focus();
    //   swal({
    //     title: "กรุณาใส่รูปภาพหน้าปก",
    //     text: "เพื่อประโยชน์ของท่านกรุณาใส่ภาพหน้าปก",
    //     icon: "warning",
    //     button: "OK",
    //   });
    //   return false;
    // }
    // if($('#imgInp').val()==""){
    //   $('#imgInp').focus();
    //   swal({
    //     title: "กรุณาใส่รูปภาพโปรไฟล์",
    //     text: "เพื่อประโยชน์ของท่านกรุณาใส่ภาพหน้าปก",
    //     icon: "warning",
    //     button: "OK",
    //   });
    //   return false;
    // }
    // if($('input[name="member_type"]:checked').length < 1){
    //   $('#member_type1').focus();
    //   swal({
    //     title: "กรุณาเลือกประเภทสมาชิก",
    //     text: "เพื่อประโยชน์ของท่านกรุณาเลือกประเภทสมาชิก",
    //     icon: "warning",
    //     button: "OK",
    //   });
    //   return false;
    // }else{
    //   if($('#member_type1:checked').length == 1 && $('input[name="member_group"]:checked').length < 1){
    //     swal({
    //       title: "กรุณาเลือกกลุ่มผู้ซื้อ",
    //       text: "เพื่อประโยชน์ของท่านกรุณาเลือกกลุ่มผู้ซื้อ",
    //       icon: "warning",
    //       button: "OK",
    //     });
    //     return false;
    //   }
      // if($('#lat').val()=="" || $('#long').val()==""){
      //   $('#dvMap').focus();
      //   swal({
      //     title: "กรุณาระบุตำแหน่งของร้านท่าน",
      //     text: "เพื่อประโยชน์ของท่านกรุณาระบุตำแหน่งของร้านท่าน",
      //     icon: "warning",
      //     button: "OK",
      //   });
      //   return false;
      // }
      var type="{{ Auth::user()->member_type }}"
      if(type=='Customer'){
        // var certificate = $("input[name='certificate[]']")
        // .map(function(){return $(this).val();}).get();
        var certificate = $("input[name='certificate[]']");
        var certificate_number = $("input[name='certificate_number[]']");
        var extall="jpg,jpeg,gif,png";

        // alert(certificate.length);
        for (var i = 0; i < certificate.length; i++) {
          ext = certificate.eq(i).val().split('.').pop().toLowerCase();
          if(certificate.eq(i).val()==""){
            swal({
              title: "กรุณาเลือกรูปใบเซอร์",
              text: "เพื่อประโยชน์ของท่านกรุณาเลือกรูปใบเซอร์",
              icon: "warning",
              button: "OK",
            });
            $("input[name='certificate[]']")[i].focus();
            return false;
          }else{
            if(parseInt(extall.indexOf(ext)) < 0) {
              swal({
                title: "กรุณาเลือกภาพที่ถูกต้อง",
                text: 'สามารถใช้ได้เฉพาะไฟล์ : ' + extall,
                icon: "warning",
                button: "OK",
              });
              $("input[name='certificate[]']")[i].focus();
              certificate.eq(i).val('');
              return false;
            }
          }
          if (certificate_number.eq(i).val()=="") {
            swal({
              title: "กรุณากรอกหมายเลขใบเซอร์",
              text: "เพื่อประโยชน์ของท่านกรุณากรอกหมายเลขใบเซอร์",
              icon: "warning",
              button: "OK",
            });
            $("input[name='certificate_number[]']")[i].focus();
            return false;
          }
        }

      }
      if($('#name').val()==""){
      $('#name').focus();
      swal({
        title: "กรุณาใส่ชื่อและนามสกุล",
        text: "เพื่อประโยชน์ของท่านกรุณาใส่ชื่อและนามสกุล",
        icon: "warning",
        button: "OK",
      });
      return false;
    }
    if($('#company_name').val()==""){
      $('#company_name').focus();
      swal({
        title: "กรุณากรอกชื่อ บริษัท/หน่วยงาน/ร้านค้า",
        text: "เพื่อประโยชน์ของท่านกรุณากรอกชื่อบริษัท/หน่วยงาน/ร้านค้า",
        icon: "warning",
        button: "OK",
      });
      return false;
    }
    if($('#company_code').val()==""){
      $('#company_code').focus();
      swal({
        title: "กรุณากรอกเลขที่ สำนักงาน/หน่วยงาน/ร้านค้า",
        text: "เพื่อประโยชน์ของท่านกรุณากรอกเลขที่ สำนักงาน/หน่วยงาน/ร้านค้า",
        icon: "warning",
        button: "OK",
      });
      return false;
    }
    if($('#tax_id').val()==""){
      $('#tax_id').focus();
      swal({
        title: "กรุณากรอกเลขที่เลขประจำตัวผู้เสียภาษี",
        text: "เพื่อประโยชน์ของท่านกรุณากรอกเลขที่เลขประจำตัวผู้เสียภาษี",
        icon: "warning",
        button: "OK",
      });
      return false;
    }
    if($('#address').val()==""){
      $('#address').focus();
      swal({
        title: "กรุณากรอกที่อยู่",
        text: "เพื่อประโยชน์ของท่านกรุณากรอกที่อยู่",
        icon: "warning",
        button: "OK",
      });
      return false;
    }
    if($('#zipcode').val()==""){
      $('#zipcode').focus();
      swal({
        title: "กรุณากรอกรหัสไปรษณีย์",
        text: "เพื่อประโยชน์ของท่านกรุณากรอกรหัสไปรษณีย์",
        icon: "warning",
        button: "OK",
      });
      return false;
    }
    if($('#district').val()==""){
      $('#district').focus();
      swal({
        title: "กรุณาเลือกเขต/อำเภอ",
        text: "เพื่อประโยชน์ของท่านกรุณาเลือกเขต/อำเภอ",
        icon: "warning",
        button: "OK",
      });
      return false;
    }
    if($('#province').val()==""){
      $('#province').focus();
      swal({
        title: "กรุณาเลือกจังหวัด",
        text: "เพื่อประโยชน์ของท่านกรุณาเลือกจังหวัด",
        icon: "warning",
        button: "OK",
      });
      return false;
    }
    if($('#email').val()==""){
      $('#email').focus();
      swal({
        title: "กรุณากรอกอีเมล์",
        text: "เพื่อประโยชน์ของท่านกรุณากรอกอีเมล",
        icon: "warning",
        button: "OK",
      });
      return false;
    }else{
      mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (!($('#email').val().match(mailformat)))
      {
        $('#email').focus();
        swal({
          title: "รูปแบบอีเมลไม่ถูกต้อง",
          text: "เพื่อประโยชน์ของท่านกรุณากรอกอีเมลให้ถูกต้อง",
          icon: "warning",
          button: "OK",
        });
        return false;
      }
    }
    if($('#mobile').val()==""){
      $('#mobile').focus();
      swal({
        title: "กรุณากรอกเบอร์โทรศัพท์มือถือ",
        text: "เพื่อประโยชน์ของท่านกรุณากรอกเบอร์โทรศัพท์มือถือ",
        icon: "warning",
        button: "OK",
      });
      return false;
    }
    // if(checkusername!='pass'){
    //   $('#username').focus();
    //   swal({
    //     title: "Username นี่มีผู้ใช้แล้ว",
    //     text: "เพื่อประโยชน์ของท่านกรุณากรอก username ให้ถูกต้อง",
    //     icon: "warning",
    //     button: "OK",
    //   });
    //   return false;
    // }
    // if($('#username').val()==''){
    //   $('#username').focus();
    //   swal({
    //     title: "กรุณากรอก username ให้ถูกต้อง",
    //     text: "เพื่อประโยชน์ของท่านกรุณากรอก username ให้ถูกต้อง",
    //     icon: "warning",
    //     button: "OK",
    //   });
    //   return false;
    // }

    // if($('#password').val()=='' || $('#password').val().length<=6){
    //   $('#password').focus();
    //   swal({
    //     title: "กรุณากรอก password ให้ถูกต้อง ",
    //     text: "เพื่อประโยชน์ของท่านกรุณากรอก password ให้ถูกต้อง \n กรอก Password 6 ตัวขึ้นไป",
    //     icon: "warning",
    //     button: "OK",
    //   });
    //   return false;
    // }

  }
  </script>
  <script type="text/javascript">
  $('#district').select2({
    placeholder : 'กรุณาเลือกอำเภอ'
  });
  $('#province').select2({
    placeholder : 'กรุณาเลือกจังหวัด'
  });
  $(document).ready(function(){
    var clone="";
    clone+="<div class=\"row\"\>";
    clone+="<div class=\"col-sm-6 col-lg-6 mb-0 mb-lg-2\"\>";
    clone+="<input id=\"certificate\" name=\"certificate[]\" onchange=\"return checkfile();\" type=\"file\" class=\"form-control form-control-sm form-control-file p-0\"\>";
    clone+="</div>";
    clone+="<div class=\"col-sm-6 col-lg-6 mb-0 mb-lg-2\"\>";
    clone+="<input id=\"certificate-number\" name=\"certificate_number[]\" type=\"text\" class=\"form-control form-control-sm\"\>";
    clone+="</div>";
    clone+="</div>";
    $('.cloned-cer-btn').click(function (){
      $('.cloned-cer').append(clone);
    });
  });
  </script>
@endsection
