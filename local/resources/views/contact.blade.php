@extends('layout.main')
@section('content')

  <!-- Site Content -->
  <div class="container px-xs-0">
  <div id="site-content" class="m-xs-0">
    @php
      $contact= \App\Models\Contact::first();
    @endphp
    <div class="col-12"><h3 class="font-cloud">ติดต่อเรา</h3>
      <ul class="w-50 list-unstyled list-inline-item text-right" style="position: absolute; top: 0; right: 10px;">
        <li><a class="float-right opacity social facebook" itle="Facebook" href="{{ $contact->facebook }}" target="_blank"></a></li>
        <li><a class="float-right opacity social youtube" itle="YouTube" href="{{ $contact->youtube }}" target="_blank"></a></li>
        <li><a class="float-right opacity social line" href="{{ $contact->line }}" target="_blank" data-container="body" data-toggle="popover" data-placement="auto" data-html="true" data-content="<img class='img-fluid' src='{{asset('assets/images/product/product_01.jpg')}}'>"></a></li>
      </ul>
      <hr>
    </div>
    <div class="col-12">
      <div class="row">
        <div class="col-md-6">
          <div id="map-contact"></div>
          <script>
            function initMap() {
              var uluru = {lat: {{$contact->lat}}, lng: {{ $contact->lng }}};
              var map = new google.maps.Map(document.getElementById('map-contact'), {
                zoom: 16,
                center: uluru
              });
              var marker = new google.maps.Marker({
                position: uluru,
                map: map
              });
            }
          </script>
          <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGqNz-KUAKL435y6zraREiT-o5s4tuiEM&callback=initMap"></script>
        </div>
        <div class="col-md-6">
          <h5 class="font-cloud pt-4 pt-md-0">ข้อมูลการติดต่อ</h5>
          <div class="row mb-3">
            <div class="col-md-12">
              @if ($contact)
                <ul class="list-unstyled">
                  <li><i class="icon-info rounded-circle fa fa-home"></i>{{$contact->address}}</li>
                  <li><i class="icon-info rounded-circle fa fa-phone"></i>{{$contact->phone}}</li>
                  <li><i class="icon-info rounded-circle fa fa-fax"></i>{{$contact->fax}}</li>
                  <li><i class="icon-info rounded-circle fa fa-mobile"></i>{{$contact->mobile}}</li>
                  <li><i class="icon-info rounded-circle fa fa-envelope-o"></i>{{$contact->email}}</li>
                </ul>
              @else
                <ul class="list-unstyled">
                  <li><i class="icon-info rounded-circle fa fa-home"></i>90/16 Sriayutthaya Road, Vachirapayabaan, Dusit, Bangkok 10300</li>
                  <li><i class="icon-info rounded-circle fa fa-phone"></i>022-222-2222</li>
                  <li><i class="icon-info rounded-circle fa fa-fax"></i>022-222-2223</li>
                  <li><i class="icon-info rounded-circle fa fa-mobile"></i>087-234-5678</li>
                  <li><i class="icon-info rounded-circle fa fa-envelope-o"></i>contact@gombeta.com</li>
                </ul>
              @endif


            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <form class="contactus" id="contact" action="{{ url('/ContactAdmin') }}" method="post">
                {{ csrf_field() }}
                <h5 class="font-cloud">ส่งข้อความถึงเรา</h5>
                <input type="text" class="form-control form-control-sm border-bottom-0 rounded-0 rounded-top" placeholder="ชื่อ-สกุล" name="name" required>
                <input type="email" class="form-control form-control-sm border-bottom-0 rounded-0" placeholder="อีเมล์" name="mail" required>
                <input type="text" class="form-control form-control-sm border-bottom-0 rounded-0" placeholder="หัวข้อ" name="title" required>
                <textarea class="form-control form-control-sm rounded-0 rounded-bottom" placeholder="ข้อความ ..." name="detail" rows="5" required></textarea>
                <button class="btn btn-success mt-2" type="submit">ส่งข้อความ</button>
                <button class="btn btn-danger mt-2" type="reset">ยกเลิก</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  </div>

  <!-- Site Content -->

  <!-- Site footer -->
  @include('inc_footer')
  <!-- Site footer -->

  <!-- Site js -->
  @include('inc_js')
  <!-- Site js -->

  <!-- Site Sidewarp -->
  @include('inc_sidewarp')
  <!-- Site Sidewarp -->

@endsection
