<?php
  // set the month array
  $monthThaiArray = array(
  "1"   => "มกราคม",
  "2"   => "กุมภาพันธ์",
  "3"   => "มีนาคม",
  "4"   => "เมษายน",
  "5"   => "พฤษภาคา",
  "6"   => "มิถุนายน",
  "7"   => "กรกฎาคม",
  "8"   => "สิงหาคม",
  "9"   => "กันยายน",
  "10"  => "ตุลาคม",
  "11"  => "พฤษจิกายน",
  "12"  => "ธันวาคม",
  );
  // set start and end year range
  $yearThaiArray = range(2550, date("Y")+543);
?>
@php
  // Session::put('year','2013');
if(!Session::has('month')){
  $month=date('n');
}else{
  $month=Session::get('month');
}
if(!Session::has('year')){
  $year=date("Y");
}else{
  $year=Session::get('year');
}

if($month=='All Month'){
  $order=\App\Models\OrderData::where('user_id',Auth::user()->id)
  ->whereYear('created_at',$year)
  ->get();
}
if($year=='All Year'){
  $order=\App\Models\OrderData::where('user_id',Auth::user()->id)
  ->whereMonth('created_at',$month)
  ->get();
}
if($month=='All Month' and $year=='All Year'){
  $order=\App\Models\OrderData::where('user_id',Auth::user()->id)
  ->get();
}else if($month!='All Month' and $year!='All Year'){
  $order=\App\Models\OrderData::where('user_id',Auth::user()->id)
  ->whereYear('created_at',$year)
  ->whereMonth('created_at',$month)
  ->get();
}
  $no=1;

  // dd($order);
@endphp
<a href="javascript:void(0)" class="closebtn" onclick="closeOrderHistory()">×</a>
<div id="history" class="container animated bounceInRight my-5 pb-2" data-print-content>

  <!-- Headding -->
  <div id="title-main" class="title-main position-sticky border border-success">
    <div class="row align-items-center">
      <div class="col-md-4 font-cloud text-success"><h3><i class="fa fa-history mr-2"></i>ประวัติการสั่งซื้อ</h3></div>
      <div class="col-md-8">
        <div class="font-cloud float-md-right text-center pb-2 remove-this">
          <select name="month" onchange="changeM(this.value)" id="monthSelect">
            <option value="All Month">All Month</option>
            <?php for($i=1;$i<=12;$i++) { ?>
            <option value="<?php echo $i;?>" <?php if($i==$month ){ echo 'selected';}?>><?php echo $monthThaiArray[$i];?></option>
            <?php } ?>
          </select>

          <select name="year" onchange="changeY(this.value)" id="yearSelect">
            <option value="All Year">All Year</option>
            <?php
            foreach ($yearThaiArray as $years) {
              $selected="";
              if($year!='All Year'){
                $selected = ($years == $year+543) ? 'selected' : '';
              }
              echo '<option '.$selected.' value="'.$years.'">'.$years.'</option>';
            }
            ?>
          </select>
          <a class="title-print badge rounded-0" target="_blank" href="{{url('order_history_print')}}" data-print><i class="fa fa-print mr-2"></i>Print</a>
        </div>
      </div>
    </div>
  </div>
  @if (count($order)>0)
    @foreach ($order as $key )
      <div class="row my-3">
      <div class="col-12">
        <div class="title-doc position-sticky">
          <h6 class="font-cloud">
            <span class="badge badge-success float-left rounded-0 p-2 mr-3">No.<br>{{$no++}}</span>
            <span class="text-black d-inline-block mt-1">วันที่ {{ date('d',strtotime($key->created_at)) }} {{ $monthThaiArray[date('n',strtotime($key->created_at))] }} {{ date('Y',strtotime($key->created_at))+543 }} | {{ date('H:m',strtotime($key->created_at)) }} </span><br>
            <small>เลขที่เอกสาร : {{$key->order_id}}</small>
          </h6>
        </div>
        <table class="table table-bordered">
          <thead>
            <tr class="font-cloud">
              <th class="position-sticky">#</th>
              <th class="position-sticky">จำนวน</th>
              <th class="position-sticky">ราคา/ชิ้น</th>
              <th class="position-sticky">ส่วนลด/ชิ้น</th>
              <th class="position-sticky">ราคารวม</th>
            </tr>
          </thead>
          <tbody>
            @php
            $status="รอการตรวจสอบ";
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $key->created_at);
            $dateText="(รับสินค้า ".$date->addDays($key->time)->formatLocalized('%A %d') ." - ".$date->addDays($key->time+4)->formatLocalized('%A %d %B %Y').")";
            if($key->status_confirm==1 and $key->status_order==0){
              $status="<span class=\"text-info\">สถานะ : จัดเตรียมสินค้า</span>";
            }else if($key->status_confirm==1 and $key->status_order==1){
              $status="<span class=\"text-primary\">สถานะ : กำลังส่งสินค้า</span>";
            }else if($key->status_confirm==1 and $key->status_order==2){
              $status="<span class=\"text-success\">สถานะ : รับสินค้าแล้ว</span>";
            }
              $num=1;
              $order_details=\App\Models\orderDetail::leftJoin('product','order_detail.product_id','=','product.id')
              ->leftJoin('product_category_sub','product.category_sub_id','=','product_category_sub.id')
              ->leftJoin('product_category','product_category_sub.category_id','=','product_category.id')
              ->select('order_detail.*','product_category.name as name_category','product.*')
              ->where('order_id',$key->id)->get();
              // dd($order_details);
              $totalPiece=0;
              $totalCrate=0;
              $total=0;
              $totalDiscount=0;
              $totalAll=0;
              @endphp
            @foreach ($order_details as $order_detail )
              @php
              $discount=0;

              $promotion=\App\Models\ProductPromotion::where('product_id',$order_detail->product_id)->orderBy('amount','DESC')->get();
              foreach ($promotion as $pro ) {
                if ($order_detail->qty>=$pro->amount) {
                  $discount=$pro->discount;
                  break;
                }
              }
              $discountPerPiece=($order_detail->price/$order_detail->piece)-(($order_detail->price/$order_detail->piece)*((100-$discount)/100));
              $totalPiece+=$order_detail->piece*$order_detail->qty;
              $totalCrate+=$order_detail->qty;
              $totalAll+=(($order_detail->price/$order_detail->piece)-$discountPerPiece)*$order_detail->qty*$order_detail->piece;
              $total+=$order_detail->qty*$order_detail->price;
              $totalDiscount=$total-$totalAll;
              @endphp
            <tr>
              <td rowspan="2">{{$num++}}.</td>
              <td colspan="5">{{$num++}}<br><small class="text-muted">{!! $status !!} {{ $dateText }}</small></td>
            </tr>
            <tr class="table-light">
              <td class="text-right">{{ $order_detail->qty }} ลัง <small>({{ $order_detail->piece }}ชิ้น)</small></td>
              <td class="text-right">{{$order_detail->price/$order_detail->piece}}</td>
              <td class="text-right"><small>({{ $discount }}%)</small> {{$discountPerPiece }}</td>
              <td class="text-right">{{ (($order_detail->price/$order_detail->piece)-$discountPerPiece)*$order_detail->qty*$order_detail->piece }}</td>
            </tr>
            @endforeach
            <tr class="table-warning">
              <td></td>
              <td class="text-right"><strong>{{$totalCrate}} ลัง<small> ({{$totalPiece}}ชิ้น)</small></strong></td>
              <td class="text-right"><strong>{{ $total }}</strong></td>
              <td class="text-right text-danger"><strong>{{ $totalDiscount }}</strong></td>
              <td class="text-right text-danger"><strong>{{$totalAll}}</strong></td>
            </tr>


          </tbody>
        </table>
        <div class="alert alert-light font-cloud rounded-0">
          <div class="row align-items-center">
          <div class="col-md-9">
            <span class="lead">{!! $status !!}</span>
            <span class="text-dark">{{ $dateText }}</span><br>
            <span>โอนเงินเข้าบัญชี: {{ $key->bank }}  | ยอดโอน: {{ number_format($key->money) }} บาท | วันที่โอน: {{ $key->date }}</span>
          </div>
          <div class="col-md-3">
            <a href="{{ url('local/storage/app/images/reciept/'.$key->file) }}" download class="float-md-right text-success my-3"><i class="fa fa-file-text-o">&nbsp;&nbsp;</i>หลักฐานการโอน</a>
          </div>
          </div>
        </div>
        <hr class="border-dashed border-success my-5">
      </div>
      </div>
    @endforeach
  @else
    <div class="col-md-12 text-center">
      ไม่พบประวัติการสั่งซื้อ
    </div>
  @endif
  <!-- Purchase Order -->
  {{-- <div class="row my-3">
  <div class="col-12">
    <div class="title-doc position-sticky">
      <h6 class="font-cloud">
        <span class="badge badge-success float-left rounded-0 p-2 mr-3">No.<br>03</span>
        <span class="text-black d-inline-block mt-1">วันที่ 15 กันยายน 2560 | 15:30 </span><br>
        <small>เลขที่เอกสาร : PO-00002314</small>
      </h6>
    </div>
    <table class="table table-bordered">
      <thead>
        <tr class="font-cloud">
          <th class="position-sticky">#</th>
          <th class="position-sticky">จำนวน</th>
          <th class="position-sticky">ราคา/ชิ้น</th>
          <th class="position-sticky">ส่วนลด/ชิ้น</th>
          <th class="position-sticky">ราคารวม</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td rowspan="2">1</td>
          <td colspan="5">product 01! You should check in on some of those fields below.<br><small class="text-muted"><span class="text-success">สถานะ : รับแล้ว</span> (รับสินค้า เสาร์, 16 - อังคาร, 19 ก.ย. 2560)</small></td>
        </tr>
        <tr class="table-light">
          <td class="text-right">10 ลัง <small>(120ชิ้น)</small></td>
          <td class="text-right">500</td>
          <td class="text-right"><small>(5%)</small> 25</td>
          <td class="text-right">57,000</td>
        </tr>

        <tr>
          <td rowspan="2">2</td>
          <td colspan="5">product 02! You should check in on some of those fields below.<br><small class="text-muted"><span class="text-success">สถานะ : รับแล้ว</span> (รับสินค้า เสาร์, 16 - อังคาร, 19 ก.ย. 2560)</small></td>
        </tr>
        <tr class="table-light">
          <td class="text-right">10 ลัง <small>(120ชิ้น)</small></td>
          <td class="text-right">500</td>
          <td class="text-right"><small>(5%)</small> 25</td>
          <td class="text-right">57,000</td>
        </tr>

        <tr>
          <td rowspan="2">3</td>
          <td colspan="5">product 03! You should check in on some of those fields below.<br><small class="text-muted"><span class="text-success">สถานะ : รับแล้ว</span> (รับสินค้า เสาร์, 16 - อังคาร, 19 ก.ย. 2560)</small></td>
        </tr>
        <tr class="table-light">
          <td class="text-right">10 ลัง <small>(120ชิ้น)</small></td>
          <td class="text-right">500</td>
          <td class="text-right"><small>(5%)</small> 25</td>
          <td class="text-right">57,000</td>
        </tr>

        <tr>
          <td rowspan="2">4</td>
          <td colspan="5">product 04! You should check in on some of those fields below.<br><small class="text-muted"><span class="text-info">สถานะ : ขนส่ง</span> (รับสินค้า พุธ, 20 - อาทิตย์, 23 ก.ย. 2560)</small></td>
        </tr>
        <tr class="table-light">
          <td class="text-right">10 ลัง <small>(120ชิ้น)</small></td>
          <td class="text-right">500</td>
          <td class="text-right"><small>(5%)</small> 25</td>
          <td class="text-right">57,000</td>
        </tr>

        <tr>
          <td rowspan="2">5</td>
          <td colspan="5">product 05! You should check in on some of those fields below.<br><small class="text-muted"><span class="text-info">สถานะ : ขนส่ง</span> (รับสินค้า พุธ, 20 - อาทิตย์, 23 ก.ย. 2560)</small></td>
        </tr>
        <tr class="table-light">
          <td class="text-right">10 ลัง <small>(120ชิ้น)</small></td>
          <td class="text-right">500</td>
          <td class="text-right"><small>(5%)</small> 25</td>
          <td class="text-right">57,000</td>
        </tr>

        <tr class="table-warning">
          <td></td>
          <td class="text-right"><strong>50 ลัง<small> (600ชิ้น)</small></strong></td>
          <td class="text-right"><strong>300,000</strong></td>
          <td class="text-right text-danger"><strong>1,5000</strong></td>
          <td class="text-right text-danger"><strong>285,000</strong></td>
        </tr>

      </tbody>
    </table>
    <div class="alert alert-light font-cloud rounded-0">
      <div class="row align-items-center">
      <div class="col-md-9">
        <span class="lead text-success">สถานะ : รับสินค้าบางส่วน</span>
        <span class="text-dark">(รับสินค้า เสาร์, 16 - อาทิตย์, 23 ก.ย. 2560)</span><br>
        <span>โอนเงินเข้าบัญชี: ธนาคารกสิกรไทย | ยอดโอน: 2,550 บาท | วันที่โอน: 15/09/2560 เวลา 15:30 น.</span>
      </div>
      <div class="col-md-3">
        <a href="" class="float-md-right text-success my-3"><i class="fa fa-file-text-o">&nbsp;&nbsp;</i>หลักฐานการโอน</a>
      </div>
      </div>
    </div>
    <hr class="border-dashed border-success my-5">
  </div>
  </div> --}}

  <!-- Purchase Order -->
  {{-- <div class="row my-3">
  <div class="col-12">
    <div class="title-doc position-sticky">
      <h6 class="font-cloud">
        <span class="badge badge-success float-left rounded-0 p-2 mr-3">No.<br>02</span>
        <span class="text-black d-inline-block mt-1">วันที่ 10 กันยายน 2560 | 10:55 </span><br>
        <small>เลขที่เอกสาร : PO-00002313</small>
      </h6>
    </div>
    <table class="table table-bordered">
      <thead>
        <tr class="font-cloud">
          <th class="position-sticky">#</th>
          <th class="position-sticky">จำนวน</th>
          <th class="position-sticky">ราคา/ชิ้น</th>
          <th class="position-sticky">ส่วนลด/ชิ้น</th>
          <th class="position-sticky">ราคารวม</th>
        </tr>
      </thead>
      <tbody>

        <tr>
          <td rowspan="2">1</td>
          <td colspan="5">product 01! You should check in on some of those fields below.<br><small class="text-muted"><span class="text-success">สถานะ : รับแล้ว</span> (รับสินค้า จันทร์, 11 - พุธ, 13 ก.ย. 2560)</small></td>
        </tr>
        <tr class="table-light">
          <td class="text-right">10 ลัง <small>(120ชิ้น)</small></td>
          <td class="text-right">500</td>
          <td class="text-right"><small>(5%)</small> 25</td>
          <td class="text-right">57,000</td>
        </tr>


        <tr>
          <td rowspan="2">2</td>
          <td colspan="5">product 02! You should check in on some of those fields below.<br><small class="text-muted"><span class="text-success">สถานะ : รับแล้ว</span> (รับสินค้า จันทร์, 11 - พุธ, 13 ก.ย. 2560)</small></td>
        </tr>
        <tr class="table-light">
          <td class="text-right">10 ลัง <small>(120ชิ้น)</small></td>
          <td class="text-right">500</td>
          <td class="text-right"><small>(5%)</small> 25</td>
          <td class="text-right">57,000</td>
        </tr>

        <tr>
          <td rowspan="2">3</td>
          <td colspan="5">product 03! You should check in on some of those fields below.<br><small class="text-muted"><span class="text-success">สถานะ : รับแล้ว</span> (รับสินค้า จันทร์, 11 - พุธ, 13 ก.ย. 2560)</small></td>
        </tr>
        <tr class="table-light">
          <td class="text-right">10 ลัง <small>(120ชิ้น)</small></td>
          <td class="text-right">500</td>
          <td class="text-right"><small>(5%)</small> 25</td>
          <td class="text-right">57,000</td>
        </tr>

        <tr>
          <td rowspan="2">4</td>
          <td colspan="5">product 04! You should check in on some of those fields below.<br><small class="text-muted"><span class="text-success">สถานะ : รับแล้ว</span> (รับสินค้า จันทร์, 11 - พุธ, 13 ก.ย. 2560)</small></td>
        </tr>
        <tr class="table-light">
          <td class="text-right">10 ลัง <small>(120ชิ้น)</small></td>
          <td class="text-right">500</td>
          <td class="text-right"><small>(5%)</small> 25</td>
          <td class="text-right">57,000</td>
        </tr>

        <tr>
          <td rowspan="2">5</td>
          <td colspan="5">product 05! You should check in on some of those fields below.<br><small class="text-muted"><span class="text-success">สถานะ : รับแล้ว</span> (รับสินค้า จันทร์, 11 - พุธ, 13 ก.ย. 2560)</small></td>
        </tr>
        <tr class="table-light">
          <td class="text-right">10 ลัง <small>(120ชิ้น)</small></td>
          <td class="text-right">500</td>
          <td class="text-right"><small>(5%)</small> 25</td>
          <td class="text-right">57,000</td>
        </tr>

        <tr class="table-warning">
          <td></td>
          <td class="text-right"><strong>50 ลัง<small> (600ชิ้น)</small></strong></td>
          <td class="text-right"><strong>300,000</strong></td>
          <td class="text-right text-danger"><strong>1,5000</strong></td>
          <td class="text-right text-danger"><strong>285,000</strong></td>
        </tr>

      </tbody>
    </table>
    <div class="alert alert-light font-cloud rounded-0">
      <div class="row align-items-center">
      <div class="col-md-9">
        <span class="lead text-success">สถานะ : รับสินค้าครบแล้ว</span>
        <span class="text-dark">(รับสินค้า จันทร์, 11 - พุธ, 13 ก.ย. 2560)</span><br>
        <span>โอนเงินเข้าบัญชี: ธนาคารทหารไทย | ยอดโอน: 2,550 บาท | วันที่โอน: 10/09/2560 เวลา 10:55 น.</span>
      </div>
      <div class="col-md-3">
        <a href="" class="float-md-right text-success my-3"><i class="fa fa-file-text-o">&nbsp;&nbsp;</i>หลักฐานการโอน</a>
      </div>
      </div>
    </div>
    <hr class="border-dashed border-success my-5">
  </div>
  </div> --}}

  <!-- Purchase Order -->
  {{-- <div class="row my-3">
  <div class="col-12">
    <div class="title-doc position-sticky">
      <h6 class="font-cloud">
        <span class="badge badge-success float-left rounded-0 p-2 mr-3">No.<br>01</span>
        <span class="text-black d-inline-block mt-1">วันที่ 01 กันยายน 2560 | 20:30 </span><br>
        <small>เลขที่เอกสาร : PO-00002312</small>
      </h6>
    </div>
    <table class="table table-bordered">
      <thead>
        <tr class="font-cloud">
          <th class="position-sticky">#</th>
          <th class="position-sticky">จำนวน</th>
          <th class="position-sticky">ราคา/ชิ้น</th>
          <th class="position-sticky">ส่วนลด/ชิ้น</th>
          <th class="position-sticky">ราคารวม</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td rowspan="2">1</td>
          <td colspan="5">product 01! You should check in on some of those fields below.<br><small class="text-muted"><span class="text-success">สถานะ : รับแล้ว</span> (รับสินค้า เสาร์, 2 - จันทร์, 4 ต.ค. 2560)</small></td>
        </tr>
        <tr class="table-light">
          <td class="text-right">10 ลัง <small>(120ชิ้น)</small></td>
          <td class="text-right">500</td>
          <td class="text-right"><small>(5%)</small> 25</td>
          <td class="text-right">57,000</td>
        </tr>

        <tr>
          <td rowspan="2">2</td>
          <td colspan="5">product 02! You should check in on some of those fields below.<br><small class="text-muted"><span class="text-success">สถานะ : รับแล้ว</span> (รับสินค้า เสาร์, 2 - จันทร์, 4 ต.ค. 2560)</small></td>
        </tr>
        <tr class="table-light">
          <td class="text-right">10 ลัง <small>(120ชิ้น)</small></td>
          <td class="text-right">500</td>
          <td class="text-right"><small>(5%)</small> 25</td>
          <td class="text-right">57,000</td>
        </tr>

        <tr>
          <td rowspan="2">3</td>
          <td colspan="5">product 03! You should check in on some of those fields below.<br><small class="text-muted"><span class="text-success">สถานะ : รับแล้ว</span> (รับสินค้า เสาร์, 2 - จันทร์, 4 ต.ค. 2560)</small></td>
        </tr>
        <tr class="table-light">
          <td class="text-right">10 ลัง <small>(120ชิ้น)</small></td>
          <td class="text-right">500</td>
          <td class="text-right"><small>(5%)</small> 25</td>
          <td class="text-right">57,000</td>
        </tr>

        <tr>
          <td rowspan="2">4</td>
          <td colspan="5">product 04! You should check in on some of those fields below.<br><small class="text-muted"><span class="text-success">สถานะ : รับแล้ว</span> (รับสินค้า เสาร์, 2 - จันทร์, 4 ต.ค. 2560)</small></td>
        </tr>
        <tr class="table-light">
          <td class="text-right">10 ลัง <small>(120ชิ้น)</small></td>
          <td class="text-right">500</td>
          <td class="text-right"><small>(5%)</small> 25</td>
          <td class="text-right">57,000</td>
        </tr>

        <tr>
          <td rowspan="2">5</td>
          <td colspan="5">product 05! You should check in on some of those fields below.<br><small class="text-muted"><span class="text-success">สถานะ : รับแล้ว</span> (รับสินค้า เสาร์, 2 - จันทร์, 4 ต.ค. 2560)</small></td>
        </tr>
        <tr class="table-light">
          <td class="text-right">10 ลัง <small>(120ชิ้น)</small></td>
          <td class="text-right">500</td>
          <td class="text-right"><small>(5%)</small> 25</td>
          <td class="text-right">57,000</td>
        </tr>

        <tr class="table-warning">
          <td></td>
          <td class="text-right"><strong>50 ลัง<small> (600ชิ้น)</small></strong></td>
          <td class="text-right"><strong>300,000</strong></td>
          <td class="text-right text-danger"><strong>1,5000</strong></td>
          <td class="text-right text-danger"><strong>285,000</strong></td>
        </tr>

      </tbody>
    </table>
    <div class="alert alert-light font-cloud rounded-0">
      <div class="row align-items-center">
      <div class="col-md-9">
        <span class="lead text-success">สถานะ : รับสินค้าครบแล้ว</span>
        <span class="text-dark">(รับสินค้า เสาร์, 2 - จันทร์, 4 ต.ค. 2560)</span><br>
        <span>โอนเงินเข้าบัญชี: ธนาคารทหารไทย | ยอดโอน: 2,550 บาท | วันที่โอน: 01/09/2560 เวลา 20:30 น.</span>
      </div>
      <div class="col-md-3">
        <a href="" class="float-md-right text-success my-3"><i class="fa fa-file-text-o">&nbsp;&nbsp;</i>หลักฐานการโอน</a>
      </div>
      </div>
    </div>
    <hr class="border-dashed border-success my-5">
  </div>
  </div> --}}

</div>

<!-- Close Button -->
<div class="fixed-bottom text-center my-3 remove-this">
  <a class="btn btn-secondary rounded-circle opacity-3" href="javascript:void(0)" onclick="closeOrderHistory()"><i class="fa fa-close"></i></a>
</div>

<!-- Print Page -->
<script src="js/jquery.printDiv.js"></script>
<script>
  // Print
  $(document).ready(function() {
    $('[data-print]').click(function() {
      $('div.remove-this').addClass('d-print-none');
      $('[data-print-content]').printDiv();
      location.reload(true);
    });
  });
</script>
