{{-- <small><strong>500 บาท/ชิ้น, 6,000 บาท/ลัง(12ชิ้น)<br>สั่งซื้อขั้นต่ำ 5 ลัง = 30,000 บาท</strong></small> --}}
<small><strong>{{ number_format(($product->price/$product->piece), 2, '.', ',') }} บาท/ชิ้น, {{ $product->price }} บาท/ลัง({{$product->piece}}ชิ้น)<br>สั่งซื้อขั้นต่ำ {{$product->order_minimum}} ลัง = {{ number_format(($product->price*$product->order_minimum), 2, '.', ',') }} บาท</strong></small>
{{-- <small><strong>{{ number_format(($product->price/$product->piece), 2, '.', '') }} บาท/ชิ้น, {{ $product->price }} บาท/ลัง({{$product->piece}}ชิ้น)<br> --}}
