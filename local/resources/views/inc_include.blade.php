<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="{{asset('assets/images/icons/favicon.png')}}">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<title>GOM Beta</title>

<!-- Bootstrap core CSS -->
{{ Html::style('assets/css/bootstrap.min.css') }}
{{-- {{ Html::script('https://code.jquery.com/ui/1.12.1/jquery-ui.js')}} --}}
{{-- <link href="css/bootstrap.min.css" rel="stylesheet"> --}}

<!-- Custom styles for this template -->
{{ Html::style('assets/datepicker/css/bootstrap-datetimepicker.min.css') }}
{{ Html::style('assets/css/style.css') }}
{{ Html::style('assets/css/thbanklogos.css') }}
{{-- <link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/thbanklogos.css" id="stylesheet"> --}}
{{ Html::style('assets/css/animate.css') }}
{{-- <link rel="stylesheet" href="css/animate.css" id="stylesheet"> --}}
<!-- <link rel="stylesheet" href="css/table.css" id="stylesheet"> -->
{{ Html::style('https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css') }}
