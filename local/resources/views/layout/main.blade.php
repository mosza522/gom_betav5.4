<!DOCTYPE html>
<html lang="en">
<head>
  @include('inc_include')

  @yield('head')

</head>

<body>

  @if (session('success'))
    <script>
        swal("Successfully", "{{ Session::get('success') }}", "success");
    </script>
  @endif

  @if (session('wrong'))
    <script>
        swal("Something Wrong!!", "{{ Session::get('wrong') }}", "error");
    </script>
  @endif
  @if (session('status'))
    <script>
        swal("Successfully", "{{ Session::get('status') }}", "success");
    </script>
@endif
@if ($errors->has('username'))
    <script>
        swal("Something Wrong", "{{'Username หรือ Password ไม่ถูกต้อง'}}", "error");
    </script>
@endif
@if ($errors->has('email'))
    <script>
        swal("Something Wrong", "{{ $errors->first('email') }}", "error");
    </script>
@endif
@if ($errors->has('password'))
    <script>
        swal("Something Wrong", "{{ $errors->first('password') }}", "error");
    </script>
@endif
@if ($errors->has('password_confirmation'))
    <script>
        swal("Something Wrong", "{{ $errors->first('password_confirmation') }}", "error");
    </script>
@endif
<div id="site-boxed">

  <!-- Header -->
  @include('inc_header')
  <style>
  #snackbar {
      visibility: hidden;
      min-width: 250px;
      margin-left: -125px;
      background-color: #333;
      color: #fff;
      text-align: center;
      border-radius: 2px;
      padding: 16px;
      position: fixed;
      z-index: 1;
      left: 50%;
      bottom: 30px;
      font-size: 17px;
  }

  #snackbar.show {
      visibility: visible;
      -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
      animation: fadein 0.5s, fadeout 0.5s 2.5s;
  }

  @-webkit-keyframes fadein {
      from {bottom: 0; opacity: 0;}
      to {bottom: 30px; opacity: 1;}
  }

  @keyframes fadein {
      from {bottom: 0; opacity: 0;}
      to {bottom: 30px; opacity: 1;}
  }

  @-webkit-keyframes fadeout {
      from {bottom: 30px; opacity: 1;}
      to {bottom: 0; opacity: 0;}
  }

  @keyframes fadeout {
      from {bottom: 30px; opacity: 1;}
      to {bottom: 0; opacity: 0;}
  }
  </style>
  <!-- Header -->
  @yield('content')
  @php
    $productname=\App\Models\Product::get();
    $namepro= array();
    foreach ($productname as $key ) {
      if(Session::has('banproduct')){
        $banproduct=array_unique(Session::get('banproduct'));
        $ban=false;
        foreach ($banproduct as $key2 ) {
          if($key2==$key->id){
            $ban=true;
          }
        }
        if(!$ban){
          array_push($namepro,array("label"=>$key->name,"value"=>$key->id));
        }
      }else{
        array_push($namepro,array("label"=>$key->name,"value"=>$key->id));
      }
      }
      // if (!Auth::guest()){
      //   $banproduct=array_unique(Session::get('banproduct'));
      //   foreach ($productname as $key ) {
      //     for ($i=0; $i < count($banproduct) ; $i++) {
      //       if($banproduct[$i]!=$key->id){
      //         array_push($namepro,array("label"=>$key->name,"value"=>$key->id));
      //       }
      //     }
      //   }
      // }
      // else{
      //   foreach ($productname as $key ) {
      //     array_push($namepro,array("label"=>$key->name,"value"=>$key->id));
      //   }
      // }


  @endphp
  <script src="{{asset('assets/js/scripts.js')}}"></script>
  <script type="text/javascript">
  $(function() {
    var productName = $.parseJSON('{!! json_encode($namepro) !!}');
    // alert(JSON.stringify(productName));
    var NoResultsLabel = "No Results";
    $( ".searchbox" ).autocomplete({
      source: function(request, response) {
        var results = $.ui.autocomplete.filter(productName, request.term);

        if (!results.length) {
          results = [NoResultsLabel];
        }

        response(results);
      },
      select: function (event, ui) {
        if (ui.item.label === NoResultsLabel) {
          event.preventDefault();
        }
        $( ".searchbox" ).val(ui.item.label);
        window.location="{{ url('product_details') }}"+"/"+ui.item.value;
        // alert(ui.item.label);
      }
      // focus: function (event, ui) {
      //   if (ui.item.label === NoResultsLabel) {
      //     event.preventDefault();
      //   }
      //   $(".searchbox").autocomplete("search");
      // }
    });

  });
  // $('#form-search').submit(function(){
  //   alert($('.searchbox').val());
  //   return false;
  // });
  function changeM(month) {
    $.ajax({
      url: '{{url('changM')}}/'+month,
      type: 'get',
      dataType:'html',
      success: function (html) {
        window.location="{{ url('/order') }}";
      },
    });
  }
  function changeY(year) {

    if(isNaN(year)){
      newyear=year;
    }else{
      newyear=year-543;
    }
    $.ajax({
      url: '{{url('changY')}}/'+newyear,
      type: 'get',
      dataType:'html',
      success: function (html) {
        window.location="{{ url('/order') }}";
      },
    });
  }
  </script>
</div>

</body>
</html>
