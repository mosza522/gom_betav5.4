@extends('layout.main')
@section('content')
@php
$product=\App\Models\Product::leftjoin('product_brand','product.band_id','=','product_brand.id')
->leftJoin('product_category_sub', 'product.category_sub_id', '=', 'product_category_sub.id')
->leftJoin('product_category', 'product_category_sub.category_id', '=', 'product_category.id')
->select('product.*','product_brand.name as brand','product_category_sub.name as nameSubCategory','product_brand.id as brand_id'
,'product_category_sub.id as category_sub_id','product_category.name as nameCategory','product_category.id as idCategory')
->where('product.id',$id)
->first();
$img=\App\Models\ProductImg::where('product_id',$id)->get();
$promotion=\App\Models\ProductPromotion::where('product_id',$id)->get();
$sku= \App\Models\Sku::where('product_id',$id)->get();
$zones= \App\Models\Zone::where('product_id',$id)->get();
$total=$product->order_minimum*$product->price;
@endphp
  <!-- Breadcrumb -->
  <nav class="breadcrumb mb-5">
  <div class="container text-right">
    <a class="breadcrumb-item" href="product">{{$product->nameCategory}}</a>
    <a class="breadcrumb-item" href="product">{{$product->nameSubCategory}}</a>
    <span class="breadcrumb-item active">{{$product->common_name}}</span>
  </div>
  </nav>

  <!-- container -->
  <div class="container">

    <!-- Product -->
    <div class="row">
      <!-- Boxed Left -->
      <div class="col-lg-9">
        <div class="border border-dashed py-3">
          <div class="row px-3">
            <div class="col-md-6 col-lg-7">
              <div class="product-slide">
                <ul class="thumb-box">
                  @foreach ($img as $key )
                  <li><a class="url" href="#hash_{{$key->id}}"><img src="{{asset('local/storage/app/images/product')}}/{{ $key->name }}" class="img-fluid"></a></li>

                  @endforeach
                  {{-- <li><a class="url" href="#hash_01"><img src="{{asset('assets/images/product/product_35.png')}}" class="img-fluid"></a></li>
                  <li><a class="url" href="#hash_02"><img src="{{asset('assets/images/product/product_35.png')}}" class="img-fluid"></a></li>
                  <li><a class="url" href="#hash_03"><img src="{{asset('assets/images/product/product_35.png')}}" class="img-fluid"></a></li>
                  <li><a class="url" href="#hash_04"><img src="{{asset('assets/images/product/product_35.png')}}" class="img-fluid"></a></li>
                  <li><a class="url" href="#hash_05"><img src="{{asset('assets/images/product/product_35.png')}}" class="img-fluid"></a></li> --}}

                  </ul>
                <div class="slide-box owl-carousel owl-theme">
                  @foreach ($img as $key )
                    <div class="item" data-hash="hash_{{$key->id}}">
                      <img src="{{asset('local/storage/app/images/product')}}/{{ $key->name }}" class="img-fluid">
                    </div>
                  @endforeach

                  {{-- <div class="item" data-hash="hash_01">
                    <img src="{{asset('assets/images/product/product_35.png')}}" class="img-fluid">
                  </div>
                  <div class="item" data-hash="hash_02">
                    <img src="{{asset('assets/images/product/product_35.png')}}" class="img-fluid">
                  </div>
                  <div class="item" data-hash="hash_03">
                    <img src="{{asset('assets/images/product/product_35.png')}}" class="img-fluid">
                  </div>
                  <div class="item" data-hash="hash_04">
                    <img src="{{asset('assets/images/product/product_35.png')}}" class="img-fluid">
                  </div>
                  <div class="item" data-hash="hash_05">
                    <img src="{{asset('assets/images/product/product_35.png')}}" class="img-fluid">
                  </div> --}}
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-5">
              <h6 class="font-cloud">{{ $product->nameCategory }} / {{ $product->common_name }} / {{ $product->name }}<small> | ยี่ห้อ&nbsp;<a href="{{url('profile')}}/{{ $product->band_id }}">{{ $product->brand }}</a></small></h6>
              <hr class="border-dashed my-2">
              <div class="row">
                <div class="col-6">ราคา : </div>
                <div class="col-6"><strong>{{ number_format($product->price/$product->piece) }} บาท</strong> / ชิ้น</div>
              </div>
              <div class="row">
                <div class="col-6">สั่งซื้อขั้นต่ำ : </div>
                <div class="col-6">{{ $product->order_minimum }} ลัง <small>({{$product->piece}} / ลัง)</small></div>
              </div>
              <div class="row">
                <div class="col-6">ความสามารถจัดหา : </div>
                <div class="col-6">{{ $product->supply }} <small>ลัง / เดือน</small></div>
              </div>
              <div class="row">
                <div class="col-6">เวลาส่งมอบ : </div>
                <div class="col-6">ภายใน {{ $product->time }} วัน</div>
              </div>
              <div class="row">
                <div class="col-12">
                  <h6 class="my-2 text-success"><i class="fa fa-street-view mr-1"></i>Promotion </h6>
                  @foreach ($promotion as $element)
                    <a class="badge badge-pill badge-light border border-dashed font-weight-normal p-2 mb-1 w-25"> {{$element->amount}} ลัง | <strong class="text-success">-{{$element->discount}}%</strong></a>
                  @endforeach
                </div>
              </div>
              <hr class="border-dashed my-3">
              <form id="cartForm" onsubmit="return addCart()">
              <div class="form-group row mb-1">
                  <div class="col-6">
                    <div class="input-group">
                      <span class="input-group-btn">
                        <button type="button" class="btn btn-sm btn-number float-left" disabled="disabled" data-type="minus" data-field="amount">-</button>
                      </span>
                      <input id="amount" type="text" name="amount" class="form-control form-control-sm input-number text-center float-left" value="{{ $product->order_minimum }}" min="{{ $product->order_minimum }}" max="100" onchange="return checkmax(this.value,{{ $product->id }})">
                      <span class="input-group-btn">
                        <button type="button" class="btn btn-sm btn-number float-left" data-type="plus" data-field="amount">+</button>
                      </span>
                      <span class="input-group-btn">
                        <button type="button" class="btn btn-sm float-left" disabled="disabled" value="จำนวน">ลัง</button>
                      </span>
                    </div>
                  </div>
                  <label class="col-6">สั่งซื้อขั้นต่ำ {{ $product->order_minimum }} ลัง / สูงสุด 100 ลัง</label>
                </div>
                <div class="row">
                  <div class="col-6">ราคารวม</div>
                  <div class="col-6"><strong id="price">{{number_format($product->order_minimum*$product->price)}}</strong><small class="float-right">บาท</small></div>
                </div>
                <div class="row">
                  <div class="col-6">ส่วนลด <span id="discountNum" class="badge badge-pill badge-light border border-secondary border-dashed float-right">0%</span></div>
                  <div class="col-6"><strong id="discount">
                    @foreach ($promotion as $element)
                      @if ($product->order_minimum >= $element->amount)
                        @php
                        $discount=$total/$element->discount;
                          $discount;
                          break;
                        @endphp
                      @else
                        @php
                          $discount=0;
                        @endphp
                      @endif
                    @endforeach
                    {{ number_format($discount,2) }}
                  </strong><small class="float-right">บาท</small></div>
                </div>
                <div class="row">
                  <div class="col-6">ยอดสุทธิ </div>
                  <div class="col-6 text-danger"><strong id="total">{{ number_format($total-$discount,2) }}</strong><small class="float-right">บาท</small></div>
                </div>

                <hr class="border-dashed my-2">
                <small class="text-success d-block mb-2"><i class="fa fa-info"></i> กันเขตร้านค้า
                  @php
                  $text="";
                  @endphp
                  @foreach ($zones as $zone)
                    @php
                    $text.="ซื้อ $zone->num ลัง : กันเขต $zone->radius km/$zone->time วัน <br />";
                    @endphp
                  @endforeach
            		{!! $text !!}</small>


                <input type="hidden" id="hidden_id" name="id" value="{{ $id }}">
                <button type="submit" class="btn btn-block btn-success rounded-0">เพิ่มในตะกร้าสินค้า</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- Boxed Right -->
      <div class="col-lg-3">
        <div class="border border-dashed text-muted h-100 py-3">
          <div class="row px-3">
            <div class="col-md-12 fr-view ">
              <h6 class="w-100 font-cloud text-dark"><i class="fa fa-info-circle mr-2"></i>ข้อมูลเบื้องต้น</h6>
              <hr class="border-dashed">
              <div class="text-center">
                {!! $product->detail_title !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Content -->
    <div class="row">
      <!-- Boxed Left -->
      <div class="col-md-12 col-xl-9">
        <div class="row">
          <div class="col-12">
            <!-- NavTap -->
            <div id="data_link" class="mt-5">
              <ul class="con-scroll doc-pills nav nav-pills nav-justified font-cloud">
                <li class="nav-item w-20">
                  <a id="tab-main-01" class="nav-scroll h-100 nav-link active" href="#doc_pill_01">
                    <i class="fa fa-info-circle mr-sm-2"></i>
                    <span class="d-none d-sm-inline-block">ข้อมูลสินค้า</span>
                  </a>
                </li>
                <li class="nav-item w-20">
                  <a id="tab-main-02" class="nav-scroll h-100 nav-link" href="#doc_pill_02">
                    <i class="fa fa-building mr-sm-2"></i>
                    <span class="d-none d-sm-inline-block">ข้อมูลผู้จำหน่าย</span>
                  </a>
                </li>
                <li class="nav-item w-20">
                  <a id="tab-main-03" class="nav-scroll h-100 nav-link" href="#doc_pill_03">
                    <i class="fa fa-street-view mr-sm-2"></i>
                    <span class="d-none d-sm-inline-block">แผนที่ร้านค้า</span>
                  </a>
                </li>
                <li class="nav-item w-20">
                  <a id="tab-main-03" class="nav-scroll h-100 nav-link" href="#doc_pill_04">
                    <i class="fa fa-commenting mr-sm-2"></i>
                    <span class="d-none d-sm-inline-block">รีวิว</span>
                  </a>
                </li>
                <li class="nav-item w-20">
                  <a class="h-100 nav-link" data-toggle="modal" href="inc_cart-add">
                    <i class="fa fa-shopping-basket mr-sm-2"></i>
                    <span class="d-none d-sm-inline-block">ใส่ตะกร้า</span>
                  </a>
                </li>
              </ul>
            </div>
            <!-- Details -->
            <div class="border border-light bg-light py-5 mb-5">
              <div data-target=".con-scroll" id="doc_pill_01">
                <h5 class="font-cloud font-weight-bold mx-3">ข้อมูลสินค้า</h5>
                <hr class="border-dashed mx-3">
                <div class="row px-3">
                  <div class="col-12 fr-view">
                    <table class="table table-sm table-bordered border-dotted mb-4">
                      <thead class="bg-white">
                        <tr>
                          <th class="border-0" colspan="2"><small>SKU (ข้อมูลจำเพาะ)</small></th>
                        </tr>
                      </thead>
                      <tbody class="text-muted">
                        @php
                          $i=1;
                        @endphp
                        @foreach ($sku as $key )
                          <tr>
                            <td class="border-dotted">SKU {{ $i++ }}</td>
                            <td class="border-dotted">{{ $key->sku}}</td>
                          </tr>
                        @endforeach
                        {{-- <tr>
                          <td class="border-dotted">SKU Title 01</td>
                          <td class="border-dotted">SKU Data 01</td>
                        </tr>
                        <tr>
                          <td class="border-dotted">SKU Title 02</td>
                          <td class="border-dotted">SKU Data 02</td>
                        </tr>
                        <tr>
                          <td class="border-dotted">SKU Title 03</td>
                          <td class="border-dotted">SKU Data 03</td>
                        </tr>
                        <tr>
                          <td class="border-dotted">SKU Title 04</td>
                          <td class="border-dotted">SKU Data 04</td>
                        </tr>
                        <tr>
                          <td class="border-dotted">SKU Title 05</td>
                          <td class="border-dotted">SKU Data 05</td>
                        </tr>
                        <tr>
                          <td class="border-dotted">SKU Title 06</td>
                          <td class="border-dotted">SKU Data 06</td>
                        </tr>
                        <tr>
                          <td class="border-dotted">SKU Title 07</td>
                          <td class="border-dotted">SKU Data 07</td>
                        </tr>
                        <tr>
                          <td class="border-dotted">SKU Title 08</td>
                          <td class="border-dotted">SKU Data 08</td>
                        </tr> --}}
                      </tbody>
                    </table>
                    {!! $product->detail !!}
                    {{-- <p>Morbi in felis elementum, sodales lacus ac, aliquam purus. Donec aliquet ligula ligula, ut efficitur mauris porttitor nec. Sed magna dui, efficitur at mauris vel, egestas faucibus ante. Aliquam elementum, velit eu consequat tempor, libero lectus cursus dui, at vehicula nisi lacus vel risus. Integer interdum molestie risus, eget hendrerit magna blandit vitae. In sed elit sed augue ultricies varius vel accumsan tellus. Sed malesuada orci nisl, eu finibus lectus maximus non.</p>
                    <img src="{{asset('assets/images/product/product_banner.jpg')}}" class="img-fluid mb-3">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vehicula, nisi sed tincidunt congue, sapien quam eleifend massa, vitae dapibus tellus neque in orci. Sed pharetra felis in nisl aliquam, ac auctor orci condimentum. Cras finibus est eget mi luctus, id consectetur dui sollicitudin. Curabitur semper purus non lacus scelerisque, id sodales eros tempor. Nullam euismod tortor ipsum, ac rhoncus odio euismod ultricies. Nunc maximus lobortis tincidunt. Morbi ornare lorem nec turpis fermentum sollicitudin tempor nec quam. Morbi in felis elementum, sodales lacus ac, aliquam purus. Donec aliquet ligula ligula, ut efficitur mauris porttitor nec. Sed magna dui, efficitur at mauris vel, egestas faucibus ante. Aliquam elementum, velit eu consequat tempor, libero lectus cursus dui, at vehicula nisi lacus vel risus. Integer interdum molestie risus, eget hendrerit magna blandit vitae. In sed elit sed augue ultricies varius vel accumsan tellus. Sed malesuada orci nisl, eu finibus lectus maximus non.
                    </p> --}}
                  </div>
                </div>
              </div>
            </div>
            <!-- Company -->
            @php

              $banner=\App\Models\BrandBanner::where('brand_id',$product->band_id)->get();
              $band=\App\Models\ProductBrands::find($product->band_id);
            @endphp
            <div class="border border-light bg-light pt-5 my-5">
              <div data-target=".con-scroll" id="doc_pill_02">
                <h5 class="font-cloud font-weight-bold mx-3">ข้อมูลผู้จำหน่าย</h5>
                <hr class="border-dashed mx-3">
                <div class="image_upload bg-light m-0">
                  <!-- Image Cover -->
                  <div class="img_cover">
                    <div class="img_cover_Inp d-none">
                      <input class="up_img_cover" id="imgCoverInp" type="file"/>
                      <label class="up_img_cover m-0" for="imgCoverInp" title="Edit"><i class="fa fa-camera"></i></label>
                    </div>
                    <div class="cove_pic owl-carousel owl-theme">

                      @foreach ($banner as $element)
                        <a class="item" href="javascript:void(0)" title="View Products">
                          <img id="cove_pic" src="{{asset('local/storage/app/images/banner')}}/{{ $element->banner }}">
                        </a>
                      @endforeach
                      {{-- <a class="item" href="product" title="View Products">
                        <img id="cove_pic" src="{{asset('assets/images/company_banner3.jpg')}}">
                      </a>
                      <a class="item" href="product" title="View Products">
                        <img id="cove_pic" src="{{asset('assets/images/company_banner2.jpg')}}">
                      </a> --}}
                    </div>
                  </div>
                  <!-- Image Profile -->
                  <div  class="img_profile">
                    <div class="profile_pic rounded-circle">
                      <a href="{{ url('profile') }}/{{ $product->band_id }}" title="View Products">
                        <img id="profile_pic" class="rounded-circle" src="{{asset('local/storage/app/images/logo')}}/{{ $band->logo }}">
                      </a>
                    </div>
                    <div class="img_profile_Inp d-none">
                      <input class="up_img_profile" id="imgInp" type="file"/>
                      <label class="up_img_profile m-0" for="imgInp" title="Edit"><i class="fa fa-camera"></i></label>
                    </div>
                  </div>
                  <!-- Description -->
                  <div class="position-relative text-justify p-3 p-lg-4" style="top:-60px;">
                    <h5 class="text-center text-success text-uppercase">{{$band->name}}</h5>
                    <hr class="border-dashed">
                    <p class="text-center">
                      {!! $band->history !!}
                    </p>
                    <hr class="border-dashed">
                  </div>
                  <div class="text-center">
                    <a class="btn btn-success mb-5" href="{{url('profile')}}/{{ $product->band_id }}">Profile Page</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- ShopMap -->
            <div class="border border-light bg-light py-5 my-5">
              <div data-target=".con-scroll" id="doc_pill_03">
                <h5 class="font-cloud font-weight-bold mx-3">แผนที่ร้านค้า</h5>
                <hr class="border-dashed mx-3">
                <div class="row px-3">
                  <div class="col-12">
                    <div class="mb-3" id="dvMap" style="height: 70vh; width: 100%;"></div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Reviews -->
            @php
              $review=\App\Models\Review::leftjoin('users','review.user_id','=','users.id')
              ->select('review.*','users.img_profile','users.name')
              ->where('product_id',$id)
              ->orderBy('review.id','DESC')->get();
              $no=count($review);
            @endphp
            <div class="border border-light bg-light py-5 my-5">
              <div data-target=".con-scroll" id="doc_pill_04">
                <div class="row px-3">
                  <div class="col-12">
                    <h5 class="font-cloud font-weight-bold">รีวิวสินค้า
                      <span class="text-success font-weight-normal"><i class="fa fa-comments">&nbsp;</i>{{count($review)}} รีวิว</span>
                      @if (!Auth::guest())
                        <button id="write_review" type="button" class="btn btn-success rounded-0 px-2 px-sm-5 float-right"><i class="fa fa-pencil mr-2"></i>เขียนรีวิว</button>
                      @endif
                    </h5>
                    <hr class="border-dashed">
                    <form id="form_review" action="{{ url('review') }}" method="post">
                      {!! csrf_field() !!}
                      <div class="jumbotron rounded-0 py-4">
                        <div class="form-group">
                          <label for="review-title">หัวข้อรีวิว</label>
                          <input type="text" class="form-control rounded-0" id="review-title" name="title">
                          <input type="hidden" name="id" value="{{ $id }}">

                        </div>
                        <div class="form-group">
                          <label for="review-detail">เนื้อหาของรีวิว</label>
                          <textarea class="form-control rounded-0" id="review-detail" rows="5" name="body"></textarea>
                        </div>
                        <button type="submit" class="btn btn-block btn-success rounded-0 px-5">ส่งรีวิว</button>
                      </div>
                    </form>
                    <ul class="list-unstyled">
                      @foreach ($review as $key )
                        <li class="media my-4">
                          <img class="rounded-circle mr-3 mb-2" width="75px" height="75px" src="{{asset('local\storage\app\images\user')}}\{{ $key->img_profile }}" alt="">
                          <div class="media-body">
                            <h6 class="font-cloud my-0"><span class="badge badge-pill badge-site mr-2">{{$no--}}</span>{{$key->title}}</h6>
                            <p class="my-0">{{ $key->body }}</p>
                            <small class="text-muted">โดย: {{ $key->name }} {{ $key->created_at }}</small>
                            <hr class="border border-light border-dotted">
                          </div>
                        </li>
                      @endforeach
                      {{-- <li class="media my-4">
                        <img class="rounded-circle mr-3 mb-2" src="{{asset('assets/images/icons/user_01.png')}}" alt="">
                        <div class="media-body">
                          <h6 class="font-cloud my-0"><span class="badge badge-pill badge-site mr-2">03</span>Review Title</h6>
                          <p class="my-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                          <small class="text-muted">โดย: Username 25-12-2017 18:20</small>
                          <hr class="border border-light border-dotted">
                        </div>
                      </li>
                      <li class="media my-4">
                        <img class="rounded-circle mr-3 mb-2" src="{{asset('assets/images/icons/user_02.png')}}" alt="">
                        <div class="media-body">
                          <h6 class="font-cloud my-0"><span class="badge badge-pill badge-site mr-2">02</span>Review Title</h6>
                          <p class="my-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                          <small class="text-muted">โดย: Username 25-12-2017 18:20</small>
                          <hr class="border border-light border-dotted">
                        </div>
                      </li>
                      <li class="media my-4">
                        <img class="rounded-circle mr-3 mb-2" src="{{asset('assets/images/icons/user_03.png')}}" alt="">
                        <div class="media-body">
                          <h6 class="font-cloud my-0"><span class="badge badge-pill badge-site mr-2">01</span>Review Title</h6>
                          <p class="my-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                          <small class="text-muted">โดย: Username 25-12-2017 18:20</small>
                          <hr class="border border-light border-dotted">
                        </div>
                      </li> --}}
                    </ul>
                    <hr class="border-dashed mt-5">
                    <nav aria-label="Page navigation example">
                      <ul class="pagination pagination-sm justify-content-end">
                        <li class="page-item float-left disabled">
                          <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true" class="fa fa-angle-left"></span>
                          </a>
                        </li>
                        <li class="page-item float-left active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item float-left"><a class="page-link" href="#">2</a></li>
                        <li class="page-item float-left"><a class="page-link" href="#">3</a></li>
                        <li class="page-item float-left"><a class="page-link" href="#">4</a></li>
                        <li class="page-item float-left"><a class="page-link" href="#">5</a></li>
                        <li class="page-item float-left">
                          <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true" class="fa fa-angle-right"></span>
                          </a>
                        </li>
                      </ul>
                    </nav>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Boxed Right -->
      <div class="col-md-12 col-xl-3">
        <div class="border border-light bg-light py-4 my-5">
          <div class="row p-3">
            <div class="col-12">
              <h5 class="font-cloud font-weight-bold">สืนค้าที่เกียวข้อง</h5>
              <div id="product-boxed">
                <div class="product-item side">
                  <div class="card rounded-0 bg-transparent">
                    <img class="card-img-top" src="{{asset('assets/images/product/product_29.png')}}" alt="Card image cap">
                    <div class="card-body position-relative">
                      <div class="product-btn btn-group invisible">
                        <a class="btn btn-sm btn-success w-50 border" data-toggle="modal" href="inc_cart-add">ช้อปเลย</a>
                        <a class="btn btn-sm btn-success w-50 border" href="product_details">ดูสินค้า</a>
                      </div>
                      <p class="card-text text-line-max2">This card has supporting text below as a natural lead-in to additional content.</p>
                    </div>
                    <div class="card-footer text-center text-sm-left">
                      <small class="discount"><del>1,000 ฿</del></small>
                      <span class="price">฿ 500 <small>/ ชิ้น</small></span>
                      <span class="tag float-sm-right" data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="Price" data-content="">-50% <i class="fa fa-info-circle"></i></span>
                      {{-- <span class="tag float-sm-right" data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="Price" data-content="@include('inc_info-price')">-50% <i class="fa fa-info-circle"></i></span> --}}
                    </div>
                  </div>
                </div>
                <div class="product-item side">
                  <div class="card rounded-0 bg-transparent">
                    <img class="card-img-top" src="{{asset('assets/images/product/product_02.png')}}" alt="Card image cap">
                    <div class="card-body position-relative">
                      <div class="product-btn btn-group invisible">
                        <a class="btn btn-sm btn-success w-50 border" data-toggle="modal" href="inc_cart-add">ช้อปเลย</a>
                        <a class="btn btn-sm btn-success w-50 border" href="product_details">ดูสินค้า</a>
                      </div>
                      <p class="card-text text-line-max2">This card has supporting text below as a natural lead-in to additional content.</p>
                    </div>
                    <div class="card-footer text-center text-sm-left">
                      <small class="discount"><del>1,000 ฿</del></small>
                      <span class="price">฿ 500 <small>/ ชิ้น</small></span>
                      <span class="tag float-sm-right" data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="Price" data-content="">-50% <i class="fa fa-info-circle"></i></span>
                      {{-- <span class="tag float-sm-right" data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="Price" data-content="@include('inc_info-price')">-50% <i class="fa fa-info-circle"></i></span> --}}
                    </div>
                  </div>
                </div>
                <div class="product-item side">
                  <div class="card rounded-0 bg-transparent">
                    <img class="card-img-top" src="{{asset('assets/images/product/product_30.png')}}" alt="Card image cap">
                    <div class="card-body position-relative">
                      <div class="product-btn btn-group invisible">
                        <a class="btn btn-sm btn-success w-50 border" data-toggle="modal" href="inc_cart-add">ช้อปเลย</a>
                        <a class="btn btn-sm btn-success w-50 border" href="product_details">ดูสินค้า</a>
                      </div>
                      <p class="card-text text-line-max2">This card has supporting text below as a natural lead-in to additional content.</p>
                    </div>
                    <div class="card-footer text-center text-sm-left">
                      <small class="discount"><del>1,000 ฿</del></small>
                      <span class="price">฿ 500 <small>/ ชิ้น</small></span>
                      <span class="tag float-sm-right" data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="Price" data-content="">-50% <i class="fa fa-info-circle"></i></span>
                      {{-- <span class="tag float-sm-right" data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="Price" data-content="@include('inc_info-price')">-50% <i class="fa fa-info-circle"></i></span> --}}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  <div id="snackbar">เพิ่มสินค้าลงในตะกร้าแล้ว</div>
  <!-- container -->

  <!-- Site footer -->
  @include('inc_footer')
  <!-- Site footer -->

  <!-- Site js -->
  @include('inc_js')
  <!-- Site js -->

  <!-- Site Sidewarp -->
  @include('inc_sidewarp')
  <!-- Site Sidewarp -->

  <!-- Owl Carousel -->
  <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/owl.theme.default.css')}}">
  <script src="{{asset('assets/js/owl.carousel.js')}}"></script>
  <script>
    $(document).ready(function() {

      // Slider Product
      var owl = $('.slide-box.owl-carousel');
      owl.owlCarousel({
        navText: [ '', '' ],
        loop: false,
        items: 1,
        dots: false,
        nav: false,
        mouseDrag: false,
        callbacks: true,
        URLhashListener: true,
        autoplayHoverPause: true,
        startPosition: '#hash_03'
      });

      // Slider Company
      var owl = $('.cove_pic.owl-carousel');
      owl.owlCarousel({
        navText: [ '', '' ],
        items: 1,
        loop: true,
        autoplay: true,
        autoplayTimeout: 6000,
        autoHeight: true,
        autoplayHoverPause: true,
        // animateOut: 'fadeOut',
        dots: true,
        responsive: {
          0:     {  nav: false  },
          768:   {  nav: true   }
        }
      });

    });
  </script>
  <!-- Owl Carousel -->

  <script>
    /* Tigger Gallery */
    $("a.url").mouseover(function () {
      $(this).trigger("click");
    });
  </script>

  <!-- PhotoSwipe -->
   @include('inc_photoswipe-root')
  <script src="{{asset('assets/js/photoswipe.min.js')}}"></script>
  <script src="{{asset('assets/js/photoswipe-ui-default.min.js')}}"></script>
  <script src="{{asset('assets/js/photoswipe-custom.js')}}"></script>
  <!-- PhotoSwipe -->

  <!-- Map -->
  <script type="text/javascript">
      // Referance >>> https://www.aspsnippets.com/Articles/Google-Maps-V3-Draggable-Markers-Example-Drag-and-Drop-Markers-in-Google-Maps-using-JavaScript.aspx
      var markers = [
          {
              "title": '{{ $band->name }}',
              "lat": '{{ $band->lat }}',
              "lng": '{{ $band->lng }}',
              "description": '{{ $band->address }}'
          }
      ];
      window.onload = function () {
          var mapOptions = {
              center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
              zoom: 12,
              mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          var infoWindow = new google.maps.InfoWindow();
          var latlngbounds = new google.maps.LatLngBounds();
          var geocoder = geocoder = new google.maps.Geocoder();
          var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
          for (var i = 0; i < markers.length; i++) {
              var data = markers[i]
              var myLatlng = new google.maps.LatLng(data.lat, data.lng);
              var marker = new google.maps.Marker({
                  position: myLatlng,
                  map: map,
                  title: data.title,
                  draggable: true,
                  animation: google.maps.Animation.DROP
              });
              (function (marker, data) {
                  google.maps.event.addListener(marker, "click", function (e) {
                      infoWindow.setContent(data.description);
                      infoWindow.open(map, marker);
                  });
                  google.maps.event.addListener(marker, "dragend", function (e) {
                      var lat, lng, address;
                      geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                          if (status == google.maps.GeocoderStatus.OK) {
                              lat = marker.getPosition().lat();
                              lng = marker.getPosition().lng();
                              address = results[0].formatted_address;
                              alert("Latitude: " + lat + "\nLongitude: " + lng + "\nAddress: " + address);
                          }
                      });
                  });
              })(marker, data);
              latlngbounds.extend(marker.position);
          }

          // var bounds = new google.maps.LatLngBounds();   // For Multiple markers
          // map.setCenter(latlngbounds.getCenter());       // For Multiple markers
          // map.fitBounds(latlngbounds);                   // For Multiple markers
      }
  </script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGqNz-KUAKL435y6zraREiT-o5s4tuiEM&callback=initMap"></script>
  <!-- Map -->
  <script type="text/javascript">
  function checkmax(num,id) {
    // alert(num);
    if (num>100) {
      // return this.val('100');
      $('#amount').val('100');
    }
    $.ajax({
      type: 'get',
      url: '{{url('checkPrice')}}'+'/'+id+'/'+num,
      data: {_token: "{{ csrf_token() }}"},
      dataType:'json',
      success:function(html){
        price = $('#amount').val();
        price = price*html[0].price;
        discount = (price*(html[0].discount/100));
        $('#price').html(price);
        $('#discount').html(parseInt(discount));
        $('#discountNum').html(html[0].discount+"%");
        $('#total').html(price-discount);
        // swal("Successfully", "ลบเสร็จสิ้น !!", "success");
        // dataTable.clear();
        // dataTable.rows.add(html).draw();
        // $('#product').val();

      }
    });
  }
  function toast() {
  var x = document.getElementById("snackbar");
  x.className = "show";
  setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
  }
  function addCart() {
    // num = parseInt($('#product_num').text());
    // num++;
    // $('#product_num').text(num);
    // $('.modal').modal('hide');
    var formData = $('#cartForm').serialize();
    $.ajax({
      type: 'get',
      url: '{{url('addToCart')}}',
      data: formData,
      dataType:'json',
      success:function(html){
        $('#product_num').text(html.length);
        toast();
        setTimeout(function(){
          window.location="{{ url('index') }}"
        }, 1000);
      },
      cache: false,
      contentType: false,
      processData: false
    });
    return false;
  }
  </script>
@endsection
