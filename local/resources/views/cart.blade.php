@extends('layout.main')
@section('content')

  <!-- Section Nav -->
  <div id="data_link">
    <ul class="con-scroll cart-tab nav nav-tabs nav-justified font-cloud mb-4">
      <li class="nav-item w-50">
        <a class="nav-scroll nav-link active" id="nav-tab-cart" href="#tab-cart">ตะกร้าสินค้า<i class="fa fa-angle-right float-right mr-3"></i></a>
      </li>
      <li class="nav-item w-50">
        <a class="nav-scroll nav-link" id="nav-tab-checkout" href="#tab-checkout">ชำระเงิน<i class="fa fa-angle-right float-right mr-3"></i></a>
      </li>
    </ul>
  </div>
  <!-- Section Nav -->


  <!-- Site Content -->
  <div class="container">

    <div class="site-content">
      <div data-target=".con-scroll" id="tab-cart">
        <h3 class="font-cloud text-center pt-4 pt-md-2 mb-5">ตะกร้าสินค้า</h3>

        <div class="row">
          <div class="col-md-12">
          <div id="no-item" class="jumbotron text-center">
            <i class="fa fa-shopping-basket" style="font-size: 120px;"></i>
            <h1 class="lead font-cloud mt-3">ตะกร้าสินค้าว่างค่ะ</h1>
            <p>กรุณากลับไปเลือกซื้อสินค้าต่อค่ะ</p>
            <hr class="my-4">
            <a class="btn btn-success btn-lg font-cloud lead" href="{{url('index')}}" role="button">เลือกซื้อสินค้าต่อ</a>
          </div>
          </div>
        </div>

        <table id="cart-table-lg" class="table d-none d-lg-block">
          <thead class="table-light">
            <tr class="font-cloud">
              <th class="position-sticky">#</th>
              <th class="position-sticky" colspan="2">สินค้า</th>
              <th class="position-sticky">จำนวน</th>
              <th class="position-sticky text-right">ราคา/ชิ้น</th>
              <th class="position-sticky text-right">ส่วนลด/ชิ้น</th>
              <th class="position-sticky text-right">ราคารวม</th>
              <th class="position-sticky text-center">ลบ</th>
            </tr>
          </thead>
          <tbody>
            @php
            $user=\App\Models\User::leftjoin('district','users.district','=','district.DISTRICT_ID')
            ->leftjoin('province','users.province','=','province.PROVINCE_ID')
            ->select('users.*','district.DISTRICT_NAME as district_name','province.PROVINCE_NAME as province_name')
            ->where('id',Auth::user()->id)
            ->first();
            @endphp
            @if(Session::has('cart'))
              @php
              $inCart=array_values(array_unique(Session::get('cart')));
              $products=\App\Models\Product::whereIn('id',$inCart)->get();
              $num=1;
                // print_r(Session::get('cart'));
                $total_piece=0;
                $total_bf_discount=0;
                $total_discount=0;
                $total_af_discount=0;
              @endphp
              @foreach ($products as $product)
                @php
                $img=\App\Models\ProductImg::where('product_id',$product->id)->first();
                // $array=array();
                $array=Session::get('cart');
                $total_products = 0;
                foreach ($array as $key) {
                  if($key==$product->id){
                    $total_products++;
                  }
                }
                $discount=0;
                $promotion=\App\Models\ProductPromotion::where('product_id',$product->id)->orderBy('amount','DESC')->get();
                foreach ($promotion as $key ) {
                  if ($total_products>=$key->amount) {
                    $discount=$key->discount;
                    break;
                  }
                }
                $total=$total_products*$product->price;
                $total_piece+=$total_products*$product->piece;
                $total_bf_discount+=$total;
                $total_discount+=$total-$total*((100-$discount)/100);
                // Session::forget('cart');
                @endphp
                <tr>
                  <th>{{ $num++ }}</th>
                  <td><img class="rounded-circle" src="{{asset('local/storage/app/images/product')}}/{{ $img->name }}"></td>
                  <td>{{ $product->name }}<br><small class="text-muted">(รับสินค้า {{\Carbon::now()->addDays($product->time)->formatLocalized('%A %d') ." - ". \Carbon::now()->addDays($product->time+4)->formatLocalized('%A %d %B %Y')}})</small></td>
                  <td>
                    <div class="input-group">
                      <span class="input-group-btn">
                        @if ($product->order_minimum==$total_products)
                          <button type="button" class="btn btn-sm btn-number float-left" disabled data-type="minus" data-field="{{ $product->id }}">-</button>
                        @else
                          <button type="button" class="btn btn-sm btn-number float-left" data-type="minus" data-field="{{ $product->id }}">-</button>
                        @endif
                        {{-- <button type="button" class="btn btn-sm btn-number float-left" data-type="minus" data-field="{{ $product->id }}">-</button> --}}
                      </span>
                      <input type="text" id="{{ $product->id }}" name="{{ $product->id }}" onchange="calculate(this.value,{{ $product->id }})" class="form-control-sm input-number text-center float-left" value="{{ $total_products }}" min="{{$product->order_minimum}}" max="100">
                      <span class="input-group-btn">
                        <button type="button"  class="btn btn-sm btn-number float-left" data-type="plus" data-field="{{ $product->id }}">+</button>
                      </span>
                      <span class="input-group-btn">
                        <button type="button" class="btn btn-sm float-left" disabled="disabled">ลัง ({{$product->piece}}ชิ้น)</button>
                      </span>
                    </div>
                  </td>
                  <td class="text-right" id="price{{ $product->id }}">{{ ($total*((100-$discount)/100))/($product->piece*$total_products) }}</td>
                  <td class="text-right"><small>(<span  id="discountNum{{ $product->id }}">{{$discount}}%</span>)</small> <span id="discount{{ $product->id }}">{{($discount/100)*($product->price/$product->piece)}}</span></td>
                  <td class="text-right" id="total{{ $product->id }}"> {{$total*((100-$discount)/100)}}</td>
                  <td class="text-center"><button class="badge badge-pill badge-danger" onclick="if (confirm('ต้องการลบสินค้าออกจากตระกร้า'))del({{ $product->id }})">X</button></td>
                </tr>
              @endforeach


            {{-- <tr>
              <th>1</th>
              <td><img class="rounded-circle" src="{{asset('assets/images/icons/user_02.png')}}"></td>
              <td>product 01! You should check in on some of those fields below.<br><small class="text-muted">(รับสินค้า เสาร์, 16 - อังคาร, 19 ก.ย. 2560)</small></td>
              <td>
                <div class="input-group">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" disabled="disabled" data-type="minus" data-field="quant[1]">-</button>
                  </span>
                  <input type="text" name="quant[1]" class="form-control-sm input-number text-center float-left" value="10" min="5" max="100">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" data-type="plus" data-field="quant[1]">+</button>
                  </span>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm float-left" disabled="disabled">ลัง (120ชิ้น)</button>
                  </span>
                </div>
              </td>
              <td class="text-right">500</td>
              <td class="text-right"><small>(5%)</small> 25</td>
              <td class="text-right">57,000</td>
              <td class="text-center"><button class="badge badge-pill badge-danger">X</button></td>
            </tr>
            <tr>
              <th>2</th>
              <td><img class="rounded-circle" src="{{asset('assets/images/icons/user_02.png')}}"></td>
              <td>product 02! You should check in on some of those fields below.<br><small class="text-muted">(รับสินค้า เสาร์, 16 - อังคาร, 19 ก.ย. 2560)</small></td>
              <td>
                <div class="input-group">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" disabled="disabled" data-type="minus" data-field="quant[2]">-</button>
                  </span>
                  <input type="text" name="quant[2]" class="form-control-sm input-number text-center float-left" value="10" min="5" max="100">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" data-type="plus" data-field="quant[2]">+</button>
                  </span>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm float-left" disabled="disabled">ลัง (120ชิ้น)</button>
                  </span>
                </div>
              </td>
              <td class="text-right">500</td>
              <td class="text-right"><small>(5%)</small> 25</td>
              <td class="text-right">57,000</td>
              <td class="text-center"><button class="badge badge-pill badge-danger">X</button></td>
            </tr>
            <tr>
              <th>3</th>
              <td><img class="rounded-circle" src="{{asset('assets/images/icons/user_02.png')}}"></td>
              <td>product 03! You should check in on some of those fields below.<br><small class="text-muted">(รับสินค้า เสาร์, 16 - อังคาร, 19 ก.ย. 2560)</small></td>
              <td>
                <div class="input-group">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" disabled="disabled" data-type="minus" data-field="quant[3]">-</button>
                  </span>
                  <input type="text" name="quant[3]" class="form-control-sm input-number text-center float-left" value="10" min="5" max="100">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" data-type="plus" data-field="quant[3]">+</button>
                  </span>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm float-left" disabled="disabled">ลัง (120ชิ้น)</button>
                  </span>
                </div>
              </td>
              <td class="text-right">500</td>
              <td class="text-right"><small>(5%)</small> 25</td>
              <td class="text-right">57,000</td>
              <td class="text-center"><button class="badge badge-pill badge-danger">X</button></td>
            </tr>
            <tr>
              <th>4</th>
              <td><img class="rounded-circle" src="{{asset('assets/images/icons/user_02.png')}}"></td>
              <td>product 04! You should check in on some of those fields below.<br><small class="text-muted">(รับสินค้า พุธ, 20 - อาทิตย์, 23 ก.ย. 2560)</small></td>
              <td>
                <div class="input-group">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" disabled="disabled" data-type="minus" data-field="quant[4]">-</button>
                  </span>
                  <input type="text" name="quant[4]" class="form-control-sm input-number text-center float-left" value="10" min="5" max="100">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" data-type="plus" data-field="quant[4]">+</button>
                  </span>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm float-left" disabled="disabled">ลัง (120ชิ้น)</button>
                  </span>
                </div>
              </td>
              <td class="text-right">500</td>
              <td class="text-right"><small>(5%)</small> 25</td>
              <td class="text-right">57,000</td>
              <td class="text-center"><button class="badge badge-pill badge-danger">X</button></td>
            </tr>
            <tr>
              <th>5</th>
              <td><img class="rounded-circle" src="{{asset('assets/images/icons/user_02.png')}}"></td>
              <td>product 05! You should check in on some of those fields below.<br><small class="text-muted">(รับสินค้า พุธ, 20 - อาทิตย์, 23 ก.ย. 2560)</small></td>
              <td>
                <div class="input-group">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" disabled="disabled" data-type="minus" data-field="quant[5]">-</button>
                  </span>
                  <input type="text" name="quant[5]" class="form-control-sm input-number text-center float-left" value="10" min="5" max="100">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" data-type="plus" data-field="quant[5]">+</button>
                  </span>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm float-left" disabled="disabled">ลัง (120ชิ้น)</button>
                  </span>
                </div>
              </td>
              <td class="text-right">500</td>
              <td class="text-right"><small>(5%)</small> 25</td>
              <td class="text-right">57,000</td>
              <td class="text-center"><button class="badge badge-pill badge-danger">X</button></td>
            </tr> --}}
            <tr class="table-light">
              <th class="text-center" colspan="3">สรุปการสั่งซื้อ ({{ count($inCart) }} รายการ)</th>
              <td><strong>{{ count(Session::get('cart')) }} ลัง<small> ({{ $total_piece }}ชิ้น)</small></strong></td>
              <td class="text-right"><strong>{{ number_format($total_bf_discount) }}</strong></td>
              <td class="text-right text-danger"><strong>{{ number_format($total_discount) }}</strong></td>
              <td class="text-right text-danger"><strong>{{ number_format($total_bf_discount-$total_discount) }}</strong></td>
              <td class="text-center"><button class="badge badge-pill badge-danger" onclick="if(confirm('ลบสินค้าทั้งหมดในตะกร้า'))delAll();">X</button></td>
            </tr>
            <tr class="table-light">
              <th class="text-center" colspan="8"> <button class="btn btn-success" onclick="location.reload()" >คำนวณใหม่</button> </th>
            </tr>
          </tbody>
        </table>
        @endif
        <div class="row my-3">
        <div class="col-12">
        <table id="cart-table-sm" class="table d-block d-lg-none">
          <thead class="table-light">
            <tr class="font-cloud">
              <th class="position-sticky">#</th>
              <th class="position-sticky text-right">ราคา/ชิ้น</th>
              <th class="position-sticky text-right">ส่วนลด/ชิ้น</th>
              <th class="position-sticky text-right">ราคารวม</th>
              <th class="position-sticky text-center">ลบ</th>
            </tr>
          </thead>
          <tbody>

            <tr>
              <th rowspan="2">1</th>
              <td><img class="rounded-circle" src="{{asset('assets/images/icons/user_02.png')}}"></td>
              <td colspan="3">product 01! You should check in on some of those fields below.<small class="text-muted"> (รับสินค้า เสาร์, 16 - อังคาร, 19 ก.ย. 2560)</small><br>
                <div class="input-group mt-1">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" disabled="disabled" data-type="minus" data-field="quant[1]">-</button>
                  </span>
                  <input type="text" name="quant[1]" class="form-control-sm input-number text-center float-left" value="10" min="5" max="100">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" data-type="plus" data-field="quant[1]">+</button>
                  </span>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm float-left" disabled="disabled">ลัง (120ชิ้น)</button>
                  </span>
                </div>
              </td>
            </tr>
            <tr>
              <td class="text-right">500</td>
              <td class="text-right"><small>(5%)</small> 25</td>
              <td class="text-right">57,000</td>
              <td class="text-center"><button class="badge badge-pill badge-danger">X</button></td>
            </tr>

            <tr>
              <th rowspan="2">2</th>
              <td><img class="rounded-circle" src="{{asset('assets/images/icons/user_02.png')}}"></td>
              <td colspan="3">product 02! You should check in on some of those fields below.<small class="text-muted"> (รับสินค้า เสาร์, 16 - อังคาร, 19 ก.ย. 2560)</small><br>
                <div class="input-group mt-1">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" disabled="disabled" data-type="minus" data-field="quant[2]">-</button>
                  </span>
                  <input type="text" name="quant[2]" class="form-control-sm input-number text-center float-left" value="10" min="5" max="100">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" data-type="plus" data-field="quant[2]">+</button>
                  </span>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm float-left" disabled="disabled">ลัง (120ชิ้น)</button>
                  </span>
                </div>
              </td>
            </tr>
            <tr>
              <td class="text-right">500</td>
              <td class="text-right"><small>(5%)</small> 25</td>
              <td class="text-right">57,000</td>
              <td class="text-center"><button class="badge badge-pill badge-danger">X</button></td>
            </tr>

            <tr>
              <th rowspan="2">3</th>
              <td><img class="rounded-circle" src="{{asset('assets/images/icons/user_02.png')}}"></td>
              <td colspan="3">product 03! You should check in on some of those fields below.<small class="text-muted"> (รับสินค้า เสาร์, 16 - อังคาร, 19 ก.ย. 2560)</small><br>
                <div class="input-group mt-1">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" disabled="disabled" data-type="minus" data-field="quant[3]">-</button>
                  </span>
                  <input type="text" name="quant[3]" class="form-control-sm input-number text-center float-left" value="10" min="5" max="100">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" data-type="plus" data-field="quant[3]">+</button>
                  </span>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm float-left" disabled="disabled">ลัง (120ชิ้น)</button>
                  </span>
                </div>
              </td>
            </tr>
            <tr>
              <td class="text-right">500</td>
              <td class="text-right"><small>(5%)</small> 25</td>
              <td class="text-right">57,000</td>
              <td class="text-center"><button class="badge badge-pill badge-danger">X</button></td>
            </tr>

            <tr>
              <th rowspan="2">4</th>
              <td><img class="rounded-circle" src="{{asset('assets/images/icons/user_02.png')}}"></td>
              <td colspan="3">product 04! You should check in on some of those fields below.<small class="text-muted"> (รับสินค้า พุธ, 20 - อาทิตย์, 23 ก.ย. 2560)</small><br>
                <div class="input-group mt-1">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" disabled="disabled" data-type="minus" data-field="quant[4]">-</button>
                  </span>
                  <input type="text" name="quant[4]" class="form-control-sm input-number text-center float-left" value="10" min="5" max="100">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" data-type="plus" data-field="quant[4]">+</button>
                  </span>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm float-left" disabled="disabled">ลัง (120ชิ้น)</button>
                  </span>
                </div>
              </td>
            </tr>
            <tr>
              <td class="text-right">500</td>
              <td class="text-right"><small>(5%)</small> 25</td>
              <td class="text-right">57,000</td>
              <td class="text-center"><button class="badge badge-pill badge-danger">X</button></td>
            </tr>

            <tr>
              <th rowspan="2">5</th>
              <td><img class="rounded-circle" src="{{asset('assets/images/icons/user_02.png')}}"></td>
              <td colspan="3">product 05! You should check in on some of those fields below.<small class="text-muted"> (รับสินค้า พุธ, 20 - อาทิตย์, 23 ก.ย. 2560)</small><br>
                <div class="input-group mt-1">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" disabled="disabled" data-type="minus" data-field="quant[5]">-</button>
                  </span>
                  <input type="text" name="quant[5]" class="form-control-sm input-number text-center float-left" value="10" min="5" max="100">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-number float-left" data-type="plus" data-field="quant[5]">+</button>
                  </span>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-sm float-left" disabled="disabled">ลัง (120ชิ้น)</button>
                  </span>
                </div>
              </td>
            </tr>
            <tr>
              <td class="text-right">500</td>
              <td class="text-right"><small>(5%)</small> 25</td>
              <td class="text-right">57,000</td>
              <td class="text-center"><button class="badge badge-pill badge-danger">X</button></td>
            </tr>

            <tr class="table-light">
              <th></th>
              <td class="text-right"><strong>300,000</strong></td>
              <td class="text-right text-danger"><strong>1,5000</strong></td>
              <td class="text-right text-danger"><strong>285,000</strong></td>
              <td class="text-center"><button class="badge badge-pill badge-danger delAll">X</button></td>
            </tr>
          </tbody>
        </table>
        </div>
        </div>

      </div>
    </div>
    @if (Session::has('cart'))
      <div class="site-content">
      <div data-target=".con-scroll" id="tab-checkout">

        <h3 class="font-cloud text-center pt-4 pt-md-2">ชำระเงิน</h3>
        <hr class="mb-5">

        <form id="shipping">
          {{ csrf_field() }}
          <div class="row">
          <div class="col-12 border border-light p-3 p-lg-5">
            <div id="summary">
              <h5 class="font-cloud">สรุปการสั่งซื้อ ({{ count($inCart) }} รายการ)</h5>
              <hr class="border-dashed">
              <div class="font-cloud">จำนวนรวม: <span class="float-right lead">{{ count(Session::get('cart')) }} ลัง ({{ $total_piece }}ชิ้น)</span></div><hr class="border-dashed">
              <div class="font-cloud">มูลค่าสินค้า: <span class="float-right lead">{{ number_format($total_bf_discount) }} บาท</span></div>
              <hr class="border-dashed">
              <div class="font-cloud">ส่วนลด: <span class="float-right lead">{{ number_format($total_discount) }} บาท</span></div>
              <hr class="border-dashed">
              <div class="font-cloud">ยอดสุทธิ: <span class="float-right text-danger lead">{{ number_format($total_bf_discount-$total_discount) }} บาท</span></div>
            </div>
          </div>
          </div>
          @endif
          @if (Session::has('cart'))
          <div class="row">

            <!-- Address Shipping -->
            <div class="col-lg-12 border border-light p-3 p-lg-5" id="address-shipping">
              <h5 class="font-cloud text-success text-center text-md-left">
                <i class="fa fa-truck mr-2"></i>ที่อยู่สำหรับจัดส่ง
                {{-- <div class="btn-group float-none float-md-right mt-3 mt-md-0">
                  <button type="button" id="colneusr1" class="btn btn-sm btn-outline-success"><i class="fa fa-clone mr-2"></i>ใช้ที่อยู่สมาชิก</button>
                  <a href="javascript:void(0)" class="btn btn-sm btn-outline-success" onclick="openSign()"><i class="fa fa-sign-in mr-2"></i>เข้าสู่ระบบ</a>
                </div> --}}
              </h5>
              <hr class="border-dashed border-success">

              <div class="row">
                <div class="col-sm-6 col-lg-3 mb-2">
                  <label for="name1">ชื่อ-นามสกุล</label>
                  <input type="text" class="form-control form-control-sm" id="name1" name="name1" value="{{ $user->name }}"  required>
                </div>
                <div class="col-sm-6 col-lg-3 mb-2">
                  <label for="address1">ที่อยู่</label>
                  <input type="text" class="form-control form-control-sm" id="address1" name="address1" value="{{ $user->address }}"  required>
                </div>
                <div class="col-sm-6 col-lg-3 mb-2">
                  <label for="zipcode1">รหัสไปรษณีย์</label>
                  <input type="text" class="form-control form-control-sm" id="zipcode1" name="zipcode1" value="{{ $user->zipcode }}" required>
                </div>
                <div class="col-sm-6 col-lg-3 mb-2">
                  <label for="district1">เขต/อำเภอ</label>
                  <input type="text" class="form-control form-control-sm" id="district1" name="district1" value="{{ $user->district_name }}" required>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 col-lg-3 mb-2">
                  <label for="province1">จังหวัด</label>
                  <input type="text" class="form-control form-control-sm" id="province1" name="province1" value="{{ $user->province_name }}" required>
                </div>
                <div class="col-sm-6 col-lg-3 mb-2">
                  <label for="email1">อีเมล์</label>
                  <input type="email" class="form-control form-control-sm" id="email1" name="email1" value="{{ $user->email }}" required>
                </div>
                <div class="col-sm-6 col-lg-3 mb-2">
                  <label for="mobile1">โทรศัพท์มือถือ</label>
                  <input type="tel" class="form-control form-control-sm" id="mobile1" name="mobile1" value="{{ $user->mobile }}" required>
                </div>
                @php
                  $payment=\App\Models\Payment::leftJoin('bank','payment.bank_id','=','bank.id')->get();
                @endphp
                <div class="col-sm-6 col-lg-3 mb-2">
                  <label for="bank">โอนเงินเข้าบัญชี</label>
                  <select class="form-control form-control-sm custom-select d-block" id="bank" name="bank" required>
                    <option value=""></option>
                    @foreach ($payment as $key)
                    <option value="{{ $key->name }}">{{ $key->name }}</option>
                    @endforeach

                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 col-lg-3 mb-2">
                  <label for="money">ยอดโอน (บาท)</label>
                  <input type="number" class="form-control form-control-sm" id="money" name="money" required>
                </div>
                <div class="col-sm-6 col-lg-3 mb-2">
                  <label for="dtp_input1">วันที่/เวลาโอน</label>
                  <div class="input-group date form_datetime" data-link-field="dtp_input1">
                    <input class="form-control form-control-sm" id="date-input" name="date" size="16" type="text" value="" readonly required>
                    {{-- <span class="input-group-addon"><span class="fa fa-close"></span></span> --}}
                    <span class="input-group-addon"><span class="fa fa-th"></span></span>
                  </div>
                </div>
                <div class="col-sm-6 col-lg-6 mb-2">
                  <label for="up-file">หลักฐานการโอนเงิน</label>
                  <button type="button" class="btn btn-sm btn-block text-left fileup-btn"><i class="fa fa-file-text-o mr-2"></i>แนบไฟล์เอกสาร
                    <input type="file" id="up-file" name="file" multiple required>
                    <div class="invalid-feedback">กรุณากรอกข้อมูลในช่องสีแดงให้ครบด้วยค่ะ</div>
                  </button>
                  <a class="control-button btn btn-link" style="display: none" href="javascript:$.fileup('up-file', 'upload', '*')">Upload all</a>
                  <a class="control-button btn btn-link" style="display: none" href="javascript:$.fileup('up-file', 'remove', '*')">Remove all</a>
                  <!--<div id="up-file-dropzone" class="dropzone">Dropzone</div>-->
                  <div id="up-file-queue" class="queue"></div>
                </div>
              </div>
              <div class="row">
                <div class="col-12 my-3">
                  <small class="text-danger">* กรุณาแนบหลักฐานการโอนเงินและกรอกข้อมูลให้ครบทุกช่องด้วยค่ะ ( :</small>
                </div>
              </div>
              <div class="border border-dashed p-3">
                <label class="custom-control custom-checkbox align-items-center">
                  <input id="tax-checkbox" type="checkbox" class="custom-control-input">
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">ที่อยู่สำหรับใบเสร็จ/ใบกำกับภาษี</span>
                </label>
              </div>
              <div id="tax_check">

              </div>
            </div>
            <!-- Address Shipping -->

            <!-- Address Tax -->
            <div class="col-lg-12 border border-light p-3 p-lg-5" id="address-tax">
              <h5 class="font-cloud text-success text-center text-md-left">
                <i class="fa fa-send-o mr-2"></i>ที่อยู่สำหรับใบเสร็จ/ใบกำกับภาษี
                <div class="btn-group float-none float-md-right mt-3 mt-md-0">
                  <button type="button" id="colneusr2" class="btn btn-sm btn-outline-success"><i class="fa fa-clone mr-2"></i>ใช้ที่อยู่สมาชิก</button>
                  <button type="button" id="colneval" class="btn btn-sm btn-outline-success"><i class="fa fa-clone mr-2"></i>คัดลอกจากด้านบน</button>
                </div>
              </h5>
              <hr class="border-dashed border-success">

              <div class="row" id="tax_invoice">
                <div class="col-sm-6 col-lg-3 mb-2">
                  <label for="name2">ชื่อ-นามสกุล</label>
                  <input type="text" class="form-control form-control-sm" id="name2" name="name2">
                </div>
                <div class="col-sm-6 col-lg-3 mb-2">
                  <label for="address2">ที่อยู่</label>
                  <input type="text" class="form-control form-control-sm" id="address2" name="address2" >
                </div>
                <div class="col-sm-6 col-lg-3 mb-2">
                  <label for="zipcode2">รหัสไปรษณีย์</label>
                  <input type="text" class="form-control form-control-sm" id="zipcode2" name="zipcode2">
                </div>
                <div class="col-sm-6 col-lg-3 mb-2">
                  <label for="district2">เขต/อำเภอ</label>
                  <input type="text" class="form-control form-control-sm" id="district2" name="district2">
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 col-lg-3 mb-2">
                  <label for="province2">จังหวัด</label>
                  <input type="text" class="form-control form-control-sm" id="province2" name="province2" >
                </div>
                <div class="col-sm-6 col-lg-3 mb-2">
                  <label for="email2">อีเมล์</label>
                  <input type="email" class="form-control form-control-sm" id="email2" name="email2">
                </div>
                <div class="col-sm-6 col-lg-3 mb-2">
                  <label for="mobile2">โทรศัพท์มือถือ</label>
                  <input type="tel" class="form-control form-control-sm" id="mobile2" name="mobile2">
                </div>
                <div class="col-sm-6 col-lg-3 mb-2">
                  <label for="taxid">เลขประจำตัวผู้เสียภาษี</label>
                  <input type="text" class="form-control form-control-sm" id="taxid" name="taxid">
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 col-lg-3 mb-2">
                  <label for="trader">เลขที่ สำนักงาน/หน่วยงาน/ร้านค้า</label>
                  <input type="text" class="form-control form-control-sm" id="trader" name="trader">
                </div>
              </div>
            </div>
            <!-- Address Tax -->
          </div>

          <!-- Button -->
          <button id="checkout" type="submit" class="btn btn-success d-block mx-auto mt-5">ยืนยันการชำระเงิน</button>
        </form>
        @endif

      </div>
    </div>
    <div id="snackbar">ยืนยันการชำระเงิน ตรวจสอบถานะการส่งสินค้าที่ประวัติการสั่งซื้อ </div>
  </div>
  <!-- Site Content -->

  <!-- Site footer -->
  @include('inc_footer')
  <!-- Site footer -->

  <!-- Site js -->
  @include('inc_js')
  <!-- Site js -->

  <!-- Site Sidewarp -->
  @include('inc_sidewarp')
  <!-- Site Sidewarp -->
  @if (Session::has('cart') and count(Session::get('cart'))>0)
    <script type="text/javascript">
    $("#no-item").hide();
  </script>
@else
  <script type="text/javascript">
  $("#cart-table-lg").remove();
</script>

@endif

  <script>
    // Form Check
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
      "use strict";
      window.addEventListener("load", function() {
        var form = document.getElementById("shipping");
        form.addEventListener("submit", function(event) {
          if (form.checkValidity() == false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add("was-validated");
          $('body,html').animate({
            scrollTop: 0
          }, 800);
        }, false);
      }, false);
    }());

    // Step Button
    $(document).ready(function(){
      $("#go-checkout").click(function () {
        $("#nav-tab-checkout").trigger('click');
      });
    });

    // Remove Item
    $(document).ready(function(){
      $(".delAll").click(function () {
        $(this).fadeOut('slow');
        $("#cart-table-sm, #cart-table-lg, .cart-tab").fadeOut('slow');
        $("#no-item").show('slow');
        $('body,html').animate({scrollTop: 0}, 300);
      });
    });

    // Address Tax
    $(document).ready(function(){
      $("#address-tax").css("display","none");

      $('#tax-checkbox').click(function() {
        if ($('#tax-checkbox').is(":checked")) {
          $("#address-tax").slideDown('fast');
          $('#tax_check').html('<input type="hidden" name="tax_check" value="1" />');
          $('#tax_invoice').find('input:text').prop('required','true');
        } else {
          $("#address-tax").slideUp('fast');
          $('#tax_check').html('<input type="hidden" name="tax_check" value="0" />');
          $('#tax_invoice').find('input:text').removeAttr('required');
        }
      });
    });


  </script>

  <!-- Multiupload -->
  <script src="{{asset('js/jquery.growl.js')}}"></script>
  <script src="{{asset('js/fileup.js')}}"></script>
  <script>
    $.fileup({
      url: 'example.com/your/path?file_upload=1',
      inputID: 'up-file',
      dropzoneID: 'up-file-dropzone',
      queueID: 'up-file-queue',
      onSelect: function(file) {
        $('#shipping .control-button').show();
      },
      onRemove: function(file, total) {
        if (file === '*' || total === 1) {
          $('#shipping .control-button').hide();
        }
      },
      onSuccess: function(response, file_number, file) {
        $.growl.notice({ title: "Upload success!", message: file.name });
      },
      onError: function(event, file, file_number) {
        $.growl.error({ message: "Upload error!", message: file.name });
      }
    })
    .dragEnter(function(event) {
        $(event.target).addClass('over');
    })
    .dragLeave(function(event) {
        $(event.target).removeClass('over');
    })
    .dragEnd(function(event) {
        $(event.target).removeClass('over');
    });
  </script>
  <!-- Multiupload -->

  <!-- Date Time Picker -->
  <script src="{{asset('assets/js/bootstrap-datetimepicker.js')}}"></script>
  <script src="{{asset('assets/js/bootstrap-datetimepicker.th.js')}}"></script>
  <script>
    $('.form_datetime').datetimepicker({
      language: 'th',
      format: "dd MM yyyy - hh:ii",
      weekStart: 1,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      forceParse: 0,
      showMeridian: 1,
      minuteStep: 1,
      pickerPosition: "center"
    });
  </script>
  <!-- Date Time Picker -->
<script type="text/javascript">
  function calculate(value,id) {
    $.ajax({
      type: 'get',
      url: '{{url('checkPrice')}}'+'/'+id+'/'+value,
      data: {_token: "{{ csrf_token() }}",
      type: "cart"
      },
      dataType:'json',
      success:function(html){
        // alert(html[0].in_cart);
        $('#product_num').html(html[0].in_cart);
        price = $('#'+id).val();
        price = price*html[0].price;
        discount = (price*(html[0].discount/100));
        pricePerPiece=(html[0].price/html[0].piece)
        $('#price'+id).html(pricePerPiece-(pricePerPiece*(html[0].discount/100)));
        $('#discount'+id).html(parseFloat(pricePerPiece-(pricePerPiece-(pricePerPiece*(html[0].discount/100)))));
        $('#discountNum'+id).html(html[0].discount+"%");
        $('#total'+id).html(price-discount);
        // swal("Successfully", "ลบเสร็จสิ้น !!", "success");
        // dataTable.clear();
        // dataTable.rows.add(html).draw();
        // $('#product').val();

      }
    });
  }
  function del(id) {
    $.ajax({
      type: 'get',
      url: '{{url('delProduct')}}'+'/'+id,
      data: {_token: "{{ csrf_token() }}"},
      dataType:'html',
      success:function(html){
        alert('ลบสินค้าออกจ้าตะกร้าแล้ว');
        setTimeout(function(){
        location.reload();
      },1000);
    }
    });
  }
  function delAll() {
    $.ajax({
      type: 'get',
      url: '{{url('delAll')}}',
      data: {_token: "{{ csrf_token() }}"},
      dataType:'html',
      success:function(html){
        alert('ลบสินค้าทั้งหมดออกจ้าตะกร้าแล้ว');
        setTimeout(function(){
        location.reload();
      },1000);
    }
    });
  }
  $('#colneusr2').click(function(){
    $('#name2').val('{{ Auth::user()->name }}');
    $('#address2').val('{{ Auth::user()->address }}');
    $('#zipcode2').val('{{ Auth::user()->zipcode }}');
    $('#district2').val('{{ $user->district_name }}');
    $('#province2').val('{{ $user->province_name }}');
    $('#email2').val('{{ Auth::user()->email }}');
    $('#mobile2').val('{{ Auth::user()->mobile }}');

  });
  $('#colneval').click(function(){
    $('#name2').val($('#name1').val());
    $('#address2').val($('#address1').val());
    $('#zipcode2').val($('#zipcode1').val());
    $('#district2').val($('#district1').val());
    $('#province2').val($('#province1').val());
    $('#email2').val($('#email1').val());
    $('#mobile2').val($('#mobile1').val());

  });
  function toast() {
  var x = document.getElementById("snackbar");
  x.className = "show";
  setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
  }
  $('#shipping').submit(function (){
    if($('#date-input').val()==""){
      alert('กรุณาเลือกวันที่/เวลาโอน');
      $('#date-input').focus();
      return false
    }
    var formData = new FormData($(this)[0]);
    $.ajax({
      url: '{{url('ConfirmCart')}}',
      type: 'POST',
      data: formData,
      dataType:'html',
      success: function (html) {
        if(html=="complete"){
          toast();
          setTimeout(function(){
            location.reload();
          },1000);
        }
      },
      cache: false,
      contentType: false,
      processData: false
    });
  return false;
  });
  $(document).ready(function(){
    $('#bank').select2({
      placeholder:'กรุณาเลือกธนาคาร',
    });
  });
</script>

@endsection
