@extends('layout.main')
@section('content')

  <!-- Breadcrumb -->
  @php
  if(Session::has('banproduct')){
    $banproduct=array_unique(Session::get('banproduct'));
  }else{
    $banproduct=['0'];
  }
    $subcat=\App\Models\SubCategory::leftJoin('product_category','product_category_sub.category_id','=','product_category.id')
    ->select('product_category_sub.*','product_category.name as category')
    ->where('product_category_sub.id',$id)
    ->first();
  @endphp

  <nav class="breadcrumb mb-5">
  <div class="container text-right">
    <a class="breadcrumb-item" href="{{url('index')}}">หน้าหลัก</a>
    <span class="breadcrumb-item active">{{ $subcat->category }}</span>
    <span class="breadcrumb-item active">{{ $subcat->name }}</span>
  </div>
  </nav>

  <!-- container -->
  <div class="container">

    <!-- Slider -->
    <div class="row">
      <div class="col-12">
        <div class="owl-carousel owl-theme">
          <div class="item">
            <a href="{{url('product_details')}}">
              <img class="img-fluid" src="{{asset('assets/images/company_banner3.jpg')}}">
            </a>
          </div>
          <div class="item">
            <a href="{{url('product_details')}}">
              <img class="img-fluid" src="{{asset('assets/images/company_banner2.jpg')}}">
            </a>
          </div>
          <div class="item">
            <a href="{{url('product_details')}}">
              <img class="img-fluid" src="{{asset('assets/images/company_banner4.jpg')}}">
            </a>
          </div>
        </div>
      </div>
    </div>
    <!-- Slider -->
    @php
      $cat=\App\Models\ProductCategory::get();
      $i=1;
    @endphp
    <!-- Filter -->
    <div class="row">
      <div class="col-12">
        <div class="cat_area" id="data_link">
          <ul id="cat_nav" class="cat-pills nav nav-pills nav-justified font-cloud">
            @foreach ($cat as $element)
              <li class="nav-item w-10"><a id="cat-main-{{ sprintf("%'.02d", $i++) }}" class="h-100 nav-link" href="#cat-sub-{{ $element->id }}" data-toggle="pill" role="tab" aria-controls="cat-sub-{{ $element->id }}" aria-expanded="false">{{ $element->name }}</a></li>
            @endforeach
        {{-- <li class="nav-item w-10"><a id="cat-main-01" class="h-100 nav-link" href="#cat-sub-01" data-toggle="pill" role="tab" aria-controls="cat-sub-01" aria-expanded="false">ยาฆ่าหญ้า</a></li>
            <li class="nav-item w-10"><a id="cat-main-02" class="h-100 nav-link" href="#cat-sub-02" data-toggle="pill" role="tab" aria-controls="cat-sub-02" aria-expanded="false">ยาคุม/ฆ่า</a></li>
            <li class="nav-item w-10"><a id="cat-main-03" class="h-100 nav-link" href="#cat-sub-03" data-toggle="pill" role="tab" aria-controls="cat-sub-03" aria-expanded="false">ยาฆ่าแมลง</a></li>
            <li class="nav-item w-10"><a id="cat-main-04" class="h-100 nav-link" href="#cat-sub-04" data-toggle="pill" role="tab" aria-controls="cat-sub-04" aria-expanded="false">สารจับใบ</a></li>
            <li class="nav-item w-10"><a id="cat-main-05" class="h-100 nav-link" href="#cat-sub-05" data-toggle="pill" role="tab" aria-controls="cat-sub-05" aria-expanded="false">เชื้อรา</a></li>
            <li class="nav-item w-10"><a id="cat-main-06" class="h-100 nav-link" href="#cat-sub-06" data-toggle="pill" role="tab" aria-controls="cat-sub-06" aria-expanded="false">ฮอร์โมน</a></li>
            <li class="nav-item w-10"><a id="cat-main-07" class="h-100 nav-link" href="#cat-sub-07" data-toggle="pill" role="tab" aria-controls="cat-sub-07" aria-expanded="false">ปุ๋ย</a></li>
            <li class="nav-item w-10"><a id="cat-main-08" class="h-100 nav-link" href="#cat-sub-08" data-toggle="pill" role="tab" aria-controls="cat-sub-08" aria-expanded="false">เมล็ดพันธ์</a></li>
            <li class="nav-item w-10"><a id="cat-main-09" class="h-100 nav-link" href="#cat-sub-09" data-toggle="pill" role="tab" aria-controls="cat-sub-09" aria-expanded="false">อุปกรณ์</a></li>
            <li class="nav-item w-10"><a id="cat-main-10" class="h-100 nav-link" href="#cat-sub-10" data-toggle="pill" role="tab" aria-controls="cat-sub-09" aria-expanded="false">ยี่ห้อ</a></li> --}}
          </ul>
          <div id="cat_nav_sub" class="tab-content">
            @foreach ($cat as $element)
              <div id="cat-sub-{{ $element->id }}" aria-labelledby="cat-main-{{ $element->id }}" role="tabpanel" class="tab-pane" aria-expanded="false">
                <div class="cat-pills-sub nav nav-pills bg-light p-3">
                  @php
                    $sub=\App\Models\SubCategory::where('category_id',$element->id)->get();
                  @endphp
                  @if (count($sub)>0)
                    @foreach ($sub as $key)
                      @php
                        $product=\App\Models\Product::where('category_sub_id',$key->id)->whereNotIn('id',$banproduct)->get();
                      @endphp
                      <a class="nav-item nav-link" href="{{ url('product') }}/{{ $key->id }}">{{$key->name}}<span class="badge badge-pill badge-light border float-right">{{count($product)}}</span></a>
                    @endforeach
                  @else
                    <a class="nav-item nav-link" href="javascript:void(0)">ไม่พบข้อมูล</a>
                  @endif

                </div>
              </div>
            @endforeach
            {{-- <div id="cat-sub-01" aria-labelledby="cat-main-01" role="tabpanel" class="tab-pane" aria-expanded="false">
              <div class="cat-pills-sub nav nav-pills bg-light p-3">
                <a class="nav-item nav-link" href="#">ยาฆ่าหญ้า 01 <span class="badge badge-pill badge-light border float-right">250</span></a>
                <a class="nav-item nav-link" href="#">ยาฆ่าหญ้า 02 <span class="badge badge-pill badge-light border float-right">320</span></a>
                <a class="nav-item nav-link" href="#">ยาฆ่าหญ้า 03 <span class="badge badge-pill badge-light border float-right">120</span></a>
                <a class="nav-item nav-link" href="#">ยาฆ่าหญ้า 04 <span class="badge badge-pill badge-light border float-right">450</span></a>
                <a class="nav-item nav-link" href="#">ยาฆ่าหญ้า 05 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">ยาฆ่าหญ้า 06 <span class="badge badge-pill badge-light border float-right">580</span></a>
                <a class="nav-item nav-link" href="#">ยาฆ่าหญ้า 07 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">ยาฆ่าหญ้า 08 <span class="badge badge-pill badge-light border float-right">580</span></a>
              </div>
            </div>
            <div id="cat-sub-02" aria-labelledby="cat-main-02" role="tabpanel" class="tab-pane" aria-expanded="false">
              <div class="cat-pills-sub nav nav-pills bg-light p-3">
                <a class="nav-item nav-link" href="#">ยาคุม/ฆ่า 01 <span class="badge badge-pill badge-light border float-right">250</span></a>
                <a class="nav-item nav-link" href="#">ยาคุม/ฆ่า 02 <span class="badge badge-pill badge-light border float-right">320</span></a>
                <a class="nav-item nav-link" href="#">ยาคุม/ฆ่า 03 <span class="badge badge-pill badge-light border float-right">120</span></a>
                <a class="nav-item nav-link" href="#">ยาคุม/ฆ่า 04 <span class="badge badge-pill badge-light border float-right">450</span></a>
                <a class="nav-item nav-link" href="#">ยาคุม/ฆ่า 05 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">ยาคุม/ฆ่า 06 <span class="badge badge-pill badge-light border float-right">580</span></a>
                <a class="nav-item nav-link" href="#">ยาคุม/ฆ่า 07 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">ยาคุม/ฆ่า 08 <span class="badge badge-pill badge-light border float-right">580</span></a>
              </div>
            </div>
            <div id="cat-sub-03" aria-labelledby="cat-main-03" role="tabpanel" class="tab-pane" aria-expanded="false">
              <div class="cat-pills-sub nav nav-pills bg-light p-3">
                <a class="nav-item nav-link" href="#">ยาฆ่าแมลง 01 <span class="badge badge-pill badge-light border float-right">250</span></a>
                <a class="nav-item nav-link" href="#">ยาฆ่าแมลง 02 <span class="badge badge-pill badge-light border float-right">320</span></a>
                <a class="nav-item nav-link" href="#">ยาฆ่าแมลง 03 <span class="badge badge-pill badge-light border float-right">120</span></a>
                <a class="nav-item nav-link" href="#">ยาฆ่าแมลง 04 <span class="badge badge-pill badge-light border float-right">450</span></a>
                <a class="nav-item nav-link" href="#">ยาฆ่าแมลง 05 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">ยาฆ่าแมลง 06 <span class="badge badge-pill badge-light border float-right">580</span></a>
                <a class="nav-item nav-link" href="#">ยาฆ่าแมลง 07 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">ยาฆ่าแมลง 08 <span class="badge badge-pill badge-light border float-right">580</span></a>
              </div>
            </div>
            <div id="cat-sub-04" aria-labelledby="cat-main-04" role="tabpanel" class="tab-pane" aria-expanded="false">
              <div class="cat-pills-sub nav nav-pills bg-light p-3">
                <a class="nav-item nav-link" href="#">สารจับใบ 01 <span class="badge badge-pill badge-light border float-right">250</span></a>
                <a class="nav-item nav-link" href="#">สารจับใบ 02 <span class="badge badge-pill badge-light border float-right">320</span></a>
                <a class="nav-item nav-link" href="#">สารจับใบ 03 <span class="badge badge-pill badge-light border float-right">120</span></a>
                <a class="nav-item nav-link" href="#">สารจับใบ 04 <span class="badge badge-pill badge-light border float-right">450</span></a>
                <a class="nav-item nav-link" href="#">สารจับใบ 05 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">สารจับใบ 06 <span class="badge badge-pill badge-light border float-right">580</span></a>
                <a class="nav-item nav-link" href="#">สารจับใบ 07 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">สารจับใบ 08 <span class="badge badge-pill badge-light border float-right">580</span></a>
              </div>
            </div>
            <div id="cat-sub-05" aria-labelledby="cat-main-05" role="tabpanel" class="tab-pane" aria-expanded="false">
              <div class="cat-pills-sub nav nav-pills bg-light p-3">
                <a class="nav-item nav-link" href="#">เชื้อรา 01 <span class="badge badge-pill badge-light border float-right">250</span></a>
                <a class="nav-item nav-link" href="#">เชื้อรา 02 <span class="badge badge-pill badge-light border float-right">320</span></a>
                <a class="nav-item nav-link" href="#">เชื้อรา 03 <span class="badge badge-pill badge-light border float-right">120</span></a>
                <a class="nav-item nav-link" href="#">เชื้อรา 04 <span class="badge badge-pill badge-light border float-right">450</span></a>
                <a class="nav-item nav-link" href="#">เชื้อรา 05 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">เชื้อรา 06 <span class="badge badge-pill badge-light border float-right">580</span></a>
                <a class="nav-item nav-link" href="#">เชื้อรา 07 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">เชื้อรา 08 <span class="badge badge-pill badge-light border float-right">580</span></a>
              </div>
            </div>
            <div id="cat-sub-06" aria-labelledby="cat-main-06" role="tabpanel" class="tab-pane" aria-expanded="false">
              <div class="cat-pills-sub nav nav-pills bg-light p-3">
                <a class="nav-item nav-link" href="#">ฮอร์โมน 01 <span class="badge badge-pill badge-light border float-right">250</span></a>
                <a class="nav-item nav-link" href="#">ฮอร์โมน 02 <span class="badge badge-pill badge-light border float-right">320</span></a>
                <a class="nav-item nav-link" href="#">ฮอร์โมน 03 <span class="badge badge-pill badge-light border float-right">120</span></a>
                <a class="nav-item nav-link" href="#">ฮอร์โมน 04 <span class="badge badge-pill badge-light border float-right">450</span></a>
                <a class="nav-item nav-link" href="#">ฮอร์โมน 05 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">ฮอร์โมน 06 <span class="badge badge-pill badge-light border float-right">580</span></a>
                <a class="nav-item nav-link" href="#">ฮอร์โมน 07 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">ฮอร์โมน 08 <span class="badge badge-pill badge-light border float-right">580</span></a>
              </div>
            </div>
            <div id="cat-sub-07" aria-labelledby="cat-main-07" role="tabpanel" class="tab-pane" aria-expanded="false">
              <div class="cat-pills-sub nav nav-pills bg-light p-3">
                <a class="nav-item nav-link" href="#">ปุ๋ย 01 <span class="badge badge-pill badge-light border float-right">250</span></a>
                <a class="nav-item nav-link" href="#">ปุ๋ย 02 <span class="badge badge-pill badge-light border float-right">320</span></a>
                <a class="nav-item nav-link" href="#">ปุ๋ย 03 <span class="badge badge-pill badge-light border float-right">120</span></a>
                <a class="nav-item nav-link" href="#">ปุ๋ย 04 <span class="badge badge-pill badge-light border float-right">450</span></a>
                <a class="nav-item nav-link" href="#">ปุ๋ย 05 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">ปุ๋ย 06 <span class="badge badge-pill badge-light border float-right">580</span></a>
                <a class="nav-item nav-link" href="#">ปุ๋ย 07 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">ปุ๋ย 08 <span class="badge badge-pill badge-light border float-right">580</span></a>
              </div>
            </div>
            <div id="cat-sub-08" aria-labelledby="cat-main-08" role="tabpanel" class="tab-pane" aria-expanded="false">
              <div class="cat-pills-sub nav nav-pills bg-light p-3">
                <a class="nav-item nav-link" href="#">เมล็ดพันธ์ 01 <span class="badge badge-pill badge-light border float-right">250</span></a>
                <a class="nav-item nav-link" href="#">เมล็ดพันธ์ 02 <span class="badge badge-pill badge-light border float-right">320</span></a>
                <a class="nav-item nav-link" href="#">เมล็ดพันธ์ 03 <span class="badge badge-pill badge-light border float-right">120</span></a>
                <a class="nav-item nav-link" href="#">เมล็ดพันธ์ 04 <span class="badge badge-pill badge-light border float-right">450</span></a>
                <a class="nav-item nav-link" href="#">เมล็ดพันธ์ 05 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">เมล็ดพันธ์ 06 <span class="badge badge-pill badge-light border float-right">580</span></a>
                <a class="nav-item nav-link" href="#">เมล็ดพันธ์ 07 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">เมล็ดพันธ์ 08 <span class="badge badge-pill badge-light border float-right">580</span></a>
              </div>
            </div>
            <div id="cat-sub-09" aria-labelledby="cat-main-09" role="tabpanel" class="tab-pane" aria-expanded="false">
              <div class="cat-pills-sub nav nav-pills bg-light p-3">
                <a class="nav-item nav-link" href="#">อุปกรณ์ 01 <span class="badge badge-pill badge-light border float-right">250</span></a>
                <a class="nav-item nav-link" href="#">อุปกรณ์ 02 <span class="badge badge-pill badge-light border float-right">320</span></a>
                <a class="nav-item nav-link" href="#">อุปกรณ์ 03 <span class="badge badge-pill badge-light border float-right">120</span></a>
                <a class="nav-item nav-link" href="#">อุปกรณ์ 04 <span class="badge badge-pill badge-light border float-right">450</span></a>
                <a class="nav-item nav-link" href="#">อุปกรณ์ 05 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">อุปกรณ์ 06 <span class="badge badge-pill badge-light border float-right">580</span></a>
                <a class="nav-item nav-link" href="#">อุปกรณ์ 07 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">อุปกรณ์ 08 <span class="badge badge-pill badge-light border float-right">580</span></a>
              </div>
            </div>
            <div id="cat-sub-10" aria-labelledby="cat-main-10" role="tabpanel" class="tab-pane" aria-expanded="false">
              <div class="cat-pills-sub nav nav-pills bg-light p-3">
                <a class="nav-item nav-link" href="#">Brand 01 <span class="badge badge-pill badge-light border float-right">250</span></a>
                <a class="nav-item nav-link" href="#">Brand 02 <span class="badge badge-pill badge-light border float-right">320</span></a>
                <a class="nav-item nav-link" href="#">Brand 03 <span class="badge badge-pill badge-light border float-right">120</span></a>
                <a class="nav-item nav-link" href="#">Brand 04 <span class="badge badge-pill badge-light border float-right">450</span></a>
                <a class="nav-item nav-link" href="#">Brand 05 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">Brand 06 <span class="badge badge-pill badge-light border float-right">580</span></a>
                <a class="nav-item nav-link" href="#">Brand 07 <span class="badge badge-pill badge-light border float-right">280</span></a>
                <a class="nav-item nav-link" href="#">Brand 08 <span class="badge badge-pill badge-light border float-right">580</span></a>
              </div>
            </div> --}}
          </div>
        </div>
      </div>
    </div>
    <!-- Filter -->

    <!-- Product Item -->
    <div class="row mt-5">
    <div class="col-12">

      <!-- Title -->
        @php
          $product=\App\Models\Product::where('category_sub_id',$id)
          ->whereNotIn('id',$banproduct)
          ->orderBy('created_at','ASC')
          ->count();
          // dd($product);
        @endphp
        <div class="row">
          <div class="col-md-8 col-lg-9">
            <h5 class="font-cloud text-center text-sm-left">
              <span>{{$subcat->name}}</span>
              <span class="badge badge-pill badge-light font-weight-normal">พบสินค้าจำนวน {{ $product }}</span>
            </h5>
            </div>
            @if ($product<1)
              </div>
            @endif
            @php
            $brand=\App\Models\ProductBrands::whereIn('id',function($q)use ($subcat,$banproduct) {
              $q->select('band_id')->from('product')->where('category_sub_id','=',$subcat->id)->whereNotIn('id',$banproduct);
            })->get();
            $Sort='DESC';
            if(Session::has('sort')){
              $Sort=Session::get('sort');
            }
            // dd($brand);
            @endphp
            @for ($i = 0; $i < count($brand); $i++)
              @if ($i==0)
                <div class="col-md-8 col-lg-9">
              <h6 class="text-center text-sm-left"><small>กรองจาก : ยี่ห้อ <a href="{{url('profile')}}/{{ $brand[$i]->id }}">{{$brand[$i]->name}}</a></small></h6>
              </div>
            <div class="col-md-4 col-lg-3">
              <div class="input-group input-group-sm float-right mt-3 mt-md-0">
                <span class="input-group-addon" >จัดเรียงตาม</span>
                <select class="form-control addon" id="sort{{ $brand[$i]->id }}" onchange="SortBy(this.value)">
                  @if ($Sort=='DESC')
                    <option value="DESC" selected>สินค้ามาใหม่</option>
                    <option value="ASC" >เก่า - ใหม่</option>
                    <option value="HL">ราคา: สูง - ต่ำ</option>
                    <option value="LH">ราคา: ต่ำ - สูง</option>
                    <option value="sell">ส่วนลด</option>
                  @elseif ($Sort=='DESC')
                    <option value="DESC">สินค้ามาใหม่</option>
                    <option value="ASC" selected>เก่า - ใหม่</option>
                    <option value="HL">ราคา: สูง - ต่ำ</option>
                    <option value="LH">ราคา: ต่ำ - สูง</option>
                    <option value="sell">ส่วนลด</option>
                  @elseif ($Sort=='HL')
                    <option value="DESC" >สินค้ามาใหม่</option>
                    <option value="ASC" >เก่า - ใหม่</option>
                    <option value="HL" selected>ราคา: สูง - ต่ำ</option>
                    <option value="LH">ราคา: ต่ำ - สูง</option>
                    <option value="sell">ส่วนลด</option>
                  @elseif ($Sort=='LH')
                    <option value="DESC" >สินค้ามาใหม่</option>
                    <option value="ASC" >เก่า - ใหม่</option>
                    <option value="HL">ราคา: สูง - ต่ำ</option>
                    <option value="LH" selected>ราคา: ต่ำ - สูง</option>
                    <option value="sell">ส่วนลด</option>
                  @endif

                </select>
              </div>
            </div>
          </div>
        @else
          <div class="row">
            <div class="col-md-8 col-lg-9">
              <h6 class="text-center text-sm-left"><small>กรองจาก : ยี่ห้อ <a href="{{url('profile')}}/{{ $brand[$i]->id }}">{{$brand[$i]->name}}</a></small></h6>
            </div>
            {{-- <div class="col-md-4 col-lg-3">
              <div class="input-group input-group-sm float-right mt-3 mt-md-0">
                <span class="input-group-addon" >จัดเรียงตาม</span>
                <select class="form-control addon" id="sort{{ $brand[$i]->id }}" onchange="SortBy('{{ $id }}','{{ $brand[$i]->id }}')">
                  <option value="DESC">สินค้ามาใหม่</option>
                  <option value="HL">ราคา: สูง - ต่ำ</option>
                  <option value="LH">ราคา: ต่ำ - สูง</option>
                  <option value="band">ยี่ห้อสินค้า</option>
                  <option value="sell">ส่วนลด</option>
                </select>
              </div>
            </div> --}}
          </div>
        @endif
          <hr class="border-dashed">

          <!-- Item Box -->
          <div class="row">
            <div class="col-12">
              <div id="product-boxed" class="pt-3 product_box{{ $brand[$i]->id }}">
                @php

                if(Session::has('brand')){
                  $arraySubcat=Session::get('subcat');
                  $arrayBrand=Session::get('brand');
                  $arrayPage=Session::get('portionpage');
                  // $search=array_search($brand[$i]->id,$arrayBrand);
                  // print_r($arraySubcat);
                  // print_r($arrayBrand);
                  // print_r($arrayPage);
                  for ($l=0; $l < count($arraySubcat); $l++) {
                    if($arraySubcat[$l]==$id){
                      for ($j=0; $j < count($arrayBrand); $j++) {
                        if($arrayBrand[$j]==$brand[$i]->id and $j==$l){
                          $page=$arrayPage[$j];
                          break;
                        }else{
                          $page=1;
                        }
                      }
                      break;
                    }else{
                      $page=1;
                    }
                  }
                  // echo $search=array_search($brand[$i]->id,$arrayBrand);
                  // if($search!=null){
                  //   dd($arrayPage[$search]);
                  //   $page=$arrayPage[$search];
                  // }
                  // else{
                  //
                  //   $page=1;
                  // }
                }else{
                  $page=1;
                }
                $perpage=30;
                $start=($page-1)*$perpage;
                $productCount=\App\Models\Product::where('band_id',$brand[$i]->id)
                ->where('category_sub_id',$subcat->id)
                ->whereNotIn('id',$banproduct)
                ->count();
                $total_page=ceil($productCount/$perpage);
                // dd($total_page);
                if($Sort=='DESC'){
                  $products=\App\Models\Product::where('band_id',$brand[$i]->id)
                  ->where('category_sub_id',$subcat->id)
                  ->whereNotIn('id',$banproduct)
                  ->orderBy('created_at','DESC')
                  ->skip($start)
                  ->take($perpage)
                  ->get();
                }
                else if($Sort=='ASC'){
                  $products=\App\Models\Product::where('band_id',$brand[$i]->id)
                  ->where('category_sub_id',$subcat->id)
                  ->whereNotIn('id',$banproduct)
                  ->orderBy('created_at','ASC')
                  ->skip($start)
                  ->take($perpage)
                  ->get();
                }
                else if($Sort=='HL'){
                  $products=\App\Models\Product::where('band_id',$brand[$i]->id)
                  ->where('category_sub_id',$subcat->id)
                  ->whereNotIn('id',$banproduct)
                  ->select('product.*')
                  ->selectRaw('price/piece as sum')
                  ->orderBy('sum','DESC')
                  ->skip($start)
                  ->take($perpage)
                  ->get();
                }
                else if($Sort=='LH'){
                  $products=\App\Models\Product::where('band_id',$brand[$i]->id)
                  ->where('category_sub_id',$subcat->id)
                  ->whereNotIn('id',$banproduct)
                  ->select('product.*')
                  ->selectRaw('price/piece as sum')
                  ->orderBy('sum','ASC')
                  ->skip($start)
                  ->take($perpage)
                  ->get();
                }
                  // $products=\App\Models\Product::where('band_id',$brand[$i]->id)
                  // ->where('category_sub_id',$subcat->id)
                  // ->whereNotIn('id',$banproduct)
                  // ->orderBy('created_at','DESC')
                  // ->get();
                @endphp
                {{-- @for ($i=0; $i < 5; $i++) --}}
                @foreach ($products as $product)
                  @php
                  // $banproduct=array_unique(Session::get('banproduct'));
                  // $ban=false;
                  // foreach ($banproduct as $key2 ) {
                  //   if($key2==$product->id){
                  //     $ban=true;
                  //   }
                  // }
                  // if($ban){
                  //   continue;
                  // }
                    $img=\App\Models\ProductImg::where('product_id',$product->id)->first();
                  @endphp
                    <div id="procuct{{ $product->id }}">
                      <div class="product-item">
                      <div class="card rounded-0">
                        <img class="card-img-top" src="{{asset('local/storage/app/images/product')}}/{{ $img->name }}" alt="" onclick="return alertKm();">
                        <div class="card-body position-relative">
                          @if (!Auth::guest())
                          <div class="product-btn btn-group invisible">
                            <a  class="btn btn-sm btn-success w-50 border" data-toggle="modal" href="{{url('inc_cart-add')}}/{{ $product->id }}">ช้อปเลย</a>
                            <a  class="btn btn-sm btn-success w-50 border" href="{{url('product_details')}}/{{ $product->id }}">ดูสินค้า</a>
                          </div>
                          @endif
                          <p class="card-text text-line-max2" style="height:40px">{{ $product->name }} <br /></p>
                        </div>
                        <div class="card-footer text-center text-sm-left">
                          @if (Auth::guest())
                            <a href="{{ url('login') }}"><span class="price">เข้าสู่ระบบเพื่อตรวจสอบราคา </span></a>
                            <span class="" data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="Price & Promotion" data-content="เข้าสู่ระบบเพื่อตรวจสอบราคา"></span>
                          @else
                            <span class="price">฿ {{ $product->price/$product->piece }} <small>/ ชิ้น</small></span>
                            <span class="tag float-sm-right " data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="Price & Promotion" data-content="@include('inc_info-price') @include('inc_info-promotion')">PRO | <i class="fa fa-street-view"></i></span>
                          @endif
                        </div>
                      </div>
                    </div>
                    </div>
                  @endforeach
                {{-- @endfor --}}
              </div>
            </div>
          </div>
          <div id="snackbar">เพิ่มสินค้าลงในตะกร้าแล้ว</div>
          <!-- Pagination -->
          @php
            // if(Session::has('page')){
            //
            // }if($page==1){

          @endphp
          <div class="row">
            <div class="col-12 mt-3">
              <nav aria-label="Page navigation example">
                <ul class="pagination pagination-sm justify-content-end">
                  @php
                  $p=0;
                  @endphp
                  @if ($page==1)
                    <li class="page-item float-left disabled">
                      <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>
                  @else
                    <li class="page-item float-left">
                      <a class="page-link" href="{{url('changePage/'.$brand[$i]->id.'/1')}}" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>
                  @endif
                  @for ($j=1; $j <= $total_page; $j++)
                    @php
                    $page1=$page-2;
                    $page2=$page+2;
                    @endphp
                    @if ($total_page<4)
                      @if ($page==$j)
                        <li class="page-item float-left active"><a class="page-link" href="{{url('changePage/'.$id.'/'.$brand[$i]->id.'/'.$j)}}">{{ $j }}</a></li>
                      @else
                        <li class="page-item float-left"><a class="page-link" href="{{url('changePage/'.$id.'/'.$brand[$i]->id.'/'.$j)}}">{{ $j }}</a></li>
                      @endif
                    @else
                      @if ($page1>0 and $page2<=$total_page)
                        @if ($page==$j)
                          <li class="page-item float-left active"><a class="page-link" href="{{url('changePage/'.$id.'/'.$brand[$i]->id.'/'.$j)}}">{{ $j }}</a></li>
                        @elseif ($j>=$page1 and $j<=$page2)
                          <li class="page-item float-left"><a class="page-link" href="{{url('changePage/'.$id.'/'.$brand[$i]->id.'/'.$j)}}">{{ $j }}</a></li>
                        @endif
                      @elseif($page2>$total_page)
                        @php
                        $page1=$page1+($total_page-$page2);
                        @endphp
                        @if ($page==$j)
                          <li class="page-item float-left active"><a class="page-link" href="{{url('changePage/'.$id.'/'.$brand[$i]->id.'/'.$j)}}">{{ $j }}</a></li>
                          @php
                          $p++;
                          @endphp
                        @elseif($p<5 and $j>=$page1)
                          <li class="page-item float-left"><a class="page-link" href="{{url('changePage/'.$id.'/'.$brand[$i]->id.'/'.$j)}}">{{ $j }}</a></li>
                          @php
                          $p++;
                          @endphp
                        @endif
                      @elseif($page1<=0)
                        @php
                        $page2=5;
                        @endphp
                        @if ($page==$j)
                          <li class="page-item float-left active"><a class="page-link" href="{{url('changePage/'.$id.'/'.$brand[$i]->id.'/'.$j)}}">{{ $j }}</a></li>
                          @php
                          $p++;
                          @endphp
                        @elseif($p<5 and $j<=$page2)
                          <li class="page-item float-left"><a class="page-link" href="{{url('changePage/'.$id.'/'.$brand[$i]->id.'/'.$j)}}">{{ $j }}</a></li>
                          @php
                          $p++;
                          @endphp
                        @endif
                      @endif
                    @endif
                  @endfor
                  @if ($page==$total_page)
                    <li class="page-item float-left disabled">
                      <a class="page-link" href="{{url('changePage/'.$id.'/'.$brand[$i]->id.'/'.$total_page)}}" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  @else
                    <li class="page-item float-left">
                      <a class="page-link" href="{{url('changePage/'.$id.'/'.$brand[$i]->id.'/'.$total_page)}}" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  @endif
                  {{-- <li class="page-item float-left active"><a class="page-link" href="#">1</a></li>
                  <li class="page-item float-left"><a class="page-link" href="#">2</a></li>
                  <li class="page-item float-left"><a class="page-link" href="#">3</a></li>
                  <li class="page-item float-left"><a class="page-link" href="#">4</a></li>
                  <li class="page-item float-left"><a class="page-link" href="#">5</a></li> --}}

                </ul>
              </nav>
            </div>
          </div>

            @endfor





    </div>
    </div>

  </div>
  @php
    if(Auth::guest()){
      $lat=0;
      $lng=0;
    }else{
      $lat=Auth::user()->lat;
      $lng=Auth::user()->longtitude;
    }
  @endphp


  <!-- Site footer -->
  @include('inc_footer')
  <!-- Site footer -->

  <!-- Site js -->
  @include('inc_js')
  <!-- Site js -->

  <!-- Site Sidewarp -->
  @include('inc_sidewarp')
  <!-- Site Sidewarp -->

  <!-- Owl Carousel -->
  <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/owl.theme.default.css')}}">
  <script src="{{asset('assets/js/owl.carousel.js')}}"></script>
  <script>
    $(document).ready(function() {
      var owl = $('.owl-carousel');
      owl.owlCarousel({
        navText: [ '', '' ],
        items: 1,
        loop: true,
        autoplay: true,
        autoplayTimeout: 6000,
        autoHeight: true,
        autoplayHoverPause: true,
        animateOut: 'fadeOut',
        responsive: {
          0:     {  nav: false, dots: false  },
          768:   {  nav: true,  dots: false  },
          1000:  {  nav: true,  dots: false  }
        }
      });
    });
  </script>
  <!-- Owl Carousel -->

  <script>
    $(document).ready(function(){
      // Add smooth scrolling to all links
      $("a.nav-scroll").on('click', function(event) {
        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
          // Prevent default anchor click behavior
          event.preventDefault();
          // Store hash
          var hash = $(this.hash).offset() ? $(this.hash).offset().top : 0;
          //change this number to create the additional off set
          var customoffset = 320

          // Using jQuery's animate() method to add smooth page scroll
          // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
          $('html, body').animate({scrollTop:hash - customoffset}, 300, function(){
            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
          });
        } // End if

      });
    });
    // var p1 ={
    //     "lat": parseFloat({{$lat}}),
    //     "lng": parseFloat({{$lng}}),
    //   };
    //   var p2 = {
    //       "lat": parseFloat('13.829884106119847'),
    //       "lng": parseFloat('100.42264724384768'),
    //   };
    // var rad = function(x) {
    //   return x * Math.PI / 180;
    // };
    //
    // var getDistance = function(p1, p2) {
    //   var R = 6378137; // Earth’s mean radius in meter
    //   var dLat = rad(p2.lat - p1.lat);
    //   var dLong = rad(p2.lng - p1.lng);
    //   var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    //   Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
    //   Math.sin(dLong / 2) * Math.sin(dLong / 2);
    //   var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    //   var d = (R * c)/1000;
    //   return d; // returns the distance in meter
    // };
    // console.log(parseFloat(getDistance(p1,p2)).toFixed(2)+" KM");

    // function cart
    function toast() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }
    // function cart
    function checkmax(num,id) {
      // alert(num);
      if (num>100) {
        // return this.val('100');
        $('#amount').val('100');
      }
      $.ajax({
        type: 'get',
        url: '{{url('checkPrice')}}'+'/'+id+'/'+num,
        data: {_token: "{{ csrf_token() }}"},
        dataType:'json',
        success:function(html){
          price = $('#amount').val();
          price = price*html[0].price;
          discount = (price*(html[0].discount/100));
          $('#price').html(price);
          $('#discount').html(parseInt(discount));
          $('#discountNum').html(html[0].discount+"%");
          $('#total').html(price-discount);
          // swal("Successfully", "ลบเสร็จสิ้น !!", "success");
          // dataTable.clear();
          // dataTable.rows.add(html).draw();
          // $('#product').val();

        }
      });
    }
    function clearCart() {
      $('#cart-modal').remove();
    }
    function addCart() {
      // num = parseInt($('#product_num').text());
      // num++;
      // $('#product_num').text(num);
      $('.modal').modal('hide');
      var formData = $('#cartForm').serialize();
      $.ajax({
        type: 'get',
        url: '{{url('addToCart')}}',
        data: formData,
        dataType:'json',
        success:function(html){
          $('#product_num').text(html.length);
          toast();

        },
        cache: false,
        contentType: false,
        processData: false
      });
      return false;
    }

    function SortBy(sort) {
      // alert(sort);
      // alert(brand+'\n'+SubCategory+'\n'+$('#sort'+brand).val());
      // var popover = $('.tag').data('bs.popover');
      $.ajax({
        type: 'get',
        url: '{{url('SortBy')}}'+'/'+sort,
        data: {_token: "{{ csrf_token() }}"},
        dataType:'html',
        success:function(html){
          // console.log(html);
          location.reload();
          // $('.product_box'+brand).html(html);
        }
      });

    }
    </script>


@endsection
