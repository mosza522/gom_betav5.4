@php
    $product=\App\Models\Product::leftjoin('product_brand','product.band_id','=','product_brand.id')
    ->leftJoin('product_category_sub', 'product.category_sub_id', '=', 'product_category_sub.id')
    ->leftJoin('product_category', 'product_category_sub.category_id', '=', 'product_category.id')
    ->select('product.*','product_brand.name as brand','product_category_sub.name as nameSubCategory','product_brand.id as brand_id'
    ,'product_category_sub.id as category_sub_id','product_category.name as nameCategory','product_category.id as idCategory')
    ->where('product.id',$id)
    ->first();
    $img=\App\Models\ProductImg::where('product_id',$product->id)->first();
    $promotion=\App\Models\ProductPromotion::where('product_id',$product->id)->orderBy('discount','DESC')->get();
    $total=$product->order_minimum*$product->price;
  	$zones=\App\Models\Zone::where('product_id',$product->id)->orderBy('num','DESC')->get();
    @endphp

      <div class="modal-dialog modal-lg" id="cart-modal">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title font-cloud" id="exampleModalLabel">{{$product->nameCategory}} / {{$product->common_name}} / {{$product->name}}<small> | ยี่ห้อ&nbsp;<a href="{{url('profile')}}/{{ $product->brand_id }}">{{$product->brand}}</a></small></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-6 border border-left-0 border-top-0 border-bottom-0">
              <img class="img-fluid mb-2 mx-auto d-block" src="{{asset('local/storage/app/images/product')}}/{{ $img->name }}">
            </div>
            <div class="col-lg-6">
              <div class="row">
                <div class="col-6">ราคา : </div>
                <div class="col-6"><strong>{{$product->price/$product->piece}} บาท</strong> / ชิ้น</div>
              </div>
              <div class="row">
                <div class="col-6">สั่งซื้อขั้นต่ำ : </div>
                <div class="col-6">{{$product->order_minimum}} ลัง <small>({{ $product->piece }}ชิ้น / ลัง)</small></div>
              </div>
              <div class="row">
                <div class="col-6">ความสามารถจัดหา : </div>
                <div class="col-6">{{$product->supply}} <small>ลัง / เดือน</small></div>
              </div>
              <div class="row">
                <div class="col-6">เวลาส่งมอบ : </div>
                <div class="col-6">ภายใน {{ $product->time }} วัน</div>
              </div>
              <div class="row">
                <div class="col-12">
                  <h6 class="my-2 text-success"><i class="fa fa-street-view mr-1"></i>Promotion </h6>
                  @foreach ($promotion as $element)
                    <a class="badge badge-pill badge-light border border-dashed font-weight-normal p-2 mb-1 w-25"> {{$element->amount}} ลัง | <strong class="text-success">-{{$element->discount}}%</strong></a>
                  @endforeach
                  {{-- <a class="badge badge-pill badge-light border border-dashed font-weight-normal p-2 mb-1 w-25"> 10 ลัง | <strong class="text-success">-5%</strong></a>
                  <a class="badge badge-pill badge-light border border-dashed font-weight-normal p-2 mb-1 w-25"> 20 ลัง | <strong class="text-success">-10%</strong></a>
                  <a class="badge badge-pill badge-light border border-dashed font-weight-normal p-2 mb-1 w-25"> 30 ลัง | <strong class="text-success">-15%</strong></a> --}}
                </div>
              </div>

              <hr class="border-dotted my-3">

              <form id="cartForm" onsubmit="return addCart()">
                {{ csrf_field() }}
                <input type="hidden" id="hidden_id" name="id" value="{{ $product->id }}">
                <div class="form-group row mb-1">
                  <div class="col-6">
                    <div class="input-group">
                      {{-- <span class="input-group-btn">
                        <button id="minus" type="button" onclick="minus_product()" class="btn btn-sm btn-number float-left" disabled="disabled" data-type="minus" data-field="quant[3]">-</button>
                      </span> --}}
                      <input id="amount" type="number" name="amount" min="{{$product->order_minimum}}" max="100" value="{{$product->order_minimum}}" onchange="return checkmax(this.value,{{ $product->id }})">
                      {{-- <input id="amount" type="text" name="quant[3]" class="form-control form-control-sm input-number text-center float-left" min="{{$product->order_minimum}}" value="{{$product->order_minimum}}" onkeypress="return check_number(this)"> --}}
                      {{-- <span class="input-group-btn">
                        <button type="button" onclick="add_product()" class="btn btn-sm btn-number float-left" data-type="plus" data-field="quant[3]">+</button>
                      </span> --}}
                      <span class="input-group-btn">
                        <button type="button"  class="btn btn-sm float-left" disabled="disabled" value="จำนวน">ลัง</button>
                      </span>
                    </div>
                  </div>
                  <label class="col-6">สั่งซื้อขั้นต่ำ {{$product->order_minimum}} ลัง / สูงสุด 100 ลัง</label>
                </div>

                <div class="row">
                  <div class="col-6">ราคารวม</div>
                  <div class="col-6"><strong id="price">{{number_format($product->order_minimum*$product->price)}}</strong><small class="float-right">บาท</small></div>
                </div>
                <div class="row">
                  <div class="col-6">ส่วนลด <span id="discountNum" class="badge badge-pill badge-light border border-secondary border-dashed float-right">0%</span></div>
                  <div class="col-6"><strong id="discount">
                    @foreach ($promotion as $element)
                      @if ($product->order_minimum >= $element->amount)
                        @php
                        $discount=$total/$element->discount;
                          $discount;
                          break;
                        @endphp
                      @else
                        @php
                          $discount=0;
                        @endphp
                      @endif
                    @endforeach
                    {{ number_format($discount,2) }}
                  </strong><small class="float-right">บาท</small></div>
                </div>
                <div class="row">
                  <div class="col-6">ยอดสุทธิ </div>
                  <div class="col-6 text-danger"><strong id="total">{{ number_format($total-$discount,2) }}</strong><small class="float-right">บาท</small></div>
                </div>

                <hr class="border-dashed my-2">
                <small class="text-success d-block mb-2"><i class="fa fa-info"></i> กันเขตร้านค้า
                  @php
                  $text="";
                  @endphp
                  @foreach ($zones as $zone)
                    @php
                    $text.="ซื้อ $zone->num ลัง : กันเขต $zone->radius km/$zone->time วัน <br />";
                    @endphp
                  @endforeach
            		{!! $text !!}</small>

                <div class="form-group row">
                  <div class="col-6">
                    <button type="submit" class="btn btn-sm btn-block btn-success">เพิ่มในตะกร้าสินค้า</button>
                  </div>
                  <div class="col-6">
                    <a class="btn btn-sm btn-block btn-primary" href="{{url('product_details')}}/{{ $product->id }}">รายละเอียดสินค้า</a>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
      </div>
