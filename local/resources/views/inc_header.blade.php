
<!-- Main Head -->
<div id="site-header" class="animated fadeInDown">

  <div class="head-banner">
    <a href="{{url('register')}}">
      <img class="img-fluid switch-banner">
    </a>
  </div>

  <div class="head-bar-top">
  <div class="container-fluid">
      {{-- <ul class="menu-top font-cloud">
        <li class="nav-item dropdown">
          <a class="nav-link main" href="#"><i class="fa fa-user-circle"></i>&nbsp;บัญชีของฉัน</a>
          <ul class="dropdown-menu animated fadeInDown">
            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-user-circle"></i>&nbsp;Username</a></li>
            <li class="nav-item"><a class="nav-link" onclick="openOrderHistory()"><i class="fa fa-history"></i>&nbsp;ประวัติการสั่งซื้อ</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('setting')}}"><i class="fa fa-cog"></i>&nbsp;ตั้งค่าบัญชี</a></li>
            <li class="nav-item"><a class="nav-link" href="logout"><i class="fa fa-sign-out"></i>&nbsp;ออกจากระบบ</a></li>
          </ul>
        </li>
        <li class="nav-item"><a class="nav-link main" href="{{url('register')}}">สมัครสามชิก</a></li>
        <li class="nav-item"><a class="nav-link main" id="openSign" onclick="openSign()">เข้าสู่ระบบ</a></li>
        <li class="nav-item"><a class="nav-link main" href="{{url('cart')}}">ตะกร้าสินค้า</a></li>
        <li class="nav-item"><a class="nav-link main" href="{{url('cart')}}">ชำระเงิน</a></li>
        <li class="nav-item"><a class="nav-link" href="{{url('help_center')}}">ศูนย์ช่วยเหลือ</a></li>
        <li class="nav-item"><a class="nav-link main" href="{{url('contact')}}">ติดต่อเรา</a></li>
        <li class="nav-item">
          <a class="nav-link main px-2 d-inline-block" href="" title="Facebook"><i class="fa fa-facebook"></i></a>
          <a class="nav-link main px-2 d-inline-block" href="" title="Twitter"><i class="fa fa-twitter-square"></i></a>
          <a class="nav-link main px-2 d-inline-block" href="" title="YouTube"><i class="fa fa-youtube-play"></i></a>
        </li>
      </ul> --}}
      <ul class="menu-top font-cloud">
      @if (!empty(Auth::user()))
        <li class="nav-item dropdown">
          <a class="nav-link main" href="#"><i class="fa fa-user-circle"></i>&nbsp;บัญชีของฉัน</a>
          <ul class="dropdown-menu animated fadeInDown">
            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-user-circle"></i>&nbsp;{{ Auth::user()->name }}</a></li>
            <li class="nav-item"><a class="nav-link" id="openOrderHistory" onclick="openOrderHistory()"><i class="fa fa-history"></i>&nbsp;ประวัติการสั่งซื้อ</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('setting')}}"><i class="fa fa-cog"></i>&nbsp;ตั้งค่าบัญชี</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('backoffice')}}"><i class="fa fa-cog"></i>&nbsp;Backoffice</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('logout')}}"><i class="fa fa-sign-out"></i>&nbsp;ออกจากระบบ</a></li>
          </ul>
        </li>
        <li class="nav-item"><a class="nav-link main" href="{{url('cart')}}">ตะกร้าสินค้า</a></li>
        <li class="nav-item"><a class="nav-link main" href="{{url('cart')}}">ชำระเงิน</a></li>
        <li class="nav-item"><a class="nav-link main" href="{{url('help_center')}}">ศูนย์ช่วยเหลือ</a></li>
        <li class="nav-item"><a class="nav-link main" href="{{url('contact')}}">ติดต่อเรา</a></li>
        @else
        <li class="nav-item"><a class="nav-link main" href="{{url('register')}}">สมัครสามชิก</a></li>
        <li class="nav-item"><a class="nav-link main" id="openSign" onclick="openSign()">เข้าสู่ระบบ</a></li>
        <li class="nav-item"><a class="nav-link main" href="{{url('help_center')}}">ศูนย์ช่วยเหลือ</a></li>
        <li class="nav-item"><a class="nav-link main" href="{{url('contact')}}">ติดต่อเรา</a></li>
      @endif
      @php
        $contact= \App\Models\Contact::first();
      @endphp
      @if (isset($contact))
        <li class="nav-item">
        <a class="nav-link main px-2 d-inline-block" href="{{ $contact->facebook }}" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a>
          <a class="nav-link main px-2 d-inline-block" href="{{ $contact->youtube }}" target="_blank" title="Twitter"><i class="fa fa-twitter-square"></i></a>
          <a class="nav-link main px-2 d-inline-block" href="{{ $contact->line }}" target="_blank" title="YouTube"><i class="fa fa-youtube-play"></i></a>
        </li>
      @endif

      </ul>
  </div>
  </div>




  <div class="head-bar-mid">
  <div class="container-fluid">
    <div class="row align-items-center">

      <div class="col-sm-3 col-md-3 col-lg-2" id="site-logo">
        <a href="{{url('index')}}">
          <img class="img-fluid opacity-9 animated pulse" src="{{asset('assets/images/logo.png')}}">
        </a>
      </div>

      <div class="col-sm-9 col-md-9 col-lg-10 my-2 text-center text-sm-left">
        {{-- <form id="form-search" role="search" method="get"> --}}
          <div class="search-box float-none float-sm-left mx-auto">
            <input class="form-control search-input searchbox" id="search-input" type="search" name="search"   placeholder="Search for Products, Brands and more">
            {{-- <button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button> --}}
          </div>
        {{-- </form> --}}
        <div class="btn-box pl-0 pl-lg-2">
          <a class="btn btn-search-toggle d-md-none d-inline-block" id="btn-search-toggle">
            <i class="fa fa-search openclosesearch"></i>
            <i class="fa fa-search-minus openclosesearch" style="display:none"></i>
          </a>
          <a class="btn btn-cart" href="{{url('cart')}}" title="ดูตะกร้าสินค้า">
            <i class="fa fa-shopping-basket"></i>
            <span class="item-number" id="product_num">@if (Session::has('cart'))
              {{ count(Session::get('cart')) }}
            @else
              0
            @endif</span>
          </a>
          <a class="btn btn-cate" title="ดูหมวดหมู่สินค้า">
            <i class="fa fa-leaf reverse mr-2"></i>หมวดหมู่
          </a>
          <a class="btn btn-menu">
            <i class="fa fa-ellipsis-v mr-2"></i>เมนู
          </a>
        </div>
      </div>

    </div>
  </div>
  </div>

  <div class="head-bar-bot">
  <div class="container-fluid">
      <ul class="menu-bot font-cloud">
        @php
          $category=\App\Models\ProductCategory::all();
        @endphp
        @foreach ($category as $key )
          <li class="nav-item dropdown">
            <a href="javascript:void(0)" class="nav-link main">{{ $key->name }}</a>
            <ul class="dropdown-menu animated fadeInDown">
              @php
              $subcategory=\App\Models\SubCategory::where('category_id',$key->id)->get();
              @endphp
              @if ($subcategory->count() > 0)
                @for ($i=0; $i < $subcategory->count(); $i++)
                  <li class="nav-item"><a class="nav-link" href="{{url('product')}}/{{ $subcategory[$i]->id }}">{{$subcategory[$i]->name}}</a></li>
                @endfor
              @else
                <li class="nav-item"><a class="nav-link" href="javascript:void(0)">ไม่พบข้อมูล</a></li>
              @endif
            </ul>
          </li>
        @endforeach

        {{-- <li class="nav-item dropdown">
          <a href="{{url('product')}}" class="nav-link main">ยาฆ่าหญ้า</a>
          <ul class="dropdown-menu animated fadeInDown">
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ยาฆ่าหญ้า 01</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ยาฆ่าหญ้า 02</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ยาฆ่าหญ้า 03</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ยาฆ่าหญ้า 04</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ยาฆ่าหญ้า 05</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a href="{{url('product')}}" class="nav-link main">ยาคุม/ฆ่า</a>
          <ul class="dropdown-menu animated fadeInDown">
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ยาคุม 01</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ยาคุม 02</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ยาคุม 03</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ยาคุม 04</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ยาคุม 05</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a href="{{url('product')}}" class="nav-link main">ยาฆ่าแมลง</a>
          <ul class="dropdown-menu animated fadeInDown">
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ยาฆ่าแมลง 01</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ยาฆ่าแมลง 02</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ยาฆ่าแมลง 03</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ยาฆ่าแมลง 04</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ยาฆ่าแมลง 05</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a href="{{url('product')}}" class="nav-link main">สารจับใบ</a>
          <ul class="dropdown-menu animated fadeInDown">
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">สารจับใบ 01</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">สารจับใบ 02</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">สารจับใบ 03</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">สารจับใบ 04</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">สารจับใบ 05</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a href="{{url('product')}}" class="nav-link main">เชื้อรา</a>
          <ul class="dropdown-menu animated fadeInDown">
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">เชื้อรา 01</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">เชื้อรา 02</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">เชื้อรา 03</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">เชื้อรา 04</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">เชื้อรา 05</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a href="{{url('product')}}" class="nav-link main">ฮอร์โมน</a>
          <ul class="dropdown-menu animated fadeInDown">
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ฮอร์โมน 01</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ฮอร์โมน 02</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ฮอร์โมน 03</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ฮอร์โมน 04</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ฮอร์โมน 05</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a href="{{url('product')}}" class="nav-link main">ปุ๋ย</a>
          <ul class="dropdown-menu animated fadeInDown">
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ปุ๋ย 01</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ปุ๋ย 02</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ปุ๋ย 03</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ปุ๋ย 04</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">ปุ๋ย 05</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a href="{{url('product')}}" class="nav-link main">เมล็ดพันธ์</a>
          <ul class="dropdown-menu animated fadeInDown">
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">เมล็ดพันธ์ 01</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">เมล็ดพันธ์ 02</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">เมล็ดพันธ์ 03</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">เมล็ดพันธ์ 04</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">เมล็ดพันธ์ 05</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown"><!-- Add Class .active -->
          <a href="{{url('product')}}" class="nav-link main">อุปกรณ์</a>
          <ul class="dropdown-menu animated fadeInDown">
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">อุปกรณ์ 01</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">อุปกรณ์ 02</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">อุปกรณ์ 03</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">อุปกรณ์ 04</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('product')}}">อุปกรณ์ 05</a></li>
          </ul>
        </li> --}}
      </ul>
  </div>
  </div>

</div>
@php
  if (isset($login)) {
    echo "<script>
    setTimeout(function(){ $('#openSign').trigger('click'); }, 100);
    </script>";
  }

@endphp
@php
  if (isset($my)) {
    echo "<script>
    setTimeout(function(){ $('#openOrderHistory').trigger('click'); }, 100);
    </script>";
  }

@endphp

<!-- Main Head -->
