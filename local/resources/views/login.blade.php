<!-- Side Sign -->
<a href="javascript:void(0)" class="closebtn" onclick="closeSign()">×</a>
<div class="container animated bounceInRight my-5">

  <form class="form-signin" action="{{url('login')}}" method="POST" id="login">
    {{ csrf_field() }}
    <img class="img-fluid mb-4" src="{{asset('assets/images/logo-login.png')}}">
    <h6 class="text-success font-cloud">Please sign in</h6>
    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="text" id="inputEmail" name="username" class="form-control opacity-6" placeholder="username" required autofocus>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" name="password" class="form-control opacity-6" placeholder="password" required>

    <div class="d-block mb-2">
      <label class="custom-control custom-checkbox align-items-center">
        <input id="subscribe" type="checkbox" value="remember-me" class="custom-control-input">
        <span class="custom-control-indicator"></span>
        <span class="custom-control-description">Remember me</span>
      </label>
    </div>

    <button class="btn btn-lg btn-success btn-block opacity-6" type="submit">Sign in</button>
    <a href="javascript:void(0)" class="text-secondary mt-2" id="forgot"><small><i class="fa fa-question-circle"></i>&nbsp;ลืมรหัสผ่าน</small></a>
    <span class="text-secondary opacity-4 px-2">|</span>
    <a href="{{url('register')}}" class="text-secondary mt-2"><small><i class="fa fa-user-circle-o"></i>&nbsp;สมัครสมาชิก</small></a>

    <div class="text-center">
      <a class="btn btn-secondary rounded-circle mt-5 opacity-3" href="javascript:void(0)" onclick="closeSign()"><i class="fa fa-close"></i></a>
    </div>
  </form>
  <form class="form-signin" action="{{ route('password.email') }}" method="POST" id="forgotpass">
    {{ csrf_field() }}
    <img class="img-fluid mb-4" src="{{asset('assets/images/logo-login.png')}}">
    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <h6 class="text-success font-cloud">Please type your e-mail</h6>
    <input type="email" id="email" name="email" class="form-control opacity-6" placeholder="test@mail.com" required autofocus>
    @if ($errors->has('email'))
        <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    @endif
    </div>
    <button class="btn btn-lg btn-success btn-block opacity-6" type="submit">Send Password Reset Link</button>
    <div class="text-center">
      <a class="btn btn-secondary rounded-circle mt-5 opacity-3" href="javascript:void(0)" onclick="closeSign()"><i class="fa fa-close"></i></a>
    </div>
  </form>

  {{--resetpass--}}


</div>
<script type="text/javascript">
$('#forgotpass').hide();
  $('#forgot').click(function(){
$('#forgotpass').show();
$('#login').hide();
  });

</script>
