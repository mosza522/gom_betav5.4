@extends('layout.main')
@section('content')

  <!-- Highlight -->
  @if (isset($resetpass))
    <form class="form-signin" action="{{ route('password.request') }}" method="POST" id="resetpass">
      {{ csrf_field() }}
      <img class="img-fluid mb-4" src="{{asset('assets/images/logo-login.png')}}">
      <input type="hidden" name="token" value="{{ $token }}">
      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
      <h6 class="text-success font-cloud">E-Mail Address</h6>
      <input id="email" type="email" class="form-control opacity-6" name="email" value="{{ $email or old('email') }}" required autofocus>

      </div>
      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
      <h6 class="text-success font-cloud">Password</h6>
      <input id="password" type="password" class="form-control opacity-6" name="password" required>

      </div>

      <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
        <h6 class="text-success font-cloud">Confirm Password</h6>
      <input id="password-confirm" type="password" class="form-control opacity-6" name="password_confirmation" required>

      </div>
      <button class="btn btn-lg btn-success btn-block opacity-6" type="submit">Send Password Reset Link</button>
      <div class="text-center">
        <a class="btn btn-secondary rounded-circle mt-5 opacity-3" href="javascript:void(0)" onclick="closeSign()"><i class="fa fa-close"></i></a>
      </div>
    </form>
  @endif
@endsection
