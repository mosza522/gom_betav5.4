<footer class="site-footer mt-5">

  <div class="section-01">
    <div class="container my-5">
      <div class="row">

        <div class="col-md-2">
          <h5>Categiries
            <small class="switch float-right">Open</small>
            <small class="switch float-right" style="display:none;">Close</small>
          </h5>
          @php
            $category=\App\Models\ProductCategory::all();
          @endphp
          <ul>
            @foreach ($category as $key )

              <li><a href="{{url('product')}}/{{ $key->id }}"><small>{{ $key->name }}</small></a></li>
            @endforeach
            {{-- <li><a href=""><small>ยาฆ่าหญ้า</small></a></li>
            <li><a href=""><small>ยาคุม/ฆ่า</small></a></li>
            <li><a href=""><small>ยาฆ่าแมลง</small></a></li>
            <li><a href=""><small>สารจับใบ</small></a></li>
            <li><a href=""><small>เชื้อรา</small></a></li>
            <li><a href=""><small>ฮอร์โมน</small></a></li>
            <li><a href=""><small>ปุ๋ย</small></a></li>
            <li><a href=""><small>เมล็ดพันธ์</small></a></li>
            <li><a href=""><small>อุปกรณ์</small></a></li> --}}
          </ul>
        </div>

        <div class="col-md-2">
          <h5>Menu
            <small class="switch float-right">Open</small>
            <small class="switch float-right" style="display:none;">Close</small>
          </h5>
          <ul>
            <li><a href="{{url('register')}}"><small>สมัครสามชิก</small></a></li>
            <li><a onclick="openSign()"><small>เข้าสู่ระบบ</small></a></li>
            <li><a href="{{url('cart')}}"><small>ตะกร้าสินค้า</small></a></li>
            <li><a href="{{url('cart')}}"><small>ชำระเงิน</small></a></li>
            <li><a href="{{url('help_center')}}"><small>ศูนย์ช่วยเหลือ</small></a></li>
            <li><a href="{{url('contact')}}"><small>ติดต่อเรา</small></a></li>
          </ul>
        </div>

        <div class="col-md-3 mt-4 mt-md-0">
          <div class="contact">
            @php
            $contact= \App\Models\Contact::first();
            @endphp
            <h5>Address</h5>
            <p class="mb-0 mb-md-2">{{$contact->address}}</p>
            <p class="mb-0"><small><i class="fa fa-envelope-o"></i>{{$contact->email}}</small></p>
            <p class="mb-0"><small><i class="fa fa-phone-square"></i>{{$contact->phone}}</small></p>
            <p class="mb-0"><small><i class="fa fa-fax"></i>{{$contact->fax}}</small></p>
            <p class="mb-0"><small><i class="fa fa-mobile"></i>{{$contact->mobile}}</small></p>
          </div>
        </div>

        <div class="col-md-5 mt-4 mt-md-0">
          <h5>GOMBeta</h5>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce porttitor, magna eu facilisis mattis, elit orci condimentum libero, eget dictum lectus metus id odio. Praesent ut ligula sit amet erat varius pellentesque. Nunc pharetra nisi felis, eu lacinia dui convallis finibus.<br><small>© GOMBeta 2018</small></p>
          <!-- <h5>Exclusive Deals and Offers!</h5> -->
          <small class="d-block mb-1">Subscribe to the GOMBeta newsletter</small>
          <form>
            <div class="input-group">
                <input type="email" class="form-control" placeholder="Your Email Adress" required>
                <span class="input-group-btn">
                  <button class="btn btn-success" type="submit">Subscribe!!</button>
                </span>
            </div>
          </form>
        </div>

      </div>
    </div>
  </div>


  <div class="section-02">
    <div class="container my-5">
      <div class="row">
        @php
          $payment=\App\Models\Payment::leftJoin('bank','payment.bank_id','=','bank.id')->get();
        @endphp
        <div class="col-sm-12 col-lg-10 text-center text-md-left">
          <h6 class="font-cloud">ช่องทางการชำระเงิน</h6>
          @foreach ($payment as $key )
            <a data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="{{ $key->name }}" data-content="<small>เลขบัญชี : {{ $key->account_no }}<br>ชื่อบัญชี : {{ $key->account_name }}<br>ประเภทบัญชี : {{ $key->type }}<br>สาขา : {{ $key->branch }}</small>">
              <i class="opacity-8 thbanks thbanks-3x {{ $key->icon }}" aria-hidden="true"></i>
            </a>
          @endforeach
          {{-- <a data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="ธนาคารไทยพาณิชย์" data-content="<small>เลขบัญชี : 408-3-28199-6<br>ชื่อบัญชี : ธีรยุทธ รุ่งเรือง<br>ประเภทบัญชี : ออมทรัพย์<br>สาขา : เซ็นทรัลพลาซ่า เวสต์เกต</small>">
            <i class="opacity-8 thbanks thbanks-3x thbanks-scb" aria-hidden="true"></i>
          </a>
          <a data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="ธนาคารกรุงเทพ" data-content="<small>เลขบัญชี : 555-5-55555-5<br>ชื่อบัญชี : ธีรยุทธ รุ่งเรือง<br>ประเภทบัญชี : ออมทรัพย์<br>สาขา : เซ็นทรัลพลาซ่า เวสต์เกต</small>">
            <i class="opacity-8 thbanks thbanks-3x thbanks-bbl" aria-hidden="true"></i>
          </a>
          <a data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="ธนาคารทหารไทย" data-content="<small>เลขบัญชี : 273-1-00195-0<br>ชื่อบัญชี : ธีรยุทธ รุ่งเรือง<br>ประเภทบัญชี : กระแสรายวัน<br>สาขา : เซ็นทรัลพลาซ่า เวสต์เกต</small>">
            <i class="opacity-8 thbanks thbanks-3x thbanks-tmb" aria-hidden="true"></i>
          </a>
          <a data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="ธนาคารกรุงไทย" data-content="<small>เลขบัญชี : 555-5-55555-5<br>ชื่อบัญชี : ธีรยุทธ รุ่งเรือง<br>ประเภทบัญชี : ออมทรัพย์<br>สาขา : เซ็นทรัลพลาซ่า เวสต์เกต</small>">
            <i class="opacity-8 thbanks thbanks-3x thbanks-ktb" aria-hidden="true"></i>
          </a>
          <a data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="ธนาคารกสิกรไทย" data-content="<small>เลขบัญชี : 031-1-41178-0<br>ชื่อบัญชี : ธีรยุทธ รุ่งเรือง<br>ประเภทบัญชี : ออมทรัพย์<br>สาขา : เซ็นทรัลพลาซ่า เวสต์เกต</small>">
            <i class="opacity-8 thbanks thbanks-3x thbanks-kbank" aria-hidden="true"></i>
          </a>
          <a data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="ธนาคาร ธกส." data-content="<small>เลขบัญชี : 555-5-55555-5<br>ชื่อบัญชี : ธีรยุทธ รุ่งเรือง<br>ประเภทบัญชี : ออมทรัพย์<br>สาขา : เซ็นทรัลพลาซ่า เวสต์เกต</small>">
            <i class="opacity-8 thbanks thbanks-3x thbanks-baac" aria-hidden="true"></i>
          </a>
          <a data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="ธนาคารกรุงศรีอยุธยา" data-content="<small>เลขบัญชี : 778-1-09637-4<br>ชื่อบัญชี : ธีรยุทธ รุ่งเรือง<br>ประเภทบัญชี : ออมทรัพย์<br>สาขา : เซ็นทรัลพลาซ่า เวสต์เกต</small>">
            <i class="opacity-8 thbanks thbanks-3x thbanks-bay" aria-hidden="true"></i>
          </a>
          <a data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="ธนาคารออมสิน" data-content="<small>เลขบัญชี : 555-5-55555-5<br>ชื่อบัญชี : ธีรยุทธ รุ่งเรือง<br>ประเภทบัญชี : ออมทรัพย์<br>สาขา : เซ็นทรัลพลาซ่า เวสต์เกต</small>">
            <i class="opacity-8 thbanks thbanks-3x thbanks-citi" aria-hidden="true"></i>
          </a> --}}
        </div>

        <div class="col-sm-12 col-lg-2 text-center text-md-left mt-5 mt-lg-0">
          <div class="float-none float-lg-right">
            <h6 class="font-cloud">Fallow Us</h6>
            <a class="social line" href="{{ $contact->line }}" target="_blank"></a>
            <a class="social youtube" itle="YouTube" href="{{ $contact->youtube }}" target="_blank"></a>
            <a class="social facebook" itle="Facebook" href="{{ $contact->facebook }}" target="_blank"></a>
          </div>
        </div>

      </div>
    </div>
  </div>

  <button id="goTop" title="Go to top"><i class="fa fa-angle-up"></i></button>

</footer>
<script type="text/javascript">


</script>
