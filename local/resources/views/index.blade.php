@extends('layout.main')
@section('content')
  <!-- Highlight -->
  {{-- {{ dd(Session::get('banproduct')) }} --}}
  <div id="highlight">
    <div class="row">
    <div class="col-12">
      <ul class="nav nav-pills nav-justified mb-0 mb-lg-2">
        <li class="nav-item">
          <a id="banner_tab" class="nav-link active">HIGHLIGHT</a>
        </li>
        <li class="nav-item">
          <a id="brand_tab" class="nav-link">BRANDS</a>
        </li>
        <li class="nav-item">
          <a id="category_tab" class="nav-link">GATEGORIES</a>
        </li>
      </ul>
    </div>
    </div>
    <div class="row" id="banner">
    <div class="col-12">
      <div id="pills-01" class="p-md-2 px-lg-0">
        <div class="banner owl-carousel owl-theme">
          @php
            $hl=\App\Models\Highlight::get();
            $count=1;
          @endphp
          <div class="item">
            @foreach ($hl as $key )
              <a href="{{url('product_details')}}/{{ $key->product_id }}">
                <img class="card-img" src="{{asset('local/storage/app/images/img-highlight')}}/{{ $key->banner }}">
              </a>
              @if ($count%1==0 and count($hl)!=$count)
              </div>
              <div class="item">
              @endif
              @php
              $count++;
              @endphp
            @endforeach
          </div>
          {{-- <div class="item">
            <a href="{{url('product_details')}}">
              <img class="card-img" src="{{asset('assets/images/product/add_01.jpg')}}">
            </a>
            <a href="{{url('product_details')}}">
              <img class="card-img" src="{{asset('assets/images/product/add_06.jpg')}}">
            </a>
            <a href="{{url('product_details')}}">
              <img class="card-img" src="{{asset('assets/images/product/add_02.jpg')}}">
            </a>
            <a href="{{url('product_details')}}">
              <img class="card-img" src="{{asset('assets/images/product/add_07.jpg')}}">
            </a>
            <a href="{{url('product_details')}}">
              <img class="card-img" src="{{asset('assets/images/product/add_03.jpg')}}">
            </a>
          </div>
          <div class="item">
            <a href="{{url('product_details')}}">
              <img class="card-img" src="{{asset('assets/images/product/add_09.jpg')}}">
            </a>
            <a href="{{url('product_details')}}">
              <img class="card-img" src="{{asset('assets/images/product/add_08.jpg')}}">
            </a>
            <a href="{{url('product_details')}}">
              <img class="card-img" src="{{asset('assets/images/product/add_04.jpg')}}">
            </a>
            <a href="{{url('product_details')}}">
              <img class="card-img" src="{{asset('assets/images/product/add_09.jpg')}}">
            </a>
            <a href="{{url('product_details')}}">
              <img class="card-img" src="{{asset('assets/images/product/add_08.jpg')}}">
            </a>
            <a href="{{url('product_details')}}">
              <img class="card-img" src="{{asset('assets/images/product/add_04.jpg')}}">
            </a>
          </div> --}}
        </div>
      </div>
    </div>
    </div>
    <div class="row" id="brand">
    <div class="col-12">
      <div id="pills-02" class="p-2 p-lg-0">
        <div class="row px-3">
          @php
          $count=\App\Models\ProductBrands::count();
            $brand=\App\Models\ProductBrands::limit(12)->get();
            $userId="";
            if (!Auth::guest()) {
              $userId=Auth::user()->id;
            }
          @endphp
          @foreach ($brand as $element)
            <div class="col-6 col-sm-2 px-0" >
              <a href="{{url('profile')}}/{{ $element->id }}">
                <img class="img-fluid border" width="100%" src="{{asset('local/storage/app/images/logo').'/'.$element->logo}}">
              </a>
            </div>
          @endforeach
          {{-- <div class="col-6 col-sm-2 px-0">
            <a href="{{url('profile')}}">
              <img class="img-fluid border" src="{{asset('assets/images/brand/brand_01.png')}}">
            </a>
          </div>
          <div class="col-6 col-sm-2 px-0">
            <a href="{{url('profile')}}">
              <img class="img-fluid border" src="{{asset('assets/images/brand/brand_01.png')}}">
            </a>
          </div>
        </div> --}}
        {{--  <div class="col-6 col-sm-2 px-0">
            <a href="{{url('profile')}}">
              <img class="img-fluid border" src="{{asset('assets/images/brand/brand_01.png')}}">
            </a>
          </div>
          <div class="col-6 col-sm-2 px-0">
            <a href="{{url('profile')}}">
              <img class="img-fluid border" src="{{asset('assets/images/brand/brand_01.png')}}">
            </a>
          </div>
           <div class="col-6 col-sm-2 px-0">
            <a href="{{url('profile')}}">
              <img class="img-fluid border" src="{{asset('assets/images/brand/brand_01.png')}}">
            </a>
          </div>
          <div class="col-6 col-sm-2 px-0">
            <a href="{{url('profile')}}">
              <img class="img-fluid border" src="{{asset('assets/images/brand/brand_01.png')}}">
            </a>
          </div>
          <div class="col-6 col-sm-2 px-0">
            <a href="{{url('profile')}}">
              <img class="img-fluid border" src="{{asset('assets/images/brand/brand_01.png')}}">
            </a>
          </div>
          <div class="col-6 col-sm-2 px-0">
            <a href="{{url('profile')}}">
              <img class="img-fluid border" src="{{asset('assets/images/brand/brand_01.png')}}">
            </a>
          </div>
        </div>
        <div class="row px-3">
          <div class="col-6 col-sm-2 px-0">
            <a href="{{url('profile')}}">
              <img class="img-fluid border" src="{{asset('assets/images/brand/brand_01.png')}}">
            </a>
          </div>
          <div class="col-6 col-sm-2 px-0">
            <a href="{{url('profile')}}">
              <img class="img-fluid border" src="{{asset('assets/images/brand/brand_01.png')}}">
            </a>
          </div>
          <div class="col-6 col-sm-2 px-0">
            <a href="{{url('profile')}}">
              <img class="img-fluid border" src="{{asset('assets/images/brand/brand_01.png')}}">
            </a>
          </div>
          <div class="col-6 col-sm-2 px-0">
            <a href="{{url('profile')}}">
              <img class="img-fluid border" src="{{asset('assets/images/brand/brand_01.png')}}">
            </a>
          </div>
          <div class="col-6 col-sm-2 px-0">
            <a href="{{url('profile')}}">
              <img class="img-fluid border" src="{{asset('assets/images/brand/brand_01.png')}}">
            </a>
          </div>
          <div class="col-6 col-sm-2 px-0">
            <a href="{{url('profile')}}">
              <img class="img-fluid border" src="{{asset('assets/images/brand/brand_01.png')}}">
            </a>
          </div>--}}
        </div>
        @if ($count>12)
          <a class="btn btn-block btn-success rounded-0" data-toggle="modal" href="{{url('inc_brand-list')}}">ALL BRAND</a>
        @endif
      </div>
    </div>
    </div>
    <div class="row" id="category">
    <div class="col-12">
      <div id="pills-03" class="p-2 p-lg-0">
        <div class="card-columns">
          @php
            $category=\App\Models\ProductCategory::all();
          @endphp
          @foreach ($category as $element)
            <div class="card rounded-0">
              <a href="{{url('product')}}/{{ $element->id }}">
                <img class="card-img" src="{{asset('local/storage/app/images/img-category').'/'.$element->img}}">
              </a>
            </div>
          @endforeach
          {{-- <div class="card rounded-0">
            <a href="{{url('product')}}">
              <img class="card-img" src="{{asset('assets/images/category/cate_cover_01.jpg')}}">
            </a>
          </div>
          <div class="card rounded-0">
            <a href="{{url('product')}}">
              <img class="card-img" src="{{asset('assets/images/category/cate_cover_04.jpg')}}">
            </a>
          </div>
          <div class="card rounded-0">
            <a href="{{url('product')}}">
              <img class="card-img" src="{{asset('assets/images/category/cate_cover_07.jpg')}}">
            </a>
          </div>
          <div class="card rounded-0">
            <a href="{{url('product')}}">
              <img class="card-img" src="{{asset('assets/images/category/cate_cover_02.jpg')}}">
            </a>
          </div>
          <div class="card rounded-0">
            <a href="{{url('product')}}">
              <img class="card-img" src="{{asset('assets/images/category/cate_cover_05.jpg')}}">
            </a>
          </div>
          <div class="card rounded-0">
            <a href="{{url('product')}}">
              <img class="card-img" src="{{asset('assets/images/category/cate_cover_08.jpg')}}">
            </a>
          </div>
          <div class="card rounded-0">
            <a href="{{url('product')}}">
              <img class="card-img" src="{{asset('assets/images/category/cate_cover_03.jpg')}}">
            </a>
          </div>
          <div class="card rounded-0">
            <a href="{{url('product')}}">
              <img class="card-img" src="{{asset('assets/images/category/cate_cover_06.jpg')}}">
            </a>
          </div>
          <div class="card rounded-0">
            <a href="{{url('product')}}">
              <img class="card-img" src="{{asset('assets/images/category/cate_cover_09.jpg')}}">
            </a>
          </div> --}}
        </div>
      </div>
    </div>
    </div>
  </div>
  <!-- Highlight -->


  <!-- container -->
  <div class="container">
    @php
    // $order
      // $product_id=\App\Models\OrderDetail::leftJoin('order_data','order_detail.order_id','=','order_data.id')
      // ->get();
      // dd($product_id);

    @endphp
    <!-- Promotion of month -->
    <div class="row my-5">
    <div class="col-12">
      <h3>Promotion of month</h3><hr class="border-dashed">
      <div class="product-slide owl-carousel owl-theme">
        @php
        $products=\App\Models\PromotionOfmonth::leftJoin('product','hl_promotionofmonth.product_id','product.id')
        ->select('product.*')
        ->get();
        @endphp
        @foreach ($products as $product)
          @php
          $img=\App\Models\ProductImg::where('product_id',$product->id)->first();
          @endphp
          <div class="item product-item product_id{{ $product->id }}">
            <div class="card rounded-0 pt-3">
              <img class="card-img-top" src="{{asset('local/storage/app/images/product')}}/{{ $img->name }}" alt="">
              <div class="card-body position-relative">
                @if (!Auth::guest())
                <div class="product-btn btn-group invisible">
                  <a  class="btn btn-sm btn-success w-50 border" data-toggle="modal" href="{{url('inc_cart-add')}}/{{ $product->id }}" onclick="return clearCart()">ช้อปเลย</a>
                  <a  class="btn btn-sm btn-success w-50 border" href="{{url('product_details')}}/{{ $product->id }}">ดูสินค้า</a>
                </div>
                @endif
                <p class="card-text text-line-max2" style="height:40px">{{ $product->name }} <br /></p>
              </div>
              <div class="card-footer text-center text-sm-left">
                @if (Auth::guest())
                  <a href="{{ url('login') }}"><span class="price">เข้าสู่ระบบเพื่อตรวจสอบราคา </span></a>
                  <span class="" data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="Price & Promotion" data-content="เข้าสู่ระบบเพื่อตรวจสอบราคา"></span>
                @else
                  <span class="price">฿ {{ $product->price/$product->piece }} <small>/ ชิ้น</small></span>
                  <span class="tag float-sm-right" data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="Price & Promotion" data-content="@include('inc_info-price') @include('inc_info-promotion')">PRO | <i class="fa fa-street-view"></i></span>
                @endif
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
    </div>

    <!-- Sale of month -->
    <div class="row my-5">
    <div class="col-12">
      <h3>Sale of month</h3><hr class="border-dashed">
      <div class="product-slide owl-carousel owl-theme">
        @php
        $products=\App\Models\SaleOfMonth::leftJoin('product','hl_saleofmonth.product_id','product.id')
        ->select('product.*')
        ->get();
        @endphp
        @foreach ($products as $product)
          @php
          $img=\App\Models\ProductImg::where('product_id',$product->id)->first();
          @endphp
          <div class="item product-item product_id{{ $product->id }}">
            <div class="card rounded-0 pt-3">
              <img class="card-img-top" src="{{asset('local/storage/app/images/product')}}/{{ $img->name }}" alt="">
              <div class="card-body position-relative">
                @if (!Auth::guest())
                <div class="product-btn btn-group invisible">
                  <a  class="btn btn-sm btn-success w-50 border" data-toggle="modal" href="{{url('inc_cart-add')}}/{{ $product->id }}" onclick="return clearCart()">ช้อปเลย</a>
                  <a  class="btn btn-sm btn-success w-50 border" href="{{url('product_details')}}/{{ $product->id }}">ดูสินค้า</a>
                </div>
                @endif
                <p class="card-text text-line-max2" style="height:40px">{{ $product->name }} <br /></p>
              </div>
              <div class="card-footer text-center text-sm-left">
                @if (Auth::guest())
                  <a href="{{ url('login') }}"><span class="price">เข้าสู่ระบบเพื่อตรวจสอบราคา </span></a>
                  <span class="" data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="Price & Promotion" data-content="เข้าสู่ระบบเพื่อตรวจสอบราคา"></span>
                @else
                  <span class="price">฿ {{ $product->price/$product->piece }} <small>/ ชิ้น</small></span>
                  <span class="tag float-sm-right" data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="Price & Promotion" data-content="@include('inc_info-price') @include('inc_info-promotion')">PRO | <i class="fa fa-street-view"></i></span>
                @endif
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
    </div>

    <!-- Near by shop -->
    <div class="row my-5">
    <div class="col-12">
      <h3>Near by shop</h3><hr class="border-dashed">
      <div class="product-slide owl-carousel owl-theme">
        @php
        $products=\App\Models\Product::leftJoin('product_brand','product.band_id','=','product_brand.id')
        ->select('product.*','product_brand.lat','product_brand.lng')
        ->orderBy('product.created_at','DESC')->get();
        $lat= array();
        $lng= array();
        $l_id= array();
        $lat_user="";
        $lng_user="";
        if (!Auth::guest()) {
          $lat_user=Auth::user()->lat;
          $lng_user=Auth::user()->longtitude;
        }
        @endphp
        @foreach ($products as $product)
          @php
          array_push($lat,$product->lat);
          array_push($lng,$product->lng);
          array_push($l_id,$product->id);

          $img=\App\Models\ProductImg::where('product_id',$product->id)->first();
          @endphp
          <div class="item product-item product_id{{ $product->id }}" id="product_id{{ $product->id }}">
            <div class="card rounded-0 pt-3">
              <img class="card-img-top" src="{{asset('local/storage/app/images/product')}}/{{ $img->name }}" alt="">
              <div class="card-body position-relative">
                @if (!Auth::guest())
                <div class="product-btn btn-group invisible">
                  <a  class="btn btn-sm btn-success w-50 border" data-toggle="modal" href="{{url('inc_cart-add')}}/{{ $product->id }}" onclick="return clearCart()">ช้อปเลย</a>
                  <a  class="btn btn-sm btn-success w-50 border" href="{{url('product_details')}}/{{ $product->id }}">ดูสินค้า</a>
                </div>
                @endif
                <p class="card-text text-line-max2" style="height:40px">{{ $product->name }} <br /></p>
              </div>
              <div class="card-footer text-center text-sm-left">
                @if (Auth::guest())
                  <a href="{{ url('login') }}"><span class="price">เข้าสู่ระบบเพื่อตรวจสอบราคา </span></a>
                  <span class="" data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="Price & Promotion" data-content="เข้าสู่ระบบเพื่อตรวจสอบราคา"></span>
                @else
                  <span class="price">฿ {{ $product->price/$product->piece }} <small>/ ชิ้น</small></span>
                  <span class="tag float-sm-right" data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="Price & Promotion" data-content="@include('inc_info-price') @include('inc_info-promotion')">PRO | <i class="fa fa-street-view"></i></span>
                @endif
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
    </div>

    <!-- New product of month -->
    <div class="row my-5">
    <div class="col-12">
      <h3>New product of month</h3><hr class="border-dashed">
      <div class="product-slide owl-carousel owl-theme">
        @php
        $products=\App\Models\Product::orderBy('created_at','DESC')->get();
        @endphp
        @foreach ($products as $product)
          @php
          $img=\App\Models\ProductImg::where('product_id',$product->id)->first();
          @endphp
          <div class="item product-item product_id{{ $product->id }}">
            <div class="card rounded-0 pt-3">
              <img class="card-img-top" src="{{asset('local/storage/app/images/product')}}/{{ $img->name }}" alt="">
              <div class="card-body position-relative">
                @if (!Auth::guest())
                <div class="product-btn btn-group invisible">
                  <a  class="btn btn-sm btn-success w-50 border" data-toggle="modal" href="{{url('inc_cart-add')}}/{{ $product->id }}" onclick="return clearCart()">ช้อปเลย</a>
                  <a  class="btn btn-sm btn-success w-50 border" href="{{url('product_details')}}/{{ $product->id }}">ดูสินค้า</a>
                </div>
                @endif
                <p class="card-text text-line-max2" style="height:40px">{{ $product->name }} <br /></p>
              </div>
              <div class="card-footer text-center text-sm-left">
                @if (Auth::guest())
                  <a href="{{ url('login') }}"><span class="price">เข้าสู่ระบบเพื่อตรวจสอบราคา </span></a>
                  <span class="" data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="Price & Promotion" data-content="เข้าสู่ระบบเพื่อตรวจสอบราคา"></span>
                @else
                  <span class="price">฿ {{ $product->price/$product->piece }} <small>/ ชิ้น</small></span>
                  <span class="tag float-sm-right" data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="Price & Promotion" data-content="@include('inc_info-price') @include('inc_info-promotion')">PRO | <i class="fa fa-street-view"></i></span>
                @endif
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
    </div>
      <div id="snackbar">เพิ่มสินค้าลงในตะกร้าแล้ว</div>
    <!-- Recoment to you -->
    <div class="row my-5">
    <div class="col-12">
      <h3>Recoment to you</h3><hr class="border-dashed">
      <div class="product-slide owl-carousel owl-theme">
        @php
        $products=\App\Models\Recommend::leftJoin('product','hl_recommend.product_id','product.id')
        ->select('product.*')
        ->get();
        @endphp
        @foreach ($products as $product)
          @php
          $img=\App\Models\ProductImg::where('product_id',$product->id)->first();
          @endphp
          <div class="item product-item product_id{{ $product->id }}">
            <div class="card rounded-0 pt-3">
              <img class="card-img-top" src="{{asset('local/storage/app/images/product')}}/{{ $img->name }}" alt="">
              <div class="card-body position-relative">
                @if (!Auth::guest())
                <div class="product-btn btn-group invisible">
                  <a  class="btn btn-sm btn-success w-50 border" data-toggle="modal" href="{{url('inc_cart-add')}}/{{ $product->id }}" onclick="return clearCart()">ช้อปเลย</a>
                  <a  class="btn btn-sm btn-success w-50 border" href="{{url('product_details')}}/{{ $product->id }}">ดูสินค้า</a>
                </div>
                @endif
                <p class="card-text text-line-max2" style="height:40px">{{ $product->name }} <br /></p>
              </div>
              <div class="card-footer text-center text-sm-left">
                @if (Auth::guest())
                  <a href="{{ url('login') }}"><span class="price">เข้าสู่ระบบเพื่อตรวจสอบราคา </span></a>
                  <span class="" data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="Price & Promotion" data-content="เข้าสู่ระบบเพื่อตรวจสอบราคา"></span>
                @else
                  <span class="price">฿ {{ $product->price/$product->piece }} <small>/ ชิ้น</small></span>
                  <span class="tag float-sm-right" data-container="body" data-toggle="popover" data-placement="auto" data-html="true" title="Price & Promotion" data-content="@include('inc_info-price') @include('inc_info-promotion')">PRO | <i class="fa fa-street-view"></i></span>
                @endif
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
    </div>
<p id="demo"></p>
  </div>
{{-- Zone --}}
@php
$radius=array();
$time=array();
$product_data=\App\Models\OrderDetail::leftJoin('order_data','order_detail.order_id','=','order_data.id')
->leftJoin('product','order_detail.product_id','=','product.id')
->leftJoin('users','order_data.user_id','=','users.id')
->select('order_data.created_at','order_detail.product_id','order_detail.qty','users.lat','users.longtitude as lng','users.id as userId')
->where('order_data.status_confirm','1')
->get();
foreach ($product_data as $key) {
  $pro=\App\Models\Zone::where('product_id',$key->product_id)
  ->where('num','<=',$key->qty)
  ->orderBy('num','DESC')
  ->first();
  if($pro!=""){
    array_push($radius,$pro->radius);
    array_push($time,$pro->time);
  }else{
    array_push($radius,'0');
    array_push($time,'0');
  }
}

@endphp
{{-- end Zone --}}
  <!-- container -->

  <!-- Site footer -->
  @include('inc_footer')
  <!-- Site footer -->

  <!-- Site js -->
  @include('inc_js')
  <!-- Site js -->

  <!-- Site Sidewarp -->
  @include('inc_sidewarp')
  <!-- Site Sidewarp -->


  <!-- Owl Carousel -->
  <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/owl.theme.default.css')}}">
  <script src="{{asset('assets/js/owl.carousel.js')}}"></script>
  <script>
    $(document).ready(function() {
      // Near by shop
      var lat = $.parseJSON('{!! json_encode($lat) !!}');
      var lng = $.parseJSON('{!! json_encode($lng) !!}');
      var l_id = $.parseJSON('{!! json_encode($l_id) !!}');
      var lat_user = "{{ $lat_user }}";
      var lng_user = "{{ $lng_user }}";
      var radius = $.parseJSON('{!! json_encode($radius) !!}');
      var time = $.parseJSON('{!! json_encode($time) !!}');
      var product_data = $.parseJSON('{!! json_encode($product_data) !!}');
      var id_user = "{{ $userId }}"
      var rad = function(x) {
        return x * Math.PI / 180;
      };

      var getDistance = function(p1, p2) {
        var R = 6378137; // Earth’s mean radius in meter
        var dLat = rad(p2.lat - p1.lat);
        var dLong = rad(p2.lng - p1.lng);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
        Math.sin(dLong / 2) * Math.sin(dLong / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = (R * c)/1000;
        return d; // returns the distance in meter
      };

      // var d=new Date(product_data[0].created_at.split(" ")[0]);
      var now = new Date();
      // alert(product_data[0].lng);
      for (var i = 0; i < product_data.length; i++) {
        var d=new Date(product_data[i].created_at.split(" ")[0]);
        if((Math.abs(d - now) / 36e5)/24< time[i] && parseFloat(getDistance({
          "lat":parseFloat(product_data[i].lat),
          "lng":parseFloat(product_data[i].lng)
        },
        {
          "lat":parseFloat(lat_user),
          "lng":parseFloat(lng_user)
        })).toFixed(2)<radius[i] && id_user!=product_data[i].userId) {
          $('.product_id'+product_data[i].product_id).remove();
          console.log("product id control zone :"+product_data[i].product_id);
          console.log("Distance control :"+radius[i]);
          console.log("Time control :"+time[i]);
          console.log("Time :"+(Math.abs(d - now) / 36e5)/24);
          console.log("Distance :"+parseFloat(getDistance({
            "lat":parseFloat(product_data[i].lat),
            "lng":parseFloat(product_data[i].lng)
          },
          {
            "lat":parseFloat(lat_user),
            "lng":parseFloat(lng_user)
          })).toFixed(2));
          $.ajax({
            type: 'get',
            url: '{{url('ban')}}'+'/'+product_data[i].product_id,
            data: {_token: "{{ csrf_token() }}"},
            dataType:'html',
            success:function(html){
              // alert(html)
            }
          });
        }
        console.log('Distance : '+parseFloat(getDistance({
          "lat":parseFloat(product_data[i].lat),
          "lng":parseFloat(product_data[i].lng)
        },
        {
          "lat":parseFloat(lat_user),
          "lng":parseFloat(lng_user)
        })).toFixed(2));
        console.log('Product id : '+product_data[i].product_id);
        console.log('Radius control : '+radius[i]);
        console.log('time control : '+time[i]);
        console.log('time gone : '+(Math.abs(d - now) / 36e5)/24);
      }
        // alert((Math.abs(d - now) / 36e5)/24);

      // alert(parseFloat(getDistance({
      //   "lat":parseFloat(product_data[3].lat),
      //   "lng":parseFloat(product_data[3].lng)
      // },
      // {
      //   "lat":parseFloat(lat_user),
      //   "lng":parseFloat(lng_user)
      // })).toFixed(2));

      // alert(time);

      // var po1={
      //   "lat":parseFloat(lat[0]),
      //   "lng":parseFloat(lng[0])
      // };
      // var po2={
      //   "lat":parseFloat(lat_user),
      //   "lng":parseFloat(lng_user)
      // };



      for (var i = 0; i < lat.length; i++) {
        if(parseFloat(getDistance({
          "lat":parseFloat(lat[i]),
          "lng":parseFloat(lng[i])
        },
        {
          "lat":parseFloat(lat_user),
          "lng":parseFloat(lng_user)
        })).toFixed(2)>=101){
          $('#product_id'+l_id[i]).remove();
        }

        console.log('Distance : '+parseFloat(getDistance({
          "lat":parseFloat(lat[i]),
          "lng":parseFloat(lng[i])
        },
        {
          "lat":parseFloat(lat_user),
          "lng":parseFloat(lng_user)
        })).toFixed(2));
      }
      // Near by shop


      var owl = $('.product-slide');
      owl.owlCarousel({
        navText: [ '', '' ],
        loop: false,
        margin: 0,
        responsiveClass: true,
        responsive: {
          0:    { items: 2, nav: false  },
          321:  { items: 3, nav: false  },
          768:  { items: 4, nav: false  },
          992:  { items: 5, nav: false  },
          1200: { items: 6, nav: true   }
        }
      })
    });

    $(document).ready(function() {
      var owl = $('.banner');
      owl.owlCarousel({
        navText: [ '', '' ],
        items: 1,
        margin: 0,
        loop: true,
        autoplayTimeout: 6000,
        autoHeight: true,
        autoplayHoverPause: true,
        animateOut: 'fadeOut',
        responsive: {
          0:     {  autoplay: false, nav: false, dots: true  },
          768:   {  autoplay: true,  nav: true, dots: true   },
          1200:  {  autoplay: true,  nav: true,  dots: false }
        }
      });
      $('.play').on('click', function() {
        owl.trigger('play.owl.autoplay', [1000])
      })
      $('.stop').on('click', function() {
        owl.trigger('stop.owl.autoplay')
      })
    });

    function toast() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }
    // function cart
    function checkmax(num,id) {
      // alert(num);
      if (num>100) {
        // return this.val('100');
        $('#amount').val('100');
      }
      $.ajax({
        type: 'get',
        url: '{{url('checkPrice')}}'+'/'+id+'/'+num,
        data: {_token: "{{ csrf_token() }}"},
        dataType:'json',
        success:function(html){
          price = $('#amount').val();
          price = price*html[0].price;
          discount = (price*(html[0].discount/100));
          $('#price').html(price);
          $('#discount').html(parseInt(discount));
          $('#discountNum').html(html[0].discount+"%");
          $('#total').html(price-discount);
          // swal("Successfully", "ลบเสร็จสิ้น !!", "success");
          // dataTable.clear();
          // dataTable.rows.add(html).draw();
          // $('#product').val();

        }
      });
    }
    function clearCart() {
      $('#cart-modal').remove();
    }
    function addCart() {
      // num = parseInt($('#product_num').text());
      // num++;
      // $('#product_num').text(num);
      $('.modal').modal('hide');
      var formData = $('#cartForm').serialize();
      $.ajax({
        type: 'get',
        url: '{{url('addToCart')}}',
        data: formData,
        dataType:'json',
        success:function(html){
          $('#product_num').text(html.length);
          toast();
        },
        cache: false,
        contentType: false,
        processData: false
      });
      return false;
    }

  </script>

  <!-- Owl Carousel -->

@endsection
