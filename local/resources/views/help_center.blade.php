@extends('layout.main')
@section('content')
@php
  $help= \App\Models\HelpCenter::first();
@endphp
  <!-- Section Nav -->
  <div id="data_link">
    <ul class="con-scroll doc-pills nav nav-pills nav-justified font-cloud">
      <li class="nav-item w-20">
        <a class="nav-scroll h-100 nav-link active" id="doc_tab_01" href="#doc_con_01">
          <i class="fa fa-shopping-bag mr-sm-2"></i>
          <span class="d-none d-sm-inline-block">การสั่งซื้อสินค้า</span>
        </a>
      </li>
      <li class="nav-item w-20">
        <a class="nav-scroll h-100 nav-link" id="doc_tab_02" href="#doc_con_02">
          <i class="fa fa-credit-card-alt mr-sm-2"></i>
          <span class="d-none d-sm-inline-block">การชำระเงิน</span>
        </a>
      </li>
      <li class="nav-item w-20">
        <a class="nav-scroll h-100 nav-link" id="doc_tab_03" href="#doc_con_03">
          <i class="fa fa-truck mr-sm-2"></i>
          <span class="d-none d-sm-inline-block">การส่งสินค้า</span>
        </a>
      </li>
      <li class="nav-item w-20">
        <a class="nav-scroll h-100 nav-link" id="doc_tab_04" href="#doc_con_04">
          <i class="fa fa-quora mr-sm-2"></i>
          <span class="d-none d-sm-inline-block">คำถามที่พบบ่อย</span>
        </a>
      </li>
      <li class="nav-item w-20">
        <a class="nav-scroll h-100 nav-link" id="doc_tab_05" href="#doc_con_05">
          <i class="fa fa-user-secret mr-sm-2"></i>
          <span class="d-none d-sm-inline-block">นโยบายความเป็นส่วนตัว</span>
        </a>
      </li>
    </ul>
  </div>
    <!-- Section Nav -->

  <!-- container -->
  <div class="container">

    <!-- Section 01 -->
    <div class="site-content">
      <div data-target=".con-scroll" id="doc_con_01">
        <h3 class="font-cloud text-success text-center mx-3 pt-4">การสั่งซื้อสินค้า</h3>
        <hr class="border-dashed mx-3">
        <div class="row px-3">
          <div class="col-12">
            {!! $help->how_to_buy !!}
          </div>
        </div>
      </div>
    </div>

    <!-- Section 02 -->
    <div class="site-content">
      <div data-target=".con-scroll" id="doc_con_02">
        <h3 class="font-cloud text-success text-center mx-3 pt-4">การชำระเงิน</h3>
        <hr class="border-dashed mx-3">
        <div class="row px-3">
          <div class="col-12">
            {!! $help->how_to_pay !!}
            @php
            $payment=\App\Models\Payment::leftJoin('bank','payment.bank_id','=','bank.id')->get();
            @endphp
            @foreach ($payment as $key )
              <div class="col-md-3 float-left mb-4">
              <div class="card border">
                <i class="thbanks thbanks-5x {{$key->icon}} text-center w-100 pt-3" aria-hidden="true"></i>
                <div class="card-body">
                  <h5 class="card-title font-cloud text-center">{{$key->name}}</h5>
                  <p class="card-text m-0">เลขบัญชี : {{$key->account_no}}</p>
                  <p class="card-text m-0">ชื่อบัญชี : {{$key->account_name}}</p>
                  <p class="card-text m-0">ประเภทบัญชี : {{$key->type}}</p>
                  <p class="card-text m-0">สาขา : {{$key->branch}} </p>
                </div>
              </div>
              </div>
            @endforeach
            {{-- <div class="col-md-3 float-left mb-4">
            <div class="card border">
              <i class="thbanks thbanks-5x thbanks-scb text-center w-100 pt-3" aria-hidden="true"></i>
              <div class="card-body">
                <h5 class="card-title font-cloud text-center">ไทยพาณิชย์</h5>
                <p class="card-text m-0">เลขบัญชี : 408-3-28199-6</p>
                <p class="card-text m-0">ชื่อบัญชี : ออมทรัพย์</p>
                <p class="card-text m-0">สาขา : เซ็นทรัลพลาซ่า เวสต์เกต</p>
              </div>
            </div>
            </div>
            <div class="col-md-3 float-left mb-4">
            <div class="card border">
              <i class="thbanks thbanks-5x thbanks-bbl text-center w-100 pt-3" aria-hidden="true"></i>
              <div class="card-body">
                <h5 class="card-title font-cloud text-center">กรุงเทพ</h5>
                <p class="card-text m-0">เลขบัญชี : 555-5-55555-5</p>
                <p class="card-text m-0">ชื่อบัญชี : ออมทรัพย์</p>
                <p class="card-text m-0">สาขา : เซ็นทรัลพลาซ่า เวสต์เกต</p>
              </div>
            </div>
            </div>
            <div class="col-md-3 float-left mb-4">
            <div class="card border">
              <i class="thbanks thbanks-5x thbanks-tmb text-center w-100 pt-3" aria-hidden="true"></i>
              <div class="card-body">
                <h5 class="card-title font-cloud text-center">ทหารไทย</h5>
                <p class="card-text m-0">เลขบัญชี : 273-1-00195-0</p>
                <p class="card-text m-0">ชื่อบัญชี : กระแสรายวัน</p>
                <p class="card-text m-0">สาขา : เซ็นทรัลพลาซ่า เวสต์เกต</p>
              </div>
            </div>
            </div>
            <div class="col-md-3 float-left mb-4">
            <div class="card border">
              <i class="thbanks thbanks-5x thbanks-ktb text-center w-100 pt-3" aria-hidden="true"></i>
              <div class="card-body">
                <h5 class="card-title font-cloud text-center">กรุงไทย</h5>
                <p class="card-text m-0">เลขบัญชี : 555-5-55555-5</p>
                <p class="card-text m-0">ชื่อบัญชี : ออมทรัพย์</p>
                <p class="card-text m-0">สาขา : เซ็นทรัลพลาซ่า เวสต์เกต</p>
              </div>
            </div>
            </div>
            <div class="col-md-3 float-left mb-4">
            <div class="card border">
              <i class="thbanks thbanks-5x thbanks-kbank text-center w-100 pt-3" aria-hidden="true"></i>
              <div class="card-body">
                <h5 class="card-title font-cloud text-center">กสิกรไทย</h5>
                <p class="card-text m-0">เลขบัญชี : 031-1-41178-0</p>
                <p class="card-text m-0">ชื่อบัญชี : ออมทรัพย์</p>
                <p class="card-text m-0">สาขา : เซ็นทรัลพลาซ่า เวสต์เกต</p>
              </div>
            </div>
            </div>
            <div class="col-md-3 float-left mb-4">
            <div class="card border">
              <i class="thbanks thbanks-5x thbanks-baac text-center w-100 pt-3" aria-hidden="true"></i>
              <div class="card-body">
                <h5 class="card-title font-cloud text-center">ธกส.</h5>
                <p class="card-text m-0">เลขบัญชี : 555-5-55555-5</p>
                <p class="card-text m-0">ชื่อบัญชี : ออมทรัพย์</p>
                <p class="card-text m-0">สาขา : เซ็นทรัลพลาซ่า เวสต์เกต</p>
              </div>
            </div>
            </div>
            <div class="col-md-3 float-left mb-4">
            <div class="card border">
              <i class="thbanks thbanks-5x thbanks-bay text-center w-100 pt-3" aria-hidden="true"></i>
              <div class="card-body">
                <h5 class="card-title font-cloud text-center">กรุงศรีอยุธยา</h5>
                <p class="card-text m-0">เลขบัญชี : 778-1-09637-4</p>
                <p class="card-text m-0">ชื่อบัญชี : ออมทรัพย์</p>
                <p class="card-text m-0">สาขา : เซ็นทรัลพลาซ่า เวสต์เกต</p>
              </div>
            </div>
            </div>
            <div class="col-md-3 float-left mb-4">
            <div class="card border">
              <i class="thbanks thbanks-5x thbanks-gsb text-center w-100 pt-3" aria-hidden="true"></i>
              <div class="card-body">
                <h5 class="card-title font-cloud text-center">ออมสิน</h5>
                <p class="card-text m-0">เลขบัญชี : 555-5-55555-5</p>
                <p class="card-text m-0">ชื่อบัญชี : ออมทรัพย์</p>
                <p class="card-text m-0">สาขา : เซ็นทรัลพลาซ่า เวสต์เกต</p>
              </div>
            </div>
            </div> --}}

          </div>
        </div>
      </div>
    </div>

    <!-- Section 03 -->
    <div class="site-content mt-0 pt-0">
      <div data-target=".con-scroll" id="doc_con_03">
        <h3 class="font-cloud text-success text-center mx-3 pt-4">การส่งสินค้า</h3>
        <hr class="border-dashed mx-3">
        <div class="row px-3">
          <div class="col-12">
            {!! $help->how_to_send !!}
          </div>
        </div>
      </div>
    </div>

    <!-- Section 04 -->
    @php
      $ans=json_decode( $help->ans );
      $question=json_decode( $help->question );
    @endphp
    <div class="site-content mt-0 pt-0">
      <div data-target=".con-scroll" id="doc_con_04">
        <h3 class="font-cloud text-success text-center mx-3 pt-4">คำถามที่พบบ่อย</h3>
        <hr class="border-dashed mx-3">
        <div class="row px-3">
          <div class="col-12">
            @for ($i=0; $i < count($ans); $i++)
              <p>
                <span class="badge badge-pill badge-success mr-2">Q{{ $i }}</span>{{ $question[$i] }}<br>
                <span class="badge badge-pill badge-primary mr-2">A{{ $i }}</span>{{ $ans[$i] }}
              </p>
            @endfor
            {{-- <p>
              <span class="badge badge-pill badge-success mr-2">Q1</span>Lorem ipsum dolor sit amet, consectetur adipiscing elit?<br>
              <span class="badge badge-pill badge-primary mr-2">A1</span>Phasellus ut ligula id nisl vehicula sollicitudin.
            </p>
            <p>
              <span class="badge badge-pill badge-success mr-2">Q2</span>Cras at enim imperdiet, condimentum leo sed, scelerisque mauris?<br>
              <span class="badge badge-pill badge-primary mr-2">A2</span>Ut at erat non lectus ultricies facilisis.
            </p>
            <p>
              <span class="badge badge-pill badge-success mr-2">Q3</span>Vestibulum sit amet tellus nec mauris ultrices dignissim vel eget orci?<br>
              <span class="badge badge-pill badge-primary mr-2">A3</span>In eget tortor ultricies, sodales ligula eu, tincidunt urna.
            </p>
            <p>
              <span class="badge badge-pill badge-success mr-2">Q4</span>Praesent scelerisque elit sed ligula vestibulum, eu tristique mauris ornare?<br>
              <span class="badge badge-pill badge-primary mr-2">A4</span>Donec tristique nibh ut mi euismod vulputate.
            </p>
            <p>
              <span class="badge badge-pill badge-success mr-2">Q5</span>Etiam tincidunt sapien sit amet nisi ultrices fringilla ac ut nunc?<br>
              <span class="badge badge-pill badge-primary mr-2">A5</span>Quisque a sapien sit amet arcu imperdiet laoreet.
            </p>
            <p>
              <span class="badge badge-pill badge-success mr-2">Q6</span>Nullam nec lectus sit amet eros finibus scelerisque?<br>
              <span class="badge badge-pill badge-primary mr-2">A6</span>Nulla vel ante in ex laoreet pharetra.
            </p> --}}
          </div>
        </div>
      </div>
    </div>

    <!-- Section 05 -->
    <div class="site-content mt-0 pt-0">
      <div data-target=".con-scroll" id="doc_con_05">
        <h3 class="font-cloud text-success text-center mx-3 pt-4">นโยบายความเป็นส่วนตัว</h3>
        <hr class="border-dashed mx-3">
        <div class="row px-3">
          <div class="col-12">
            {!!$help->policy!!}
          </div>
        </div>
      </div>
    </div>

  </div>


  <!-- Site footer -->
  @include('inc_footer')
  <!-- Site footer -->

  <!-- Site js -->
  @include('inc_js')
  <!-- Site js -->

  <!-- Site Sidewarp -->
  @include('inc_sidewarp')
  <!-- Site Sidewarp -->
@endsection
