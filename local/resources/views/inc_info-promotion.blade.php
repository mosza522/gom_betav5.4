{{-- <small>
	<hr><strong class='text-success'>Pro1 &bull;&nbsp;</strong>10 ลัง -5% เหลือ 475฿/ชิ้น&nbsp;<strong>รวม 57,000฿</strong><br>กันเขต 50km/30วัน
	<hr class='my-1 border-dashed'><strong class='text-success'>Pro2 &bull;&nbsp;</strong>20 ลัง -10% เหลือ 450฿/ชิ้น&nbsp;<strong>รวม 108,000฿</strong><br>กันเขต 100km/60วัน
	<hr class='my-1 border-dashed'><strong class='text-success'>Pro3 &bull;&nbsp;</strong>30 ลัง -15% เหลือ 425฿/ชิ้น&nbsp;<strong>รวม 153,000฿</strong><br>กันเขต 150km/90วัน
</small> --}}
<small>
@php
	$promotions=\App\Models\ProductPromotion::where('product_id',$product->id)->get();
	$zones=\App\Models\Zone::where('product_id',$product->id)->get();
	$i=1;
	$text="";
@endphp
@foreach ($promotions as $promotion)
@if ($i==1)
	<hr><strong class='text-success'>Pro{{$i}} &bull;&nbsp;</strong>{{ $promotion->amount }} ลัง -{{ $promotion->discount }}%
	เหลือ ฿{{ number_format((($product->price/$product->piece)-($product->price/$product->piece)*($promotion->discount/100)), 2, '.', ',') }}/ชิ้น&nbsp;<strong>
		รวม {{ number_format((($product->price-($product->price*($promotion->discount/100)))*$promotion->amount), 2, '.', ',') }}฿</strong><br>
		@foreach ($zones as $zone)
			@if ($zone->num <= $promotion->amount  )
				@php
					$text="กันเขต $zone->radius km/$zone->time วัน";
				@endphp
			@endif
		@endforeach
		{{ $text }}

		@php
			$i++;
		@endphp
@else
	<hr class='my-1 border-dashed'><strong class='text-success'>Pro{{$i}} &bull;&nbsp;</strong>{{ $promotion->amount }} ลัง -{{ $promotion->discount }}%
	เหลือ ฿{{ number_format((($product->price/$product->piece)-($product->price/$product->piece)*($promotion->discount/100)), 2, '.', ',') }}/ชิ้น&nbsp;<strong>
		รวม {{ number_format((($product->price-($product->price*($promotion->discount/100)))*$promotion->amount), 2, '.', ',') }}฿</strong><br>
		@foreach ($zones as $zone)
			@if ($zone->num <= $promotion->amount  )
				@php
					$text="กันเขต $zone->radius km/$zone->time วัน";
				@endphp
			@endif
		@endforeach
		{{ $text }}

		@php
			$i++;
		@endphp
	@endif
@endforeach

	{{-- <hr><strong class='text-success'>Pro1 &bull;&nbsp;</strong>10 ลัง -5% เหลือ 475฿/ชิ้น&nbsp;<strong>รวม 57,000฿</strong><br>กันเขต 50km/30วัน --}}


	{{-- <hr class='my-1 border-dashed'><strong class='text-success'>Pro2 &bull;&nbsp;</strong>20 ลัง -10% เหลือ 450฿/ชิ้น&nbsp;<strong>รวม 108,000฿</strong><br>กันเขต 100km/60วัน --}}
	{{-- <hr class='my-1 border-dashed'><strong class='text-success'>Pro3 &bull;&nbsp;</strong>30 ลัง -15% เหลือ 425฿/ชิ้น&nbsp;<strong>รวม 153,000฿</strong><br>กันเขต 150km/90วัน --}}
</small>
