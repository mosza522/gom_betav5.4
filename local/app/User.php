<?php

namespace App;

use App\Notifications\MyOwnResetPassword as ResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;
	protected $table = 'users';

  public function sendPasswordResetNotification($token)
      {
          $this->notify(new ResetPasswordNotification($token));
      }
}
