<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandImg extends Model
{
    protected $table		= 'brand_img';
    public $timestamps 	= false;
}
