<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderData extends Model
{
  protected $table		= 'order_data';
  public $timestamps 	= true;

  public function sell($month=null,$year=null)
  {
    $return = array();
    $detail= array();
    $totalprice= array();

    if($year=="All Year"){
      $datas= \App\Models\OrderData::leftJoin('users','order_data.user_id','users.id')
      ->select('order_data.*','users.name as name')
      ->where('order_data.status_confirm','1')
      ->whereMonth('order_data.created_at',$month)
      ->orderBy('status_confirm','ASC')
      ->get();
    }
    if($month=="All Month"){
      $datas= \App\Models\OrderData::leftJoin('users','order_data.user_id','users.id')
      ->select('order_data.*','users.name as name')
      ->where('order_data.status_confirm','1')
      ->whereYear('order_data.created_at',$year)
      ->orderBy('status_confirm','ASC')
      ->get();
    }
    if(($month==null and $year==null) or ($month=="All Month" and $year=="All Year")){
      $datas= \App\Models\OrderData::leftJoin('users','order_data.user_id','users.id')
      ->select('order_data.*','users.name as name')
      ->where('order_data.status_confirm','1')
      ->orderBy('status_confirm','ASC')
      ->get();
    }
    else if($month!='All Month' and $year!='All Year'){
      $datas= \App\Models\OrderData::leftJoin('users','order_data.user_id','users.id')
      ->select('order_data.*','users.name as name')
      ->where('order_data.status_confirm','1')
      ->whereMonth('order_data.created_at',$month)
      ->whereYear('order_data.created_at',$year)
      ->orderBy('status_confirm','ASC')
      ->get();
    }
    foreach ($datas as $key ) {
      $text="";
      $text.='<table class="table table-bordered">';
        $text.='<thead class="table-light">';
          $text.='<tr class="font-cloud">';
            $text.='<th class="position-sticky">#</th>';
            $text.='<th class="position-sticky">สินค้า</th>';
            $text.='<th class="position-sticky">หมวด</th>';
            $text.='<th class="position-sticky">จำนวน</th>';
            $text.='<th class="position-sticky">ราคา/ชิ้น</th>';
            $text.='<th class="position-sticky">ส่วนลด/ชิ้น</th>';
            $text.='<th class="position-sticky">ราคารวม</th>';
          $text.='</tr>';
        $text.='</thead>';
        $text.='<tbody>';
          $num=1;
            $order_details=\App\Models\orderDetail::leftJoin('product','order_detail.product_id','=','product.id')
            ->leftJoin('product_category_sub','product.category_sub_id','=','product_category_sub.id')
            ->leftJoin('product_category','product_category_sub.category_id','=','product_category.id')
            ->select('order_detail.*','product_category.name as name_category','product.*')
            ->where('order_id',$key->id)->get();
            // dd($order_details);
            $total=0;
          foreach ($order_details as $order_detail ) {

            $discount=0;

            $promotion=\App\Models\ProductPromotion::where('product_id',$order_detail->product_id)->orderBy('amount','DESC')->get();
            foreach ($promotion as $pro ) {
              if ($order_detail->qty>=$pro->amount) {
                $discount=$pro->discount;
                break;
              }
            }
            $discountPerPiece=($order_detail->price/$order_detail->piece)-(($order_detail->price/$order_detail->piece)*((100-$discount)/100));


            $text.='<tr>';
            $text.='<th>'.$num++.'</th>';
            $text.='<td>'.$order_detail->name.'<br><small class="text-muted"></small></td>';
            $text.='<td>'.$order_detail->name_category .'</td>';
            $text.='<td class="text-right">'.$order_detail->qty .' ลัง <small>('. $order_detail->piece .'ชิ้น)</small></td>';
            $text.='<td class="text-right">'.number_format($order_detail->price/$order_detail->piece).'</td>';
            $text.='<td class="text-right"><small>('. $discount .'%)</small> '.number_format($discountPerPiece) .'</td>';
            $text.='<td class="text-right">'. number_format((($order_detail->price/$order_detail->piece)-$discountPerPiece)*$order_detail->qty*$order_detail->piece) .' </td>';
          $text.='</tr>';

            $total+=(($order_detail->price/$order_detail->piece)-$discountPerPiece)*$order_detail->qty*$order_detail->piece;

        }
          $text.='<tr>';
            $text.='<td colspan="6" class="text-right"><b>ราคารวม</b></td>';
            $text.='<td class="text-right">'. number_format($total) .' บาท</td>';
          $text.='</tr>';
        $text.='</tbody>';
      $text.='</table>';
      array_push($detail,$text);
      array_push($totalprice,$total);
    }
    array_push($return,array("data"=>$datas,"detail"=>$detail,'total'=>$totalprice));

    return $return;
  }
}
