<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandBanner extends Model
{
    protected $table		= 'brand_banner';
    public $timestamps 	= false;
}
