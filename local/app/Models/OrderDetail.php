<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
  protected $table		= 'order_detail';
  public $timestamps 	= false;

    public function bestSell($month=null,$year=null)
    {
      if($year=="All Year"){
        $datas= \App\Models\OrderDetail::leftJoin('product','order_detail.product_id','=','product.id')
        ->leftJoin('product_category_sub','product.category_sub_id','=','product_category_sub.id')
        ->leftJoin('product_category','product_category_sub.category_id','=','product_category.id')
        ->leftJoin('product_brand','product.band_id','=','product_brand.id')
        ->leftJoin('order_data','order_detail.order_id','=','order_data.id')
        ->leftJoin('users','product.created_by','=','users.id')
        ->whereMonth('order_data.created_at',$month)
        ->groupBy('product_id')
        ->select('product.*','product_category.name as category','product_category_sub.name as subCategory','product_brand.name as brand'
        ,'users.name as username'
        )
        ->selectRaw('sum(qty) as sum')
        ->selectRaw('(product.price - product.cost)*sum(qty) as profit ')
        ->orderBy('sum','DESC')
        ->get();
      }
      if($month=="All Month"){
        $datas= \App\Models\OrderDetail::leftJoin('product','order_detail.product_id','=','product.id')
        ->leftJoin('product_category_sub','product.category_sub_id','=','product_category_sub.id')
        ->leftJoin('product_category','product_category_sub.category_id','=','product_category.id')
        ->leftJoin('product_brand','product.band_id','=','product_brand.id')
        ->leftJoin('order_data','order_detail.order_id','=','order_data.id')
        ->leftJoin('users','product.created_by','=','users.id')
        ->whereYear('order_data.created_at',$year)
        ->groupBy('product_id')
        ->select('product.*','product_category.name as category','product_category_sub.name as subCategory','product_brand.name as brand'
        ,'users.name as username'
        )->selectRaw('sum(qty) as sum')
        ->selectRaw('(product.price - product.cost)*sum(qty) as profit ')
        ->orderBy('sum','DESC')
        ->get();
      }
      if(($month==null and $year==null) or ($month=="All Month" and $year=="All Year")){
        $datas= \App\Models\OrderDetail::leftJoin('product','order_detail.product_id','=','product.id')
        ->leftJoin('product_category_sub','product.category_sub_id','=','product_category_sub.id')
        ->leftJoin('product_category','product_category_sub.category_id','=','product_category.id')
        ->leftJoin('product_brand','product.band_id','=','product_brand.id')
        ->leftJoin('order_data','order_detail.order_id','=','order_data.id')
        ->leftJoin('users','product.created_by','=','users.id')
        ->groupBy('product_id')
        ->select('product.*','product_category.name as category','product_category_sub.name as subCategory','product_brand.name as brand'
        ,'users.name as username'
        )->selectRaw('sum(qty) as sum')
        ->selectRaw('(product.price - product.cost)*sum(qty) as profit ')
        ->orderBy('sum','DESC')
        ->get();
      }
      else if($month!='All Month' and $year!='All Year'){
        $datas= \App\Models\OrderDetail::leftJoin('product','order_detail.product_id','=','product.id')
        ->leftJoin('product_category_sub','product.category_sub_id','=','product_category_sub.id')
        ->leftJoin('product_category','product_category_sub.category_id','=','product_category.id')
        ->leftJoin('product_brand','product.band_id','=','product_brand.id')
        ->leftJoin('order_data','order_detail.order_id','=','order_data.id')
        ->leftJoin('users','product.created_by','=','users.id')
        ->whereMonth('order_data.created_at',$month)
        ->whereYear('order_data.created_at',$year)
        ->groupBy('product_id')
        ->select('product.name','product_category.name as category','product_category_sub.name as subCategory','product_brand.name as brand'
        ,'users.name as username'
        )->selectRaw('sum(qty) as sum')
        ->selectRaw('(product.price - product.cost)*sum(qty) as profit ')
        ->orderBy('sum','DESC')
        ->get();
      }

      return $datas;
    }
}
