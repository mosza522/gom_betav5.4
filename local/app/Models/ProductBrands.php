<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductBrands extends Model
{
    protected $table		= 'product_brand';
    public $timestamps 	= true;
}
