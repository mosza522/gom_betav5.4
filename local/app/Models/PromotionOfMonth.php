<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromotionOfmonth extends Model
{
  protected $table	= 'hl_promotionofmonth';
  public $timestamps 	= false;
}
