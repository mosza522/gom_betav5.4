<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPromotion extends Model
{
  protected $table		= 'product_promotion';
  public $timestamps 	= false;
}
