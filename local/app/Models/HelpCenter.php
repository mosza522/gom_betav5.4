<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HelpCenter extends Model
{
  protected $table		= 'helpcenter';
  public $timestamps 	= false;
}
