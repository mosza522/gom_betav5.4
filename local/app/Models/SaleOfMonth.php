<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaleOfMonth extends Model
{
  protected $table	= 'hl_saleofmonth';
  public $timestamps 	= false;
}
