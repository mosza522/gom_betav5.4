<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Payment extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $payment = new \App\Models\Payment;
        $payment->bank_id=$r->bank_id;
        $payment->account_no=$r->account_no;
        $payment->account_name=$r->account_no;
        $payment->type=$r->type;
        $payment->branch=$r->branch;
        $payment->save();
        $datas= \App\Models\Payment::leftJoin('bank','payment.bank_id','=','bank.id')
        ->select('payment.*','bank.name','bank.icon')
        ->get();
        $i=1;
        $json= array();
        $json2= array();
        $json3= array();
        foreach ($datas as $key) {
          array_push($json, array(''.$i++,$key->name,'<i class="opacity-8 thbanks thbanks-1x '.$key->icon.'" aria-hidden="true"></i>',$key->account_name,$key->account_no,$key->type,$key->branch,
          '<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return show('.$key->id.')" class="btn btn-warning">Edit</a>
          <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
        }
        $bank= \App\Models\Bank::whereNotIn('bank.id',function($q){
          $q->select('bank_id')->from('payment');
        })->get();
        array_push($json3, array('table'=>$json,'select'=>$bank));
        return response()->json($json3);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $payment=\App\Models\Payment::find($id);
      $bank= \App\Models\Bank::whereNotIn('bank.id',function($q) use ($payment){
        $q->select('bank_id')->from('payment')->where('bank_id','!=',$payment->bank_id);
      })
      ->get();
      $json= array() ;
      array_push($json, array('data'=>$payment,'select'=>$bank));
      return response()->json($json);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
      $payment=\App\Models\Payment::find($id);
      $payment->bank_id=$r->bank_id;
      $payment->account_no=$r->account_no;
      $payment->account_name=$r->account_no;
      $payment->type=$r->type;
      $payment->branch=$r->branch;
      $payment->save();

      $datas= \App\Models\Payment::leftJoin('bank','payment.bank_id','=','bank.id')
      ->select('payment.*','bank.name','bank.icon')
      ->get();
      $i=1;
      $json= array();
      $json2= array();
      $json3= array();
      foreach ($datas as $key) {
        array_push($json, array(''.$i++,$key->name,'<i class="opacity-8 thbanks thbanks-1x '.$key->icon.'" aria-hidden="true"></i>',$key->account_name,$key->account_no,$key->type,$key->branch,
        '<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return show('.$key->id.')" class="btn btn-warning">Edit</a>
        <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
      }
      $bank= \App\Models\Bank::whereNotIn('bank.id',function($q){
        $q->select('bank_id')->from('payment');
      })->get();
      array_push($json3, array('table'=>$json,'select'=>$bank));
      return response()->json($json3);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $payment = \App\Models\Payment::find($id);
      $payment->delete();
      $datas= \App\Models\Payment::leftJoin('bank','payment.bank_id','=','bank.id')
      ->select('payment.*','bank.name','bank.icon')
      ->get();
      $i=1;
      $json= array();
      $json2= array();
      $json3= array();
      foreach ($datas as $key) {
        array_push($json, array(''.$i++,$key->name,'<i class="opacity-8 thbanks thbanks-1x '.$key->icon.'" aria-hidden="true"></i>',$key->account_name,$key->account_no,$key->type,$key->branch,
        '<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return show('.$key->id.')" class="btn btn-warning">Edit</a>
        <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
      }
      $bank= \App\Models\Bank::whereNotIn('bank.id',function($q){
        $q->select('bank_id')->from('payment');
      })->get();
      array_push($json3, array('table'=>$json,'select'=>$bank));
      return response()->json($json3);
    }
}
