<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Storage;
use Session;
class Contact extends Controller
{
  public function store(request $r)
  {
    $contact=\App\Models\Contact::get()->count();
    if($contact>0){
      $contact= \App\Models\Contact::first();
      $contact->lat=$r->lat;
      $contact->lng=$r->lng;
      $contact->line=$r->line;
      $contact->facebook=$r->facebook;
      $contact->youtube=$r->youtube;
      $contact->address=$r->address;
      $contact->phone=$r->phone;
      $contact->fax=$r->fax;
      $contact->mobile=$r->mobile;
      $contact->email=$r->email;
      $contact->save();
    }else{
      $contact=new \App\Models\Contact;
      $contact->lat=$r->lat;
      $contact->lng=$r->lng;
      $contact->line=$r->line;
      $contact->facebook=$r->facebook;
      $contact->youtube=$r->youtube;
      $contact->address=$r->address;
      $contact->phone=$r->phone;
      $contact->fax=$r->fax;
      $contact->mobile=$r->mobile;
      $contact->email=$r->email;
      $contact->save();
    }
  }
  public function clear()
  {
    $contact = \App\Models\Contact::first();
    if($contact->delete()){
      echo "finished";
    }
  }
  public function contactAdmin(request $r)
  {
    $cu = new \App\Models\ContactUs;
    $cu->name=$r->name;
    $cu->mail=$r->mail;
    $cu->title=$r->title;
    $cu->detail=$r->detail;
    if(!Auth::guest()){
      $cu->created_by=Auth::user()->id;
    }
    // $cu->save();
    if($cu->save()){
      Session::flash('success','ส่งข้อมูลเสร็จสิ้น');
      return redirect()->back();
    }
  }
  public function delContactus($id)
  {
  $json= array();
  $cu = \App\Models\ContactUs::find($id);
  $cu->delete();
  $cu= \App\Models\ContactUs::get();
  $i=1;
  foreach ($cu as $key ) {
    array_push($json,array($i++,$key->name,$key->mail,$key->title,$key->detail
    ,"<a href='javascript:void(0);' onclick='return sweetalert($key->id)' class='btn btn-danger'>Delete</a>"));
  }
  return response()->json($json);
  }
}
