<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Storage;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
      if($r->type=="category"){
        $img = $r->file('img');
        $name_img= time().'-'.$img->getClientOriginalName();
        $cat= new \App\Models\ProductCategory;
        $cat->name = $r->name_category;
        $cat->img = $name_img;
        $cat->note = $r->note;
        $cat->created_by = Auth::user()->id;
        if($cat->save()){
          $img->storeAs('/images/img-category',$name_img);
          $cat = \App\Models\ProductCategory::get();
          $i=1;
          $json= array();
          foreach ($cat as $key) {
            array_push($json, array(''.$i++,$key->name,
            '<div class="tz-gallery">
              <div class="row">
                <div class="col-sm-12 col-md-12">
                    <a class="lightbox" href="'.asset('local/storage/app/images/img-category/').'/'.$key->img.'">
                        <img src="'.asset('local/storage/app/images/img-category/').'/'.$key->img.'"  width="250px" height="250px" alt="">
                    </a>
                </div>
              </div>
            </div>',$key->note,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return show('.$key->id.')" class="btn btn-warning">Edit</a>
            <a href=\'javascript:void(0);\' onclick=\'return sweetalert('.$key->id.')\' class=\'btn btn-danger\'>Delete</a>'));
          }

          echo json_encode($json);
        }
        else echo "Failed";
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $cat = \App\Models\ProductCategory::find($id);
      echo json_encode($cat);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id=null)
    {
      $cat= \App\Models\ProductCategory::find($r->id);
      $cat->name = $r->name_category;
      $cat->note = $r->note;
      $cat->updated_by=Auth::user()->id;
      if($r->file('img')){
        Storage::delete('images/img-category/'.$cat->img);
        $img = $r->file('img');
        $name_img= time().'-'.$img->getClientOriginalName();
        $img->storeAs('images/img-category/',$name_img);
        $cat->img = $name_img;
      }
      if($cat->save()){
        $cat = \App\Models\ProductCategory::get();
        $i=1;
        $json= array();
        foreach ($cat as $key) {
          array_push($json, array(''.$i++,$key->name,'<div class="tz-gallery">
            <div class="row">
              <div class="col-sm-12 col-md-12">
                  <a class="lightbox" href="'.asset('local/storage/app/images/img-category/').'/'.$key->img.'">
                      <img src="'.asset('local/storage/app/images/img-category/').'/'.$key->img.'"  width="250px" height="250px" alt="">
                  </a>
              </div>
            </div>
          </div>',$key->note,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return show('.$key->id.')" class="btn btn-warning">Edit</a>
          <a href=\'javascript:void(0);\' onclick=\'return sweetalert('.$key->id.')\' class=\'btn btn-danger\'>Delete</a>'));
        }

        echo json_encode($json);
      }else echo "Failed";

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat = \App\Models\ProductCategory::find($id);
        Storage::delete('images/img-category/'.$cat->img);
        if($cat->delete()){
          $cat = \App\Models\ProductCategory::get();
          $i=1;
          $json= array();
          foreach ($cat as $key) {
            array_push($json, array(''.$i++,$key->name,'<div class="tz-gallery">
              <div class="row">
                <div class="col-sm-12 col-md-12">
                    <a class="lightbox" href="'.asset('local/storage/app/images/img-category/').'/'.$key->img.'">
                        <img src="'.asset('local/storage/app/images/img-category/').'/'.$key->img.'"  width="250px" height="250px" alt="">
                    </a>
                </div>
              </div>
            </div>',$key->note,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return show('.$key->id.')" class="btn btn-warning">Edit</a>
            <a href=\'javascript:void(0);\' onclick=\'return sweetalert('.$key->id.')\' class=\'btn btn-danger\'>Delete</a>'));
          }

          echo json_encode($json);
        }
          else echo "Failed";
    }
    public function img($id)
    {
      $cat = \App\Models\ProductCategory::find($id);
      echo $cat->img;
    }

}
