<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Storage;
class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
      $logo = $r->file('logo');
      $name_logo= time().'-'.$logo->getClientOriginalName();

      $band= new \App\Models\ProductBrands;
      $band->name = $r->name;
      $band->logo = $name_logo;
      $band->history = $r->history;
      $band->vision = $r->vision;
      $band->video = $r->video;
      $band->phone = $r->phone;
      $band->fax = $r->fax;
      $band->mobile = $r->mobile;
      $band->email = $r->email;
      $band->address = $r->address;
      $band->website = $r->website;
      $band->facebook = $r->facebook;
      $band->line = $r->line;
      $band->lat = $r->lat;
      $band->lng = $r->lng;
      $band->created_by = Auth::user()->id;
      $logo->storeAs('/images/logo',$name_logo);
      $band->save();

      foreach ($r->file('banner') as $key ) {
        $brand=\App\Models\ProductBrands::orderBy('id','desc')->first();
        $name_banner= time().'-'.$key->getClientOriginalName();
        $banner= new \App\Models\BrandBanner;
        $banner->brand_id = $brand->id;
        $banner->banner = $name_banner;
        if($banner->save()){
          $key->storeAs('/images/banner',$name_banner);
        }

      }
      if($r->gallary!=null){
        for ($i=0; $i < count($r->file('gallary')) ; $i++) {
        $brand=\App\Models\ProductBrands::orderBy('id','desc')->first();
        $gallary = $r->file('gallary')[$i];
        $name_gallary= time().'-'.$gallary->getClientOriginalName();

        $Brandimg= new \App\Models\BrandImg;
        $Brandimg->brand_id = $brand->id;
        $Brandimg->img = $name_gallary;
        $Brandimg->caption = $r->caption[$i];

        if($Brandimg->save()){
          $gallary->storeAs('/images/img',$name_gallary);
        }
        }
      }

        $Brand = \App\Models\ProductBrands::get();
        $i=1;
        $json= array();
        if (Auth::user()->member_type=="Admin"){
          foreach ($Brand as $key) {
            array_push($json, array(''.$i++,$key->name,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalShow" onclick="return show('.$key->id.')" class="btn btn-primary">Details</a>'
            ,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return edit('.$key->id.')" class="btn btn-warning">Edit</a>
            <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
          }
        }else{
          foreach ($Brand as $key) {
            array_push($json, array(''.$i++,$key->name,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalShow" onclick="return show('.$key->id.')" class="btn btn-primary">Details</a>'
            ,''));
          }

        }
      echo json_encode($json);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Brand=\App\Models\ProductBrands::find($id);
        $BrandBanner=\App\Models\BrandBanner::where('brand_id',$id)->get();
        $Brandimg=\App\Models\BrandImg::where('brand_id',$id)->get();
        $i=1;
        $json= array();
        foreach ($Brand as $key) {
          array_push($json, array('brand'=>$Brand,'banner'=>$BrandBanner,'img'=>$Brandimg));
        }
        echo json_encode($json);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
      $band= \App\Models\ProductBrands::find($id);
      if($r->file('logo')){
        Storage::delete('images/logo/'.$band->logo);
        $logo = $r->file('logo');
        $name_logo= time().'-'.$logo->getClientOriginalName();
        $logo->storeAs('/images/logo',$name_logo);
        $band->logo = $name_logo;
      }
      $band->name = $r->name;
      $band->history = $r->history;
      $band->vision = $r->vision;
      $band->video = $r->video;
      $band->phone = $r->phone;
      $band->fax = $r->fax;
      $band->mobile = $r->mobile;
      $band->email = $r->email;
      $band->address = $r->address;
      $band->website = $r->website;
      $band->facebook = $r->facebook;
      $band->line = $r->line;
      $band->lat = $r->lat;
      $band->lng = $r->lng;
      $band->updated_by=Auth::user()->id;
      $band->save();
      if($r->file('banner')){
        foreach ($r->file('banner') as $key ) {
          $name_banner= time().'-'.$key->getClientOriginalName();
          $banner= new \App\Models\BrandBanner;
          $banner->brand_id = $id;
          $banner->banner = $name_banner;
          if($banner->save()){
            $key->storeAs('/images/banner',$name_banner);
          }

        }
      }
      if($r->file('gallary')){
        for ($i=0; $i < count($r->file('gallary')) ; $i++) {
          $gallary = $r->file('gallary')[$i];
          $name_gallary= time().'-'.$gallary->getClientOriginalName();
          $Brandimg= new \App\Models\BrandImg;
          $Brandimg->brand_id = $id;
          $Brandimg->img = $name_gallary;
          $Brandimg->caption = $r->caption[$i];

          if($Brandimg->save()){
            $gallary->storeAs('/images/img',$name_gallary);
          }
        }
      }
      $Brand = \App\Models\ProductBrands::get();
      $i=1;
      $json= array();
      if (Auth::user()->member_type=="Admin"){
        foreach ($Brand as $key) {
          array_push($json, array(''.$i++,$key->name,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalShow" onclick="return show('.$key->id.')" class="btn btn-primary">Details</a>'
          ,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return edit('.$key->id.')" class="btn btn-warning">Edit</a>
          <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
        }
      }else{
        foreach ($Brand as $key) {
          array_push($json, array(''.$i++,$key->name,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalShow" onclick="return show('.$key->id.')" class="btn btn-primary">Details</a>'
          ,''));
        }
      }
      echo json_encode($json);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $banner=\App\Models\BrandBanner::where('brand_id',$id)->get();
      $img=\App\Models\BrandImg::where('brand_id',$id)->get();
      foreach ($banner as $key) {
        Storage::delete('images/banner/'.$key->banner);
        $key->delete();
      }
      foreach ($img as $key) {
        Storage::delete('images/img/'.$key->img);
        $key->delete();
      }
      $brand=\App\Models\ProductBrands::find($id);
      Storage::delete('images/logo/'.$brand->logo);
      $brand->delete();

      $Brand = \App\Models\ProductBrands::get();
      $i=1;
      $json= array();
      if (Auth::user()->member_type=="Admin"){
        foreach ($Brand as $key) {
          array_push($json, array(''.$i++,$key->name,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalShow" onclick="return show('.$key->id.')" class="btn btn-primary">Details</a>'
          ,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return edit('.$key->id.')" class="btn btn-warning">Edit</a>
          <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
        }
      }else{
        foreach ($Brand as $key) {
          array_push($json, array(''.$i++,$key->name,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalShow" onclick="return show('.$key->id.')" class="btn btn-primary">Details</a>'
          ,''));
        }
      }
      echo json_encode($json);

    }
    public function uploads(Request $r)
    {
      // Allowed extentions.
      $allowedExts = array("gif", "jpeg", "jpg", "png");

      // Get filename.
      $temp = explode(".", $_FILES["file"]["name"]);

      // Get extension.
      $extension = end($temp);

      // An image check is being done in the editor but it is best to
      // check that again on the server side.
      // Do not use $_FILES["file"]["type"] as it can be easily forged.
      $finfo = finfo_open(FILEINFO_MIME_TYPE);
      $mime = finfo_file($finfo, $_FILES["file"]["tmp_name"]);

      if ((($mime == "image/gif")
      || ($mime == "image/jpeg")
      || ($mime == "image/pjpeg")
      || ($mime == "image/x-png")
      || ($mime == "image/png"))
      && in_array(strtolower($extension), $allowedExts)) {
        // Generate new random name.
        $name = sha1(microtime()) . "." . $extension;

        // Save file in the uploads folder.
        move_uploaded_file($_FILES["file"]["tmp_name"], getcwd() . "/uploads/images/" . $name);

        // Generate response.
        $response = new \StdClass;
        $response->link = url('/')."/uploads/images/" . $name;
        echo stripslashes(json_encode($response));
      }
    }
    public function delBanner($id)
    {
      $banner=\App\Models\BrandBanner::find($id);
      Storage::delete('images/banner/'.$banner->banner);
      if($banner->delete()){
        $banner=\App\Models\BrandBanner::get();
        echo json_encode($banner);
      }else{
        echo 'Failed!';
      }
    }
    public function delImg($id)
    {
      $img=\App\Models\BrandImg::find($id);
      Storage::delete('images/img/'.$img->img);
      if($img->delete()){
        $img=\App\Models\BrandImg::get();
        echo json_encode($img);
      }else{
        echo 'Failed!';
      }
    }
}
