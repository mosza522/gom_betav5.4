<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Session;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/';
    protected function authenticated(Request $request, $user)
    {
      if ( Auth::user()->status_user=='0' ) {// do your margic here
        Auth::logout();
        Session::flush();
        Session::flash('wrong','รอการยืนยัน ID จากแอดมิน');
        return redirect('/');
      }

      return redirect('/');
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function username()
  	{
  			return 'username';
  	}
    public function showLoginForm()
  	{
  		 return view('index')->with(array('login'=> 'login'));
  	}
    public function logout(Request $request) {
      Auth::logout();
      Session::flush();
      return redirect('/login');
    }

}
