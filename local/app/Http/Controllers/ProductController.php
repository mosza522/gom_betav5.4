<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use Auth;
use DB;
use Session;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
      $product= new \App\Models\Product;
      $product->category_sub_id=$r->sub_category;
      $product->band_id=$r->brand;
      $product->common_name=$r->common_name;
      $product->name=$r->name;
      $product->price=$r->price;
      $product->cost=$r->cost;
      $product->order_minimum=$r->order_minimum;
      $product->piece=$r->piece;
      $product->supply=$r->supply;
      $product->time =$r->time;
      $product->detail_title =$r->detail_title;
      $product->detail =$r->detail;
      $product->created_by=Auth::user()->id;

      $product->save();
      $product=\App\Models\Product::orderBy('id','desc')->first();
      foreach ($r->file('img') as $key ) {
        $name_img= time().'-'.$key->getClientOriginalName();
        $img= new \App\Models\ProductImg;
        $img->product_id = $product->id;
        $img->name = $name_img;
        if($img->save()){
          $key->storeAs('/images/product',$name_img);
        }

      }
      for ($i=0; $i < count($r->amount); $i++) {
        $promotion=new \App\Models\ProductPromotion;
        $promotion->product_id=$product->id;
        $promotion->amount=$r->amount[$i];
        $promotion->discount=$r->discount[$i];
        $promotion->save();
      }
      for ($i=0; $i < count($r->sku); $i++) {
        $sku=new \App\Models\Sku;
        $sku->sku=$r->sku[$i];
        $sku->product_id=$product->id;
        $sku->save();
      }
      for ($i=0; $i < count($r->zone_num); $i++) {
        $zone=new \App\Models\Zone;
        $zone->num=$r->zone_num[$i];
        $zone->radius=$r->zone_radius[$i];
        $zone->time=$r->zone_time[$i];
        $zone->product_id=$product->id;
        $zone->save();
      }
      $product=\App\Models\Product::get();
      $i=1;
      $json= array();
      if (Auth::user()->member_type=="Admin"){
        foreach ($product as $key) {
          array_push($json, array(''.$i++,$key->common_name,$key->name,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalShow" onclick="return show('.$key->id.')" class="btn btn-primary">Details</a>'
          ,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return edit('.$key->id.')" class="btn btn-warning">Edit</a>
          <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
        }
      }else{
        foreach ($product as $key) {
          array_push($json, array(''.$i++,$key->common_name,$key->name,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalShow" onclick="return show('.$key->id.')" class="btn btn-primary">Details</a>'
          ,''));
        }
      }
      echo json_encode($json);



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $product=\App\Models\Product::leftjoin('product_brand','product.band_id','=','product_brand.id')
      ->leftJoin('product_category_sub', 'product.category_sub_id', '=', 'product_category_sub.id')
      ->leftJoin('product_category', 'product_category_sub.category_id', '=', 'product_category.id')
      ->select('product.*','product_brand.name as brand','product_category_sub.name as nameSubCategory','product_brand.id as brand_id'
      ,'product_category_sub.id as category_sub_id','product_category.name as nameCategory','product_category.id as idCategory')
      ->where('product.id',$id)
      ->first();
      $sub=\App\Models\SubCategory::where('category_id',$product->idCategory)->get();
      $img=\App\Models\ProductImg::where('product_id',$id)->get();
      $promotion=\App\Models\ProductPromotion::where('product_id',$id)->get();
      $sku= \App\Models\Sku::where('product_id',$id)->get();
      $zone= \App\Models\Zone::where('product_id',$id)->get();
      $json= array();
      foreach ($product as $key) {
        array_push($json, array('product'=>$product,'img'=>$img,'promotion'=>$promotion,'sku'=>$sku,'zone'=>$zone,'sub'=>$sub));
      }
      echo json_encode($json);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
      $product=  \App\Models\Product::find($id);
      $product->category_sub_id=$r->sub_category;
      $product->band_id=$r->brand;
      $product->common_name=$r->common_name;
      $product->name=$r->name;
      $product->price=$r->price;
      $product->cost=$r->cost;
      $product->order_minimum=$r->order_minimum;
      $product->piece=$r->piece;
      $product->supply=$r->supply;
      $product->time =$r->time;
      $product->detail_title =$r->detail_title;
      $product->detail =$r->detail;
      $product->updated_by=Auth::user()->id;
      $product->save();
      if($r->file('img')){
        foreach ($r->file('img') as $key ) {
          $name_img= time().'-'.$key->getClientOriginalName();
          $img= new \App\Models\ProductImg;
          $img->product_id = $id;
          $img->name = $name_img;
          if($img->save()){
            $key->storeAs('/images/product',$name_img);
          }
        }
      }
      $promotion= \App\Models\ProductPromotion::where('product_id',$id)->delete();
      for ($i=0; $i < count($r->amount); $i++) {
        $promotion=new \App\Models\ProductPromotion;
        $promotion->product_id=$id;
        $promotion->amount=$r->amount[$i];
        $promotion->discount=$r->discount[$i];
        $promotion->save();
      }
      $sku= \App\Models\Sku::where('product_id',$id)->delete();
      for ($i=0; $i < count($r->sku); $i++) {
        $sku=new \App\Models\Sku;
        $sku->sku=$r->sku[$i];
        $sku->product_id=$id;
        $sku->save();
      }
      $zone= \App\Models\Zone::where('product_id',$id)->delete();
      for ($i=0; $i < count($r->zone_num); $i++) {
        $zone=new \App\Models\Zone;
        $zone->num=$r->zone_num[$i];
        $zone->radius=$r->zone_radius[$i];
        $zone->time=$r->zone_time[$i];
        $zone->product_id=$id;
        $zone->save();
      }
      $product=\App\Models\Product::get();
      $i=1;
      $json= array();
      if (Auth::user()->member_type=="Admin"){
        foreach ($product as $key) {
          array_push($json, array(''.$i++,$key->common_name,$key->name,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalShow" onclick="return show('.$key->id.')" class="btn btn-primary">Details</a>'
          ,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return edit('.$key->id.')" class="btn btn-warning">Edit</a>
          <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
        }
      }else{
        foreach ($product as $key) {
          array_push($json, array(''.$i++,$key->common_name,$key->name,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalShow" onclick="return show('.$key->id.')" class="btn btn-primary">Details</a>'
          ,''));
        }
      }

      return response()->json($json);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $img=\App\Models\ProductImg::where('product_id',$id)->get();
      foreach ($img as $key) {
        Storage::delete('images/product/'.$key->name);
        $key->delete();
      }
      $highlight=\App\Models\highlight::where('product_id',$id)->first();
      Storage::delete('images/img-highlight/'.$highlight->banner);
      // $promotion=\App\Models\ProductPromotion::where('product_id',$id)->delete();
      // $sku= \App\Models\Sku::where('product_id',$id)->delete();
      // $zone= \App\Models\Zone::where('product_id',$id)->delete();
      $product=\App\Models\Product::find($id)->delete();
      $product=\App\Models\Product::get();
      $i=1;
      $json= array();
      if (Auth::user()->member_type=="Admin"){
        foreach ($product as $key) {
          array_push($json, array(''.$i++,$key->common_name,$key->name,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalShow" onclick="return show('.$key->id.')" class="btn btn-primary">Details</a>'
          ,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return edit('.$key->id.')" class="btn btn-warning">Edit</a>
          <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
        }
      }else{
        foreach ($product as $key) {
          array_push($json, array(''.$i++,$key->common_name,$key->name,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalShow" onclick="return show('.$key->id.')" class="btn btn-primary">Details</a>'
          ,''));
        }
      }
      echo json_encode($json);
    }
    public function delImg($id)
    {
      $img=\App\Models\ProductImg::find($id);
      $id=$img->product_id;
      Storage::delete('images/product/'.$img->name);
      if($img->delete()){
        $img=\App\Models\ProductImg::where('product_id', $id)->get();
        echo json_encode($img);
      }else{
        echo 'Failed!';
      }
    }
    public function chooseCategory($id)
    {
      $product=\App\Models\SubCategory::where('category_id',$id)->get();
      return response()->json($product);
    }
    public function checkPrice(request $r,$id,$num)
    {
      $discount=0;
      $product=\App\Models\Product::where('id',$id)->first();
      $promotion=\App\Models\ProductPromotion::where('product_id',$id)->orderBy('amount','DESC')->get();
      foreach ($promotion as $key ) {
        if ($num>=$key->amount) {
          $discount=$key->discount;
          break;
        }
      }
      if($r->type=='cart'){
        $session =array_values(Session::get('cart'));
        Session::put('cart',$session);

        for ($i=0; $i < count($session); $i++) {
          if(Session::get('cart.'.$i)==$id){
            Session::forget('cart.'.$i);
          }
        }
        $session =array_values(Session::get('cart'));
        for ($i=0; $i < $num; $i++) {
        array_push($session,$id);
        }
        Session::forget('cart');
        Session::put('cart',$session);
      }
      $json= array();
      if (Session::has('cart')) {
        $inCart=count(Session::get('cart'));
      }else{
        $inCart=0;
      }
      array_push($json, array('price'=>$product->price,'discount'=>$discount,'piece'=>$product->piece,'in_cart'=>$inCart));
      return response()->json($json);

    }
    public function addToCart(request $r)
    {
      // Session::forget('cart');
      for ($i=0; $i < $r->amount; $i++) {
        Session::push('cart', $r->id);
      }
      return response()->json(Session::get('cart'));

    }
    public function delProduct($id)
    {
      $session =array_values(Session::get('cart'));
        for ($i=0; $i < count($session); $i++) {
          if(Session::get('cart.'.$i)==$id){
            Session::forget('cart.'.$i);
          }
        }


      $session =array_values(Session::get('cart'));
      Session::put('cart',$session);
    }
    public function ConfirmCart(Request $r)
    {
      $new_id = \App\Models\OrderData::orderBy('id','DESC')->first();
      if($new_id==""){
        $new_id='PO-'.sprintf("%07d",'1');
      }else{
        $new_id='PO-'.sprintf("%07d",($new_id->id)+1);
      }
      $order_data = new \App\Models\OrderData;
      $order_data->user_id=Auth::user()->id;
      $order_data->order_id=$new_id;
      $order_data->name1=$r->name1;
      $order_data->address1=$r->address1;
      $order_data->zipcode1=$r->zipcode1;
      $order_data->district1=$r->district1;
      $order_data->province1=$r->province1;
      $order_data->email1=$r->email1;
      $order_data->mobile1=$r->mobile1;
      $order_data->bank=$r->bank;
      $order_data->money=$r->money;
      $order_data->date=$r->date;
      // $order_data->file=$r->file('file')->getClientOriginalName();
      $name_img= time().'-'.$r->file('file')->getClientOriginalName();
      $order_data->file = $name_img;
      $r->file('file')->storeAs('/images/reciept',$name_img);
      if($r->tax_check=='1'){
        $order_data->tax_invoice=$r->tax_check;
        $order_data->name2=$r->name2;
        $order_data->address2=$r->address2;
        $order_data->zipcode2=$r->zipcode2;
        $order_data->district2=$r->district2;
        $order_data->province2=$r->province2;
        $order_data->email2=$r->email2;
        $order_data->mobile2=$r->mobile2;
        $order_data->taxid=$r->taxid;
        $order_data->trader=$r->trader;
      }
      $order_data->save();
      // if($order_data->save()){
      //   echo "complete";
      // }else{
      //   echo "fail";
      // }
      $order_id = \App\Models\OrderData::orderBy('id','DESC')->first();
      $qtys=array_count_values(Session::get('cart'));

      foreach (array_keys($qtys) as $key ) {
        $order_detail = new \App\Models\OrderDetail;
        $order_detail->order_id=$order_id->id;
        $order_detail->product_id=$key;
        $order_detail->qty=$qtys[$key];
        $order_detail->save();
      }
      Session::forget('cart');
      echo "complete";
      // return response()->json(array_keys($qtys));

    }
    public function ban($id)
    {
      if(Session::has('banproduct')){
        if(!array_search($id,Session::get('banproduct'))){
          Session::push('banproduct',$id);
        }
      }else{
        Session::push('banproduct',$id);
      }

      echo "$id";
    }
    public function SortBy($sort)
    {
        Session::put('sort',$sort);
    }
    public function changePage($subcat,$brand,$page)
    {
      if(!Session::has('brand') and !Session::has('portionpage') and !Session::has('subcat')){
        Session::push('subcat',$subcat);
        Session::push('brand',$brand);
        Session::push('portionpage',$page);
      }
      else{
        $status=false;
        $arraySubcat=Session::get('subcat',[]);
        $arrayBrand=Session::get('brand',[]);
        $arrayPage=Session::get('portionpage',[]);
        // dd($arrayBrand);
        for ($j=0; $j < count($arraySubcat); $j++) {
          if($arraySubcat[$j]==$subcat){
            for ($i=0; $i < count($arrayBrand); $i++) {
              if($arrayBrand[$i]==$brand and $j==$i){
                $arrayPage[$i]=$page;
                $status=false;
                break;
              }else{
                $status=true;
              }
            }
            break;
          }else{
            $status=true;
          }
        }
        if($status==true){
          array_push($arraySubcat,$subcat);
          array_push($arrayBrand,$brand);
          array_push($arrayPage,$page);
        }
        Session::put('subcat',$arraySubcat);
        Session::put('brand',$arrayBrand);
        Session::put('portionpage',$arrayPage);
        }
        return redirect()->back();
      }
    // public function SortBy($SubCategory,$brand,$Sort)
    // {
    //   if(Session::has('banproduct')){
    //     $banproduct=array_unique(Session::get('banproduct'));
    //   }else{
    //     $banproduct=['0'];
    //   }
    //   if($Sort=='DESC'){
    //     $products=\App\Models\Product::where('band_id',$brand)
    //     ->where('category_sub_id',$SubCategory)
    //     ->whereNotIn('id',$banproduct)
    //     ->orderBy('created_at','DESC')
    //     ->get();
    //   }
    //   else if($Sort=='HL'){
    //     $products=\App\Models\Product::where('band_id',$brand)
    //     ->where('category_sub_id',$SubCategory)
    //     ->whereNotIn('id',$banproduct)
    //     ->select('product.*')
    //     ->selectRaw('price/piece as sum')
    //     ->orderBy('sum','DESC')
    //     ->get();
    //   }
    //   else if($Sort=='LH'){
    //     $products=\App\Models\Product::where('band_id',$brand)
    //     ->where('category_sub_id',$SubCategory)
    //     ->whereNotIn('id',$banproduct)
    //     ->select('product.*')
    //     ->selectRaw('price/piece as sum')
    //     ->orderBy('sum','ASC')
    //     ->get();
    //   }
    //
    //   $jsonReturn=array();
    //   // $imgArray= array();
    //   // $product =array();
    //   // foreach ($products as $key ) {
    //   //   $img=\App\Models\ProductImg::where('product_id',$key->id)->first();
    //   //   array_push($product,$key);
    //   //   array_push($imgArray,$img);
    //   // }
    //   // array_push($jsonReturn,array('product'=>$product,'img'=>$imgArray));
    //   // return response()->json($jsonReturn);
    //   $return="";
    //   foreach ($products as $product ) {
    //     $img=\App\Models\ProductImg::where('product_id',$product->id)->first();
    //     $price="<small><strong>".number_format(($product->price/$product->piece), 2, '.', ',') ."บาท/ชิ้น, $product->price บาท/ลัง($product->piece ชิ้น)<br>สั่งซื้อขั้นต่ำ $product->order_minimum ลัง = ". number_format(($product->price*$product->order_minimum), 2, '.', ',') ." บาท</strong></small>";
    //     $promotions=\App\Models\ProductPromotion::where('product_id',$product->id)->get();
    //     $zones=\App\Models\Zone::where('product_id',$product->id)->get();
    //     $i=1;
    //     $text="";
    //     $inc_promotion="<small>";
    //     foreach ($promotions as $promotion){
    //       if ($i==1){
    //         $inc_promotion.="<hr><strong class='text-success'>Pro$i &bull;&nbsp;</strong> $promotion->amount  ลัง - $promotion->discount %
    //         เหลือ ฿".number_format((($product->price/$product->piece)-($product->price/$product->piece)*($promotion->discount/100)), 2, '.', ',')."/ชิ้น&nbsp;<strong>
    //         รวม ". number_format((($product->price-($product->price*($promotion->discount/100)))*$promotion->amount), 2, '.', ',') ."฿</strong><br>";
    //         foreach ($zones as $zone){
    //           if ($zone->num <= $promotion->amount  ){
    //             $text="กันเขต $zone->radius km/$zone->time วัน";
    //           }
    //         }
    //         $inc_promotion.=$text;
    //         $i++;
    //       }
    //       else{
    //         $inc_promotion.="<hr class='my-1 border-dashed'><strong class='text-success'>Pro$i &bull;&nbsp;</strong> $promotion->amount  ลัง - $promotion->discount %
    //         เหลือ ฿".number_format((($product->price/$product->piece)-($product->price/$product->piece)*($promotion->discount/100)), 2, '.', ',')."/ชิ้น&nbsp;<strong>
    //         รวม ". number_format((($product->price-($product->price*($promotion->discount/100)))*$promotion->amount), 2, '.', ',') ."฿</strong><br>";
    //         foreach ($zones as $zone){
    //           if ($zone->num <= $promotion->amount  ){
    //             $text="กันเขต $zone->radius km/$zone->time วัน";
    //           }
    //         }
    //         $inc_promotion.=$text;
    //         $i++;
    //       }
    //     }
    //     $inc_promotion."</small>";
    //     $return.="<div id=\"procuct$product->id\">";
    //     $return.="<div class=\"product-item\">";
    //     $return.="<div class=\"card rounded-0\">";
    //     $return.="<img class=\"card-img-top\" src=\"http://localhost/5.4/gom_betaV5.4/local/storage/app/images/product/$img->name\" >";
    //     $return.="<div class=\"card-body position-relative\">";
    //     $return.="<div class=\"product-btn btn-group invisible\">";
    //     $return.="<a  class=\"btn btn-sm btn-success w-50 border\" data-toggle=\"modal\" href=\"http://localhost/5.4/gom_betaV5.4/inc_cart-add/$product->id\">ช้อปเลย</a>";
    //     $return.="<a  class=\"btn btn-sm btn-success w-50 border\" href=\"http://localhost/5.4/gom_betaV5.4/inc_cart-add/$product->id\">ดูสินค้า</a>";
    //     $return.="</div>";
    //     $return.="<p class=\"card-text text-line-max2\" style=\"height:40px\">$product->name<br /></p>";
    //     $return.="</div>";
    //     $return.="<div class=\"card-footer text-center text-sm-left\">";
    //     if (Auth::guest()){
    //       $return.="<a href=\"http://localhost/5.4/gom_betaV5.4/login\"><span class=\"price\">เข้าสู่ระบบเพื่อตรวจสอบราคา </span></a>";
    //       $return.="<span class=\"\" data-container=\"body\" data-toggle=\"popover\" data-placement=\"auto\" data-html=\"true\" title=\"Price & Promotion\" data-content=\"เข้าสู่ระบบเพื่อตรวจสอบราคา\"></span>";
    //     }
    //     else{
    //       $return.="<span class=\"price\">฿ ".$product->price/$product->piece." <small>/ ชิ้น</small></span>";
    //       $return.="<span class=\"tag float-sm-right\" data-container=\"body\" data-toggle=\"popover\" data-placement=\"auto\" data-html=\"true\" title=\"Price & Promotion\" data-content=\"".$price.$inc_promotion."\">PRO | <i class=\"fa fa-street-view\"></i></span>";
    //     }
    //     $return.="</div>";
    //     $return.="</div>";
    //     $return.="</div>";
    //     $return.="</div>";
    //   }
    //
    //   return $return;
    //
    // }
}
