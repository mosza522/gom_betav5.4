<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
class Highlight extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
      $banner= time().'-'.$r->file('banner')->getClientOriginalName();

      $hl=new \App\Models\Highlight;
      $hl->banner=$banner;
      $hl->product_id=$r->product;
      $r->file('banner')->storeAs('/images/img-highlight',$banner);
      $hl->save();
      $hl= \App\Models\highlight::leftJoin('product','highlight.product_id','=','product.id')
      ->select('highlight.*','product.name')
      ->get();
      $i=1;
      $json= array();
      $json2= array();
      $json3= array();
      foreach ($hl as $key) {
        array_push($json, array(''.$i++,$key->name,
        '<div class="tz-gallery">
          <div class="row">
            <div class="col-sm-12 col-md-12">
                <a class="lightbox" href="'.asset('local/storage/app/images/img-highlight/').'/'.$key->banner.'">
                    <img src="'.asset('local/storage/app/images/img-highlight/').'/'.$key->banner.'"  width="250px" height="250px" >
                </a>
            </div>
          </div>
        </div>'
        ,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return show('.$key->id.')" class="btn btn-warning">Edit</a>
        <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
      }
      $product= \App\Models\Product::whereNotIn('product.id',function($q){
        $q->select('product_id')->from('highlight');
      })
      ->select('product.name','product.id')
      ->get();
      array_push($json3, array('table'=>$json,'select'=>$product));
      return response()->json($json3);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hl=\App\Models\Highlight::find($id);
        $product= \App\Models\Product::whereNotIn('product.id',function($q) use ($hl){
          $q->select('product_id')->from('highlight')->where('product_id','!=',$hl->product_id);
        })
        ->select('product.name','product.id')
        ->get();
        $json= array() ;
        array_push($json, array('data'=>$hl,'select'=>$product));
        return response()->json($json);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $hl=\App\Models\Highlight::find($id);
        if($r->file('banner')){
          Storage::delete('images/img-highlight/'.$hl->banner);
          $banner= time().'-'.$r->file('banner')->getClientOriginalName();
          $r->file('banner')->storeAs('/images/img-highlight',$banner);
          $hl->banner=$banner;
        }
        $hl->product_id=$r->product;
        $hl->save();

        $hl= \App\Models\highlight::leftJoin('product','highlight.product_id','=','product.id')
        ->select('highlight.*','product.name')
        ->get();
        $i=1;
        $json= array();
        $json2= array();
        $json3= array();
        foreach ($hl as $key) {
          array_push($json, array(''.$i++,$key->name,
          '<div class="tz-gallery">
            <div class="row">
              <div class="col-sm-12 col-md-12">
                  <a class="lightbox" href="'.asset('local/storage/app/images/img-highlight/').'/'.$key->banner.'">
                      <img src="'.asset('local/storage/app/images/img-highlight/').'/'.$key->banner.'"  width="250px" height="250px" >
                  </a>
              </div>
            </div>
          </div>'
          ,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return show('.$key->id.')" class="btn btn-warning">Edit</a>
          <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
        }
        $product= \App\Models\Product::whereNotIn('product.id',function($q){
          $q->select('product_id')->from('highlight');
        })
        ->select('product.name','product.id')
        ->get();
        array_push($json3, array('table'=>$json,'select'=>$product));
        return response()->json($json3);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $hl=\App\Models\Highlight::find($id);
      Storage::delete('images/img-highlight/'.$hl->banner);
      if($hl->delete()){
        $hl= \App\Models\highlight::leftJoin('product','highlight.product_id','=','product.id')
        ->select('highlight.*','product.name')
        ->get();
        $i=1;
        $json= array();
        $json2= array();
        $json3= array();
        foreach ($hl as $key) {
          array_push($json, array(''.$i++,$key->name,
          '<div class="tz-gallery">
            <div class="row">
              <div class="col-sm-12 col-md-12">
                  <a class="lightbox" href="'.asset('local/storage/app/images/img-highlight/').'/'.$key->banner.'">
                      <img src="'.asset('local/storage/app/images/img-highlight/').'/'.$key->banner.'"  width="250px" height="250px" >
                  </a>
              </div>
            </div>
          </div>'
          ,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return show('.$key->id.')" class="btn btn-warning">Edit</a>
          <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
        }
        $product= \App\Models\Product::whereNotIn('product.id',function($q){
          $q->select('product_id')->from('highlight');
        })
        ->select('product.name','product.id')
        ->get();
        array_push($json3, array('table'=>$json,'select'=>$product));
        return response()->json($json3);
      }else{
        echo 'Failed!';
      }
    }
}
