<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use Auth;
use DB;
use Session;
class Review extends Controller
{
    public function postReview(request $r)
    {
      $review= new \App\Models\Review;
      $review->title=$r->title;
      $review->body=$r->body;
      $review->user_id=Auth::user()->id;
      $review->product_id=$r->id;
      $review->save();
      return redirect()->back();
    }
}
