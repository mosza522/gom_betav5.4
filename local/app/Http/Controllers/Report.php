<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Storage;
class Report extends Controller
{
  public function changeMYBestSell($month,$year)
  {
    $datas= new \App\Models\OrderDetail;
    $json= array();
    $i=1;
    foreach ($datas->bestSell($month,$year) as $key ) {
      array_push($json, array(''.$i++,$key->sum,$key->profit,$key->price,$key->cost,$key->name,$key->category,$key->brand,$key->username));
    }
    return response()->json($json);
  }
  public function changeMYSell($month,$year)
  {
    $datas= new \App\Models\OrderData;
    $json= array();
    $i=1;
    foreach ($datas->sell($month,$year)[0]['data'] as $key ) {
      array_push($json, array(''.$i++,$key->order_id,$datas->sell()[0]['detail'][$i-2]
      ,$datas->sell()[0]['total'][$i-2],$key->name,date('Y-m-d H:i:s',strtotime($key->created_at))));
    }
    return response()->json($json);
  }

}
