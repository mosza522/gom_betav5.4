<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PromotionOfmonth extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
      $promotion=new \App\Models\PromotionOfMonth;
      $promotion->product_id=$r->product;
      $promotion->save();
      $promotion= \App\Models\PromotionOfMonth::leftJoin('product','hl_promotionofmonth.product_id','=','product.id')
      ->select('hl_promotionofmonth.*','product.name')
      ->get();
      $i=1;
      $json= array();
      $json2= array();
      $json3= array();
      foreach ($promotion as $key) {
        array_push($json, array(''.$i++,$key->name,
        '<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return show('.$key->id.')" class="btn btn-warning">Edit</a>
        <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
      }
      $product= \App\Models\Product::whereNotIn('product.id',function($q){
        $q->select('product_id')->from('hl_promotionofmonth');
      })
      ->select('product.name','product.id')
      ->get();
      array_push($json3, array('table'=>$json,'select'=>$product));
      return response()->json($json3);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $promotion=\App\Models\PromotionOfMonth::find($id);
      $product= \App\Models\Product::whereNotIn('product.id',function($q) use ($promotion){
        $q->select('product_id')->from('hl_promotionofmonth')->where('product_id','!=',$promotion->product_id);
      })
      ->select('product.name','product.id')
      ->get();
      $json= array() ;
      array_push($json, array('data'=>$promotion,'select'=>$product));
      return response()->json($json);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
      $promotion=\App\Models\PromotionOfMonth::find($id);
      $promotion->product_id=$r->product;
      $promotion->save();

      $promotion= \App\Models\PromotionOfMonth::leftJoin('product','hl_promotionofmonth.product_id','=','product.id')
      ->select('hl_promotionofmonth.*','product.name')
      ->get();
      $i=1;
      $json= array();
      $json2= array();
      $json3= array();
      foreach ($promotion as $key) {
        array_push($json, array(''.$i++,$key->name,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return show('.$key->id.')" class="btn btn-warning">Edit</a>
        <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
      }
      $product= \App\Models\Product::whereNotIn('product.id',function($q){
        $q->select('product_id')->from('hl_promotionofmonth');
      })
      ->select('product.name','product.id')
      ->get();
      array_push($json3, array('table'=>$json,'select'=>$product));
      return response()->json($json3);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $promotion=\App\Models\PromotionOfMonth::find($id);
      if($promotion->delete()){
        $promotion= \App\Models\PromotionOfMonth::leftJoin('product','hl_promotionofmonth.product_id','=','product.id')
        ->select('hl_promotionofmonth.*','product.name')
        ->get();
        $i=1;
        $json= array();
        $json2= array();
        $json3= array();
        foreach ($promotion as $key) {
          array_push($json, array(''.$i++,$key->name,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return show('.$key->id.')" class="btn btn-warning">Edit</a>
          <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
        }
        $product= \App\Models\Product::whereNotIn('product.id',function($q){
          $q->select('product_id')->from('hl_promotionofmonth');
        })
        ->select('product.name','product.id')
        ->get();
        array_push($json3, array('table'=>$json,'select'=>$product));
        return response()->json($json3);
      }else{
        echo 'Failed!';
      }
    }
}
