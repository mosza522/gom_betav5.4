<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelpCenter extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
      $help =\App\Models\HelpCenter::first();
      if($help!=null){
        $help->how_to_buy = $r->how_to_buy;
        $help->how_to_pay = $r->how_to_pay;
        $help->how_to_send = $r->how_to_send;
        $help->question = json_encode($r->question);
        $help->ans = json_encode($r->answer);
        $help->policy = $r->Privacy_Policy;
        $help->save();
      }else{
        $help = new \App\Models\HelpCenter;
        $help->how_to_buy = $r->how_to_buy;
        $help->how_to_pay = $r->how_to_pay;
        $help->how_to_send = $r->how_to_send;
        $help->question = json_encode($r->question);
        $help->ans = json_encode($r->answer);
        $help->policy = $r->Privacy_Policy;
        $help->save();
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id=null)
    {
        $help =\App\Models\HelpCenter::first();
        if ($help->delete()) {
          echo 'finished';
        }

    }
}
