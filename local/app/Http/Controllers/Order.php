<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Storage;
class Order extends Controller
{
    public function Confirm($id)
    {
      $json=array();
      $json2=array();
      $confirm=\App\Models\OrderData::find($id);
      $confirm->status_confirm = 1;
      $confirm->updated_by = Auth::user()->id;
      $confirm->save();

      $datas= \App\Models\OrderData::leftJoin('users','order_data.user_id','users.id')
      ->select('order_data.*','users.name as name')
      ->where('order_data.status_confirm','0')
      ->orderBy('status_confirm','ASC')
      ->get();
      foreach ($datas as $key) {
        array_push($json, array($key->order_id,$key->name
        ,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalShow'.$key->id.'" onclick="return show('.$key->id.')" class="btn btn-primary">รายละเอียดใบสั่งสินค้า</a>'
        ,''.$key->created_at,"<a href='javascript:void(0);' onclick='return sweetalert($key->id)' class='btn btn-danger'>Delete</a>"));
      }
      $orderCheck= \App\Models\OrderData::where('order_data.status_confirm','0')->get()->count();
      $orderNotCheck= \App\Models\OrderData::where('order_data.status_confirm','1')->get()->count();
      array_push($json2,array('data'=>$json,'orderNotCheck'=>$orderNotCheck,'orderCheck'=>$orderCheck));
      echo json_encode($json2);
    }
    public function destroy($id)
    {
      $json=array();
      $json2=array();
      $del=\App\Models\OrderData::find($id);
      Storage::delete('images/reciept/'.$del->file);
      $del->delete();

      $datas= \App\Models\OrderData::leftJoin('users','order_data.user_id','users.id')
      ->select('order_data.*','users.name as name')
      ->where('order_data.status_confirm','0')
      ->orderBy('status_confirm','ASC')
      ->get();
      foreach ($datas as $key) {
        array_push($json, array($key->order_id,$key->name
        ,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalShow'.$key->id.'" onclick="return show('.$key->id.')" class="btn btn-primary">รายละเอียดใบสั่งสินค้า</a>'
        ,''.$key->created_at,"<a href='javascript:void(0);' onclick='return sweetalert($key->id)' class='btn btn-danger'>Delete</a>"));
      }
      $orderCheck= \App\Models\OrderData::where('order_data.status_confirm','0')->get()->count();
      $orderNotCheck= \App\Models\OrderData::where('order_data.status_confirm','1')->get()->count();
      array_push($json2,array('data'=>$json,'orderNotCheck'=>$orderNotCheck,'orderCheck'=>$orderCheck));
      echo json_encode($json2);
    }
    public function ChangeStatus($val,$id)
    {
      if($val=="1"){
        $order=\App\Models\OrderData::find($id);
        $order->status_order=$val;
        $order->send_date=\Carbon::now();
        $order->save();
      }
      else if($val=="2"){
        $order=\App\Models\OrderData::find($id);
        $order->status_order=$val;
        $order->pickup_date=\Carbon::now();
        $order->save();
      }else{
        $order=\App\Models\OrderData::find($id);
        $order->status_order=$val;
        $order->send_date="";
        $order->pickup_date="";
        $order->save();
      }


      $order=\App\Models\OrderData::find($id);
      echo json_encode($order);
    }
}
