<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubCategory extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $sub= new \App\Models\SubCategory;
        $sub->category_id= $r->category_id;
        $sub->name= $r->name;
        $sub->save();
        $sub=\App\Models\SubCategory::leftJoin('product_category','product_category_sub.category_id','=','product_category.id')
        ->select('product_category_sub.name as subCategoryName','product_category.name as CategoryName','product_category_sub.id')
        ->get();
        $i=1;
        $json= array();
        foreach ($sub as $key) {
          array_push($json, array(''.$i++,$key->CategoryName,$key->subCategoryName
          ,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return edit('.$key->id.')" class="btn btn-warning">Edit</a>
          <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
        }
        return response()->json($json);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $sub=\App\Models\SubCategory::find($id);
      return response()->json($sub);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
      $sub=\App\Models\SubCategory::find($id);
      $sub->category_id=$r->category_id;
      $sub->name=$r->name;
      $sub->save();
      $sub=\App\Models\SubCategory::leftJoin('product_category','product_category_sub.category_id','=','product_category.id')
      ->select('product_category_sub.name as subCategoryName','product_category.name as CategoryName','product_category_sub.id')
      ->get();
      $i=1;
      $json= array();
      foreach ($sub as $key) {
        array_push($json, array(''.$i++,$key->CategoryName,$key->subCategoryName
        ,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return edit('.$key->id.')" class="btn btn-warning">Edit</a>
        <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
      }
      return response()->json($json);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $sub=\App\Models\SubCategory::find($id)->delete();
      $sub=\App\Models\SubCategory::leftJoin('product_category','product_category_sub.category_id','=','product_category.id')
      ->select('product_category_sub.name as subCategoryName','product_category.name as CategoryName','product_category_sub.id')
      ->get();
      $i=1;
      $json= array();
      foreach ($sub as $key) {
        array_push($json, array(''.$i++,$key->CategoryName,$key->subCategoryName
        ,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return edit('.$key->id.')" class="btn btn-warning">Edit</a>
        <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
      }
      return response()->json($json);
    }
}
