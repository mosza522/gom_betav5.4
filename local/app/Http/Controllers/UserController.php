<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Toast;
use Session;
use Storage;
use Auth;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {

      $cover = $r->file('img_cover');
      $profile = $r->file('img_profile');
      $name_cover= time().'-'.$cover->getClientOriginalName();
      $name_profile= time().'-'.$profile->getClientOriginalName();


      $user= new \App\Models\user;
      $user->img_cover = $name_cover;
      $user->img_profile = $name_profile;
      $user->name = $r->fullname;
      $user->member_type = $r->member_type;
      $user->member_group = $r->member_group;
      $user->company_name = $r->company_name;
      $user->company_code = $r->company_code;
      $user->tax_id = $r->tax_id;
      $user->address = $r->address;
      $user->zipcode = $r->zipcode;
      $user->district = $r->district;
      $user->province = $r->province;
      $user->email = $r->email;
      $user->mobile = $r->mobile;
      $user->phone = $r->phone;
      $user->fax = $r->fax;
      $user->username = $r->username;
      $user->password = \Hash::make($r->password);
      $user->password_origin = $r->password;
      $user->area_radius = $r->area_radius;
      $user->area_time = $r->area_time;
      $user->lat = $r->lat;
      $user->longtitude = $r->long;
      $user->need = $r->need;
      if($user->save()){
        $cover->storeAs('/images/user',$name_cover);
      $profile->storeAs('/images/user',$name_profile);
    }

    if($r->file('certificate')!=null) {
      for ($i=0; $i < count($r->certificate_number) ; $i++) {
        $user=\App\Models\user::orderBy('id','desc')->first();

        $cer = $r->file('certificate')[$i];
        $name_cer= time().'-'.$cer->getClientOriginalName();


        $certificate= new \App\Models\Certificate;
        $certificate->user_id = $user->id;
        $certificate->certificate = $name_cer;
        $certificate->cer_num = $r->certificate_number[$i];

        if($certificate->save()){
        $cer->storeAs('/images/certificate',$name_cer);
        }
      }
    }
      Session::flash('success', 'สมัครสมาชิกเสร็จสิ้น รอการยืนยันจาก Admin จึงสามารถเข้าใช้งานได้');
      return redirect()->back();
      // foreach ($r->certificate_number as $key ) {
      // $user=\App\Models\user::orderBy('id','desc')->first();
      //
      // $file = $r->file('img');
      // $name= time().'-'.$file->getClientOriginalName();
      // $file->storeAs('/images/banner',$name);
      //
      //
      //
      // $file = $r->file('img');
      // $name= time().'-'.$file->getClientOriginalName();
      // $file->storeAs('/images/banner',$name);
      // }
      // member_type
      // member_group
      // fullname
      // company_name
      // company_code
      // tax_id
      // address
      // zipcode
      // district
      // province
      // email
      // mobile
      // phone
      // fax
      // certificate_number
      // username
      // password
      // area_radius
      // area_time
      // lat
      // long
      // need

        // dd($r);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $json= array();
        $user= \App\User::find($id);
        $cer= \App\Models\Certificate::where('user_id',$id)->get();
        // $array = array_merge($cer,$data2);
        $i=0;
        foreach ($cer as $key) {
          array_push($json, array('no' => $i++,'certificate' => $key->certificate, 'cer_num' => $key->cer_num));
        }
        echo json_encode(array('user'=>$user,'certificate'=>$json));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r)
    {
      // dd($r);
      $user= \App\Models\user::find(Auth::user()->id);
      if($r->imgCover!=null){
        Storage::delete('/images/user/'.$user->img_cover);
        $cover = $r->file('imgCover');
        $name_cover= time().'-'.$cover->getClientOriginalName();
        $user->img_cover = $name_cover;
        $cover->storeAs('/images/user',$name_cover);
      }
      if($r->imgProfile!=null){
        Storage::delete('/images/user/'.$user->img_profile);
        $profile = $r->file('imgProfile');
        $name_profile= time().'-'.$profile->getClientOriginalName();
        $user->img_profile = $name_profile;
        $profile->storeAs('/images/user',$name_profile);
      }
      $user->name = $r->name;
      $user->company_name = $r->company_name;
      $user->company_code = $r->company_code;
      $user->tax_id = $r->tax_id;
      $user->address = $r->address;
      $user->zipcode = $r->zipcode;
      $user->district = $r->district;
      $user->province = $r->province;
      $user->email = $r->email;
      $user->mobile = $r->mobile;
      $user->phone = $r->phone;
      $user->fax = $r->fax;
      $user->lat = $r->lat;
      $user->longtitude = $r->lng;
      $user->need = $r->need;
    if($r->file('certificate')!=null) {
      for ($i=0; $i < count($r->certificate_number) ; $i++) {
        $cer = $r->file('certificate')[$i];
        $name_cer= time().'-'.$cer->getClientOriginalName();


        $certificate= new \App\Models\Certificate;
        $certificate->user_id = $user->id;
        $certificate->certificate = $name_cer;
        $certificate->cer_num = $r->certificate_number[$i];

        if($certificate->save()){
        $cer->storeAs('/images/certificate',$name_cer);
        }
      }
    }
      $user->save();
      Session::flash('success', 'แก้ไขข้อมูลเสร็จสิ้น');
      return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $user= \App\Models\user::find($id);
      $cer= \App\Models\Certificate::where('user_id',$id)->get();
      foreach ($cer as $key ) {
        Storage::delete('images/certificate/'.$key->certificate);
        $key->delete();
      }
      $status=$user->status_user;
      $user->delete();
      if($status=='0'){
        $user = \App\Models\user::where('status_user', '0')->get();
        $i=1;
        $json= array();
        foreach ($user as $key) {
          array_push($json, array(''.$i++,$key->name,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalDetail" onclick="return detail('.$key->id.')" class="btn btn-info">รายละเอียด</a>
          ','<a href="javascript:void(0);" onclick="return confirm('.$key->id.')" class="btn btn-success">Confirm</a>
          <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a></td>
          '));
        }
      }else{
        $user = \App\Models\user::where('status_user', '1')->get();
        $i=1;
        $json= array();
        foreach ($user as $key) {
          array_push($json, array(''.$i++,$key->name,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalDetail" onclick="return detail('.$key->id.')" class="btn btn-info">รายละเอียด</a>
          ','<a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a></td>'));
        }
      }

      echo json_encode($json);
    }
    public function checkuser(Request $r)
    {

      echo $r->username;
    }
    public function confirm($id)
    {
      $user= \App\Models\user::find($id);
      $user->status_user = '1';
      if($user->save()){
        $user = \App\Models\user::where('status_user', '0')->get();
        $i=1;
        $json= array();
        foreach ($user as $key) {
          array_push($json, array(''.$i++,$key->name,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalDetail" onclick="return detail('.$key->id.')" class="btn btn-info">รายละเอียด</a>
','<a href="javascript:void(0);" onclick="return confirm('.$key->id.')" class="btn btn-success">Confirm</a>
<a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a></td>
'));
        }

        echo json_encode($json);
      }
      else echo 'Failed';
    }
    public function rePassword(Request $r,$id)
    {
      $data=\App\Models\user::find($id);
      // if(Hash::check(\Auth::User()->password, $r->pass_old)){
      if ($data->password_origin==$r->pass_old) {
        $data->password=Hash::make($r->pass_new);
        $data->password_origin=$r->pass_new;
        $data->save();
        echo 'completed';
      }else{
        echo "wrong";
      }
    }
}
