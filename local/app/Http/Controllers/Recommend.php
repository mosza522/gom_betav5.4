<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Recommend extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
      $Recommend=new \App\Models\Recommend;
      $Recommend->product_id=$r->product;
      $Recommend->save();
      $Recommend= \App\Models\Recommend::leftJoin('product','hl_recommend.product_id','=','product.id')
      ->select('hl_recommend.*','product.name')
      ->get();
      $i=1;
      $json= array();
      $json2= array();
      $json3= array();
      foreach ($Recommend as $key) {
        array_push($json, array(''.$i++,$key->name,
        '<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return show('.$key->id.')" class="btn btn-warning">Edit</a>
        <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
      }
      $product= \App\Models\Product::whereNotIn('product.id',function($q){
        $q->select('product_id')->from('hl_recommend');
      })
      ->select('product.name','product.id')
      ->get();
      array_push($json3, array('table'=>$json,'select'=>$product));
      return response()->json($json3);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $recommend=\App\Models\Recommend::find($id);
      $product= \App\Models\Product::whereNotIn('product.id',function($q) use ($recommend){
        $q->select('product_id')->from('hl_recommend')->where('product_id','!=',$recommend->product_id);
      })
      ->select('product.name','product.id')
      ->get();
      $json= array() ;
      array_push($json, array('data'=>$recommend,'select'=>$product));
      return response()->json($json);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
      $recommend=\App\Models\Recommend::find($id);
      $recommend->product_id=$r->product;
      $recommend->save();

      $recommend= \App\Models\Recommend::leftJoin('product','hl_recommend.product_id','=','product.id')
      ->select('hl_recommend.*','product.name')
      ->get();
      $i=1;
      $json= array();
      $json2= array();
      $json3= array();
      foreach ($recommend as $key) {
        array_push($json, array(''.$i++,$key->name,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return show('.$key->id.')" class="btn btn-warning">Edit</a>
        <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
      }
      $product= \App\Models\Product::whereNotIn('product.id',function($q){
        $q->select('product_id')->from('hl_recommend');
      })
      ->select('product.name','product.id')
      ->get();
      array_push($json3, array('table'=>$json,'select'=>$product));
      return response()->json($json3);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $recommend=\App\Models\Recommend::find($id);
      if($recommend->delete()){
        $recommend= \App\Models\Recommend::leftJoin('product','hl_recommend.product_id','=','product.id')
        ->select('hl_recommend.*','product.name')
        ->get();
        $i=1;
        $json= array();
        $json2= array();
        $json3= array();
        foreach ($recommend as $key) {
          array_push($json, array(''.$i++,$key->name,'<a href="javascript:void(0);" data-toggle="modal" data-target="#myModalupdate" onclick="return show('.$key->id.')" class="btn btn-warning">Edit</a>
          <a href="javascript:void(0);" onclick="return sweetalert('.$key->id.')" class="btn btn-danger">Delete</a>'));
        }
        $product= \App\Models\Product::whereNotIn('product.id',function($q){
          $q->select('product_id')->from('hl_recommend');
        })
        ->select('product.name','product.id')
        ->get();
        array_push($json3, array('table'=>$json,'select'=>$product));
        return response()->json($json3);
      }else{
        echo 'Failed!';
      }
    }
}
