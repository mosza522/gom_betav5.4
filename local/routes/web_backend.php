<?php
Route::group(['middleware' => 'auth'], function (){

  Route::get('/backoffice', function () {
    Session::put('page','index');
    return view('backoffice.index');
  });
  #user
  Route::get('/backoffice/user', function () {
    if(Auth::user()->member_type=="Admin"){
    Session::put('page','user');
    return view('backoffice.user.user');
  }else{
    Session::flash('wrong', 'ไม่มีสิทธิ์เข้าถึง');
    return redirect()->back();
  }
  });
  Route::get('/backoffice/userConfirm', function () {
    if(Auth::user()->member_type=="Admin"){
    Session::put('page','userConfirm');
    return view('backoffice.user.userConfirm');
  }else{
    Session::flash('wrong', 'ไม่มีสิทธิ์เข้าถึง');
    return redirect()->back();
  }
  });

  Route::get('user/detail/{id}', [ 'uses' => 'UserController@show']);
  Route::get('user/confirm/{id}', [ 'uses' => 'UserController@confirm']);
  Route::get('user/del/{id}', [ 'uses' => 'UserController@destroy']);
  Route::post('user/{id}/rePassword', 'UserController@rePassword');

  #end user
  // start Category_product
  Route::get('/backoffice/productCategory', function () {
    if(Auth::user()->member_type=="Admin"){
    Session::put('page','category');
    return view('backoffice.product.category');
  }else{
    Session::flash('wrong', 'ไม่มีสิทธิ์เข้าถึง');
    return redirect()->back();
  }
  });

  Route::get('product_category/{id}/img',[ 'uses' => 'CategoryController@img']);
  Route::post('product_category/{id}/update','CategoryController@update');
  Route::get('product_category/{id}', [ 'uses' => 'CategoryController@destroy']);
  Route::get('product_category/{id}/show', [ 'uses' => 'CategoryController@show']);
  Route::resource('product_category', 'CategoryController');
  // end Category_product

  // SubCategory

  Route::get('/backoffice/productSubCategory', function () {
    if(Auth::user()->member_type=="Admin"){
    Session::put('page','subcategory');
    return view('backoffice.product.subCategory');
  }else{
    Session::flash('wrong', 'ไม่มีสิทธิ์เข้าถึง');
    return redirect()->back();
  }
  });
  Route::post('subCategory/{id}/update','SubCategory@update');
  Route::resource('subCategory', 'SubCategory');

  // highlight
    Route::get('/backoffice/highlight', function () {
      if(Auth::user()->member_type=="Admin"){
      Session::put('page','highlight');
      return view('backoffice.highlight.highlight');
    }else{
      Session::flash('wrong', 'ไม่มีสิทธิ์เข้าถึง');
      return redirect()->back();
    }
    });
    Route::post('highlight/{id}/update','Highlight@update');
    Route::resource('highlight', 'Highlight');
    //end highlight

    //promotionofmonth
    Route::get('/backoffice/promotionofmonth', function () {
      if(Auth::user()->member_type=="Admin"){
      Session::put('page','promotionofmonth');
      return view('backoffice.highlight.promotionofmonth');
    }else{
      Session::flash('wrong', 'ไม่มีสิทธิ์เข้าถึง');
      return redirect()->back();
    }
    });
    Route::post('promotionofmonth/{id}/update','PromotionOfmonth@update');
    Route::resource('promotionofmonth', 'PromotionOfmonth');
    //promotionofmonth

    //saleofmonth
    Route::get('/backoffice/saleofmonth', function () {
      if(Auth::user()->member_type=="Admin"){
      Session::put('page','saleofmonth');
      return view('backoffice.highlight.saleofmonth');
    }else{
      Session::flash('wrong', 'ไม่มีสิทธิ์เข้าถึง');
      return redirect()->back();
    }
    });
    Route::post('saleofmonth/{id}/update','SaleOfMonth@update');
    Route::resource('saleofmonth', 'SaleOfMonth');
    //saleofmonth

    //recommend
    Route::get('/backoffice/recommend', function () {
      if(Auth::user()->member_type=="Admin"){
      Session::put('page','recommend');
      return view('backoffice.highlight.recommend');
    }else{
      Session::flash('wrong', 'ไม่มีสิทธิ์เข้าถึง');
      return redirect()->back();
    }
    });
    Route::post('recommend/{id}/update','Recommend@update');
    Route::resource('recommend', 'Recommend');
    //recommend



  // start brands
  Route::get('/backoffice/productBrands', function () {
    Session::put('page','brands');
    return view('backoffice.product.brands');
  });
  // Route::get('product_bands/{id}/del', [ 'uses' => 'BrandsController@destroy']);
  Route::post('product_bands/{id}/update','BrandsController@update');
  Route::get('product_bands/{id}/delBanner', [ 'uses' => 'BrandsController@delBanner']);
  Route::get('product_bands/{id}/delImg', [ 'uses' => 'BrandsController@delImg']);
  Route::resource('product_bands', 'BrandsController');
  Route::post('upload_image','BrandsController@uploads');
  // end brands
  // Product
  Route::get('/backoffice/product', function () {
    Session::put('page','product');
    return view('backoffice.product.product');
  });
  Route::post('productController/{id}/update','ProductController@update');
  Route::resource('productController', 'ProductController');
  Route::get('productController/{id}/delImg', [ 'uses' => 'ProductController@delImg']);
  Route::get('chooseCategory/{id}', [ 'uses' => 'ProductController@chooseCategory']);
  // Product
  //confirmOrder
  Route::get('/backoffice/confirmorder', function () {
    if(Auth::user()->member_type=="Admin"){
    Session::put('page','confirmorder');
    return view('backoffice.confirmorder.confirmorder');
  }else{
    Session::flash('wrong', 'ไม่มีสิทธิ์เข้าถึง');
    return redirect()->back();
  }
  });
  Route::get('confirmOrder/{id}', [ 'uses' => 'Order@confirm']);
  Route::get('delconformOrder/{id}', [ 'uses' => 'Order@destroy']);
  // Route::post('confirmOrder','Order@confirm');
  Route::get('/backoffice/order', function () {
    if(Auth::user()->member_type=="Admin"){
    Session::put('page','order');
    return view('backoffice.confirmorder.order');
  }else{
    Session::flash('wrong', 'ไม่มีสิทธิ์เข้าถึง');
    return redirect()->back();
  }
  });
  Route::get('changeStatus/{val}/{id}', [ 'uses' => 'Order@ChangeStatus']);

  Route::get('/backoffice/contact', function () {
    Session::put('page','contact');
    return view('backoffice.contact.contact');
  });
  Route::post('contact/store','Contact@store');
  Route::get('contact/clear','Contact@clear');

  Route::get('/backoffice/controlzone', function () {
    Session::put('page','controlzone');
    return view('backoffice.control.controlzone');
  });

  Route::get('/backoffice/payment', function () {
    if(Auth::user()->member_type=="Admin"){
    Session::put('page','payment');
    return view('backoffice.payment.payment');
  }else{
    Session::flash('wrong', 'ไม่มีสิทธิ์เข้าถึง');
    return redirect()->back();
  }
  });
  Route::post('payment/{id}/update','Payment@update');
  Route::resource('payment', 'Payment');

  Route::get('/backoffice/helpcenter', function () {
    if(Auth::user()->member_type=="Admin"){
    Session::put('page','helpcenter');
    return view('backoffice.helpcenter.helpcenter');
  }else{
    Session::flash('wrong', 'ไม่มีสิทธิ์เข้าถึง');
    return redirect()->back();
  }
  });
  Route::get('helpcenter/clear', 'HelpCenter@destroy');
  Route::resource('helpcenter', 'HelpCenter');

  Route::get('/backoffice/reportBestSell', function () {
    if(Auth::user()->member_type=="Admin"){
      Session::put('page','reportbestsell');
      return view('backoffice.report.bestsell');
    }else{
      Session::flash('wrong', 'ไม่มีสิทธิ์เข้าถึง');
      return redirect()->back();
    }
  });
  Route::get('changeMYBestSell/{month}/{year}', [ 'uses' => 'Report@changeMYBestSell']);

  Route::get('/backoffice/reportSell', function () {
    if(Auth::user()->member_type=="Admin"){
      Session::put('page','reportsell');
      return view('backoffice.report.sell');
    }else{
      Session::flash('wrong', 'ไม่มีสิทธิ์เข้าถึง');
      return redirect()->back();
    }
  });
  Route::get('changeMYSell/{month}/{year}', [ 'uses' => 'Report@changeMYSell']);

  Route::get('/backoffice/contactus', function () {
    if(Auth::user()->member_type=="Admin"){
      Session::put('page','contactus');
      return view('backoffice.contact.contactus');
    }else{
      Session::flash('wrong', 'ไม่มีสิทธิ์เข้าถึง');
      return redirect()->back();
    }
  });
  Route::get('delContactus/{id}', [ 'uses' => 'Contact@delContactus']);

  Route::get('/backoffice/orderDealers', function () {
    Session::put('page','orderdealers');
    return view('backoffice.dealer.order');
  });
});


?>
