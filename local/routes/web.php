<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/home', function () {
  return redirect('/');
});
Route::get('/index', function () {
  return redirect('/');
});
Route::get('/inc_cart-add', function () {
    return view('inc_cart-add');
});
Route::get('/inc_cart-add/{id}', function ($id) {
    return view('inc_cart-add')->with(array('id'=>$id));
});
Route::get('/inc_brand-list', function () {
    return view('inc_brand-list');
});
Route::get('/order_history', function () {
    return view('order_history');
});
Route::get('/order_history_print', function () {
    return view('order_history_print');
});
Route::get('/changM/{month}', function ($month) {
    Session::put('month',$month);
});
Route::get('/changY/{year}', function ($year) {
    Session::put('year',$year);
});
Route::get('order', function () {
  if(Session::has('month') or Session::has('year')){
    return view('index')->with(array('my'=>"true"));
  }
});
Route::get('/order_history-mobile', function () {
    return view('order_history-mobile');
});
Route::group(['middleware' => 'auth'], function (){
  Route::get('/cart', function () {
    return view('cart');
  });
});

Route::get('ban/{id}', [ 'uses' => 'ProductController@ban']);

Route::group(['middleware' => 'auth'], function (){
  Route::get('/product_details', function () {
      return view('product_details');
  });
  Route::get('/product_details/{id}', function ($id) {
    if(Session::has('banproduct')){
      $banproduct=array_unique(Session::get('banproduct'));
      // for ($i=0; $i < count($banproduct) ; $i++) {
      // dd(Session::get('banproduct'));
      $ban=false;
      foreach ($banproduct as $key ) {
        if($key==$id){
          $ban=true;
        }
      }
        if(!$ban){
          return view('product_details')->with(array('id'=>$id));
        }
        else{
          Session::flash('wrong','สินค้านี้ถูกกั้นเขตจากผู้ซื้อรายอื่น');
          return redirect()->back();
        }
      // }
    }
    else{
      return view('product_details')->with(array('id'=>$id));
    }

  });
});
Route::get('/register', function () {
    return view('register');
});
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/help_center', function () {
    return view('help_center');
});

Route::get('/profile', function () {
    return view('profile');
});
Route::get('/profile/{id}', function ($id) {
    return view('profile')->with(array('id'=>$id));
});
Route::get('/product/{id}', function ($id) {
    return view('product')->with(array('id'=>$id));
});
Route::get('/SortBy/{Sort}', [ 'uses' => 'ProductController@SortBy']);
Route::get('/changePage/{subcat}/{brand}/{page}',[ 'uses' => 'ProductController@changePage']);
// Route::get('/product', function () {
//     return view('product');
// });

Route::get('/loginSlide', function () {

    return view('login');


});
Route::post('updateUser', 'UserController@update');
Route::get('/setting', function () {
    return view('setting');
});
Route::get('/checkPrice/{id}/{num}', [ 'uses' => 'ProductController@checkPrice']);
Route::get('/delProduct/{id}', [ 'uses' => 'ProductController@delProduct']);
Route::get('/delAll', function () {
    Session::forget('cart');
});
Route::post('ConfirmCart', 'ProductController@ConfirmCart');
Route::post('review', 'Review@postReview');


Route::post('/ContactAdmin','Contact@contactAdmin');

// User controller
Route::post('/registerUser','UserController@store');
Route::get('/addToCart',[ 'uses' => 'ProductController@addToCart']);

#backend
require_once('web_backend.php');

// Auth
Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
// Route::get('login', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');


$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');
